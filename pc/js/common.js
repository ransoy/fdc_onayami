// JavaScript Document
var click = false;
$(function() {
	//hide validate element
	$('.validate .error').hide();

	//validate
	$(".validate").validate({
		rules: {
			contact_email: {
				required: true,
				email: true
			},
			contact_subject :{
				required: true
			},
			contact_message :{
				required: true
			},
			question_category_list: {
				required: true
			},
			question_category:{
				required: true
			},
			question_subject:{
				required: true,
				maxlength:50,
				minlength:10
			},
			question_message:{
				required: true,
				maxlength:1000,
				minlength:30
			},
			answer_message:{
				required: true,
				maxlength:1000,
				minlength:30
			},
			consult_message:{
				required: true
			}
		},
		messages: {
			contact_email: {
				required: "・メールアドレスを入力してください。",
				email: "・正しい形式で入力してください。"
			},
			contact_subject :{
				required: "・件名を入力してください。"
			},
			contact_message:{
				required: "・本文を入力してください。"
			},
			question_category_list:{
				required: "選択してください。"
			},
			question_category:{
				required: "どれか1つ選択してください。"
			},
			question_subject:{
				required: "タイトルを入力してください。",
				maxlength: "全角50文字以内で入力してください。",
				minlength: "全角10文字以上で入力してください。"
			},
			question_message:{
				required: "相談内容を入力してください。",
				maxlength: "全角1000文字以内で入力してください。",
				minlength: "全角30文字以上で入力してください。",
			},
			answer_message:{
				required: "相談内容を入力してください。",
				maxlength: "全角1000文字以内で入力してください。",
				minlength: "全角30文字以上で入力してください。",
			},
			consult_message:{
				required: "返信コメントを入力してください。"
			}
		},
		errorElement: "p",
		errorPlacement:function(error,element){
			error.insertBefore($(element));
		}
	});

	//評価ボタン
	$(".btn_wrap .btn_evaluate").on("click",function(){
		var scope = $(this).closest(".sec_consult_answer");
		var textNumber =$(this).val();
		console.log(textNumber + "1");
		$(this).removeClass("btn_red-o ic_heart-o").addClass("btn_red ic_heart");
		$(".btn_evaluate",scope).addClass("disable");
		$(this).text(textNumber + "1");
	});

	//チェックボックスクリック範囲
	$(".btn_checkbox").on("click",function(){
		$(this).children("input[type=checkbox]").click();
	});
	
	//タブ切り替え
	$(".tab_content").addClass("hide");
	$(".tab_content:first").removeClass("hide");
	$("#tab_menu li").on("click",function(){
		if($(this).is(".tab_unresolved")){
			$("#tab_menu li").removeClass("current");
			$(this).addClass("current");
			$(".tab_content").addClass("hide");
			$(".content_unresolved").removeClass("hide");
		} else if($(this).is(".tab_history")){
			$("#tab_menu li").removeClass("current");
			$(this).addClass("current");
			$(".tab_content").addClass("hide");
			$(".content_history").removeClass("hide");
		} else if($(this).is(".tab_bookmark")){
			$("#tab_menu li").removeClass("current");
			$(this).addClass("current");
			$(".tab_content").addClass("hide");
			$(".content_bookmark").removeClass("hide");
		}
	});


	//ui-count_form 
	$(document).on("click keydown keyup keypress change", ".ui-count_form .ui-count_input", function(){
		var scope = $(this).closest(".ui-count_form");
		var textLength = parseInt($(this).val().length);
		var maxCountLength = parseInt($(".ui-count_input",scope).attr("maxlength"));
		$(".input_count",scope).html(maxCountLength - textLength);
	});


	//pageTop scroll fix button
	var pageTop = $("#top_link");
	pageTop.hide();
	$(window).scroll(function () {
		if ($(this).scrollTop() > 300) {
			pageTop.fadeIn();
		} else {
			pageTop.fadeOut();
		}
	});

	//pageTop scroll swing
	$('#top_link').on("click",function(){
		var target = $(this).data('scroll');
		if(target = 'pageTop'){
			var speed = 500;
			var href= $(this).attr("href");
			var target = $(href == "#" || href == "" ? 'html' : href);
			var position = target.offset().top;
			$("html, body").animate({scrollTop:position}, speed, "swing");
			return false;
		}
	});

	$('#masonry').masonry({
		itemSelector: '.grid',
		isFitWidth: true,
		isAnimated: true
	});

	$(document).on('click', '.btn_update_eval', function(e) {
		e.preventDefault();
		
		var form = $(this);
		var message_id = form.data('mess-id');
		var user_id = form.data('user-id');
		var mode = form.data('thread-mode');
		var thread_id = $('#answer_id').val();

		if (typeof mode === 'undefined' || mode === null) {
			mode = 1;
		}

		$.ajax({
			url: '/bb/update_evaluate_ajax',
			type: 'POST',
			dataType: 'json',
			data: {user_id:user_id, message_id:message_id, thread_id:thread_id, mode:mode},
			success: function (data) {
				if (data.flag == true) {
					form.removeClass('btn_orange-o').addClass('btn_orange');
					var x = parseInt(form.find('.ic').html());					
					form.find('.ic').html(x + 1);
				}
			}
		});
	});

	$(document).on('click', '.btn_bookmark', function(e) {
		e.preventDefault();		
		var form = $(this);
		var thread_id = form.data('thread-id');
		var user_id = form.data('user-id');
		var mode = form.data('bookmark-mode');
		$.ajax({
			url: '/bb/bookmark',
			type: 'POST',
			dataType: 'json',
			data: {user_id:user_id, thread_id:thread_id, mode:mode},
			success: function (data) {
				if (data.status == 1) {
					form.removeClass('btn_blue-o').addClass('btn_blue');
					form.data('boomark-mode', 0);
				} else {
					form.removeClass('btn_blue').addClass('btn_blue-o');
					form.data('boomark-mode', 1);
				}
			}
		});
	});

	$(document).on('click', '.more_comment', function(e) {
		e.preventDefault();		
		var form = $(this);
		var reply_count = $('.sec_consult_answer .content_inner').size();
		var id = form.data('consult-id');
		var bid = $('#bid').val();
		var subid = $('#subid').val();
		var elem_countr = 'content_inner">';
		if (click) {
			return;
		}
		click = true;
		$.ajax({
			url: '/bb/contents/'+bid+'/'+subid+'/'+id,
			type: 'POST',
			dataType: 'HTML',
			data: {replay_count:reply_count},
			complete: function(){
				click = false;
			},
			success: function (data) {
				var	returned = data.split(elem_countr).length - 1;
				$('.sec_consult_answer').append(data);
				if(returned < 10 || ($('.sec_consult_answer .content_inner').size() >= $('#comment_count').val())){
					$('.more_comment').hide();
				}
				click = true;
			}
		});
	});

	$(document).on("click keydown keyup keypress change", ".ui-count_form .ui-count_input", function(){
		var scope = $(this).closest(".ui-count_form");
		var textLength = $(this).val().replace(/(\r\n|\n|\r)/g,"  ").length;
		var maxCountLength = $(".ui-count_input",scope).attr("maxlength");
		$(".input_count",scope).html(maxCountLength - textLength);
	});

	$(document).on('change','input[name="userfile"]',function(){
		var fd = new FormData();
		if ($("input[name='userfile']").val()!== '') {
			fd.append("userfile", $("input[name='userfile']").prop("files")[0] );
		}
		url = "/bb/ajaxImagefileUpload";
		$.ajax({
			type: "POST",
			url: url,
			processData: false,
			contentType: false,
			data: fd
		}).done(function(data){
			if (data.flag){
				$('input[name="image_file_name"]').val(data.filename);
			} else if (data.flag == false) {
				alert('エラーが発生しました。');
			}			
		}).fail(function(data){
			if (!data.flag) {
				alert('エラーが発生しました。');
			}
		});
	});
	
});

function highslide_set(){
	var $hs=$('.c_img > a');

	var hs_length=$hs.length;

	for (var i = 0 ; i < hs_length ; i++){
	  var this_obj=$hs.eq(i);
	  this_obj.addClass('highslide');
	  this_obj.click(function(){return hs.expand(this)});
	}
}