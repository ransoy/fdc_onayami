$(function(){
	// //pageTop scroll fix button
	var pageTop = $("#top_link");
	pageTop.hide();
	$(window).scroll(function () {
		if ($(this).scrollTop() > 300) {
			pageTop.fadeIn();
		} else {
			pageTop.fadeOut();
		}
	});

	//pageTop scroll swing
	$("#top_link").on("click",function(){
		var target = $(this).data('scroll');
		if(target = 'pageTop'){
			var speed = 500;
			var href= $(this).attr("href");
			var target = $(href == "#" || href == "" ? 'html' : href);
			var position = target.offset().top;
			$("html, body").animate({scrollTop:position}, speed, "swing");
			return false;
		}
	});
});