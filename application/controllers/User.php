<?php
//defined('BASEPATH') OR exit('No direct script access allowed');
class User extends MY_Controller {
    private $page_line_max = 5;
    private $template;
    function __construct() {
        parent::__construct();
//        $this->load->library('HelperApp');
//        $this->load->library('UserControl');
        $this->load->library('twig');
        $this->load->model("Musers");
        $this->viewData['base_url'] = base_url();
        $this->viewData['total_users'] = $this->Musers->get_user_total_number();
        if (UserControl::LoggedIn()) {
            $this->viewData['user_login_flag'] = true;
        } else {
            $this->viewData['user_login_flag'] = false;
        }
        if (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on') {
            $temp = 'https://';
        } else {
            $temp = 'http://';
        }
        $string = $_SERVER["HTTP_HOST"];
        $dome = preg_replace('/onayami/i', 'www', $string);
        $this->viewData['wwwjoyspe'] = $temp . $dome . "/";
    }

	public function index()
	{
//        echo "string",$this->config->item('language');
//        echo "string",$this->config->item('twig_path');

        if ($this->agent->is_mobile()) {
//            $this->twig->template_dir_set(TEMPLATEPATH.'sp');
//            $this->twig->display('index.html', $this->viewData);
            $this->twig->template_dir_set(TEMPLATEPATH_SP);
            $this->twig->display('search/index.html', $this->viewData);
        } else {
//	        echo BASEPATH;
//	        echo TEMPLATEPATH_PC;
//	        echo TEMPLATEPATH_SP;


            $big_cate_ar = $this->Madmin->get_big_category();

            $options = array();
            $options[0] = 'カテゴリー';
            foreach ($cate as $key => $value) {
                $options[$value['id']] = $value['name'];
            }
            $this->viewData['form_cate_opt'] = $options;
            $p_cate_names = $options;

            $this->viewData['p_cate_names'] = $p_cate_names;

            $this->viewData['test_text'] = 'テストテキスト？？？？？？？？';


            $this->twig->template_dir_set(TEMPLATEPATH_PC);
            $this->twig->display('index.html', $this->viewData);
        }

//		$this->load->view('index');
	}

    public function login()
    {


//        echo session_get_cookie_params();
//        session_start();

///$session_name = $_COOKIE[session_name()];
//session_start($session_name);

//echo "<br>";
//        var_dump($_COOKIE['PHPSESSID']);
//echo session_id();
//        var_dump($_SESSION['__ci_last_regenerate']);
//        var_dump($_SESSION);
        var_dump($_SESSION['id']);
        //$_SESSION['initiated']
        //$_SESSION['initiated']

/*        if (UserControl::LoggedIn()) {

           echo "joyspeログインしてるよ";

        } else {
           echo "joyspeログインしてない";
        }*/



//        echo "string".session_name();
//        echo "string",$this->config->item('language');
//        echo "string",$this->config->item('twig_path');

        if ($this->input->post()) {
            $this->login_validation();
        }

        if ($this->agent->is_mobile()) {
//            $this->twig->template_dir_set(TEMPLATEPATH.'sp');
//            $this->twig->display('index.html', $this->viewData);
            $this->twig->template_dir_set(TEMPLATEPATH_SP);
            $this->twig->display('search/index.html', $this->viewData);
        } else {
//          echo BASEPATH;
//          echo TEMPLATEPATH_PC;
//          echo TEMPLATEPATH_SP;

            $this->viewData['test_text'] = 'テストテキスト？？？？？？？？';


            $this->twig->template_dir_set(TEMPLATEPATH_PC);
            $this->twig->display('login.html', $this->viewData);
        }

//      $this->load->view('index');
    }

    public function login_validation_ajax()
    {
        $this->load->library("form_validation");

        $this->form_validation->set_error_delimiters('<p class="error_message">', '</p>');
        $this->form_validation->set_rules("email", "メールアドレス", "required|callback_validate_credentials");//Email入力欄のバリデーション設定、コールバック設定
        $this->form_validation->set_rules("password", "パスワード", "required"); //パスワード入力欄のバリデーション設定
        if ($this->form_validation->run()) {  //バリデーションエラーがなかった場合の処理
//            redirect(base_url().'user/my_page/');
            redirect(base_url());
        } else {                          //バリデーションエラーがあった場合の処理
            $this->viewData['validation_error'] = validation_errors();
            $this->viewData['email_error'] = form_error('email');
            $this->viewData['password_error'] = form_error('password');
        }
    }

    public function login_validation()
    {
        $this->load->library("form_validation");

        $this->load->model("musers");
        $this->form_validation->set_error_delimiters('<p class="error_message">', '</p>');
        $this->form_validation->set_rules("email", "メールアドレス", "required|callback_validate_credentials");//Email入力欄のバリデーション設定、コールバック設定
        $this->form_validation->set_rules("password", "パスワード", "required"); //パスワード入力欄のバリデーション設定
        if ($this->form_validation->run()) {  //バリデーションエラーがなかった場合の処理
//            redirect(base_url().'user/my_page/');
            redirect(base_url());
        } else {                          //バリデーションエラーがあった場合の処理
            $this->viewData['validation_error'] = validation_errors();
            $this->viewData['email_error'] = form_error('email');
            $this->viewData['password_error'] = form_error('password');
        }
    }

    //Email情報がPOSTされたときに呼び出されるコールバック機能
    public function validate_credentials()
    {
        $this->load->model("Musers");
        $res = $this->Musers->can_log_in();
        if ($res) {   //ユーザーがログインできたあとに実行する処理
            $data = array(
                "user_id" => $res['id'],
                "is_user_logged_in" => 1
            );
//            $this->session->set_userdata($data);
            $this->my_set_userdata($data);
            return true;
        } else {                  //ユーザーがログインできなかったときに実行する処理
//            $this->viewData['email_error'] = validation_errors();
            $this->form_validation->set_message("validate_credentials", "ユーザー名かパスワードが異なります。");
            return false;
        }
    }

    public function my_set_userdata($data)
    {
        session_start(); 

        if (isset($_SESSION["counter"])) { 

          $_SESSION["counter"]++; 

          print($_SESSION["counter"]."回目の読み込みです。"); 

        } else { 

           $_SESSION["counter"] = 0; 

          print("はじめての読み込みです。"); 

        }
    }

    public function logout()
    {
        $this->load->library('user_agent');
        HelperApp::clear_session();
        HelperApp::remove_session('modal');

        if ($this->agent->is_mobile()) {
            redirect(base_url());
        } else {
            redirect(base_url());
        }
    }

    public function my_page($tab = 1)
    {
        if ($this->agent->is_mobile()) {
            $this->twig->template_dir_set(TEMPLATEPATH_SP);
        } else {
            $this->twig->template_dir_set(TEMPLATEPATH_PC);
        }


        if (UserControl::LoggedIn()) {
            $user_id=  UserControl::getId();
        } else {
            redirect(MAIN_URL.'user/login');
            return;
        }

        /* パンくず */
        $this->viewData['breadcrumb_list'] = array(
                                                array(
                                                    'url' => base_url(),
                                                    'text' => 'トップページ',
                                                ),
                                                array(
                                                    'url' => base_url().'user/my_page',
                                                    'text' => 'マイページ',
                                                ),
                                            );

        $this->load->model("Mbbs");
        $this->load->model("Mbonus");
        $this->load->model("Mpoints");
        $this->load->library('pagination');

        $config['per_page'] = $this->page_line_max;
        $config['page_query_string'] = TRUE;
        $config['query_string_segment'] = 'page';
        $config['num_links'] = 2;

        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';

        $config['use_page_numbers'] = TRUE;
        $config['cur_tag_open'] = '<li><a class="pager_style current" >';
        $config['cur_tag_close'] = '</a></li>';
        $config['attributes'] = array('class' => 'pager_style');
        $config['attributes']['rel'] = FALSE;

        $config['prev_tag_open'] = '<li>';
        $config['prev_link'] = '前へ';
        $config['prev_tag_close'] = '</li>';
        $config['next_tag_open'] = '<li>';
        $config['next_link'] = '次へ';
        $config['next_tag_close'] = '</li>';

        $config['first_link'] = FALSE;
        $config['last_link'] = FALSE;

        switch ($tab) {
            case '1':
                $list_all_num  = $this->Mbbs->get_unanswered_num($user_id);
//                $list_all_num  = 0;
                $config['total_rows'] = $list_all_num;
                $config['base_url'] = base_url().'user/my_page/1/';
                $config['suffix'] = '#consultation_list';
                $config['first_url'] = base_url().'user/my_page/'.$tab.'/#consultation_list';
                $this->pagination->initialize($config);
                $this->viewData['pagination'] = $this->pagination->create_links();

                $this->viewData['list_all_num'] = $list_all_num;

                $offset = $page = null;
                if ($this->input->get('page')) {
                    $page = $this->input->get('page');
                    $offset =  ($page - 1) * $this->page_line_max;
                }
                $unanswered_ar = $this->Mbbs->get_unanswered($user_id, $this->page_line_max, $offset);
                //var_dump($unanswered_ar);
                $this->viewData['unanswered_ar'] = $unanswered_ar;

                if ($this->agent->is_mobile()) {
                    $this->template = 'mypage_a.html';
                } else {
                    $this->template = 'mypage_a.html';
                }
                break;
            case '2':
                $list_all_num  = $this->Mbbs->get_answered_num($user_id);
//                $list_all_num  = 0;
                $config['total_rows'] = $list_all_num;
                $config['base_url'] = base_url().'user/my_page/2/';
                $config['suffix'] = '#consultation_list';
                $config['first_url'] = base_url().'user/my_page/'.$tab.'/#consultation_list';
                $this->pagination->initialize($config);
                $this->viewData['pagination'] = $this->pagination->create_links();

                $this->viewData['list_all_num'] = $list_all_num;

                $offset = $page = null;
                if ($this->input->get('page')) {
                    $page = $this->input->get('page');
                    $offset =  ($page - 1) * $this->page_line_max;
                }
                $answered_ar = $this->Mbbs->get_answered($user_id, $this->page_line_max, $offset);
//                var_dump($answered_ar);
                $this->viewData['answered_ar'] = $answered_ar;

                if ($this->agent->is_mobile()) {
                    $this->template = 'mypage_b.html';
                } else {
                    $this->template = 'mypage_b.html';
                }
                break;
            case '3':
                $list_all_num  = $this->Mbbs->get_bookmark_num($user_id);
//                $list_all_num  = 0;
                if($list_all_num) {
                    $config['total_rows'] = $list_all_num;
                    $config['base_url'] = base_url().'user/my_page/3/';
                    $this->pagination->initialize($config);
                    $this->viewData['pagination'] = $this->pagination->create_links();

                    $this->viewData['list_all_num'] = $list_all_num;

                    $offset = $page = null;
                    if ($this->input->get('page')) {
                        $page = $this->input->get('page');
                        $offset =  ($page - 1) * $this->page_line_max;
                    }
                    $bookmark_ar = $this->Mbbs->get_bookmark($user_id, $this->page_line_max, $offset);
                    $this->viewData['bookmark_ar'] = $bookmark_ar;
                } else {
                    $post = $this->input->post();
                    if(!empty($post['input_keyword']) || !is_null($this->input->get('input_keyword'))) {
                        $keyword = '';
                        $page = 0;
                        if(!empty($post['page'])) {
                            $page = $post['page'];
                        } else if(!is_null($this->input->get('page'))) {
                            $page = $this->input->get('page');
                        }

                        if(!empty($post['input_keyword'])) {
                            $keyword = $post['input_keyword'];
                        } else if(!is_null($this->input->get('input_keyword'))) {
                            $keyword = $this->input->get('input_keyword');
                        }
                        $list_all_search_num  = $this->Mbbs->my_page_search_keyword_consultation_list_num($keyword);
                        $config['total_rows'] = $list_all_search_num;
                        $config['base_url'] = base_url().'user/my_page/3/';
                        $this->pagination->initialize($config);
                        $this->viewData['pagination'] = $this->pagination->create_links();

                        $this->viewData['list_all_search_num'] = $list_all_search_num;

                        $offset = $page = null;
                        if ($this->input->get('page')) {
                            $page = $this->input->get('page');
                            $offset =  ($page - 1) * $this->page_line_max;
                        }

                        $search_keyword_consultation_ar = $this->Mbbs->my_page_search_keyword_consultation_list($keyword, $this->page_line_max, $offset);
                        $this->viewData['search_keyword_consultation_ar'] = $search_keyword_consultation_ar;
                        $this->viewData['input_keyword'] = $keyword;
                    }
                }

                if ($this->agent->is_mobile()) {
                    $this->template = 'mypage_c.html';
                } else {
                    $this->template = 'mypage_c.html';
                }
                break;
            default:
                if ($this->agent->is_mobile()) {
                    $this->template = 'mypage_a.html';
                } else {
                    $this->template = 'mypage_a.html';
                }
                break;
        }
        if($this->agent->is_mobile() && $this->user['user_can_point']) {
            $this->viewData["question_label"] = '相談ボーナス';
            $this->viewData["answer_label"] = '回答ボーナス';
            $this->viewData["like_label"] = 'そう思う累計ボーナス';
            $this->viewData["weekly_label"] = 'そう思う獲得ボーナス';
            $this->viewData["max_answer_label"] = 'ボーナス対象回答数';
            $this->viewData["bonusData"] = $this->Mbonus->getUserBonusData($this->user['id']);
            $this->viewData["pointSettings"] = $this->Mpoints->getPointSettings();
        }

        $this->twig->display($this->template, $this->viewData);
    }

    public function delete_unanswer_ajax()
    {
        $this->load->model("Mbbs");

        $user_id=  UserControl::getId();

        if ($this->input->post('checks')) {
            $ar = array();
            $checks_ar = $this->input->post('checks');
            foreach($checks_ar as $check) {
                $ar[] = intval($check);
            }
        }

        $this->Mbbs->del_unanswered($user_id, $ar);

        $data = array(
            'flag' => true,
            'checks' => $ar,
        );
        //$dataをJSONにして返す
        $this->output
             ->set_content_type('application/json')
             ->set_output(json_encode($data));
    }

    public function delete_bookmark_ajax()
    {
        $this->load->model("Mbbs");

        $user_id=  UserControl::getId();

        if ($this->input->post('checks')) {
            $ar = array();
            $checks_ar = $this->input->post('checks');
            foreach($checks_ar as $check) {
                $ar[] = intval($check);
            }
        }
        $this->Mbbs->del_bookmark($user_id, $ar);

        $data = array(
            'flag' => true,
            'checks' => $ar,
        );
        //$dataをJSONにして返す
        $this->output
             ->set_content_type('application/json')
             ->set_output(json_encode($data));
    }

}
