<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Main extends MY_Controller {
    private $page_line_max = 14;
    private $template;
    function __construct() {
        parent::__construct();

        $this->load->model("Musers");
        $this->viewData['base_url'] = base_url();
        if (UserControl::LoggedIn()) {
            $this->viewData['user_login_flag'] = true;
        } else {
            $this->viewData['user_login_flag'] = false;
        }
        $this->viewData['total_users'] = $this->Musers->get_user_total_number();
        if (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on') {
            $temp = 'https://';
        } else {
            $temp = 'http://';
        }
        $string = $_SERVER["HTTP_HOST"];
        $dome = preg_replace('/onayami/i', 'www', $string);
        $this->viewData['wwwjoyspe'] = $temp . $dome . "/";
        $this->viewData['main_url'] = MAIN_URL;
    }

	public function index()
	{
          $this->load->model("Mviewlogs");

        if (UserControl::LoggedIn()) {
            $this->viewData['user_login_flag'] = true;
            $user_id=  UserControl::getId();
        } else {
            $this->viewData['user_login_flag'] = false;
        }
        $topViewed_ar = $this->Mviewlogs->get3daysTopFourViewed();
        $this->viewData['topViewed_ar'] = $topViewed_ar;

        $list_all_num = $this->Mbbs->get_consultation_list_all_num();

        $this->load->library('pagination');

        $config['base_url'] = base_url();
        $config['total_rows'] = $list_all_num;
        $config['per_page'] = $this->page_line_max;
        $config['page_query_string'] = TRUE;
        $config['query_string_segment'] = 'faq_page';
        $config['num_links'] = 2;
        $config['suffix'] = '#consultation_list';
        $config['first_url'] = '/#consultation_list';

        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';

        $config['prev_tag_open'] = '<li>';
        $config['prev_link'] = '前へ';
        $config['prev_tag_close'] = '</li>';
        $config['next_tag_open'] = '<li>';
        $config['next_link'] = '次へ';
        $config['next_tag_close'] = '</li>';


        $config['use_page_numbers'] = TRUE;
        $config['cur_tag_open'] = '<li><a class="pager_style current" >';
        $config['cur_tag_close'] = '</a></li>';
        $config['attributes'] = array('class' => 'pager_style');
        $config['attributes']['rel'] = FALSE;

        $config['first_link'] = FALSE;
        $config['last_link'] = FALSE;

        $this->pagination->initialize($config);

        $this->viewData['pagination'] = $this->pagination->create_links();

        $offset = $faq_page = null;
        if ($this->input->get('faq_page')) {
            $faq_page = $this->input->get('faq_page');
            $offset =  ($faq_page - 1) * $this->page_line_max;
        }
        $consultation_list_ar = $this->Mbbs->get_consultation_list_all($this->page_line_max, array(), $offset);
        $this->viewData['consultation_list_ar'] = $consultation_list_ar;
        
        if ($this->agent->is_mobile()) {
            $consultation_list_popularity_ar = $this->Mbbs->get_consultation_list_all($this->page_line_max, array(), null, "popular");
            $this->viewData['consultation_list_popularity_ar'] = $consultation_list_popularity_ar;

            $consultation_list_unanswered = $this->Mbbs->get_consultation_list_all($this->page_line_max, array(), null, "unanswered");
            $this->viewData['consultation_list_unanswered'] = $consultation_list_unanswered;
        } else {
            $consultation_list_all_ar = $this->Mbbs->get_consultation_list_all($this->page_line_max, array(), null, 'sidebar');
            $this->viewData['consultation_list_all_ar'] = $consultation_list_all_ar;                
        }

        $this->viewData['site_name'] = 'お悩み相談';

        $this->template = 'index.html';

        $this->twig->display($this->template, $this->viewData);
	}

    public function static_view($html) {
        if ($html == 'contact_comp') {
           $get = $this->input->get();
           if (!isset($get['send']))
                redirect('/contact');
        }
        $pages = array(
            'tos' => array('text' =>'利用規約'),
            'company' => array('text' =>'会社概要'),
            'privacy' => array('text' => '個人情報保護方針'),
            'faq' => array('text' => 'FAQ'),
            'contact' => array('text' => 'お問い合わせ'),
            'contact_completion' => array('text' => 'お問い合わせ完了'),
            'guideline' => array('text' => '禁止事項')            
        );
        $this->viewData['titles'] = $pages;
        foreach($pages as $key => $value) {
            if ($key == $html) {
                $bread_crumb = array(
                                    array(
                                        'url' => base_url(),
                                        'text' => 'トップページ',
                                    ),
                                    $value
                                );
                $this->viewData['breadcrumb_list'] = $bread_crumb;
            }
        } 
        $this->twig->display($html.'.html', $this->viewData);
    }

    public function my404() {
        $this->twig->display('404.html', $this->viewData);
    }
   
    public function send_email()
    {
        $email =  $this->input->post('contact_email');
        $subject =  $this->input->post('contact_subject');
        $message =  $this->input->post('contact_message');

        $this->load->library('email');
        $this->email->from($email, 'お悩み掲示板から問い合わせ');
        $this->email->to(ADMIN_EMAIL);
//        $this->email->to('info@joyspe.com');
        $this->email->subject($subject);
        $this->email->message($message);
        $this->email->send();
//        echo $this->email->print_debugger();
        redirect(base_url().'contact_completion?send=yes');
    }


}