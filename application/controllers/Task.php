<?php
class Task extends CI_Controller {

    public function __construct() {
        parent::__construct();
        if(!$this->input->is_cli_request()) show_error('Direct access is not allowed');
    }

    function addWeeklyPoint()
    {
        $this->load->model('Mpoints');

    	
    	$givenPoint = $this->Mpoints->checkWeeklyPointAdded();
    	if($givenPoint) {
    		echo 'This week\' points already given.';
    		return;
    	}
        
        $user_points = $this->Mpoints->addWeeklyPoints();
    	if(empty($user_points))
    		echo 'No Points given this week.';
    	else
    		echo date('Y-m-d H:i:s').': bbs_points table updated '.$user_points.' rows.';
        return;
    }


    function updateCommentRank()
    {
        $this->load->model('Mcommentrank');
        if($result = $this->Mcommentrank->updateRanking()){
            echo date('Y-m-d H:i:s').': bbs_messages_rank table updated '.$result.' rows.';
        }else{
            echo date('Y-m-d H:i:s').': failed to update bbs_messages_rank table.';
        }

    }


}
