<?php
//defined('BASEPATH') OR exit('No direct script access allowed');
class Bb extends MY_Controller {
    private $page_line_max = 10;
    private $template;
    function __construct() {
        parent::__construct();
        $this->load->library('twig');
        $this->viewData['base_url'] = base_url();
        $this->viewData['total_users'] = $this->Musers->get_user_total_number();
        if (UserControl::LoggedIn()) {
            $this->viewData['user_login_flag'] = true;
        } else {
            $this->viewData['user_login_flag'] = false;
        }
        $this->load->model("Madmin");
        $this->load->model("Musers");
        $this->load->model("Mbbs");
        $this->load->model('Mpoints');
    }

    public function index($url)
    {
    }

	public function posting()
	{
        if (UserControl::LoggedIn()) {
            $this->viewData['user_login_flag'] = true;
            $user_id=  UserControl::getId();
            HelperApp::remove_session('question_id');
            HelperApp::remove_session('big_category_id');
            HelperApp::remove_session('category_id');
        } else {
            $this->viewData['user_login_flag'] = false;
            redirect(MAIN_URL.'user/login');
            return;
        }

        if(!empty($_POST)){
            $big_id = $_POST['question_category_list'];
            $sm_id = (isset($_POST['question_category']))? $_POST['question_category'] : null ;
            $this->viewData['select_big'] = $big_id;

            $cat = $this->Madmin->get_categorys($big_id);
            $this->viewData['get_sub_cat'] = $cat;
            $this->viewData['sm_id'] = $sm_id;
        }

        $users = $this->Musers->get_users($user_id);
        $user_info = $users;

        $_POST['nickname'] = ($user_info['nick_name'] == NULL)? '匿名':$user_info['nick_name'];

        $big_cate = $this->Madmin->get_big_category();
        $this->viewData['form_cate_opt'] = $big_cate;
        $this->viewData['nickname'] = $_POST['nickname'];
          /* パンくず */
        $this->viewData['breadcrumb_list'] = array(
                                                array(
                                                    'url' => base_url(),
                                                    'text' => 'トップページ',
                                                ),
                                                array(
                                                    'url' => base_url().'bb/posting',
                                                    'text' => '相談内容の入力',
                                                ),
                                            );

        $this->template = 'bb/question.html';  
        $this->twig->display($this->template, $this->viewData);
	}


    public function confirm()
    {

        $nickname = $this->input->post('nickname');
        $big_category_id = $this->input->post('question_category_list');
        $category_id =  $this->input->post('question_category');
        $user_question_title =  $this->input->post('question_subject');
        $user_question =  $this->input->post('question_message');
        $image_file_name =  $_POST['image_file_name'];

        if (!$this->input->post()) redirect(base_url().'bb/posting', 'refresh');

        $big_category_ar = $this->Madmin->get_big_category($big_category_id);
        $category_ar = $this->Madmin->get_category($category_id);

        // html mode for link attachment
        $message_ar = $this->add_conf($user_question);

        $_POST['nickname'] = $nickname;
        $_POST['big_category_name'] = $big_category_ar[0]['name'];
        $_POST['category_name'] = $category_ar[0]['name'];

        $data = array(
                      'nickname'  => $nickname,
                      'question_category_list'  => $big_category_id,
                      'question_category'  => $category_id,
                      'question_subject' => $user_question_title,
                      'question_message'  => $user_question,
                      'userfile' => $image_file_name,
                      'message_next' =>  $message_ar['preview'],
                      'bl_max' =>  $message_ar['bl_max'],
                      'message_all' =>  $message_ar['message_all']
                    );
        $this->viewData['form_hidden'] = form_hidden($data);
        $this->viewData['data'] = $data;
        $this->viewData['breadcrumb_list'] = $this->bread_crumb('', 
                                                                    array(
                                                                           array(
                                                                                'url' => base_url(),
                                                                                'text' => 'トップページ',
                                                                            ),
                                                                            array(
                                                                                'url' => base_url(),
                                                                                'text' => '相談内容の確認',
                                                                            ),
                                                                        )
                                                                    );

        $this->template = 'bb/question_conf.html';    
        $this->twig->display($this->template, $this->viewData);
    }

    public function insert_consultation()
    {
        $nickname = $this->input->post('nickname');

        $big_category_id = $this->input->post('question_category_list');
        $category_id     = $this->input->post('question_category');

        $user_question_title = $this->input->post('question_subject');
        $user_question       = $this->input->post('question_message');


        if (UserControl::LoggedIn()) {
            $user_id=  UserControl::getId();
        } else {
            return;
        }

        $res = $this->Mbbs->insert_consultation($user_id, $user_question_title, $user_question, $big_category_id, $category_id);
        HelperApp::add_session('question_id', $res);
        HelperApp::add_session('big_category_id', $big_category_id);
        HelperApp::add_session('category_id', $category_id);

        redirect(base_url().'bb/post_completion');
    }

    public function post_completion()
    {
        $post = $this->input->post();
        if (!$post) redirect(base_url().'bb/posting', 'refresh'); 

        $nickname = $post['nickname'];
        $big_category_id = $post['question_category_list'];
        $category_id     = $post['question_category'];
        $user_question_title = $post['question_subject'];
        $user_question = $post['question_message'];       
        $bl_max = $post['bl_max'];

        $res = $this->Mbbs->insert_consultation(UserControl::getId(), $user_question_title, $user_question, $big_category_id, $category_id);
        if ($res) {
            # add point to owner
            $point = array(
                'user_id' => $this->user['id'],
                'thread_id' => $res, 
                'target' => 1
            );
            $point_id = $this->addPoint($point);
            if ($point_id)
                $this->Mpoints->updateScoutBonus($this->user['id'], $point_id['point'], '投稿ボーナス');    
        }

        /* urlから取得生成 */
        $data = array();
        $up_image_name_ar = array();
        if ($bl_max > 0) {
            
            $bl = $post['bl'];
            $bl_description = $post['bl_description'];
            $bl_title = $post['bl_title'];
            $bl_source_url = $post['bl_source_url'];

            $str2 = $str = $post['message_all'];

            //ディレクトリを作成してその中にアップロード
            $upload_path = "uploads/images/".$res;
            if(!file_exists($upload_path)){
                mkdir($upload_path,0777);
            }

            for ($i=0; $i < $bl_max; $i++) {
                $this->viewData['info_title'] = $bl_title[$i];
                $this->viewData['info_description'] = $bl_description[$i];

                /* ここから */
                $bl_image = $bl[$i];
                if ($bl_image != '') {
                    $file = $bl_image;
                    $file = preg_replace('/(.+?)\?.*/', '\\1', $file);
                    $file_type = $this->get_img_extension($file);
                    $name = date("YmdHis").$i.".".$file_type;
                    $up_image_name_ar[$i] = $name;
                    $this->viewData['info_image'] = '/'.$upload_path.'/'.$name;
                } else {
                    $up_image_name_ar[$i] = '';
                    $this->viewData['info_image'] = '';
                }
                /* ここまで */

                $this->viewData['info_source_url'] = $bl_source_url[$i];
                $template = $this->twig->render('theme_url_put_template.html', $this->viewData);
                $pattern = '/--IMAGE'.sprintf("%03d",$i).'--/';
                $str = str_replace($pattern, $template, $str);
            }
            $data['message'] = $str;
            $pattern = '/\/\-\-IMAGE[0-9][0-9][0-9]\-\-\//';
            $str2 = preg_replace($pattern, '', $str2);
            $tw_comment = $str2;
        } else {
            $data['message'] = $post['message_all'];
            $tw_comment = $post['message_all'];
        }

        /* 画像が投稿されたら true */
        $title_cat = false;
        $latest_up_load_file = null;

        $upload_image_file = $this->input->post('userfile');
        if ($upload_image_file != ''){
            list($file_name,$file_type) = explode(".",$upload_image_file);
            //ファイル名を日付と時刻にしている。
            $name = date("YmdHis").".".$file_type;
            $file = "uploads/images/".$res;
            //ディレクトリを作成してその中にアップロードしている。
            if(!file_exists($file)){
                mkdir($file,0777);
            }
            $move_path = $file.'/'.$name;
            rename("uploads/temp/".$upload_image_file, $move_path);
            $data['up_image'] = '/'.$move_path;
            $latest_up_load_file = $move_path;

            $title_cat = true;
        }

        /* 画像ダウンロード */
        if ($bl_max > 0) {
            $bl = $post['bl'];
            $upload_path = "uploads/images/".$res;
            for ($i=0; $i < $bl_max; $i++) {
                $bl_image = $bl[$i];
                if ($bl_image != '') {
                    $contents_data = file_get_contents($bl_image);
                    file_put_contents($upload_path.'/'.$up_image_name_ar[$i], $contents_data);
                    $title_cat = true;
                }
            }
            $latest_up_load_file = $upload_path.'/'.$up_image_name_ar[0];
        }

        if ($title_cat) {
            $data['title'] = '【画像】'.$post['question_subject'];
            $data['image_up_flag'] = 1;            
            //$title = '【画像】'.$post['title'].' '.$post['qclval'];
        } else {
            //$title = $post['title'].' '.$post['qclval'];
        }
        /*$bulletin = $this->bulletin_model->getRowById($post['big_cate_id']);
        $tw_text = '【'.$bulletin['jp_name'].'あるある】'.$title.' '.base_url().'reaction/'.$res.' '.mb_strimwidth($tw_comment, 0,130,"...");*/
        $this->Mbbs->updateData($res, $data);                    
        $this->viewData['question_id'] = $res;
        $this->viewData['big_category_id'] = $big_category_id;
        $this->viewData['category_id'] = $category_id;
        $this->viewData['breadcrumb_list'] = $this->bread_crumb('', array(
                                                                           array(
                                                                                'url' => base_url(),
                                                                                'text' => 'トップページ',
                                                                            ),
                                                                            array(
                                                                                'url' => base_url(),
                                                                                'text' => '相談内容の投稿完了',
                                                                            ),
                                                                        )
                                                                    );

        $this->template = 'bb/question_comp.html';
        $this->twig->display($this->template, $this->viewData);
    }

    public function contents($bigid = null, $subid = null, $id = null)
    {
        $bool = false;
        $input = $this->input->post();
        $is_request = $this->input->is_ajax_request();

        $res = $this->Mbbs->get_consultation($id);
        if (!$is_request) {
            $check = $this->Madmin->checkLinkCategory($bigid, $subid, $id);
            if (!$res || empty($check)) {
                header("HTTP/1.0 404 Not Found");
                $this->twig->display('404.html', $this->viewData);
                return;
            }                
        } 
       
        
        $question_title = $res['title'];
        $question_desc = $res['message'];

        if ($res['nick_name'] == NULL OR $res['nick_name'] == '') {
            $res['nick_name'] = '匿名';
        }

        // bookmark toggle hightlight
        $book_res = $this->Mbbs->check_bookmark(UserControl::getId(), $res['id']);
        $this->viewData['btn_bookmark_color'] = (!$book_res['flag'] || $book_res['flag'] == '')? 'btn_blue-o' : 'btn_blue';
        $this->viewData['flag'] = (!$book_res['flag'] || $book_res['flag'] == 0)? 0 : 1;

        $this->viewData['consultation_info'] = $res;
        $this->viewData['btn_orange_color'] = ($res['like_count'] > 0)? 'btn_orange' : 'btn_orange-o' ;

        $this->big_category_name = $res['big_category_name'];
        $this->category_name = $res['category_name'];
        $this->big_category_id = $res['big_cate_id'];
        $this->category_id = $res['cate_id'];
        $this->contents_id = $res['id'];
        $big_cat_id = $res['big_cate_id'];
        $this->viewData['big_category_info']['id'] = $big_cat_id;
        
        if (!$is_request) {
            $this->load->model('Mviewlogs');
            $log_data['thread_id'] = $id;
            $log_data['big_cate_id'] =  $res['big_cate_id'];
            $log_data['cate_id'] =  $res['cate_id'];     
            $this->logAccess($log_data);
            $this->template = 'bb/consult.html';
            $offset = 0;
        } else {
            $this->template = 'bb/consult_ajax.html';
            $offset = $input['replay_count'];
        }

        $res = $this->Mbbs->get_response($id, $offset);
        foreach ($res as $key => $val) {
            if ($res[$key]['nick_name'] == NULL OR $res[$key]['nick_name'] == '') {
                $res[$key]['nick_name'] = '匿名';
            }

            $arr = array(
                'message_id' => $res[$key]['id'],
                'user_id' => $res[$key]['response_id']
            );
            $res[$key]['json_data'] =json_encode($arr);
            $flag = ($res[$key]['reply_id'] != NULL)? true:false;
            $res[$key]['reply_ar'] = 
                array(
                    array(
                        'flag' => $flag
                    )
                );
            //Evaluate toggle orange button    
            $res[$key]['btn_orange_color'] = ($res[$key]['evaluate'] > 0)? 'btn_orange' : 'btn_orange-o';
        }

        $this->viewData['response_ar'] = $res;
        $this->viewData['is_own_replay'] = UserControl::getId();
        $this->viewData['breadcrumb_list'] = $this->bread_crumb($id, '', $question_title);
        $sidebar_param = array(
                'row' => 'bt.cate_id',
                'value' => $this->category_id 
            );
        $this->sidebar($this->page_line_max, $sidebar_param);
        $this->viewData['title'] = $question_title.' | ジョイスペ';
        $regex = "@(https?://([-\w\.]+[-\w])+(:\d+)?(/([\w/_\.#-]*(\?\S+)?[^\.\s])?)?)@";
        $question_desc =  preg_replace($regex, '', $question_desc); //strip out urls for meta description
        $this->viewData['meta_desc'] = $question_desc;
        $this->viewData['colorSwitch'] = $this->Mpoints->getPointSettingValue('onayami_like_tally_switch');
        $this->viewData['bid'] = $bigid;
        $this->viewData['subid'] = $subid;
        $this->twig->display($this->template, $this->viewData);
    }

    public function reply_answer()
    {
        //postで送られてきたデータ
        $consult_message = $this->input->post('consult_message');
        $message_id = $this->input->post('message_id');
        $thread_id = $this->input->post('thread_id');
        $nick_name = $this->input->post('nick_name');
        $replay_nick_name = $this->input->post('replay_nick_name');

        if (UserControl::LoggedIn()) {
            $user_id =  UserControl::getId();

            $res = $this->Mbbs->insert_reply($user_id, $thread_id, $consult_message, $message_id);
            if ($res) {
                $this->viewData['user_id'] = $user_id;
                $this->viewData['message_id'] = $res;
                $this->viewData['nick_name'] = $nick_name;
                $this->viewData['replay_nick_name'] = $replay_nick_name;
                $this->viewData['consult_message'] = $consult_message;
                $this->template = 'bb/reply_consult_ajax.html';
                $html = $this->twig->render($this->template, $this->viewData);
                $data = array(
                    'consult_message' => $consult_message,
                    'message_id' => $message_id,
                    'thread_id' => $thread_id,
                    'html' => $html,
                    'flag' => 'true',
                );
                # add point to owner
                $point = array(
                    'user_id' => $this->user['id'], 
                    'thread_id' => $res,
                    'target' => 2
                );
            }
        } else {
//            echo "ログインしていない";
            $data = array('flag' => 'false');
        }
        //postデータをもとに$arrayからデータを抽出
        //$dataをJSONにして返す
        $this->output
             ->set_content_type('application/json')
             ->set_output(json_encode($data));
    }

    public function answer($id = null)
    {
        if (UserControl::LoggedIn()) {
            $this->viewData['user_login_flag'] = true;
            $user_id=  UserControl::getId();
            HelperApp::remove_session('question_id');
        } else {
            $this->viewData['user_login_flag'] = false;
            redirect(MAIN_URL.'user/login');
            return;
        }
        $users = $this->Musers->get_users($user_id);
        $user_info = $users;
        $_POST['nickname'] = ($user_info['nick_name'] == NULL)? '匿名':$user_info['nick_name'];
        if ($this->input->post('answer_id')) {
            $answer_id = $this->input->post('answer_id');
            HelperApp::add_session('answer_id', $answer_id);
        } else {
            $answer_id = HelperApp::get_session('answer_id');
        }

        $this->viewData['answer_id'] = $answer_id;
        $this->viewData['nickname'] = $_POST['nickname'];
        $res = $this->Mbbs->get_consultation($answer_id);
        if (empty($res)) redirect(base_url(), 'refresh');
        if ($res['nick_name'] == NULL OR $res['nick_name'] == '') {
            $res['nick_name'] = '匿名';
        }
        $this->viewData['consultation_info'] = $res;

        $res = $this->Mbbs->get_consultation($answer_id);
        $question_title = $res['title'];
        $this->big_category_name = $res['big_category_name'];
        $this->category_name = $res['category_name'];
        $this->big_category_id = $res['big_cate_id'];
        $this->category_id = $res['cate_id'];
        $this->contents_id = $res['id'];
        
        $this->viewData['breadcrumb_list'] = $this->bread_crumb($answer_id, '', $question_title);
         $sidebar_param = array(
                'row' => 'bt.cate_id',
                'value' => $this->category_id 
            );
        $this->sidebar($this->page_line_max, $sidebar_param);


        $this->template = 'bb/answer.html';
        $this->twig->display($this->template, $this->viewData);
    }

    public function answer_confirm()
    {
        $post = $this->input->post();
        if (!$post) redirect(base_url().'bb/answer/', 'refresh');
        $answer_id = $post['answer_id'];
        $this->viewData['answer_id'] = $answer_id;
        $res = $this->Mbbs->get_consultation($answer_id);
        if ($res['nick_name'] == NULL OR $res['nick_name'] == '') {
            $res['nick_name'] = '匿名';
        }
        $this->viewData['consultation_info'] = $res;

        $nickname = $post['nickname'];
        $answer_message = $post['answer_message'];
        $image_file_name =  $_POST['image_file_name'];

        $_POST['nickname'] = $nickname;
        $_POST['answer_message'] = $answer_message;
        $message_ar = $this->add_conf($answer_message);
        $data = array(
                      'big_cate_id'  => $res['big_cate_id'],
                      'cate_id'  => $res['cate_id'],
                      'answer_id'  => $answer_id,
                      'nickname'  => $nickname,
                      'answer_message'  => $answer_message,
                      'userfile' => $image_file_name,
                      'message_next' =>  $message_ar['preview'],
                      'bl_max' =>  $message_ar['bl_max'],
                      'message_all' =>  $message_ar['message_all']
                    );
        $this->viewData['form_hidden'] = form_hidden($data);
        $this->viewData['data'] = $data;
        $this->viewData['breadcrumb_list'] = $this->bread_crumb($answer_id, array(
                                                                           array(
                                                                                'url' => base_url(),
                                                                                'text' => 'トップページ',
                                                                            ),
                                                                            array(
                                                                                'url' => base_url(),
                                                                                'text' => '入力内容の確認',
                                                                            ),
                                                                        )
                                                                    );
        $this->sidebar();

        $this->template = 'bb/answer_conf.html';     
        $this->twig->display($this->template, $this->viewData);
    }

    public function add_answer()
    {
        $send_response = $this->input->post('answer_message');
        $thread_id     = $this->input->post('answer_id');

        if (UserControl::LoggedIn()) {
            $user_id=  UserControl::getId();
        } else {
            return;
        }
        $res = $this->Mbbs->insert_response($user_id, $thread_id, $send_response);
        if ($res) {
            # add point to owner
            $point = array(
                'user_id' => $this->user['id'], 
                'target' => 2
            );
            $point_id = $this->addPoint($point);
            if ($point_id)
                $this->Mpoints->updateScoutBonus($this->user['id'], $point_id['point'], '回答ボーナス');
        }
        redirect(base_url().'bb/answer_completion');
    }

    public function answer_completion()
    {
        $post = $this->input->post();
        if (!$post) redirect(base_url().'bb/answer', 'refresh'); 

        $send_response = $post['answer_message'];
        $thread_id     = $post['answer_id'];
        $bl_max = $post['bl_max'];
        $user_id = UserControl::getId();
        $res = $this->Mbbs->insert_response($user_id, $thread_id, $send_response);
        $thread = $this->Mbbs->getPostCommentCount($thread_id);
        if ($res) {
            # add point to owner
            $max_comment_bonus = $this->Mpoints->getPointSettingValue('max_answer_has_bonus');
            if ($thread['comment_count'] <= $max_comment_bonus) {
                $point = array(
                            'user_id' => $this->user['id'], 
                            'thread_id' => $thread_id,
                            'target' => 2
                         );
                $point_id = $this->addPoint($point);
                if ($point_id)
                    $this->Mpoints->updateScoutBonus($this->user['id'], $point_id['point'], '回答ボーナス');
            }
           
        }
            
        /* urlから取得生成 */
        $data = array();
        $up_image_name_ar = array();
        if ($bl_max > 0) {
            $bl = $post['bl'];
            $bl_description = $post['bl_description'];
            $bl_title = $post['bl_title'];
            $bl_source_url = $post['bl_source_url'];

            $str2 = $str = $post['message_all'];

            //ディレクトリを作成してその中にアップロード
            $upload_path = "uploads/images/".$res;
            if(!file_exists($upload_path)){
                mkdir($upload_path,0777);
            }

            for ($i=0; $i < $bl_max; $i++) {
                $this->viewData['info_title'] = $bl_title[$i];
                $this->viewData['info_description'] = $bl_description[$i];

                /* ここから */
                $bl_image = $bl[$i];
                if ($bl_image != '') {
                    $file = $bl_image;
                    $file = preg_replace('/(.+?)\?.*/', '\\1', $file);
                    $file_type = $this->get_img_extension($file);
                    $name = date("YmdHis").$i.".".$file_type;
                    $up_image_name_ar[$i] = $name;
                    $this->viewData['info_image'] = '/'.$upload_path.'/'.$name;
                } else {
                    $up_image_name_ar[$i] = '';
                    $this->viewData['info_image'] = '';
                }
                /* ここまで */

                $this->viewData['info_source_url'] = $bl_source_url[$i];
                $template = $this->twig->render('theme_url_put_template.html', $this->viewData);
                $pattern = '/--IMAGE'.sprintf("%03d",$i).'--/';
                $str = str_replace($pattern, $template, $str);
            }
            $data['message'] = $str;
            $pattern = '/\/\-\-IMAGE[0-9][0-9][0-9]\-\-\//';
            $str2 = preg_replace($pattern, '', $str2);
            $tw_comment = $str2;
        } else {
            $data['message'] = $post['message_all'];
            $tw_comment = $post['message_all'];
        }
        /* 画像が投稿されたら true */
        $title_cat = false;
        $latest_up_load_file = null;

        $upload_image_file = $this->input->post('userfile');
        if ($upload_image_file != ''){
            list($file_name,$file_type) = explode(".",$upload_image_file);
            //ファイル名を日付と時刻にしている。
            $name = date("YmdHis").".".$file_type;
            $file = "uploads/images/".$res;
            //ディレクトリを作成してその中にアップロードしている。
            if(!file_exists($file)){
                mkdir($file,0777);
            }
            $move_path = $file.'/'.$name;
            rename("uploads/temp/".$upload_image_file, $move_path);
            $data['up_image'] = '/'.$move_path;
            $latest_up_load_file = $move_path;

            $title_cat = true;
        }

        /* 画像ダウンロード */
        if ($bl_max > 0) {
            $bl = $post['bl'];
            $upload_path = "uploads/images/".$res;
            for ($i=0; $i < $bl_max; $i++) {
                $bl_image = $bl[$i];
                if ($bl_image != '') {
                    $contents_data = file_get_contents($bl_image);
                    file_put_contents($upload_path.'/'.$up_image_name_ar[$i], $contents_data);
                }
            }
            $title_cat = true;
            $latest_up_load_file = $upload_path.'/'.$up_image_name_ar[0];
        }

        if ($title_cat) {
            $this->Mbbs->updateResponse($res, $data);
        }

        $answer_id = HelperApp::get_session('answer_id');
        $this->viewData['big_category_id'] = $this->input->post('big_cate_id');
        $this->viewData['category_id'] = $this->input->post('cate_id');

        $this->viewData['answer_id'] = $answer_id;
        $this->template = 'bb/answer_comp.html';
        $this->viewData['breadcrumb_list'] = $this->bread_crumb($answer_id, array(
                                                                           array(
                                                                                'url' => base_url(),
                                                                                'text' => 'トップページ',
                                                                            ),
                                                                            array(
                                                                                'url' => base_url(),
                                                                                'text' => '投稿完了',
                                                                            ),
                                                                        )
                                                                    );
        $this->sidebar();
        $this->twig->display($this->template, $this->viewData);

    }

    public function login()
    {
//        echo "string",$this->config->item('language');
//        echo "string",$this->config->item('twig_path');


        if ($this->agent->is_mobile()) {
//            $this->twig->template_dir_set(TEMPLATEPATH.'sp');
//            $this->twig->display('index.html', $this->viewData);
            $this->twig->template_dir_set(TEMPLATEPATH_SP);
            $this->twig->display('search/index.html', $this->viewData);
        } else {
//          echo BASEPATH;
//          echo TEMPLATEPATH_PC;
//          echo TEMPLATEPATH_SP;

            $cate = $this->Madmin->get_cate_parents();


            $options = array();
            $options[0] = 'カテゴリー';
            foreach ($cate as $key => $value) {
                $options[$value['id']] = $value['name'];
            }
            $this->viewData['form_cate_opt'] = $options;
            $p_cate_names = $options;

            $this->viewData['p_cate_names'] = $p_cate_names;

            $this->viewData['test_text'] = 'テストテキスト？？？？？？？？';


            $this->twig->template_dir_set(TEMPLATEPATH_PC);
            $this->twig->display('index.html', $this->viewData);
        }

//      $this->load->view('index');
    }


    public function consultation_list($big_category_id = null, $category_id = null)
    {
        if ($big_category_id == null && $category_id == null) {
            echo "無し";
            $keyword = $this->input->get('keyword');

            $consultation_list_ar = $this->Mbbs->search_keyword_consultation_list($keyword);
            var_dump($consultation_list_ar);

            echo $keyword;

        } elseif ($big_category_id != null && $category_id == null) {
            echo "大カテ";
            $consultation_list_ar = $this->Mbbs->search_consultation_list($big_category_id);
            var_dump($consultation_list_ar);
        } else {
            $consultation_list_ar = $this->Mbbs->search_consultation_list($big_category_id, $category_id);
            var_dump($consultation_list_ar);
        }


        if ($this->agent->is_mobile()) {
            $this->twig->template_dir_set(TEMPLATEPATH_SP);
            $this->twig->display('search/index.html', $this->viewData);
        } else {

            $this->viewData['test_text'] = 'テストテキスト？？？？？？？？';


            $this->twig->template_dir_set(TEMPLATEPATH_PC);
            $this->twig->display('bb/consultation_list.html', $this->viewData);
        }

//      $this->load->view('index');
    }

    function addPoint($point){
        return $this->Mpoints->insert($point);
    }

    public function update_evaluate_ajax() {
        $post = $this->input->post();
        if (!$post) return false;
        $message_id = $post['message_id'];
        $thread_id = $post['thread_id'];
        $user_id = (UserControl::LoggedIn())? $post['user_id'] : '';
        $mode = $post['mode'];

        $res = $this->Mbbs->add_evaluate(UserControl::getId(), 1, $message_id, $mode);
        if ($res) {

            if (!$mode) {
                $comment_id = null;
                $point_msg = '私もそう思う';
            } else {
                $comment_id = $message_id;
                $point_msg = '私もそう思う（質問者）';
            }

            # add point to owner
            $point = array(
                'user_id' => $user_id, 
                'thread_id' => $thread_id,
                'comment_id' => $comment_id,
                'target' => 4
            );

            $point_id = $this->addPoint($point);
            if ($point_id)
                $this->Mpoints->updateScoutBonus($user_id, $point_id['point'], $point_msg);
        }
        $data = array(
            'flag' => $res['flag'],
            'message_id' => $message_id,
            'user_id' => $user_id
        );
        //$dataをJSONにして返す
        $this->output
             ->set_content_type('application/json')
             ->set_output(json_encode($data));
    }

    /**
     * Retrieve all sub category by calling ajax
     * @return [type] [description]
     */
    public function sub_category_ajax() {
        $post = $this->input->post();
        if(empty($post['bid'])) return;
        
        $bid = $post['bid'];
        $categorys = $this->Madmin->get_categorys($bid);
        echo json_encode($categorys);
        return;
    }

    /**
     * Bookmark question you want save to you dev
     * @return json [display message ,thread id and status of the thread]
     */
    public function bookmark(){

        $thread_id = $this->input->post('thread_id');
        $user_id = $this->input->post('user_id');
        $mode = $this->input->post('mode');

        $res = $this->Mbbs->update_bookmark($user_id, $thread_id, $mode);

        $data = array(
            'message_id' => $thread_id,
            'user_id' => $user_id,
            'status' => $res
        );

        //$dataをJSONにして返す
        $this->output
             ->set_content_type('application/json')
             ->set_output(json_encode($data));
    }

    public function ajaxImagefileUpload(){
        if (!isset($_FILES['userfile'])) return false;
        $userfile = $_FILES['userfile']['name'];
        $upload_image_file = $userfile;
        list($file_name,$file_type) = explode(".",$upload_image_file);
        $name = date("YmdHis").".".$file_type;

        $config['file_name'] = $name;
        $config['upload_path'] = 'uploads/temp/';
        $config['allowed_types'] = 'gif|jpg|png';
        $config['max_size'] = '3000';
        $config['max_width']  = '1024';
        $config['max_height']  = '768';
        $this->load->library('upload', $config);

        if ( ! $this->upload->do_upload('userfile')) {
            $data_ar['flag'] = false;
        } else {
            $data_ar['flag'] = true;
        }

        $data_ar['filename'] = $name;
        $this->output
             ->set_content_type('application/json')
             ->set_output(json_encode($data_ar));
    }

    public function add_conf($text)
    {
        $url_ar = array();

        if(preg_match_all('(https?://[-_.!~*\'()a-zA-Z0-9;/?:@&=+$,%#]+)', $text, $result) !== false){
            foreach ($result[0] as $value){
                //URL表示
                $url_ar[] = $value;
            }
        }
        mb_language('Japanese');

        // スクレイピング
        $this->load->library('simple_html_dom');
        $parser = new Simple_html_dom();
        $page_info_ar = array();

        foreach ($url_ar as $key => $value) {
            $url = $value;
            $html = file_get_contents($url);
            $html = mb_convert_encoding($html, 'utf8', 'auto');
            $parser->load($html);

            $title = $parser->find('title', 0)->plaintext;

            if($parser->find("meta[name='description']", 0) != null) {
                $meta_description = $parser->find("meta[name='description']", 0)->content;
            } else {
                $meta_description = '';
            }

            $page_info_ar[$key]['title'] = $title;
            $page_info_ar[$key]['description'] = $meta_description;
            $page_info_ar[$key]['source_url'] = $url;

            $image_ar = array();

            $html_type = (preg_match('/<!doctype html>/i', $html, $matches) == 1)? 'html5':'html';
            $img_find = ($html_type == 'html5')? 'article img':'img';

            foreach($parser->find($img_find) as $element){
                if(preg_match('/(https:\/\/|http:\/\/)(.*?)\//', $element->src, $matches) != 1){
                    $url_temp_ar = parse_url($url);
                    $scheme = $url_temp_ar['scheme'];
                    $host = $url_temp_ar['host'];
                    $url_tmp = $scheme.'://'.$host.'/';
                    if(preg_match('/^\/(.*)/', $element->src, $matches) == 1){
                        $src = preg_replace('/^\//', '', $element->src);
                    } else {
                        $src = preg_replace('/^\.\//', '', $element->src);
                    }
                    $image_ar[] = $url_tmp.'/'.$src;
                } else {
                    $url = $element->src;
                    $image_ar[] = $url;
                }
            }
            $page_info_ar[$key]['image_ar'] = $image_ar;
        }
        $str = preg_replace('(https?://[-_.!~*\'()a-zA-Z0-9;/?:@&=+$,%#]+)', '/--URLIMAGE--/', $text);
        $ar = preg_match_all('/\/--URLIMAGE--\//', $str, $m, PREG_OFFSET_CAPTURE);

        $i = 0;
        foreach ($url_ar as $key => $val) {
            $str = substr_replace($str, sprintf("/--IMAGE%03d--/",$i), $m[0][$i][1], strlen('/--URLIMAGE--/'));
            $i++;
        }
        $message_all = $str;
        $preview_str = nl2br($str);

        $i = 0;
        foreach ($url_ar as $key => $value) {
            $this->viewData['info'] = $page_info_ar[$i];
            $this->viewData['info_num'] = $i;
            $template = $this->twig->render('theme_url_template.html', $this->viewData);
            $pattern = '/--IMAGE'.sprintf("%03d",$i).'--/';
            $str = str_replace($pattern, $template, $str);
            $i++;
        }

        $i = 0;
        foreach ($url_ar as $key => $value) {
            $this->viewData['info'] = $page_info_ar[$i];
            $this->viewData['info_num'] = $i;
            $template = $this->twig->render('theme_url_template.html', $this->viewData);
            $pattern = '/--IMAGE'.sprintf("%03d",$i).'--/';
            $preview_str = str_replace($pattern, $template, $preview_str);
            $i++;
        }

        $bl_max = count($url_ar);

        $data_ar = array();
        $data_ar['message_all'] =  $message_all;
        $data_ar['message'] = $str;
        $data_ar['preview'] = $preview_str;
        $data_ar['bl_max'] = $bl_max;

        return $data_ar;
    }

    public function get_img_extension($path)
    {
        //getimagesize関数で画像情報を取得する $path:画像ファイルのパスやURL
        list($img_width, $img_height, $mime_type, $attr) = getimagesize($path);
        //list関数の第3引数にはgetimagesize関数で取得した画像のMIMEタイプが格納されているので条件分岐で拡張子を決定する
        switch($mime_type){
            //jpegの場合
            case IMAGETYPE_JPEG:
                //拡張子の設定
                $img_extension = "jpg";
                break;
            //pngの場合
            case IMAGETYPE_PNG:
            //拡張子の設定
                $img_extension = "png";
                break;
            //gifの場合
            case IMAGETYPE_GIF:
                //拡張子の設定
                $img_extension = "gif";
                break;
        }
        //拡張子の出力
        return $img_extension;
    }

    function logAccess($data){
        if(!$this->Mviewlogs->AccessToday($data['thread_id']))
        $this->Mviewlogs->insert($data);
    }

    /* production ONLY */
    function postTwitter($tw_text, $media_id = 0) {
        $http_host = $_SERVER['HTTP_HOST'];
        if ($http_host != 'aruaru.joyspe.com') {
            return true;
        }
        // 設定
/*
        $api_key = 'a5a6Sv0beCatkJIUNluqf6lcA' ;        // APIキー
        $api_secret = 'hdONP8kAvSI1TQTpgEVbJqNDtvGkSh3skcchpUDTWp4gsbErys' ;        // APIシークレット
        $access_token = '715391831731638272-mzxzIEF1eg35KPfhdUoWo7PdrIHgkF1' ;      // アクセストークン
        $access_token_secret = 'DUkX1mgsXMVpYc978vjeXMLkA2EiLluFP44HfY9CvGCt8' ;        // アクセストークンシークレット[_TWITTER_OAUTH_1_]
*/

        $api_key = 'ypQ15ofHDkPRqHuyhedXfmaqb' ;        // APIキー
        $api_secret = '58iKxocq7wgMQAAFR94QyMpDJofGFfClfuJDFbJrQo6FJFVO5K' ;        // APIシークレット
        $access_token = '715392288269045763-4NGx0ADsOu2UMK6dos7rZDlrE1Jg0s4' ;      // アクセストークン
        $access_token_secret = 'CslD9IFhqqEuqyLCS9Cx4ydHel1JCY5D5dDrXv1lBmQMg' ;        // アクセストークンシークレット[_TWITTER_OAUTH_1_]
        $request_url = 'https://api.twitter.com/1.1/statuses/update.json' ;     // エンドポイント
        $request_method = 'POST' ;
//        $media_ids = 743685123363606528;
        // パラメータA (リクエストのオプション)

        if ($media_id == 0) {
            $params_a = array(
                'status' => $tw_text,
    //          'status' => 'APIを通して投稿なう。 投稿時間: ' . date( 'Y/m/d H:i' ) . '投稿元: https://joyspe.com/' ,
            ) ;
        } else {
            $params_a = array(
                'status' => $tw_text,
    //          'status' => 'APIを通して投稿なう。 投稿時間: ' . date( 'Y/m/d H:i' ) . '投稿元: https://joyspe.com/' ,
                'media_ids' => $media_id,
            ) ;
        }

        // キーを作成する (URLエンコードする)
        $signature_key = rawurlencode( $api_secret ) . '&' . rawurlencode( $access_token_secret ) ;

        // パラメータB (署名の材料用)
        $params_b = array(
            'oauth_token' => $access_token ,
            'oauth_consumer_key' => $api_key ,
            'oauth_signature_method' => 'HMAC-SHA1' ,
            'oauth_timestamp' => time() ,
            'oauth_nonce' => microtime() ,
            'oauth_version' => '1.0' ,
        ) ;

        // パラメータAとパラメータBを合成してパラメータCを作る
        $params_c = array_merge( $params_a , $params_b ) ;

        // 連想配列をアルファベット順に並び替える
        ksort( $params_c ) ;

        // パラメータの連想配列を[キー=値&キー=値...]の文字列に変換する
        $request_params = http_build_query( $params_c , '' , '&' ) ;

        // 一部の文字列をフォロー
        $request_params = str_replace( array( '+' , '%7E' ) , array( '%20' , '~' ) , $request_params ) ;

        // 変換した文字列をURLエンコードする
        $request_params = rawurlencode( $request_params ) ;

        // リクエストメソッドをURLエンコードする
        // ここでは、URL末尾の[?]以下は付けないこと
        $encoded_request_method = rawurlencode( $request_method ) ;
     
        // リクエストURLをURLエンコードする
        $encoded_request_url = rawurlencode( $request_url ) ;
     
        // リクエストメソッド、リクエストURL、パラメータを[&]で繋ぐ
        $signature_data = $encoded_request_method . '&' . $encoded_request_url . '&' . $request_params ;

        // キー[$signature_key]とデータ[$signature_data]を利用して、HMAC-SHA1方式のハッシュ値に変換する
        $hash = hash_hmac( 'sha1' , $signature_data , $signature_key , TRUE ) ;

        // base64エンコードして、署名[$signature]が完成する
        $signature = base64_encode( $hash ) ;

        // パラメータの連想配列、[$params]に、作成した署名を加える
        $params_c['oauth_signature'] = $signature ;

        // パラメータの連想配列を[キー=値,キー=値,...]の文字列に変換する
        $header_params = http_build_query( $params_c , '' , ',' ) ;

        // リクエスト用のコンテキスト
        $context = array(
            'http' => array(
                'method' => $request_method , // リクエストメソッド
                'header' => array(            // ヘッダー
                    'Authorization: OAuth ' . $header_params ,
                ) ,
            ) ,
        ) ;

        // オプションがある場合、コンテキストにPOSTフィールドを作成する
        if( $params_a )
        {
            $context['http']['content'] = http_build_query( $params_a ) ;
        }

        // cURLを使ってリクエスト
        $curl = curl_init() ;
        curl_setopt( $curl , CURLOPT_URL , $request_url ) ;
        curl_setopt( $curl , CURLOPT_HEADER, 1 ) ; 
        curl_setopt( $curl , CURLOPT_CUSTOMREQUEST , $context['http']['method'] ) ;         // メソッド
        curl_setopt( $curl , CURLOPT_SSL_VERIFYPEER , false ) ;                             // 証明書の検証を行わない
        curl_setopt( $curl , CURLOPT_RETURNTRANSFER , true ) ;                              // curl_execの結果を文字列で返す
        curl_setopt( $curl , CURLOPT_HTTPHEADER , $context['http']['header'] ) ;            // ヘッダー
        if( isset( $context['http']['content'] ) && !empty( $context['http']['content'] ) )
        {
            curl_setopt( $curl , CURLOPT_POSTFIELDS , $context['http']['content'] ) ;           // リクエストボディ
        }
        curl_setopt( $curl , CURLOPT_TIMEOUT , 5 ) ;                                        // タイムアウトの秒数
        $res1 = curl_exec( $curl ) ;
        $res2 = curl_getinfo( $curl ) ;
        curl_close( $curl ) ;

        // 取得したデータ
        $json = substr( $res1, $res2['header_size'] ) ;             // 取得したデータ(JSONなど)
        $header = substr( $res1, 0, $res2['header_size'] ) ;        // レスポンスヘッダー (検証に利用したい場合にどうぞ)

        // [cURL]ではなく、[file_get_contents()]を使うには下記の通りです…
        // $json = @file_get_contents( $request_url , false , stream_context_create( $context ) ) ;

        // JSONをオブジェクトに変換
        $obj = json_decode( $json ) ;

        // エラー判定
        if( !$json || !$obj )
        {
            //error;
            return false;
        }
        return true;
    }

    public function postTwitterImage($file_path = '')
    {
            $http_host = $_SERVER['HTTP_HOST'];
            if ($http_host != 'aruaru.joyspe.com') {
                return true;
            }
            $doc_root = $_SERVER['DOCUMENT_ROOT'];
    /**************************************************
        メディアアップロード
        // 下記のエンドポイントに対応
        https://api.twitter.com/1.1/account/update_profile_background_image.json    (背景画像)
        https://api.twitter.com/1.1/account/update_profile_image.json   (アイコン画像)
        https://api.twitter.com/1.1/account/update_profile_banner.json  (バナー画像)
        https://upload.twitter.com/1.1/media/upload.json    (画像、動画のメディアアップロード)
    **************************************************/

        // 設定
/*
        $api_key = 'a5a6Sv0beCatkJIUNluqf6lcA' ;        // APIキー
        $api_secret = 'hdONP8kAvSI1TQTpgEVbJqNDtvGkSh3skcchpUDTWp4gsbErys' ;        // APIシークレット
        $access_token = '715391831731638272-mzxzIEF1eg35KPfhdUoWo7PdrIHgkF1' ;      // アクセストークン
        $access_token_secret = 'DUkX1mgsXMVpYc978vjeXMLkA2EiLluFP44HfY9CvGCt8' ;        // アクセストークンシークレット[_TWITTER_OAUTH_2_]
*/
        $api_key = 'ypQ15ofHDkPRqHuyhedXfmaqb' ;        // APIキー
        $api_secret = '58iKxocq7wgMQAAFR94QyMpDJofGFfClfuJDFbJrQo6FJFVO5K' ;        // APIシークレット
        $access_token = '715392288269045763-4NGx0ADsOu2UMK6dos7rZDlrE1Jg0s4' ;      // アクセストークン
        $access_token_secret = 'CslD9IFhqqEuqyLCS9Cx4ydHel1JCY5D5dDrXv1lBmQMg' ;        // アクセストークンシークレット[_TWITTER_OAUTH_1_]

        $request_url = 'https://upload.twitter.com/1.1/media/upload.json' ;     // エンドポイント
        $request_method = 'POST' ;

        $file = $doc_root.$file_path;

        // パラメータA (リクエストのオプション)
        $params_a = array(
            'media_data' => base64_encode( @file_get_contents($file) ) ,
        ) ;

        // キーを作成する (URLエンコードする)
        $signature_key = rawurlencode( $api_secret ) . '&' . rawurlencode( $access_token_secret ) ;

        // パラメータB (署名の材料用)
        $params_b = array(
            'oauth_token' => $access_token ,
            'oauth_consumer_key' => $api_key ,
            'oauth_signature_method' => 'HMAC-SHA1' ,
            'oauth_timestamp' => time() ,
            'oauth_nonce' => microtime() ,
            'oauth_version' => '1.0' ,
        ) ;

        // リクエストURLにより、メディアを指定するパラメータが違う
        switch( $request_url )
        {
            case( 'https://api.twitter.com/1.1/account/update_profile_background_image.json' ) :
            case( 'https://api.twitter.com/1.1/account/update_profile_image.json' ) :
                $media_param = 'image' ;
            break ;

            case( 'https://api.twitter.com/1.1/account/update_profile_banner.json' ) :
                $media_param = 'banner' ;
            break ;

            case( 'https://upload.twitter.com/1.1/media/upload.json' ) :
                $media_param = ( isset($params_a['media']) && !empty($params_a['media']) ) ? 'media' : 'media_data' ;
            break ;
        }

        // イメージデータの取得
        $media_data = ( isset( $params_a[ $media_param ] ) ) ? $params_a[ $media_param ] : '' ;

        // 署名の材料から、動画データを除外する
        if( isset( $params_a[ $media_param ] ) ) unset( $params_a[ $media_param ] ) ;

        // バウンダリーの定義
        $boundary = '---------------' . md5( mt_rand() ) ;

        // POSTフィールドの作成 (まずはメディアのパラメータ)
        $request_body = '' ;
        $request_body .= '--' . $boundary . "\r\n" ;
        $request_body .= 'Content-Disposition: form-data; name="' . $media_param . '"; ' ;
    //  $request_body .= 'filename="' . time() . '"' ;  // [filename]は指定不要なので、おサボり…
        $request_body .= "\r\n" ;
    //  [Content-Type]と[Content-Transfer-Encoding]は指定不要なので、おサボり…
    //  $mimetype = 〜   //Mime/Typeを調べる処理
    //  $request_body .= "Content-Type: " . $mimetype . "\r\n" ;
    //  $request_body .= "Content-Transfer-Encoding: \r\n" ;
        $request_body .= "\r\n" . $media_data . "\r\n" ;

        // POSTフィールドの作成 (その他のオプションパラメータ)
        foreach( $params_a as $key => $value )
        {
            $request_body .= '--' . $boundary . "\r\n" ;
            $request_body .= 'Content-Disposition: form-data; name="' . $key . '"' . "\r\n\r\n" ;
            $request_body .= $value . "\r\n" ;
        }

        // リクエストボディの作成
        $request_body .= '--' . $boundary . '--' . "\r\n\r\n" ;

        // リクエストヘッダーの作成
        $request_header = "Content-Type: multipart/form-data; boundary=" . $boundary ;

        // パラメータAとパラメータBを合成してパラメータCを作る → ×
    //  $params_c = array_merge( $params_a , $params_b ) ;
        $params_c = $params_b ;     // 署名の材料にオプションパラメータを加えないこと

        // 連想配列をアルファベット順に並び替える
        ksort( $params_c ) ;

        // パラメータの連想配列を[キー=値&キー=値...]の文字列に変換する
        $request_params = http_build_query( $params_c , '' , '&' ) ;

        // 一部の文字列をフォロー
        $request_params = str_replace( array( '+' , '%7E' ) , array( '%20' , '~' ) , $request_params ) ;

        // 変換した文字列をURLエンコードする
        $request_params = rawurlencode( $request_params ) ;

        // リクエストメソッドをURLエンコードする
        // ここでは、URL末尾の[?]以下は付けないこと
        $encoded_request_method = rawurlencode( $request_method ) ;
     
        // リクエストURLをURLエンコードする
        $encoded_request_url = rawurlencode( $request_url ) ;
     
        // リクエストメソッド、リクエストURL、パラメータを[&]で繋ぐ
        $signature_data = $encoded_request_method . '&' . $encoded_request_url . '&' . $request_params ;

        // キー[$signature_key]とデータ[$signature_data]を利用して、HMAC-SHA1方式のハッシュ値に変換する
        $hash = hash_hmac( 'sha1' , $signature_data , $signature_key , TRUE ) ;

        // base64エンコードして、署名[$signature]が完成する
        $signature = base64_encode( $hash ) ;

        // パラメータの連想配列、[$params]に、作成した署名を加える
        $params_c['oauth_signature'] = $signature ;

        // パラメータの連想配列を[キー=値,キー=値,...]の文字列に変換する
        $header_params = http_build_query( $params_c , '' , ',' ) ;

        // リクエスト用のコンテキスト
        $context = array(
            'http' => array(
                'method' => $request_method , // リクエストメソッド
                'header' => array(            // ヘッダー
                    'Authorization: OAuth ' . $header_params ,
                    'Content-Type: multipart/form-data; boundary= ' . $boundary ,
                ) ,
                'content' => $request_body ,
            ) ,
        ) ;

        // cURLを使ってリクエスト
        $curl = curl_init() ;
        curl_setopt( $curl , CURLOPT_URL , $request_url ) ;
        curl_setopt( $curl , CURLOPT_HEADER, 1 ) ; 
        curl_setopt( $curl , CURLOPT_CUSTOMREQUEST , $context['http']['method'] ) ;         // メソッド
        curl_setopt( $curl , CURLOPT_SSL_VERIFYPEER , false ) ;                             // 証明書の検証を行わない
        curl_setopt( $curl , CURLOPT_RETURNTRANSFER , true ) ;                              // curl_execの結果を文字列で返す
        curl_setopt( $curl , CURLOPT_HTTPHEADER , $context['http']['header'] ) ;            // ヘッダー
        curl_setopt( $curl , CURLOPT_POSTFIELDS , $context['http']['content'] ) ;           // リクエストボディ
        curl_setopt( $curl , CURLOPT_TIMEOUT , 5 ) ;                                        // タイムアウトの秒数
        $res1 = curl_exec( $curl ) ;
        $res2 = curl_getinfo( $curl ) ;
        curl_close( $curl ) ;

        // 取得したデータ
        $json = substr( $res1, $res2['header_size'] ) ;             // 取得したデータ(JSONなど)
        $header = substr( $res1, 0, $res2['header_size'] ) ;        // レスポンスヘッダー (検証に利用したい場合にどうぞ)

        // [cURL]ではなく、[file_get_contents()]を使うには下記の通りです…
        // $json = @file_get_contents( $request_url , false , stream_context_create( $context ) ) ;

        // JSONをオブジェクトに変換
        $obj = json_decode( $json ) ;


        // エラー判定
        if ( !$json || !$obj ) {
            echo "データを更新することができませんでした…。設定を見直して下さい。";
        }
        return $obj->{'media_id_string'};
    }

    public function post_tw($text, $image_path = '')
    {

/*        $http_host = $_SERVER['HTTP_HOST'];
        if ($http_host != 'aruaru.joyspe.com') {
            return true;
        }*/

        $twitter = array(
          'consumer_key' => 'ypQ15ofHDkPRqHuyhedXfmaqb',
          'consumer_secret' => '58iKxocq7wgMQAAFR94QyMpDJofGFfClfuJDFbJrQo6FJFVO5K',
          'token' => '715392288269045763-4NGx0ADsOu2UMK6dos7rZDlrE1Jg0s4',
          'secret' => 'CslD9IFhqqEuqyLCS9Cx4ydHel1JCY5D5dDrXv1lBmQMg',
          'curl_ssl_verifypeer' => false ,
        );
        $this->load->library('TmhOAuth', $twitter);

        if($image_path != '' ) {

          //画像設定
          $path = $image_path; //画像のパス指定
//        $path = './uploads/tweet_image/'.'aruaru_tweet1.png'; //画像のパス指定
          $data = file_get_contents($path); //画像データ取得
          $image = base64_encode($data); //base64でエンコード

          $images = array(
              'media_data' => $image //画像データを指定
          );

          try {
              $image_upload = $this->tmhoauth->request('POST', 'https://upload.twitter.com/1.1/media/upload.json', $images, true, true);
          }catch (Exception $e){
              echo "Error occured!! This Script was terminated.";
              exit;
          }

          //画像id格納
          $decode = json_decode($this->tmhoauth->response["response"], true); //JSONデコードして
          $media_id = $decode['media_id_string']; //「media_id_string」の値を格納


          $message = array(
              'media_ids' => $media_id, //格納した画像idを指定
              'status' => $text
          );

          try {
              $this->tmhoauth->request('POST', $this->tmhoauth->url('1.1/statuses/update'), $message, true, false);
          }catch (Exception $e){
              echo "Error occured!! This Script was terminated.";
              exit;
          }

        } else {

          $message = array(
              'status' => $text
          );

          try {
              $this->tmhoauth->request('POST', $this->tmhoauth->url('1.1/statuses/update'), $message, true, false);
          }catch (Exception $e){
              echo "Error occured!! This Script was terminated.";
              exit;
          }

        }
    }



    public function post_tw_back($text, $image = '')
    {
        if($image == '' ) {
            $result = file_get_contents(
                base_url().'twitter_text_api',
              false,
              stream_context_create(
                array(
                  'http' => array(
                    'method' => 'POST',
                    'header' => implode(
                      "\r\n",
                      array(
                        'Content-Type: application/x-www-form-urlencoded'
                      )
                    ),
                    'content' => http_build_query(
                      array(
                        'text' => $text,
                        'media_id' => null
                      )
                    )
                  )
                )
              )
            );
        } else {
            $result = file_get_contents(
                base_url().'twitter_img_api',
              false,
              stream_context_create(
                array(
                  'http' => array(
                    'method' => 'POST',
                    'header' => implode(
                      "\r\n",
                      array(
                        'Content-Type: application/x-www-form-urlencoded'
                      )
                    ),
                    'content' => http_build_query(
                      array(
                        'text' => $text,
                        'image' => $image
                      )
                    )
                  )
                )
              )
            );
        }
    }

}
