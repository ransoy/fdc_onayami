<?php

/* bb/answer_comp.html */
class __TwigTemplate_ae4bb45af7f184938df2d9d4a7ca7b618144d739ea5b01c34df71e2150cffc6e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!doctype html>
<html>
<head>
<meta charset=\"utf-8\">
<title>お問い合わせ送信完了</title>
";
        // line 6
        echo twig_include($this->env, $context, "./inc/link.html");
        echo "
</head>
<body>
<div class=\"page_wrap page_answer_comp\">
";
        // line 10
        echo twig_include($this->env, $context, "./inc/header.html");
        echo "
\t<div class=\"content_wrap cf\">
\t\t<div class=\"content\">
\t\t\t<div class=\"content_inner\">
\t\t\t\t<section class=\"sec sec_answer_comp m_b_30\">
\t\t\t\t\t<h2 class=\"sec_ttl\">投稿完了</h2>
\t\t\t\t\t<h3 class=\"content_ttl\">回答の投稿が完了しました</h3>
\t\t\t\t\t<p class=\"sec_desc\">回答した内容はマイページで履歴を確認することが可能です。</p>
\t\t\t\t\t<ul class=\"btn_wrap t_center m_b_20\">
\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t<a href=\"";
        // line 20
        echo twig_escape_filter($this->env, (isset($context["base_url"]) ? $context["base_url"] : null), "html", null, true);
        echo "\" class=\"btn_style btn_gray-o\">トップページへ</a>
\t\t\t\t\t\t</li>
\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t<a href=\"";
        // line 23
        echo twig_escape_filter($this->env, (isset($context["base_url"]) ? $context["base_url"] : null), "html", null, true);
        echo "cate";
        echo twig_escape_filter($this->env, (isset($context["big_category_id"]) ? $context["big_category_id"] : null), "html", null, true);
        echo "/";
        echo twig_escape_filter($this->env, (isset($context["category_id"]) ? $context["category_id"] : null), "html", null, true);
        echo "/";
        echo twig_escape_filter($this->env, (isset($context["answer_id"]) ? $context["answer_id"] : null), "html", null, true);
        echo "\" class=\"btn_style btn_green\">回答したページへ</a></li>
\t\t\t\t\t</ul>
\t\t\t\t</section>
\t\t\t</div>
\t\t</div>
\t\t";
        // line 28
        echo twig_include($this->env, $context, "./inc/sidebar.html");
        echo "
\t</div>
\t";
        // line 30
        echo twig_include($this->env, $context, "./inc/footer.html");
        echo "
</div>
</body>
</html>
";
    }

    public function getTemplateName()
    {
        return "bb/answer_comp.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  71 => 30,  66 => 28,  52 => 23,  46 => 20,  33 => 10,  26 => 6,  19 => 1,);
    }
}
