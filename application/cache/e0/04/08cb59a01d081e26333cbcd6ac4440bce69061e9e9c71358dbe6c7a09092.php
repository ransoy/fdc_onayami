<?php

/* mypage_a.html */
class __TwigTemplate_e00408cb59a01d081e26333cbcd6ac4440bce69061e9e9c71358dbe6c7a09092 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<html lang=\"ja\">
<head>
<meta charset=\"utf-8\">
<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0, user-scalable=no, maximum-scale=1.0\">
<title>お悩み相談</title>
";
        // line 7
        echo twig_include($this->env, $context, "./inc/link.html");
        echo "
<link rel=\"stylesheet\" href=\"";
        // line 8
        echo twig_escape_filter($this->env, (isset($context["base_url"]) ? $context["base_url"] : null), "html", null, true);
        echo "sp/css/my_page.css\">
</head>
<body>
<div class=\"page_wrap page_mypage\">
\t";
        // line 12
        echo twig_include($this->env, $context, "./inc/header.html");
        echo "
\t<section class=\"sec sec_mypage\">
\t\t<h2 class=\"sec_ttl\" id=\"consultation_list\">マイページ</h2>
\t\t<ul class=\"tab_menu\" id=\"tab_menu\">
\t\t\t<li class=\"tab_unresolved current\" id=\"tab_unresolved\">未回答の相談</li>
\t\t\t<li class=\"tab_history\" id=\"tab_history\">相談履歴</li>
\t\t\t<li class=\"tab_bookmark\" id=\"tab_bookmark\">ブックマーク</li>
\t\t</ul>
\t\t<div class=\"sec_inner\">

\t\t\t<div class=\"tab_content content_unresolved\">
\t\t\t\t";
        // line 23
        if (((isset($context["list_all_num"]) ? $context["list_all_num"] : null) > 0)) {
            // line 24
            echo "\t\t\t\t<ul class=\"content_list\">
\t\t\t\t\t";
            // line 25
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["unanswered_ar"]) ? $context["unanswered_ar"] : null));
            foreach ($context['_seq'] as $context["key"] => $context["entry"]) {
                // line 26
                echo "\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t<div class=\"box_wrap\">
\t\t\t\t\t\t\t\t<div class=\"left_box\">
\t\t\t\t\t\t\t\t\t<input type=\"checkbox\" name=\"check[]\" value=\"";
                // line 29
                echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "id", array()), "html", null, true);
                echo "\">
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<div class=\"right_box\">
\t\t\t\t\t\t\t\t\t<a href=\"";
                // line 32
                echo twig_escape_filter($this->env, (isset($context["base_url"]) ? $context["base_url"] : null), "html", null, true);
                echo "cate";
                echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "big_cate_id", array()), "html", null, true);
                echo "/";
                echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "cate_id", array()), "html", null, true);
                echo "/";
                echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "id", array()), "html", null, true);
                echo "\" class=\"ic ic_angle-right\">
\t\t\t\t\t\t\t\t\t\t<p class=\"content_category icon ";
                // line 33
                echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "class", array()), "html", null, true);
                echo "\"><strong>";
                echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "big_category_name", array()), "html", null, true);
                echo "</strong>";
                echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "category_name", array()), "html", null, true);
                echo "</p>
\t\t\t\t\t\t\t\t\t\t<h3 class=\"content_ttl\">";
                // line 34
                echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "title", array()), "html", null, true);
                echo "</h3>
\t\t\t\t\t\t\t\t\t\t<p>";
                // line 35
                echo twig_escape_filter($this->env, trim(strip_tags($this->getAttribute($context["entry"], "message", array()))), "html", null, true);
                echo "</p>
\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t
\t\t\t\t\t\t\t<div class=\"content_date cf\">
\t\t\t\t\t\t\t\t<p class=\"post_date\">";
                // line 41
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["entry"], "create_date", array()), "Y.m.d H:i"), "html", null, true);
                echo "</p>
\t\t\t\t\t\t\t\t<ul class=\"posted_ability\">
\t\t\t\t\t\t\t\t\t";
                // line 43
                if (($this->getAttribute($context["entry"], "like_count", array()) > 0)) {
                    // line 44
                    echo "\t\t\t\t\t\t\t\t\t<li><p class=\"ic ic_heart\">";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "like_count", array()), "html", null, true);
                    echo "</p></li>
\t\t\t\t\t\t\t\t\t";
                } else {
                    // line 46
                    echo "\t\t\t\t\t\t\t\t\t<li><p class=\"ic ic_heart-o\">";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "like_count", array()), "html", null, true);
                    echo "</p></li>
\t\t\t\t\t\t\t\t\t";
                }
                // line 48
                echo "\t\t\t\t\t\t\t\t\t";
                if (($this->getAttribute($context["entry"], "comment_count", array()) > 0)) {
                    // line 49
                    echo "\t\t\t\t\t\t\t\t\t<li><p class=\"ic ic_comment\">";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "comment_count", array()), "html", null, true);
                    echo "</p></li>
\t\t\t\t\t\t\t\t\t";
                } else {
                    // line 51
                    echo "\t\t\t\t\t\t\t\t\t<li><p class=\"ic ic_comment-o\">";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "comment_count", array()), "html", null, true);
                    echo "</p></li>
\t\t\t\t\t\t\t\t\t";
                }
                // line 53
                echo "\t\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</li>
\t\t\t\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['key'], $context['entry'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 57
            echo "\t\t\t\t</ul>
\t\t\t\t<div class=\"content_bottom cf\">
\t\t\t\t\t<ul class=\"pager_wrap\">
\t\t\t\t\t\t";
            // line 60
            echo (isset($context["pagination"]) ? $context["pagination"] : null);
            echo "
\t\t\t\t\t</ul>
\t\t\t\t\t
\t\t\t\t\t<ul class=\"btn_wrap\">
\t\t\t\t\t\t<li><a id=\"checked_delete\" href=\"#\" class=\"btn_style btn_gray-o\">チェックした項目を削除</a></li>
\t\t\t\t\t</ul>\t

\t\t\t\t</div>
\t\t\t\t";
        } else {
            // line 69
            echo "\t\t\t\t<div class=\"search_found not_found cf\">
\t\t\t\t\t<p>未回答の相談はありません。</p>
\t\t\t\t\t<ul class=\"btn_wrap\">
\t\t\t\t\t\t<li><a href=\"";
            // line 72
            echo twig_escape_filter($this->env, (isset($context["base_url"]) ? $context["base_url"] : null), "html", null, true);
            echo "bb/posting\" class=\"btn_style btn_green\">相談する</a></li>
\t\t\t\t\t</ul>
\t\t\t\t</div>
\t\t\t\t";
        }
        // line 76
        echo "\t\t\t</div>

\t\t\t<div class=\"tab_content content_history\">
\t\t\t</div>

\t\t\t<div class=\"tab_content content_bookmark\">
\t\t\t</div>

\t\t</div>
\t</section>
\t";
        // line 86
        if ((isset($context["bonusData"]) ? $context["bonusData"] : null)) {
            // line 87
            echo "\t<section class=\"sec sec_pt_rate\">
\t\t<h2 class=\"sec_ttl\">ポイントレート</h2>
\t\t<div class=\"sec_inner\">
\t\t\t<table>
\t\t\t\t<tr>
\t\t\t\t\t<th style=\"width:53%\"></th>
\t\t\t\t\t<td>レート</td>
\t\t\t\t\t<td>獲得</td>
\t\t\t\t</tr>
\t\t\t\t<tr>
\t\t\t\t\t<th>";
            // line 97
            echo twig_escape_filter($this->env, (isset($context["question_label"]) ? $context["question_label"] : null), "html", null, true);
            echo "</th>
\t\t\t\t\t<td>";
            // line 98
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["pointSettings"]) ? $context["pointSettings"] : null), "question_bonus", array()), "html", null, true);
            echo "</td>
\t\t\t\t\t<td>";
            // line 99
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["bonusData"]) ? $context["bonusData"] : null), "question_bonus", array()), "html", null, true);
            echo "</td>
\t\t\t\t</tr>
\t\t\t\t<tr>
\t\t\t\t\t<th>";
            // line 102
            echo twig_escape_filter($this->env, (isset($context["answer_label"]) ? $context["answer_label"] : null), "html", null, true);
            echo "</th>
\t\t\t\t\t<td>";
            // line 103
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["pointSettings"]) ? $context["pointSettings"] : null), "answer_bonus", array()), "html", null, true);
            echo "</td>
\t\t\t\t\t<td>";
            // line 104
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["bonusData"]) ? $context["bonusData"] : null), "answer_bonus", array()), "html", null, true);
            echo "</td>
\t\t\t\t</tr>
\t\t\t\t<tr>
\t\t\t\t\t<th>";
            // line 107
            echo twig_escape_filter($this->env, (isset($context["like_label"]) ? $context["like_label"] : null), "html", null, true);
            echo "</th>
\t\t\t\t\t<td>";
            // line 108
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["pointSettings"]) ? $context["pointSettings"] : null), "like_points_multiply_by", array()), "html", null, true);
            echo "</td>
\t\t\t\t\t<td>";
            // line 109
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["bonusData"]) ? $context["bonusData"] : null), "like_points_multiply_by", array()), "html", null, true);
            echo "</td>
\t\t\t\t</tr>
\t\t\t\t<tr>
\t\t\t\t\t<th>";
            // line 112
            echo twig_escape_filter($this->env, (isset($context["weekly_label"]) ? $context["weekly_label"] : null), "html", null, true);
            echo "</th>
\t\t\t\t\t<td>";
            // line 113
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["pointSettings"]) ? $context["pointSettings"] : null), "evaluate_bonus", array()), "html", null, true);
            echo "</td>
\t\t\t\t\t<td>";
            // line 114
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["bonusData"]) ? $context["bonusData"] : null), "evaluate_bonus", array()), "html", null, true);
            echo "</td>
\t\t\t\t</tr>
\t\t\t\t<tr>
\t\t\t\t\t<th>";
            // line 117
            echo twig_escape_filter($this->env, (isset($context["max_answer_label"]) ? $context["max_answer_label"] : null), "html", null, true);
            echo "</th>
\t\t\t\t\t<td>";
            // line 118
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["pointSettings"]) ? $context["pointSettings"] : null), "max_answer_has_bonus", array()), "html", null, true);
            echo "</td>
\t\t\t\t\t<td></td>
\t\t\t\t</tr>
\t\t\t</table>
\t\t</div>
\t</section>
\t";
        }
        // line 125
        echo "\t";
        echo twig_include($this->env, $context, "./inc/footer.html");
        echo "
</div>
";
        // line 127
        echo twig_include($this->env, $context, "./inc/script.html");
        echo "
</body>
<script>
\t\$(\"#tab_history\").on('click', function(){
\t\twindow.location.href = '";
        // line 131
        echo twig_escape_filter($this->env, (isset($context["base_url"]) ? $context["base_url"] : null), "html", null, true);
        echo "user/my_page/2/';
\t});
\t\$(\"#tab_bookmark\").on('click', function(){
\t\twindow.location.href = '";
        // line 134
        echo twig_escape_filter($this->env, (isset($context["base_url"]) ? $context["base_url"] : null), "html", null, true);
        echo "user/my_page/3/';
\t});

\t\$('#checked_delete').on('click',function() {

\t\tvar cont = 0;
 \t\tvar checks=[];
        \$(\".content_list input[name='check[]']:checked\").each(function(){
\t\t\tcont++;
            checks.push(this.value);
        });

        console.log(cont);

\t\tif (cont == 0) {
\t\t\treturn false;
\t\t}

\t\tif (!confirm('チェックした項目を削除します\\nよろしいですか？')) {
\t\t\treturn false;
\t\t}

        console.log(checks);

\t\t\$.ajax({
\t\t\ttype: \"POST\",
\t\t\turl: \"";
        // line 160
        echo twig_escape_filter($this->env, (isset($context["base_url"]) ? $context["base_url"] : null), "html", null, true);
        echo "user/delete_unanswer_ajax\",
\t\t\tdata: {
\t\t\t    \"checks\":checks,
\t\t\t}
\t\t}).done(function(data){
\t\t\twindow.location = \"";
        // line 165
        echo twig_escape_filter($this->env, (isset($context["base_url"]) ? $context["base_url"] : null), "html", null, true);
        echo "user/my_page/1\";

\t\t}).fail(function(data){
\t\t    alert('error!!!');
\t\t});

\t});
</script>
</html>
";
    }

    public function getTemplateName()
    {
        return "mypage_a.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  327 => 165,  319 => 160,  290 => 134,  284 => 131,  277 => 127,  271 => 125,  261 => 118,  257 => 117,  251 => 114,  247 => 113,  243 => 112,  237 => 109,  233 => 108,  229 => 107,  223 => 104,  219 => 103,  215 => 102,  209 => 99,  205 => 98,  201 => 97,  189 => 87,  187 => 86,  175 => 76,  168 => 72,  163 => 69,  151 => 60,  146 => 57,  137 => 53,  131 => 51,  125 => 49,  122 => 48,  116 => 46,  110 => 44,  108 => 43,  103 => 41,  94 => 35,  90 => 34,  82 => 33,  72 => 32,  66 => 29,  61 => 26,  57 => 25,  54 => 24,  52 => 23,  38 => 12,  31 => 8,  27 => 7,  19 => 1,);
    }
}
