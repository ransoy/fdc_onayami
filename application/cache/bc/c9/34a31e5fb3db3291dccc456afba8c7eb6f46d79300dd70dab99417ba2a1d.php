<?php

/* ./inc/detail_script.html */
class __TwigTemplate_bcc934a31e5fb3db3291dccc456afba8c7eb6f46d79300dd70dab99417ba2a1d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<script src=\"";
        echo twig_escape_filter($this->env, (isset($context["base_url"]) ? $context["base_url"] : null), "html", null, true);
        echo "sp/js/jquery.validate.min.js\" type=\"text/javascript\"></script>
<script src=\"";
        // line 2
        echo twig_escape_filter($this->env, (isset($context["base_url"]) ? $context["base_url"] : null), "html", null, true);
        echo "sp/js/common.js\" type=\"text/javascript\"></script>";
    }

    public function getTemplateName()
    {
        return "./inc/detail_script.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  24 => 2,  19 => 1,);
    }
}
