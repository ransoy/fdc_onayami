<?php

/* search/category_detail.html */
class __TwigTemplate_076daaac1bc104da91ddf7a55ec05dab8d07b0ba5dac6d693b3c3b1ac6879ddc extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!doctype html>
<html>
<head>
<meta charset=\"utf-8\">
<title>";
        // line 5
        echo twig_escape_filter($this->env, (isset($context["title"]) ? $context["title"] : null), "html", null, true);
        echo "</title>
<meta name=\"description\" content=\"";
        // line 6
        echo twig_escape_filter($this->env, (isset($context["description"]) ? $context["description"] : null), "html", null, true);
        echo "\">
";
        // line 7
        echo twig_include($this->env, $context, "./inc/link.html");
        echo "
</head>
<body>
<div class=\"page_wrap page_category_detail\">
\t";
        // line 11
        echo twig_include($this->env, $context, "./inc/header.html");
        echo "
\t<div class=\"content_wrap cf\">
\t\t<div class=\"content\">
\t\t\t<div class=\"content_inner m_b_30\">
\t\t\t\t<section class=\"sec sec_category_detail\">
\t\t\t\t\t<h2 class=\"sec_ttl icon ";
        // line 16
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["big_category_info"]) ? $context["big_category_info"] : null), "class", array()), "html", null, true);
        echo "\">";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["category_info"]) ? $context["category_info"] : null), "name", array()), "html", null, true);
        echo "</h2>
\t\t\t\t\t<p class=\"sec_desc\">";
        // line 17
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["category_info"]) ? $context["category_info"] : null), "contents", array()), "html", null, true);
        echo "</p>
\t\t\t\t\t";
        // line 18
        if ((isset($context["user_login_flag"]) ? $context["user_login_flag"] : null)) {
            // line 19
            echo "\t\t\t\t\t<ul class=\"btn_wrap\">
\t\t\t\t\t\t<form id=\"form_posting\" method=\"post\" action=\"";
            // line 20
            echo twig_escape_filter($this->env, (isset($context["base_url"]) ? $context["base_url"] : null), "html", null, true);
            echo "bb/posting\" name=\"form_posting\">
\t\t\t\t\t\t<input type=\"hidden\" name=\"question_category_list\" value=\"";
            // line 21
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["big_category_info"]) ? $context["big_category_info"] : null), "id", array()), "html", null, true);
            echo "\">
\t\t\t\t\t\t<input type=\"hidden\" name=\"question_category\" value=\"";
            // line 22
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["category_info"]) ? $context["category_info"] : null), "id", array()), "html", null, true);
            echo "\">
\t\t\t\t\t\t</form>
\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t<a id=\"new_question\" class=\"btn_style btn_green\" href=\"#\">新しく相談する</a>
\t\t\t\t\t\t</li>
\t\t\t\t\t</ul>
\t\t\t\t\t";
        }
        // line 29
        echo "\t\t\t\t</section>
\t\t\t</div>
\t\t\t<section class=\"sec sec_category_list\">
\t\t\t\t<h2 class=\"sec_ttl\" id=\"consultation_list\"><span>【";
        // line 32
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["category_info"]) ? $context["category_info"] : null), "name", array()), "html", null, true);
        echo "】の全ての相談一覧</span></h2>
\t\t\t\t<form id=\"answer\" name=\"answer\" method=\"post\" action=\"";
        // line 33
        echo twig_escape_filter($this->env, (isset($context["base_url"]) ? $context["base_url"] : null), "html", null, true);
        echo "bb/answer\">
\t\t\t\t\t<input type=\"hidden\" name=\"answer_id\" id=\"answer_id\" ></input>
\t\t\t\t\t<input type=\"hidden\" name=\"bid\" id=\"bid\" ></input>
\t\t\t\t\t<input type=\"hidden\" name=\"subid\" id=\"subid\" ></input>
\t\t\t\t</form>\t
\t\t\t\t<ul class=\"consult_category_list\">
\t\t            ";
        // line 39
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["consultation_list_ar"]) ? $context["consultation_list_ar"] : null));
        foreach ($context['_seq'] as $context["key"] => $context["entry"]) {
            // line 40
            echo "\t\t\t\t\t<li class=\"consultation_list\"> <a href=\"";
            echo twig_escape_filter($this->env, (isset($context["base_url"]) ? $context["base_url"] : null), "html", null, true);
            echo "cate";
            echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "big_cate_id", array()), "html", null, true);
            echo "/";
            echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "cate_id", array()), "html", null, true);
            echo "/";
            echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "id", array()), "html", null, true);
            echo "\">
\t\t\t\t\t\t<div class=\"sec_box_top\">
\t\t\t\t\t\t\t<p class=\"sec_box_category icon16 ";
            // line 42
            echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "class", array()), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "big_category_name", array()), "html", null, true);
            echo "</p>
\t\t\t\t\t\t\t<p class=\"sec_box_category_detail\"><span>";
            // line 43
            echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "category_name", array()), "html", null, true);
            echo "</span></p>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<h3 class=\"sec_box_ttl\">";
            // line 45
            echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "title", array()), "html", null, true);
            echo "</h3>
\t\t\t\t\t\t";
            // line 46
            if ((twig_length_filter($this->env, trim(strip_tags($this->getAttribute($context["entry"], "message", array())))) > 41)) {
                // line 47
                echo "\t\t\t\t\t\t\t<p class=\"sec_box_content m_b_10\">";
                echo twig_escape_filter($this->env, twig_slice($this->env, trim(strip_tags($this->getAttribute($context["entry"], "message", array()))), 0, 41), "html", null, true);
                echo "...</p>
\t\t\t\t\t\t";
            } else {
                // line 49
                echo "\t\t\t\t\t\t\t<p class=\"sec_box_content m_b_10\">";
                echo twig_escape_filter($this->env, trim(strip_tags($this->getAttribute($context["entry"], "message", array()))), "html", null, true);
                echo "</p>
\t\t\t\t\t\t";
            }
            // line 51
            echo "\t\t\t\t\t\t<ul class=\"btn_wrap t_center m_b_20\">
\t\t\t\t\t\t\t<li><p class=\"btn_style btn_blue\">続きを読む</p></li>
\t\t\t\t\t\t\t<li><a onclick=\"set_answer(";
            // line 53
            echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "big_cate_id", array()), "html", null, true);
            echo ", ";
            echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "cate_id", array()), "html", null, true);
            echo ", ";
            echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "id", array()), "html", null, true);
            echo ");\" class=\"btn_style btn_green\">回答する</a></li>
\t\t\t\t\t\t</ul>
\t\t\t\t\t\t<div class=\"sec_box_bottom\">
\t\t\t\t\t\t\t<p class=\"posted_data\">";
            // line 56
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["entry"], "create_date", array()), "Y.m.d"), "html", null, true);
            echo "</p>
\t\t\t\t\t\t\t<ul class=\"posted_ability\">
\t\t\t\t\t\t\t\t";
            // line 58
            if (($this->getAttribute($context["entry"], "like_count", array()) > 0)) {
                // line 59
                echo "\t\t\t\t\t\t\t\t<li><p class=\"ic ic_heart\">";
                echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "like_count", array()), "html", null, true);
                echo "</p></li>
\t\t\t\t\t\t\t\t";
            } else {
                // line 61
                echo "\t\t\t\t\t\t\t\t<li><p class=\"ic ic_heart-o\">";
                echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "like_count", array()), "html", null, true);
                echo "</p></li>
\t\t\t\t\t\t\t\t";
            }
            // line 63
            echo "\t\t\t\t\t\t\t\t";
            if (($this->getAttribute($context["entry"], "comment_count", array()) > 0)) {
                // line 64
                echo "\t\t\t\t\t\t\t\t<li><p class=\"ic ic_comment\">";
                echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "comment_count", array()), "html", null, true);
                echo "</p></li>
\t\t\t\t\t\t\t\t";
            } else {
                // line 66
                echo "\t\t\t\t\t\t\t\t<li><p class=\"ic ic_comment-o\">";
                echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "comment_count", array()), "html", null, true);
                echo "</p></li>
\t\t\t\t\t\t\t\t";
            }
            // line 68
            echo "\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t</a>
\t\t\t\t\t</li>
\t\t            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['key'], $context['entry'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 73
        echo "\t\t\t\t</ul>
\t\t\t\t<ul class=\"pager_wrap\">
\t\t\t\t";
        // line 75
        echo (isset($context["pagination"]) ? $context["pagination"] : null);
        echo "
\t\t\t\t</ul>
\t\t\t</section>
\t\t</div>
\t\t";
        // line 79
        echo twig_include($this->env, $context, "./inc/sidebar.html");
        echo "
\t</div>
\t<div class=\"content_wrap p_t_0\">
\t\t<section class=\"sec sec_site_notice\">
\t\t\t<p>xxxx掲示板では女性の性に関する悩みを自由に相談できる掲示板です。匿名で相談または回答することができるので恥ずかしいあの話も気軽に話すことができます。一人で悩まずみんなで女性の性を語り合って解決しましょう！</p>
\t\t</section>
\t</div>
\t";
        // line 86
        echo twig_include($this->env, $context, "./inc/footer.html");
        echo "
</div>
";
        // line 88
        echo twig_include($this->env, $context, "./inc/script.html");
        echo "
<script type=\"text/javascript\">
\$(function() {
\t\$('#new_question').on('click', function(){
\t\t\$('#form_posting').submit();
\t});
});

function set_answer(big_cate_id, cate_id, thread_id){
\tdocument.answer.bid.value = big_cate_id;
\tdocument.answer.subid.value = cate_id;
\tdocument.answer.answer_id.value = thread_id;
\tdocument.answer.submit();
\treturn false;
}
</script> 
</body>
</html>
";
    }

    public function getTemplateName()
    {
        return "search/category_detail.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  228 => 88,  223 => 86,  213 => 79,  206 => 75,  202 => 73,  192 => 68,  186 => 66,  180 => 64,  177 => 63,  171 => 61,  165 => 59,  163 => 58,  158 => 56,  148 => 53,  144 => 51,  138 => 49,  132 => 47,  130 => 46,  126 => 45,  121 => 43,  115 => 42,  103 => 40,  99 => 39,  90 => 33,  86 => 32,  81 => 29,  71 => 22,  67 => 21,  63 => 20,  60 => 19,  58 => 18,  54 => 17,  48 => 16,  40 => 11,  33 => 7,  29 => 6,  25 => 5,  19 => 1,);
    }
}
