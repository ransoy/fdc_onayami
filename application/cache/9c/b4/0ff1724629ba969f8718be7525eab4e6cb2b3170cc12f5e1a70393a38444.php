<?php

/* search/category.html */
class __TwigTemplate_9cb40ff1724629ba969f8718be7525eab4e6cb2b3170cc12f5e1a70393a38444 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<html lang=\"ja\">
<head>
<meta charset=\"utf-8\">
<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0, user-scalable=no, maximum-scale=1.0\">
<title>";
        // line 6
        echo twig_escape_filter($this->env, (isset($context["title"]) ? $context["title"] : null), "html", null, true);
        echo "</title>
<meta name=\"description\" content=\"";
        // line 7
        echo twig_escape_filter($this->env, (isset($context["desciption"]) ? $context["desciption"] : null), "html", null, true);
        echo "\">
";
        // line 8
        echo twig_include($this->env, $context, "./inc/link.html");
        echo "
</head>
<body>
<div class=\"page_wrap page_category\">
\t";
        // line 12
        echo twig_include($this->env, $context, "./inc/header.html");
        echo "
\t<section class=\"sec sec_category\">
\t\t<h2 class=\"sec_ttl\">";
        // line 14
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["big_category_info"]) ? $context["big_category_info"] : null), "name", array()), "html", null, true);
        echo "（全て）</h2>
\t\t<div class=\"sec_inner\">
\t\t\t<p>";
        // line 16
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["big_category_info"]) ? $context["big_category_info"] : null), "contents", array()), "html", null, true);
        echo "</p>
\t\t</div>
\t</section>
\t<section class=\"sec sec_consultation_list\">
\t\t<h2 class=\"sec_ttl\" id=\"consultation_list\">";
        // line 20
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["big_category_info"]) ? $context["big_category_info"] : null), "name", array()), "html", null, true);
        echo "（全て）の相談一覧</h2>
\t\t<div class=\"sec_inner\">
\t\t\t<ul class=\"consultation_list_wrap\">
\t            ";
        // line 23
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["consultation_list_ar"]) ? $context["consultation_list_ar"] : null));
        foreach ($context['_seq'] as $context["key"] => $context["entry"]) {
            // line 24
            echo "\t\t\t\t<li class=\"consultation_list\">
\t\t\t\t\t<a href=\"";
            // line 25
            echo twig_escape_filter($this->env, (isset($context["base_url"]) ? $context["base_url"] : null), "html", null, true);
            echo "cate";
            echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "big_cate_id", array()), "html", null, true);
            echo "/";
            echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "cate_id", array()), "html", null, true);
            echo "/";
            echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "id", array()), "html", null, true);
            echo "\">
\t\t\t\t\t<div class=\"sec_box_top\">
\t\t\t\t\t\t<p class=\"sec_box_category ic ";
            // line 27
            echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "class", array()), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "big_category_name", array()), "html", null, true);
            echo "</p>
\t\t\t\t\t\t<p class=\"sec_box_category_detail\"><span>";
            // line 28
            echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "category_name", array()), "html", null, true);
            echo "</span></p>
\t\t\t\t\t</div>
\t\t\t\t\t<h3 class=\"sec_box_ttl\">";
            // line 30
            echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "title", array()), "html", null, true);
            echo "</h3>
\t\t\t\t\t<p class=\"sec_box_content\">";
            // line 31
            echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "message", array()), "html", null, true);
            echo "</p>
\t\t\t\t\t<div class=\"sec_box_bottom\">
\t\t\t\t\t\t<p class=\"posted_data\">";
            // line 33
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["entry"], "create_date", array()), "Y.m.d"), "html", null, true);
            echo "</p>
\t\t\t\t\t\t<ul class=\"posted_ability\">
\t\t\t\t\t\t\t";
            // line 35
            if (($this->getAttribute($context["entry"], "like_count", array()) > 0)) {
                // line 36
                echo "\t\t\t\t\t\t\t<li><p class=\"fa fa-heart\">";
                echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "like_count", array()), "html", null, true);
                echo "</p></li>
\t\t\t\t\t\t\t";
            } else {
                // line 38
                echo "\t\t\t\t\t\t\t<li><p class=\"fa fa-heart-o\">";
                echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "like_count", array()), "html", null, true);
                echo "</p></li>
\t\t\t\t\t\t\t";
            }
            // line 40
            echo "\t\t\t\t\t\t\t";
            if (($this->getAttribute($context["entry"], "comment_count", array()) > 0)) {
                // line 41
                echo "\t\t\t\t\t\t\t<li><p class=\"fa fa-comment\">";
                echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "comment_count", array()), "html", null, true);
                echo "</p></li>
\t\t\t\t\t\t\t";
            } else {
                // line 43
                echo "\t\t\t\t\t\t\t<li><p class=\"fa fa-comment-o\">";
                echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "comment_count", array()), "html", null, true);
                echo "</p></li>
\t\t\t\t\t\t\t";
            }
            // line 45
            echo "\t\t\t\t\t\t</ul>
\t\t\t\t\t</div>
\t\t\t\t\t</a>
\t\t\t\t</li>
\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['key'], $context['entry'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 50
        echo "
";
        // line 73
        echo "
\t\t\t</ul>
\t\t\t<ul class=\"pager_wrap\">
\t\t\t\t";
        // line 76
        echo (isset($context["pagination"]) ? $context["pagination"] : null);
        echo "
\t\t\t\t</ul>
\t\t</div>
\t</section>
\t<section class=\"sec sec_category_link\">
\t\t<h2 class=\"sec_ttl\">カテゴリのタグ一覧</h2>
\t\t<div class=\"sec_inner\">
\t\t\t<ul>
\t            ";
        // line 84
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["category_info_ar"]) ? $context["category_info_ar"] : null));
        foreach ($context['_seq'] as $context["key"] => $context["entry"]) {
            // line 85
            echo "\t\t\t\t<li> <a class=\"category_detail_list \" href=\"";
            echo twig_escape_filter($this->env, (isset($context["base_url"]) ? $context["base_url"] : null), "html", null, true);
            echo "cate";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["big_category_info"]) ? $context["big_category_info"] : null), "id", array()), "html", null, true);
            echo "/";
            echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "id", array()), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "name", array()), "html", null, true);
            echo "</a> </li>
\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['key'], $context['entry'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 87
        echo "<!--
\t\t\t\t<li> <a class=\"category_detail_list \" href=\"./\">脅迫･強要</a> </li>
\t\t\t\t<li> <a class=\"category_detail_list \" href=\"./\">本番強要</a> </li>
\t\t\t\t<li> <a class=\"category_detail_list \" href=\"./\">売春行為</a> </li>
\t\t\t\t<li> <a class=\"category_detail_list \" href=\"./\">盗聴･盗撮</a> </li>
\t\t\t\t<li> <a class=\"category_detail_list \" href=\"./\">お客さんから借金</a> </li>
\t\t\t\t<li> <a class=\"category_detail_list \" href=\"./\">嫌がらせ</a> </li>
\t\t\t\t<li> <a class=\"category_detail_list \" href=\"./\">雇用問題</a> </li>
\t\t\t\t<li> <a class=\"category_detail_list \" href=\"./\">給料未払い</a> </li>
\t\t\t\t<li> <a class=\"category_detail_list \" href=\"./\">恋愛</a> </li>
\t\t\t\t<li> <a class=\"category_detail_list \" href=\"./\">いじめ</a> </li>
\t\t\t\t<li> <a class=\"category_detail_list \" href=\"./\">確定申告</a> </li>
\t\t\t\t<li> <a class=\"category_detail_list \" href=\"./\">マイナンバー制度</a> </li>
\t\t\t\t<li> <a class=\"category_detail_list \" href=\"./\">その他</a> </li>
-->
\t\t\t</ul>
\t\t</div>
\t</section>
\t";
        // line 105
        echo twig_include($this->env, $context, "./inc/footer.html");
        echo "
</div>
";
        // line 107
        echo twig_include($this->env, $context, "./inc/script.html");
        echo "
</body>
</html>
";
    }

    public function getTemplateName()
    {
        return "search/category.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  209 => 107,  204 => 105,  184 => 87,  169 => 85,  165 => 84,  154 => 76,  149 => 73,  146 => 50,  136 => 45,  130 => 43,  124 => 41,  121 => 40,  115 => 38,  109 => 36,  107 => 35,  102 => 33,  97 => 31,  93 => 30,  88 => 28,  82 => 27,  71 => 25,  68 => 24,  64 => 23,  58 => 20,  51 => 16,  46 => 14,  41 => 12,  34 => 8,  30 => 7,  26 => 6,  19 => 1,);
    }
}
