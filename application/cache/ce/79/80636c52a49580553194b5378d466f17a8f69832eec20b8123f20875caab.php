<?php

/* ./inc/detail.html */
class __TwigTemplate_ce7980636c52a49580553194b5378d466f17a8f69832eec20b8123f20875caab extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<link rel=\"stylesheet\" href=\"";
        echo twig_escape_filter($this->env, (isset($context["base_url"]) ? $context["base_url"] : null), "html", null, true);
        echo "pc/css/hs.css\">
<script src=\"";
        // line 2
        echo twig_escape_filter($this->env, (isset($context["base_url"]) ? $context["base_url"] : null), "html", null, true);
        echo "pc/js/jquery.cookie.js\"></script>
<script src=\"";
        // line 3
        echo twig_escape_filter($this->env, (isset($context["base_url"]) ? $context["base_url"] : null), "html", null, true);
        echo "pc/js/hs.js\"></script>";
    }

    public function getTemplateName()
    {
        return "./inc/detail.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  28 => 3,  24 => 2,  19 => 1,);
    }
}
