<?php

/* ./inc/footer.html */
class __TwigTemplate_74e7b8855457b772a40d5bef1485ab216e09bdbbbfbdd6bfef6403478af09d52 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<footer>
\t<div class=\"footer_inner cf\">
\t\t<div class=\"left_box\">
\t\t\t<figure><a href=\"";
        // line 4
        echo twig_escape_filter($this->env, (isset($context["base_url"]) ? $context["base_url"] : null), "html", null, true);
        echo "\"><img src=\"";
        echo twig_escape_filter($this->env, (isset($context["base_url"]) ? $context["base_url"] : null), "html", null, true);
        echo "pc/image/common/footer_logo.png\" alt=\"風俗求人・高収入アルバイト情報ジョイスペ\"></a></figure>
\t\t\t<ul class=\"btn_wrap\">
\t\t\t\t";
        // line 6
        if ((isset($context["user_login_flag"]) ? $context["user_login_flag"] : null)) {
            // line 7
            echo "\t\t\t\t<li><a class=\"logout\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["base_url"]) ? $context["base_url"] : null), "html", null, true);
            echo "user/logout\">ログアウト</a></li>
\t\t\t\t";
        } else {
            // line 9
            echo "\t\t\t\t<li><a href=\"javascript:void(0);\" class=\"onayami_login btn_style btn_green\">ログイン</a></li>
\t\t\t\t<li><a href=\"javascript:void(0);\" class=\"onayami_signup btn_style btn_orange\">新規会員登録</a></li>
\t\t\t\t";
        }
        // line 12
        echo "\t\t\t</ul>
\t\t</div>
\t\t<div class=\"right_box\">
\t\t\t<dl class=\"nav_onayami\">
\t\t\t\t<dt><a href=\"";
        // line 16
        echo twig_escape_filter($this->env, (isset($context["base_url"]) ? $context["base_url"] : null), "html", null, true);
        echo "\">女性のお悩み相談室</a></dt>
\t\t\t\t<dd>
\t\t\t\t\t<ul>
\t\t\t\t\t\t<li><a href=\"";
        // line 19
        echo twig_escape_filter($this->env, (isset($context["base_url"]) ? $context["base_url"] : null), "html", null, true);
        echo "\">サイトトップ</a></li>
\t\t\t\t\t\t";
        // line 20
        if ((isset($context["user_login_flag"]) ? $context["user_login_flag"] : null)) {
            // line 21
            echo "\t\t\t\t\t\t<li><a href=\"";
            echo twig_escape_filter($this->env, (isset($context["base_url"]) ? $context["base_url"] : null), "html", null, true);
            echo "user/my_page\">マイページ</a></li>
\t\t\t\t\t\t<li><a href=\"";
            // line 22
            echo twig_escape_filter($this->env, (isset($context["base_url"]) ? $context["base_url"] : null), "html", null, true);
            echo "bb/posting/\">無料で相談</a></li>
\t\t\t\t\t\t";
        }
        // line 24
        echo "\t\t\t\t\t</ul>
\t\t\t\t</dd>

\t\t\t\t<dt><a href=\"./\">カテゴリー一覧</a></dt>
\t\t\t\t<dd>
\t\t\t\t\t<ul>
\t\t\t\t\t\t<li><a href=\"";
        // line 30
        echo twig_escape_filter($this->env, (isset($context["base_url"]) ? $context["base_url"] : null), "html", null, true);
        echo "cate1\">性（セックス）</a></li>
\t\t\t\t\t\t<li><a href=\"";
        // line 31
        echo twig_escape_filter($this->env, (isset($context["base_url"]) ? $context["base_url"] : null), "html", null, true);
        echo "cate2\">恋愛と結婚</a></li>
\t\t\t\t\t\t<li><a href=\"";
        // line 32
        echo twig_escape_filter($this->env, (isset($context["base_url"]) ? $context["base_url"] : null), "html", null, true);
        echo "cate3\">カラダ</a></li>
\t\t\t\t\t\t<li><a href=\"";
        // line 33
        echo twig_escape_filter($this->env, (isset($context["base_url"]) ? $context["base_url"] : null), "html", null, true);
        echo "cate4\">生理</a></li>
\t\t\t\t\t\t<li><a href=\"";
        // line 34
        echo twig_escape_filter($this->env, (isset($context["base_url"]) ? $context["base_url"] : null), "html", null, true);
        echo "cate5\">バスト（おっぱい）</a></li>
\t\t\t\t\t\t<li><a href=\"";
        // line 35
        echo twig_escape_filter($this->env, (isset($context["base_url"]) ? $context["base_url"] : null), "html", null, true);
        echo "cate6\">性交痛</a></li>
\t\t\t\t\t\t<li><a href=\"";
        // line 36
        echo twig_escape_filter($this->env, (isset($context["base_url"]) ? $context["base_url"] : null), "html", null, true);
        echo "cate7\">妊娠・出産・産後</a></li>
\t\t\t\t\t\t<li><a href=\"";
        // line 37
        echo twig_escape_filter($this->env, (isset($context["base_url"]) ? $context["base_url"] : null), "html", null, true);
        echo "cate8\">オナニー</a></li>
\t\t\t\t\t\t<li><a href=\"";
        // line 38
        echo twig_escape_filter($this->env, (isset($context["base_url"]) ? $context["base_url"] : null), "html", null, true);
        echo "cate9\">コンドーム</a></li>
\t\t\t\t\t\t<li><a href=\"";
        // line 39
        echo twig_escape_filter($this->env, (isset($context["base_url"]) ? $context["base_url"] : null), "html", null, true);
        echo "cate10\">男性の性</a></li>
\t\t\t\t\t\t<li><a href=\"";
        // line 40
        echo twig_escape_filter($this->env, (isset($context["base_url"]) ? $context["base_url"] : null), "html", null, true);
        echo "cate11\">シングルマザー</a></li>
\t\t\t\t\t\t<li><a href=\"";
        // line 41
        echo twig_escape_filter($this->env, (isset($context["base_url"]) ? $context["base_url"] : null), "html", null, true);
        echo "cate12\">男性に答えて欲しい</a></li>
\t\t\t\t\t\t<li><a href=\"";
        // line 42
        echo twig_escape_filter($this->env, (isset($context["base_url"]) ? $context["base_url"] : null), "html", null, true);
        echo "cate13\">風俗のお仕事</a></li>
\t\t\t\t\t\t<li><a href=\"";
        // line 43
        echo twig_escape_filter($this->env, (isset($context["base_url"]) ? $context["base_url"] : null), "html", null, true);
        echo "cate14\">チャットレディ</a></li>
\t\t\t\t\t</ul>
\t\t\t\t</dd>
\t\t\t</dl>
\t\t\t<dl class=\"nav_joyspe\">
\t\t\t\t<dt><a href=\"./\">全国の風俗求人情報</a></dt>
\t\t\t\t<dd>
\t\t\t\t\t<ul>
\t\t\t\t\t\t<li><a href=\"";
        // line 51
        echo twig_escape_filter($this->env, (isset($context["main_url"]) ? $context["main_url"] : null), "html", null, true);
        echo "/user/jobs/hokkaido/\">北海道・東北</a></li>
\t\t\t\t\t\t<li><a href=\"";
        // line 52
        echo twig_escape_filter($this->env, (isset($context["main_url"]) ? $context["main_url"] : null), "html", null, true);
        echo "/user/jobs/kanto/\">関東</a></li>
\t\t\t\t\t\t<li><a href=\"";
        // line 53
        echo twig_escape_filter($this->env, (isset($context["main_url"]) ? $context["main_url"] : null), "html", null, true);
        echo "/user/jobs/kitakanto/\">北関東</a></li>
\t\t\t\t\t\t<li><a href=\"";
        // line 54
        echo twig_escape_filter($this->env, (isset($context["main_url"]) ? $context["main_url"] : null), "html", null, true);
        echo "/user/jobs/hokuriku/\">北陸・甲信越</a></li>
\t\t\t\t\t\t<li><a href=\"";
        // line 55
        echo twig_escape_filter($this->env, (isset($context["main_url"]) ? $context["main_url"] : null), "html", null, true);
        echo "/user/jobs/tokai/\">東海</a></li>
\t\t\t\t\t\t<li><a href=\"";
        // line 56
        echo twig_escape_filter($this->env, (isset($context["main_url"]) ? $context["main_url"] : null), "html", null, true);
        echo "/user/jobs/kansai/\">関西</a></li>
\t\t\t\t\t\t<li><a href=\"";
        // line 57
        echo twig_escape_filter($this->env, (isset($context["main_url"]) ? $context["main_url"] : null), "html", null, true);
        echo "/user/jobs/shikoku/\">中国・四国</a></li>
\t\t\t\t\t\t<li><a href=\"";
        // line 58
        echo twig_escape_filter($this->env, (isset($context["main_url"]) ? $context["main_url"] : null), "html", null, true);
        echo "/user/jobs/kyushu/\">九州・沖縄</a></li>
\t\t\t\t\t</ul>
\t\t\t\t</dd>
\t\t\t</dl>
\t\t\t<ul class=\"footer_sitemap\">
\t\t\t\t<li><a href=\"";
        // line 63
        echo twig_escape_filter($this->env, (isset($context["base_url"]) ? $context["base_url"] : null), "html", null, true);
        echo "tos\">利用規約</a></li>
\t\t\t\t<li><a href=\"";
        // line 64
        echo twig_escape_filter($this->env, (isset($context["base_url"]) ? $context["base_url"] : null), "html", null, true);
        echo "company\">会社概要</a></li>
\t\t\t\t<li><a href=\"";
        // line 65
        echo twig_escape_filter($this->env, (isset($context["base_url"]) ? $context["base_url"] : null), "html", null, true);
        echo "privacy\">個人情報の取扱い</a></li>
\t\t\t\t<li><a href=\"";
        // line 66
        echo twig_escape_filter($this->env, (isset($context["base_url"]) ? $context["base_url"] : null), "html", null, true);
        echo "guideline\">禁止事項</a></li>
\t\t\t\t<li><a href=\"";
        // line 67
        echo twig_escape_filter($this->env, (isset($context["base_url"]) ? $context["base_url"] : null), "html", null, true);
        echo "faq\"> よくある質問</a></li>
\t\t\t\t<li><a href=\"";
        // line 68
        echo twig_escape_filter($this->env, (isset($context["base_url"]) ? $context["base_url"] : null), "html", null, true);
        echo "contact\">お問い合わせ</a></li>
\t\t\t</ul>
\t\t</div>
\t</div>
\t<small>Copyright © onayami All Rights Reserved.</small> </footer>
\t<a href=\"#\" class=\"top_link\" id=\"top_link\" data-scroll=\"pageTop\" style=\"display: block;\"><i class=\"fa fa-arrow-up\"></i></a>
";
    }

    public function getTemplateName()
    {
        return "./inc/footer.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  199 => 68,  195 => 67,  191 => 66,  187 => 65,  183 => 64,  179 => 63,  171 => 58,  167 => 57,  163 => 56,  159 => 55,  155 => 54,  151 => 53,  147 => 52,  143 => 51,  132 => 43,  128 => 42,  124 => 41,  120 => 40,  116 => 39,  112 => 38,  108 => 37,  104 => 36,  100 => 35,  96 => 34,  92 => 33,  88 => 32,  84 => 31,  80 => 30,  72 => 24,  67 => 22,  62 => 21,  60 => 20,  56 => 19,  50 => 16,  44 => 12,  39 => 9,  33 => 7,  31 => 6,  24 => 4,  19 => 1,);
    }
}
