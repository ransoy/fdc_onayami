<?php

/* bb/answer.html */
class __TwigTemplate_18e09595e9d009087e4db0f609c74c1016a7871eb673b661e2977a8239301237 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<html lang=\"ja\">
<head>
<meta charset=\"utf-8\">
<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0, user-scalable=no, maximum-scale=1.0\">
<title>お悩み相談</title>
";
        // line 7
        echo twig_include($this->env, $context, "./inc/link.html");
        echo "
</head>
<body>
<div class=\"page_wrap page_answer\">
\t";
        // line 11
        echo twig_include($this->env, $context, "./inc/header.html");
        echo "
\t<section class=\"sec sec_consult_question\">
\t\t<h2 class=\"sec_ttl ic ";
        // line 13
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["consultation_info"]) ? $context["consultation_info"] : null), "class", array()), "html", null, true);
        echo "\"><strong>";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["consultation_info"]) ? $context["consultation_info"] : null), "big_category_name", array()), "html", null, true);
        echo "</strong></h2>
\t\t<div class=\"sec_inner\">
\t\t\t<h3 class=\"content_ttl\">";
        // line 15
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["consultation_info"]) ? $context["consultation_info"] : null), "title", array()), "html", null, true);
        echo "</h3>
\t\t\t<p class=\"sec_desc\">
\t\t\t\t<span class=\"user_name\">相談者：";
        // line 17
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["consultation_info"]) ? $context["consultation_info"] : null), "nick_name", array()), "html", null, true);
        echo "さん</span>
\t\t\t\t<span class=\"post_date\">";
        // line 18
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["consultation_info"]) ? $context["consultation_info"] : null), "time_ago", array()), "html", null, true);
        echo "</span>
\t\t\t</p>
\t\t\t<div class=\"consult_question_content\">
\t\t\t\t";
        // line 21
        if (($this->getAttribute((isset($context["consultation_info"]) ? $context["consultation_info"] : null), "up_image", array()) != null)) {
            // line 22
            echo "\t\t\t\t<p><img src=\"";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["consultation_info"]) ? $context["consultation_info"] : null), "up_image", array()), "html", null, true);
            echo "\"></p>
\t\t\t\t";
        }
        // line 24
        echo "\t\t\t\t<p>";
        echo nl2br($this->getAttribute((isset($context["consultation_info"]) ? $context["consultation_info"] : null), "message", array()));
        echo "</p>
\t\t\t</div>
\t\t</div>
\t</section>
\t<section class=\"sec sec_answer\">
\t\t<form method=\"post\" action=\"";
        // line 29
        echo twig_escape_filter($this->env, (isset($context["base_url"]) ? $context["base_url"] : null), "html", null, true);
        echo "bb/answer_confirm\" class=\"answer_form validate\" novalidate>
\t\t\t<input type=\"hidden\" name=\"nickname\" value=\"";
        // line 30
        echo twig_escape_filter($this->env, (isset($context["nickname"]) ? $context["nickname"] : null), "html", null, true);
        echo "\">
\t\t\t<input type=\"hidden\" name=\"answer_id\" value=\"";
        // line 31
        echo twig_escape_filter($this->env, (isset($context["answer_id"]) ? $context["answer_id"] : null), "html", null, true);
        echo "\">
\t\t\t<h2 class=\"sec_ttl\">回答の入力</h2>
\t\t\t<div class=\"sec_inner\">
\t\t\t\t<div class=\"input_form\">
\t\t\t\t\t<dl class=\"input_nickname\">
\t\t\t\t\t\t<dt class=\"ic_required\">回答者のお名前</dt>
\t\t\t\t\t\t<dd>
\t\t\t\t\t\t\t<p>";
        // line 38
        echo twig_escape_filter($this->env, (isset($context["nickname"]) ? $context["nickname"] : null), "html", null, true);
        echo "さん</p>
\t\t\t\t\t\t\t<p class=\"t_right\"><a href=\"";
        // line 39
        echo twig_escape_filter($this->env, (isset($context["main_url"]) ? $context["main_url"] : null), "html", null, true);
        echo "user/settings\" class=\"t_link\">ニックネームの変更</a></p>
\t\t\t\t\t\t</dd>
\t\t\t\t\t</dl>
\t\t\t\t\t<dl class=\"input_content\">
\t\t\t\t\t\t<dt class=\"ic_required\">回答内容</dt>
\t\t\t\t\t\t<dd class=\"cf ui-count_form\">
\t\t\t\t\t\t\t";
        // line 46
        echo "\t\t\t\t\t\t\t";
        echo form_textarea("answer_message", set_value("answer_message"), array("placeholder" => "回答内容を入力してください", "maxlength" => 1000, "class" => "ui-count_input"));
        echo "
\t\t\t\t\t\t\t<p class=\"input_desc\">※全角30文字～1000文字</p>
\t\t\t\t\t\t\t<p class=\"input_count\">1000</p>
\t\t\t\t\t\t</dd>
\t\t\t\t\t</dl>
\t\t\t\t\t<dl>
\t\t\t\t\t\t<dt>画像アップロード</dt>
\t\t\t\t\t\t<dd>
\t\t\t\t\t\t\t<input id=\"upload_img\" type=\"file\" value=\"\" name=\"userfile\"></p>
\t\t\t\t\t\t\t<input id=\"image_file_name\" type=\"hidden\" value=\"\" name=\"image_file_name\">
\t\t\t\t\t\t</dd>
\t\t\t\t\t</dl>
\t\t\t\t\t<ul class=\"btn_wrap\">
\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t<button type=\"submit\" class=\"btn_style btn_green\">入力内容の確認へ</button>
\t\t\t\t\t\t</li>
\t\t\t\t\t</ul>
\t\t\t\t</div>
\t\t\t</div>
\t\t</form>
\t</section>
\t";
        // line 67
        echo twig_include($this->env, $context, "./inc/footer.html");
        echo "
</div>
";
        // line 69
        echo twig_include($this->env, $context, "./inc/script.html");
        echo "
<script>
\$(function(){\t
\t\$('textarea').blur(function(){
\t\t\$(this).val(\$(this).val().trim());
\t});
});
</script>
</body>
</html>
";
    }

    public function getTemplateName()
    {
        return "bb/answer.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  139 => 69,  134 => 67,  109 => 46,  100 => 39,  96 => 38,  86 => 31,  82 => 30,  78 => 29,  69 => 24,  63 => 22,  61 => 21,  55 => 18,  51 => 17,  46 => 15,  39 => 13,  34 => 11,  27 => 7,  19 => 1,);
    }
}
