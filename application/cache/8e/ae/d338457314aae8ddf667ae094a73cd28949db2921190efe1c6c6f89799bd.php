<?php

/* bb/answer_conf.html */
class __TwigTemplate_8eaed338457314aae8ddf667ae094a73cd28949db2921190efe1c6c6f89799bd extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!doctype html>
<html>
<head>
<meta charset=\"utf-8\">
<title>回答内容の確認</title>
";
        // line 6
        echo twig_include($this->env, $context, "./inc/link.html");
        echo "
</head>

<body>
<div class=\"page_wrap page_answer_conf\">
\t";
        // line 11
        echo twig_include($this->env, $context, "./inc/header.html");
        echo "
\t<div class=\"content_wrap cf\">
\t\t<div class=\"content\">
\t\t\t<section class=\"sec sec_answer_conf\">
\t\t\t\t<div class=\"content_inner\">
\t\t\t\t<!--\t<form action=\"./answer_comp.html\" method=\"post\" class=\"answer_conf_form\">  -->
\t\t\t\t\t\t<h2 class=\"sec_ttl\">相談内容の確認</h2>
\t\t\t\t\t\t<div class=\"input_form\">
\t\t\t\t\t\t\t<dl class=\"input_nickname\">
\t\t\t\t\t\t\t\t<dt>ニックネーム</dt>
\t\t\t\t\t\t\t\t<dd>
\t\t\t\t\t\t\t\t\t<p>";
        // line 22
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["data"]) ? $context["data"] : null), "nickname", array()), "html", null, true);
        echo "さん</p>
\t\t\t\t\t\t\t\t</dd>
\t\t\t\t\t\t\t</dl>
\t\t\t\t\t\t\t<form id=\"answer\" name=\"answer\" method=\"post\" action=\"\">
\t\t\t\t\t\t\t<script>
\t\t\t\t\t\t\t\tvar img_select_array = [];
\t\t\t\t\t\t\t</script>\t
\t\t\t\t\t\t\t";
        // line 29
        if (($this->getAttribute((isset($context["data"]) ? $context["data"] : null), "userfile", array()) != "")) {
            // line 30
            echo "\t\t\t\t\t\t\t<dl>
\t\t\t\t\t\t\t\t<dt>画像</dt>
\t\t\t\t\t\t\t\t<dd>
\t\t\t\t\t\t\t\t\t<p><img src=\"/uploads/temp/";
            // line 33
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["data"]) ? $context["data"] : null), "userfile", array()), "html", null, true);
            echo "\"></p>
\t\t\t\t\t\t\t\t</dd>
\t\t\t\t\t\t\t</dl>
\t\t\t\t\t\t\t";
        }
        // line 37
        echo "\t\t\t\t\t\t\t<dl class=\"input_content\">
\t\t\t\t\t\t\t\t<dt>回答内容</dt>
\t\t\t\t\t\t\t\t<dd>
\t\t\t\t\t\t\t\t\t<p>";
        // line 40
        echo $this->getAttribute((isset($context["data"]) ? $context["data"] : null), "message_next", array());
        echo "</p>
\t\t\t\t\t\t\t\t</dd>
\t\t\t\t\t\t\t</dl>
\t\t\t\t\t\t\t";
        // line 43
        echo (isset($context["form_hidden"]) ? $context["form_hidden"] : null);
        echo "
\t\t\t\t\t\t\t</form>
\t\t\t\t\t\t\t<ul class=\"btn_wrap\">
\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t<!-- <a href=\"#\" onclick=\"javascript:window.history.back(-1);return false;\" class=\"btn_style btn_gray\">入力画面へ戻る</a>  -->
\t\t\t\t\t\t\t\t\t<a href=\"#\" id=\"back\" class=\"btn_style btn_gray-o\">戻る</a>
\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t<button id=\"goto\" type=\"submit\" class=\"btn_style btn_green\">回答する</button>
\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t</div>
\t\t\t\t<!--\t</form>  -->
\t\t\t\t</div>
\t\t\t</section>
\t\t</div>
\t\t";
        // line 59
        echo twig_include($this->env, $context, "./inc/sidebar.html");
        echo "
\t</div>
\t";
        // line 61
        echo twig_include($this->env, $context, "./inc/footer.html");
        echo "
</div>
";
        // line 63
        echo twig_include($this->env, $context, "./inc/script.html");
        echo "
<script>
\$(function(){
\tvar click = true;
    \$('#back').click(function() {
        \$('#answer').attr('action', '";
        // line 68
        echo twig_escape_filter($this->env, (isset($context["base_url"]) ? $context["base_url"] : null), "html", null, true);
        echo "bb/answer');
        \$('#answer').submit();
    });
    \$('#goto').click(function(e) {
        \$('#answer').attr('action', '";
        // line 72
        echo twig_escape_filter($this->env, (isset($context["base_url"]) ? $context["base_url"] : null), "html", null, true);
        echo "bb/answer_completion');
        if (click) {
        \t\$('#answer').submit();
        \tclick = false;
        }
        e.preventDefault();
    });
});
</script>
</body>
</html>";
    }

    public function getTemplateName()
    {
        return "bb/answer_conf.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  127 => 72,  120 => 68,  112 => 63,  107 => 61,  102 => 59,  83 => 43,  77 => 40,  72 => 37,  65 => 33,  60 => 30,  58 => 29,  48 => 22,  34 => 11,  26 => 6,  19 => 1,);
    }
}
