<?php

/* bb/question.html */
class __TwigTemplate_40ab163747c3d99ec6f4f77c6a0e6f69e63a07e1fb957d9adbbefdc967fe1a3e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!doctype html>
<html>
<head>
<meta charset=\"utf-8\">
<title>相談内容の入力</title>
";
        // line 6
        echo twig_include($this->env, $context, "./inc/link.html");
        echo "
</head>

<body>
<div class=\"page_wrap page_question\">
\t";
        // line 11
        echo twig_include($this->env, $context, "./inc/header.html");
        echo "
\t<div class=\"content_wrap cf\">
\t\t<div class=\"content_inner\">
\t\t\t<section class=\"sec sec_question\">
\t\t\t\t<form method=\"post\" action=\"";
        // line 15
        echo twig_escape_filter($this->env, (isset($context["base_url"]) ? $context["base_url"] : null), "html", null, true);
        echo "bb/confirm\" class=\"question_form validate\">
\t\t\t\t\t<input type=\"hidden\" name=\"nickname\" value=\"";
        // line 16
        echo twig_escape_filter($this->env, (isset($context["nickname"]) ? $context["nickname"] : null), "html", null, true);
        echo "\">
\t\t\t\t\t<h2 class=\"sec_ttl\">相談内容の入力</h2>
\t\t\t\t\t<div class=\"input_form\">
\t\t\t\t\t\t<dl class=\"input_nickname\">
\t\t\t\t\t\t\t<dt class=\"ic_required\">ニックネーム</dt>
\t\t\t\t\t\t\t<dd><p>";
        // line 21
        echo twig_escape_filter($this->env, (isset($context["nickname"]) ? $context["nickname"] : null), "html", null, true);
        echo "さん</p>
\t\t\t\t\t\t\t<p class=\"t_right\"><a href=\"";
        // line 22
        echo twig_escape_filter($this->env, (isset($context["main_url"]) ? $context["main_url"] : null), "html", null, true);
        echo "user/settings\" class=\"t_link\">ニックネームの変更</a></p></dd>
\t\t\t\t\t\t</dl>
\t\t\t\t\t\t<dl class=\"input_category_list\">
\t\t\t\t\t\t\t<dt class=\"ic_required\">相談するカテゴリを選択</dt>
\t\t\t\t\t\t\t<dd>
\t\t\t\t\t\t\t\t<select name=\"question_category_list\" required=\"\" aria-required=\"true\" id=\"changeselect\" >
\t\t\t\t\t\t\t\t\t<option value=\"\">選択してください</option>
\t\t\t\t\t\t\t\t\t";
        // line 29
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["form_cate_opt"]) ? $context["form_cate_opt"] : null));
        foreach ($context['_seq'] as $context["key"] => $context["bcat"]) {
            // line 30
            echo "\t\t\t\t\t\t\t\t\t\t";
            if (((isset($context["select_big"]) ? $context["select_big"] : null) == $this->getAttribute($context["bcat"], "id", array()))) {
                // line 31
                echo "\t\t\t\t\t\t\t\t\t\t\t<option value=\"";
                echo twig_escape_filter($this->env, $this->getAttribute($context["bcat"], "id", array()), "html", null, true);
                echo "\" selected=\"selected\">";
                echo twig_escape_filter($this->env, $this->getAttribute($context["bcat"], "name", array()), "html", null, true);
                echo "</option>
\t\t\t\t\t\t\t\t\t\t";
            } else {
                // line 33
                echo "\t\t\t\t\t\t\t\t\t\t<option value=\"";
                echo twig_escape_filter($this->env, $this->getAttribute($context["bcat"], "id", array()), "html", null, true);
                echo "\" ";
                if (($this->getAttribute($context["bcat"], "id", array()) == $this->getAttribute((isset($context["values"]) ? $context["values"] : null), "big_cate_id", array()))) {
                    echo " selected=\"selected\" ";
                }
                echo " >";
                echo twig_escape_filter($this->env, $this->getAttribute($context["bcat"], "name", array()), "html", null, true);
                echo "</option>
\t\t\t\t\t\t\t\t\t\t";
            }
            // line 35
            echo "\t\t\t\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['key'], $context['bcat'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 36
        echo "\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t</dd>
\t\t\t\t\t\t</dl>
\t\t\t\t\t\t<dl class=\"input_category\">
\t\t\t\t\t\t\t<dt class=\"ic_required\">カテゴリー詳細を選択</dt>
\t\t\t\t\t\t\t<dd>
\t\t\t\t\t\t\t\t<select name=\"question_category\" id=\"sub_question_cat\" required=\"\" aria-required=\"true\">
\t\t\t\t\t\t\t\t\t<option value=\"\">選択してください。</option>
\t\t\t\t\t\t\t\t\t\t";
        // line 44
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["get_sub_cat"]) ? $context["get_sub_cat"] : null));
        foreach ($context['_seq'] as $context["key"] => $context["scat"]) {
            // line 45
            echo "\t\t\t\t\t\t\t\t\t\t\t";
            if (((isset($context["sm_id"]) ? $context["sm_id"] : null) == $this->getAttribute($context["scat"], "id", array()))) {
                // line 46
                echo "\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"";
                echo twig_escape_filter($this->env, $this->getAttribute($context["scat"], "id", array()), "html", null, true);
                echo "\" selected=\"selected\">";
                echo twig_escape_filter($this->env, $this->getAttribute($context["scat"], "name", array()), "html", null, true);
                echo "</option>
\t\t\t\t\t\t\t\t\t\t\t";
            } else {
                // line 48
                echo "\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"";
                echo twig_escape_filter($this->env, $this->getAttribute($context["scat"], "id", array()), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, $this->getAttribute($context["scat"], "name", array()), "html", null, true);
                echo "</option>
\t\t\t\t\t\t\t\t\t\t\t";
            }
            // line 50
            echo "\t\t\t\t\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['key'], $context['scat'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 51
        echo "\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t</dd>
\t\t\t\t\t\t</dl>
\t\t\t\t\t\t<dl class=\"input_ttl\">
\t\t\t\t\t\t\t<dt class=\"ic_required\">タイトル</dt>
\t\t\t\t\t\t\t<dd class=\"cf ui-count_form\">
\t\t\t\t\t\t\t\t";
        // line 58
        echo "\t\t\t\t\t\t\t\t";
        echo form_input("question_subject", set_value("question_subject", "", false), array("placeholder" => "タイトルを入力してください", "class" => "ui-count_input", "maxlength" => 50));
        echo "
\t\t\t\t\t\t\t\t<p class=\"input_desc\">※全角10文字～50文字</p>
\t\t\t\t\t\t\t\t<p class=\"input_count\">50</p>
\t\t\t\t\t\t\t</dd>
\t\t\t\t\t\t</dl>
\t\t\t\t\t\t<dl class=\"input_content\">
\t\t\t\t\t\t\t<dt class=\"ic_required\">相談内容</dt>
\t\t\t\t\t\t\t<dd class=\"cf ui-count_form\">
\t\t\t\t\t\t\t\t";
        // line 67
        echo "\t\t\t\t\t\t\t\t";
        echo form_textarea("question_message", set_value("question_message", "", false), array("placeholder" => "相談内容を入力してください", "class" => "ui-count_input", "maxlength" => 1000));
        echo "
\t\t\t\t\t\t\t\t<p class=\"input_desc\">※全角30文字～1000文字</p>
\t\t\t\t\t\t\t\t<p class=\"input_count\">1000</p>
\t\t\t\t\t\t\t</dd>
\t\t\t\t\t\t</dl>
\t\t\t\t\t\t<dl class=\"input_image\">
\t\t\t\t\t\t\t<dt class=\"\">画像</dt>
\t\t\t\t\t\t\t<dd class=\"cf ui-count_form\">
\t\t\t\t\t\t\t\t<input id=\"upload_img\" type=\"file\" value=\"\" name=\"userfile\">
\t\t\t\t\t\t\t\t<input id=\"image_file_name\" type=\"hidden\" value=\"\" name=\"image_file_name\">
\t\t\t\t\t\t\t\t<p class=\"input_desc\">※3Mbyte以内</p>
\t\t\t\t\t\t\t</dd>
\t\t\t\t\t\t</dl>
\t\t\t\t\t\t<ul class=\"btn_wrap\">
\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t<button type=\"submit\" class=\"btn_style btn_green\">送信する</button>
\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t</ul>
\t\t\t\t\t</div>
\t\t\t\t</form>
\t\t\t</section>
\t\t</div>
\t</div>
\t";
        // line 90
        echo twig_include($this->env, $context, "./inc/footer.html");
        echo "
</div>
";
        // line 92
        echo twig_include($this->env, $context, "./inc/script.html");
        echo "
<script>
\$(function(){
\tvar sm_id = '";
        // line 95
        echo twig_escape_filter($this->env, (isset($context["sm_id"]) ? $context["sm_id"] : null), "html", null, true);
        echo "';
\tif (!sm_id){
\t\tif (\$('#changeselect').val()) {
\t\t\tajaxCat(\$('#changeselect').val());
\t\t}
\t}
\t\$('#changeselect').change(function(){
\t\tajaxCat(\$(this).val());
\t});                                       
\t\$('#sub_question_cat').change(function(){
\t\t\$.cookie(\"sub_cat\", \$(this).val());
\t});
\t\$('input[type=text], textarea').blur(function(){
\t\t\$(this).val(\$(this).val().trim());
\t});
});
function ajaxCat(id) {
\t\$.ajax({
\t\ttype: \"POST\",
\t\turl: '/bb/sub_category_ajax',
\t\tdata: {bid:id},
\t\tdataType: 'JSON',
\t\tcache: false,
\t\tsuccess:
\t\t\tfunction(data){
\t\t\t\t\tvar opt = '<option value>選択してください。</option>';
\t\t\t\t\t\$.each(data, function(i, category){
\t\t\t\t\t\t\tselctd = (typeof \$.cookie(\"sub_cat\") !== \"undefined\" && \$.cookie(\"sub_cat\") == category['id'])? 'selected=selected':'';
\t\t\t\t\t\t\topt += '<option value=\"'+category['id']+'\"'+selctd+'>'+category['name']+'</option>';
\t\t\t\t\t});
\t\t\t\t\t\$(\"#sub_question_cat\").html(opt);
\t\t\t\t}
\t});
}
</script>
</body>
</html>
";
    }

    public function getTemplateName()
    {
        return "bb/question.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  197 => 95,  191 => 92,  186 => 90,  159 => 67,  147 => 58,  139 => 51,  133 => 50,  125 => 48,  117 => 46,  114 => 45,  110 => 44,  100 => 36,  94 => 35,  82 => 33,  74 => 31,  71 => 30,  67 => 29,  57 => 22,  53 => 21,  45 => 16,  41 => 15,  34 => 11,  26 => 6,  19 => 1,);
    }
}
