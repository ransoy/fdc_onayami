<?php

/* bb/answer_comp.html */
class __TwigTemplate_61c428d837a5daceebf9c1d4a962650dc64c1c166859619a48276f72c478921e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<html lang=\"ja\">
<head>
<meta charset=\"utf-8\">
<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0, user-scalable=no, maximum-scale=1.0\">
<title>お悩み相談</title>
";
        // line 7
        echo twig_include($this->env, $context, "./inc/link.html");
        echo "
</head>
<body>
<div class=\"page_wrap page_answer_comp\">
\t";
        // line 11
        echo twig_include($this->env, $context, "./inc/header.html");
        echo "
\t<section class=\"sec sec_answer_comp\">
\t\t<h2 class=\"sec_ttl\">回答完了</h2>
\t\t<div class=\"sec_inner\">
\t\t\t<h3 class=\"content_ttl\">投稿が完了しました</h3>
\t\t\t<p class=\"t_left\">回答した内容はマイページで履歴を確認することが可能です。</p>
\t\t\t<ul class=\"btn_wrap\">
\t\t\t\t<li><a href=\"";
        // line 18
        echo twig_escape_filter($this->env, (isset($context["base_url"]) ? $context["base_url"] : null), "html", null, true);
        echo "\" class=\"btn_style btn_gray-o\">トップページへ</a></li>
\t\t\t\t<li><a href=\"";
        // line 19
        echo twig_escape_filter($this->env, (isset($context["base_url"]) ? $context["base_url"] : null), "html", null, true);
        echo "cate";
        echo twig_escape_filter($this->env, (isset($context["big_category_id"]) ? $context["big_category_id"] : null), "html", null, true);
        echo "/";
        echo twig_escape_filter($this->env, (isset($context["category_id"]) ? $context["category_id"] : null), "html", null, true);
        echo "/";
        echo twig_escape_filter($this->env, (isset($context["answer_id"]) ? $context["answer_id"] : null), "html", null, true);
        echo "\" class=\"btn_style btn_green\">回答したページへ</a></li>
\t\t\t</ul>
\t\t</div>
\t</section>
\t";
        // line 23
        echo twig_include($this->env, $context, "./inc/footer.html");
        echo "
</div>
";
        // line 25
        echo twig_include($this->env, $context, "./inc/script.html");
        echo "
</body>
</html>
";
    }

    public function getTemplateName()
    {
        return "bb/answer_comp.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  66 => 25,  61 => 23,  48 => 19,  44 => 18,  34 => 11,  27 => 7,  19 => 1,);
    }
}
