<?php

/* bb/question_comp.html */
class __TwigTemplate_c07e6d462f8e052d8f44ad4e22dcd2fc1bf402f90d672c20704dda66def3558e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<html lang=\"ja\">
<head>
<meta charset=\"utf-8\">
<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0, user-scalable=no, maximum-scale=1.0\">
<title>お悩み相談</title>
";
        // line 7
        echo twig_include($this->env, $context, "./inc/link.html");
        echo "
</head>
<body>
<div class=\"page_wrap page_question_comp\">
\t";
        // line 11
        echo twig_include($this->env, $context, "./inc/header.html");
        echo "
\t<section class=\"sec sec_question_comp\">
\t\t<h2 class=\"sec_ttl\">相談内容の投稿完了</h2>
\t\t<div class=\"sec_inner\">
\t\t\t<div class=\"caution\">
\t\t\t\t<h3 class=\"content_ttl\">投稿が完了しました</h3>
\t\t\t\t<p class=\"t_left\">全ての回答に返信がつくとは限りませんので</p>
\t\t\t\t<p class=\"t_left\">ご理解いただきますようお願い申し上げます。</p>
\t\t\t</div>
\t\t\t<ul class=\"btn_wrap\">
\t\t\t\t<li><a href=\"";
        // line 21
        echo twig_escape_filter($this->env, (isset($context["base_url"]) ? $context["base_url"] : null), "html", null, true);
        echo "user/my_page\" class=\"btn_style btn_gray-o\">マイページへ</a></li>
\t\t\t\t<li><a href=\"";
        // line 22
        echo twig_escape_filter($this->env, (isset($context["base_url"]) ? $context["base_url"] : null), "html", null, true);
        echo "cate";
        echo twig_escape_filter($this->env, (isset($context["big_category_id"]) ? $context["big_category_id"] : null), "html", null, true);
        echo "/";
        echo twig_escape_filter($this->env, (isset($context["category_id"]) ? $context["category_id"] : null), "html", null, true);
        echo "/";
        echo twig_escape_filter($this->env, (isset($context["question_id"]) ? $context["question_id"] : null), "html", null, true);
        echo "\" class=\"btn_style btn_green\">相談したページへ</a></li>
\t\t\t</ul>
\t\t</div>
\t</section>
\t";
        // line 26
        echo twig_include($this->env, $context, "./inc/footer.html");
        echo "
</div>
";
        // line 28
        echo twig_include($this->env, $context, "./inc/script.html");
        echo "
</body>
</html>
";
    }

    public function getTemplateName()
    {
        return "bb/question_comp.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  69 => 28,  64 => 26,  51 => 22,  47 => 21,  34 => 11,  27 => 7,  19 => 1,);
    }
}
