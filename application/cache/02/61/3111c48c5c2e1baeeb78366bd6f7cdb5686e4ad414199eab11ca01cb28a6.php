<?php

/* bb/question_comp.html */
class __TwigTemplate_02613111c48c5c2e1baeeb78366bd6f7cdb5686e4ad414199eab11ca01cb28a6 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!doctype html>
<html>
<head>
<meta charset=\"utf-8\">
<title>投稿完了</title>
";
        // line 6
        echo twig_include($this->env, $context, "./inc/link.html");
        echo "
</head>
<body>
<div class=\"page_wrap page_question_comp\">
";
        // line 10
        echo twig_include($this->env, $context, "./inc/header.html");
        echo "

\t<div class=\"content_wrap cf\">
\t\t<div class=\"content_inner\">
\t\t<section class=\"sec sec_question_comp\">
\t\t\t<h2 class=\"sec_ttl\">相談内容の投稿完了</h2>
\t\t\t<div class=\"input_form\">
\t\t\t\t<div class=\"caution\">
\t\t\t\t\t<h3 class=\"content_ttl\">投稿が完了しました</h3>
\t\t\t\t\t<p class=\"sec_desc\">全ての回答に返信がつくとは限りませんので<br>ご理解いただきますようお願い申し上げます。</p>
\t\t\t\t</div>
\t\t\t\t<ul class=\"btn_wrap t_center m_b_20\">
\t\t\t\t\t<li>
\t\t\t\t\t\t<a href=\"";
        // line 23
        echo twig_escape_filter($this->env, (isset($context["base_url"]) ? $context["base_url"] : null), "html", null, true);
        echo "user/my_page\" class=\"btn_style btn_gray-o\">マイページへ</a></li>
\t\t\t\t\t<li>
\t\t\t\t\t\t<a href=\"";
        // line 25
        echo twig_escape_filter($this->env, (isset($context["base_url"]) ? $context["base_url"] : null), "html", null, true);
        echo "cate";
        echo twig_escape_filter($this->env, (isset($context["big_category_id"]) ? $context["big_category_id"] : null), "html", null, true);
        echo "/";
        echo twig_escape_filter($this->env, (isset($context["category_id"]) ? $context["category_id"] : null), "html", null, true);
        echo "/";
        echo twig_escape_filter($this->env, (isset($context["question_id"]) ? $context["question_id"] : null), "html", null, true);
        echo "\" class=\"btn_style btn_green\">相談したページへ</a></li>
\t\t\t\t</ul>
\t\t\t</div>
\t\t</section>
\t\t</div>
\t</div>
\t";
        // line 31
        echo twig_include($this->env, $context, "./inc/footer.html");
        echo "
</div>
</body>
</html>";
    }

    public function getTemplateName()
    {
        return "bb/question_comp.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  69 => 31,  54 => 25,  49 => 23,  33 => 10,  26 => 6,  19 => 1,);
    }
}
