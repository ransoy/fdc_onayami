<?php

/* ./inc/footer.html */
class __TwigTemplate_c85bf5de0c85ff3ff8606cccfab6b1904698eac39fbf2741e5dd06650b28a007 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<section class=\"sec sec_support\">
\t<h2 class=\"sec_ttl\">サポート</h2>
\t<ul class=\"support_menu\">
\t\t<li><a href=\"";
        // line 4
        echo twig_escape_filter($this->env, (isset($context["base_url"]) ? $context["base_url"] : null), "html", null, true);
        echo "tos\">利用規約</a></li>
\t\t<li><a href=\"";
        // line 5
        echo twig_escape_filter($this->env, (isset($context["base_url"]) ? $context["base_url"] : null), "html", null, true);
        echo "company\">会社概要</a></li>
\t\t<li><a href=\"";
        // line 6
        echo twig_escape_filter($this->env, (isset($context["base_url"]) ? $context["base_url"] : null), "html", null, true);
        echo "privacy\">個人情報の取扱い</a></li>
\t\t<li><a href=\"";
        // line 7
        echo twig_escape_filter($this->env, (isset($context["base_url"]) ? $context["base_url"] : null), "html", null, true);
        echo "guideline\">禁止事項</a></li>
\t\t<li><a href=\"";
        // line 8
        echo twig_escape_filter($this->env, (isset($context["base_url"]) ? $context["base_url"] : null), "html", null, true);
        echo "faq\"> よくある質問</a></li>
\t\t<li><a href=\"";
        // line 9
        echo twig_escape_filter($this->env, (isset($context["base_url"]) ? $context["base_url"] : null), "html", null, true);
        echo "contact\">お問い合わせ</a></li>
\t</ul>
</section>

<footer>
\t<small>&copy;お悩み相談</small>
</footer>";
    }

    public function getTemplateName()
    {
        return "./inc/footer.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  44 => 9,  40 => 8,  36 => 7,  32 => 6,  28 => 5,  24 => 4,  19 => 1,);
    }
}
