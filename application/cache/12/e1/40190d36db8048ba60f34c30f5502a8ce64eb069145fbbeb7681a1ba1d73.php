<?php

/* mypage_b.html */
class __TwigTemplate_12e140190d36db8048ba60f34c30f5502a8ce64eb069145fbbeb7681a1ba1d73 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<html lang=\"ja\">
<head>
<meta charset=\"utf-8\">
<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0, user-scalable=no, maximum-scale=1.0\">
<title>お悩み相談</title>
";
        // line 7
        echo twig_include($this->env, $context, "./inc/link.html");
        echo "
<link rel=\"stylesheet\" href=\"";
        // line 8
        echo twig_escape_filter($this->env, (isset($context["base_url"]) ? $context["base_url"] : null), "html", null, true);
        echo "sp/css/my_page.css\">
</head>
<body>
<div class=\"page_wrap page_mypage\">
\t";
        // line 12
        echo twig_include($this->env, $context, "./inc/header.html");
        echo "
\t<section class=\"sec sec_mypage\">
\t\t<h2 class=\"sec_ttl\" id=\"consultation_list\">マイページ</h2>
\t\t<ul class=\"tab_menu\" id=\"tab_menu\">
\t\t\t<li class=\"tab_unresolved\" id=\"tab_unresolved\">未回答の相談</li>
\t\t\t<li class=\"tab_history current\" id=\"tab_history\">相談履歴</li>
\t\t\t<li class=\"tab_bookmark\" id=\"tab_bookmark\">ブックマーク</li>
\t\t</ul>
\t\t<div class=\"sec_inner\">

\t\t\t<div class=\"tab_content content_unresolved\">
\t\t\t</div>

\t\t\t<div class=\"tab_content content_history\">
\t\t\t";
        // line 26
        if (((isset($context["list_all_num"]) ? $context["list_all_num"] : null) > 0)) {
            // line 27
            echo "\t\t\t\t<ul class=\"content_list\">
\t\t\t\t\t";
            // line 28
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["answered_ar"]) ? $context["answered_ar"] : null));
            foreach ($context['_seq'] as $context["key"] => $context["entry"]) {
                // line 29
                echo "\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t<div class=\"box_wrap\">
\t\t\t\t\t\t\t\t<div class=\"left_box\">
\t\t\t\t\t\t\t\t\t<input type=\"checkbox\" name=\"check[]\" value=\"";
                // line 32
                echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "id", array()), "html", null, true);
                echo "\">
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<div class=\"right_box\">
\t\t\t\t\t\t\t\t\t<a href=\"";
                // line 35
                echo twig_escape_filter($this->env, (isset($context["base_url"]) ? $context["base_url"] : null), "html", null, true);
                echo "cate";
                echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "big_cate_id", array()), "html", null, true);
                echo "/";
                echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "cate_id", array()), "html", null, true);
                echo "/";
                echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "id", array()), "html", null, true);
                echo "\" class=\"ic ic_angle-right\">
\t\t\t\t\t\t\t\t\t\t<p class=\"content_category ic ";
                // line 36
                echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "class", array()), "html", null, true);
                echo "\"><strong>";
                echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "big_category_name", array()), "html", null, true);
                echo "</strong>";
                echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "category_name", array()), "html", null, true);
                echo "</p>
\t\t\t\t\t\t\t\t\t\t<h3 class=\"content_ttl\">";
                // line 37
                echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "title", array()), "html", null, true);
                echo "</h3>
\t\t\t\t\t\t\t\t\t\t<p>";
                // line 38
                echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "message", array()), "html", null, true);
                echo "</p>
\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"content_date cf\">
\t\t\t\t\t\t\t\t<p class=\"post_date\">";
                // line 43
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["entry"], "create_date", array()), "Y.m.d H:i"), "html", null, true);
                echo "</p>
\t\t\t\t\t\t\t\t<ul class=\"posted_ability\">
\t\t\t\t\t\t\t\t\t";
                // line 45
                if (($this->getAttribute($context["entry"], "like_count", array()) > 0)) {
                    // line 46
                    echo "\t\t\t\t\t\t\t\t\t<li><p class=\"ic ic_heart\">";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "like_count", array()), "html", null, true);
                    echo "</p></li>
\t\t\t\t\t\t\t\t\t";
                } else {
                    // line 48
                    echo "\t\t\t\t\t\t\t\t\t<li><p class=\"ic ic_heart-o\">";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "like_count", array()), "html", null, true);
                    echo "</p></li>
\t\t\t\t\t\t\t\t\t";
                }
                // line 50
                echo "\t\t\t\t\t\t\t\t\t";
                if (($this->getAttribute($context["entry"], "comment_count", array()) > 0)) {
                    // line 51
                    echo "\t\t\t\t\t\t\t\t\t<li><p class=\"ic ic_comment\">";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "comment_count", array()), "html", null, true);
                    echo "</p></li>
\t\t\t\t\t\t\t\t\t";
                } else {
                    // line 53
                    echo "\t\t\t\t\t\t\t\t\t<li><p class=\"ic ic_comment-o\">";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "comment_count", array()), "html", null, true);
                    echo "</p></li>
\t\t\t\t\t\t\t\t\t";
                }
                // line 55
                echo "\t\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</li>
\t\t\t\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['key'], $context['entry'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 59
            echo "\t\t\t\t</ul>
\t\t\t\t<div class=\"content_bottom cf\">
\t\t\t\t\t<ul class=\"pager_wrap\">
\t\t\t\t\t\t";
            // line 62
            echo (isset($context["pagination"]) ? $context["pagination"] : null);
            echo "
\t\t\t\t\t</ul>

\t\t\t\t\t<ul class=\"btn_wrap\">
\t\t\t\t\t\t<li><a id=\"checked_delete\" href=\"#\" class=\"btn_style btn_gray-o\">チェックした項目を削除</a></li>
\t\t\t\t\t</ul>

\t\t\t\t</div>
\t\t\t";
        } else {
            // line 71
            echo "\t\t\t\t<div class=\"search_found not_found cf\">
\t\t\t\t\t<p>過去に相談した履歴はありません。</p>
\t\t\t\t\t<ul class=\"btn_wrap\">
\t\t\t\t\t\t<li><a href=\"";
            // line 74
            echo twig_escape_filter($this->env, (isset($context["base_url"]) ? $context["base_url"] : null), "html", null, true);
            echo "bb/posting\" class=\"btn_style btn_green\">相談する</a></li>
\t\t\t\t\t</ul>
\t\t\t\t</div>
\t\t\t";
        }
        // line 78
        echo "\t\t\t</div>

\t\t\t<div class=\"tab_content content_bookmark\">
\t\t\t</div>

\t\t</div>
\t</section>
\t";
        // line 85
        if ((isset($context["bonusData"]) ? $context["bonusData"] : null)) {
            // line 86
            echo "\t<section class=\"sec sec_pt_rate\">
\t\t<h2 class=\"sec_ttl\">ポイントレート</h2>
\t\t<div class=\"sec_inner\">
\t\t\t<table>
\t\t\t\t<tr>
\t\t\t\t\t<th style=\"width:53%\"></th>
\t\t\t\t\t<td>レート</td>
\t\t\t\t\t<td>獲得</td>
\t\t\t\t</tr>
\t\t\t\t<tr>
\t\t\t\t\t<th>投稿ボーナス</th>
\t\t\t\t\t<td>";
            // line 97
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["pointSettings"]) ? $context["pointSettings"] : null), "question_bonus", array()), "html", null, true);
            echo "</td>
\t\t\t\t\t<td>";
            // line 98
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["bonusData"]) ? $context["bonusData"] : null), "question_bonus", array()), "html", null, true);
            echo "</td>
\t\t\t\t</tr>
\t\t\t\t<tr>
\t\t\t\t\t<th>回答ボーナス</th>
\t\t\t\t\t<td>";
            // line 102
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["pointSettings"]) ? $context["pointSettings"] : null), "answer_bonus", array()), "html", null, true);
            echo "</td>
\t\t\t\t\t<td>";
            // line 103
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["bonusData"]) ? $context["bonusData"] : null), "answer_bonus", array()), "html", null, true);
            echo "</td>
\t\t\t\t</tr>
\t\t\t\t<tr>
\t\t\t\t\t<th>私もそう思う（回答者）</th>
\t\t\t\t\t<td>";
            // line 107
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["pointSettings"]) ? $context["pointSettings"] : null), "evaluate_bonus", array()), "html", null, true);
            echo "</td>
\t\t\t\t\t<td>";
            // line 108
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["bonusData"]) ? $context["bonusData"] : null), "evaluate_bonus", array()), "html", null, true);
            echo "</td>
\t\t\t\t</tr>
\t\t\t\t<tr>
\t\t\t\t\t<th>私もそう思う（質問者）</th>
\t\t\t\t\t<td>";
            // line 112
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["pointSettings"]) ? $context["pointSettings"] : null), "max_answer_has_bonus", array()), "html", null, true);
            echo "</td>
\t\t\t\t\t<td>";
            // line 113
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["bonusData"]) ? $context["bonusData"] : null), "max_answer_has_bonus", array()), "html", null, true);
            echo "</td>
\t\t\t\t</tr>
\t\t\t</table>
\t\t</div>
\t</section>
\t";
        }
        // line 119
        echo "\t";
        echo twig_include($this->env, $context, "./inc/footer.html");
        echo "
</div>
";
        // line 121
        echo twig_include($this->env, $context, "./inc/script.html");
        echo "
<script>
\t\$(\"#tab_unresolved\").on('click', function(){
\t\twindow.location.href = '";
        // line 124
        echo twig_escape_filter($this->env, (isset($context["base_url"]) ? $context["base_url"] : null), "html", null, true);
        echo "user/my_page/1/';
\t});
\t\$(\"#tab_history\").on('click', function(){
\t});
\t\$(\"#tab_bookmark\").on('click', function(){
\t\twindow.location.href = '";
        // line 129
        echo twig_escape_filter($this->env, (isset($context["base_url"]) ? $context["base_url"] : null), "html", null, true);
        echo "user/my_page/3/';
\t});

\t\$('#checked_delete').on('click',function() {
\t\tvar cont = 0;
 \t\tvar checks=[];
        \$(\".content_list input[name='check[]']:checked\").each(function(){
\t\t\tcont++;
            checks.push(this.value);
        });

\t\tif (cont == 0) {
\t\t\treturn false;
\t\t}

\t\tif (!confirm('チェックした項目を削除します\\nよろしいですか？')) {
\t\t\treturn false;
\t\t}

        console.log(checks);
\t\t\$.ajax({
\t\t\ttype: \"POST\",
\t\t\turl: \"";
        // line 151
        echo twig_escape_filter($this->env, (isset($context["base_url"]) ? $context["base_url"] : null), "html", null, true);
        echo "user/delete_unanswer_ajax\",
\t\t\tdata: {
\t\t\t    \"checks\":checks,
\t\t\t}
\t\t}).done(function(data){
\t\t\twindow.location = \"";
        // line 156
        echo twig_escape_filter($this->env, (isset($context["base_url"]) ? $context["base_url"] : null), "html", null, true);
        echo "user/my_page/2\";
\t\t}).fail(function(data){
\t\t    alert('error!!!');
\t\t});
\t});
</script> 
</body>
</html>
";
    }

    public function getTemplateName()
    {
        return "mypage_b.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  300 => 156,  292 => 151,  267 => 129,  259 => 124,  253 => 121,  247 => 119,  238 => 113,  234 => 112,  227 => 108,  223 => 107,  216 => 103,  212 => 102,  205 => 98,  201 => 97,  188 => 86,  186 => 85,  177 => 78,  170 => 74,  165 => 71,  153 => 62,  148 => 59,  139 => 55,  133 => 53,  127 => 51,  124 => 50,  118 => 48,  112 => 46,  110 => 45,  105 => 43,  97 => 38,  93 => 37,  85 => 36,  75 => 35,  69 => 32,  64 => 29,  60 => 28,  57 => 27,  55 => 26,  38 => 12,  31 => 8,  27 => 7,  19 => 1,);
    }
}
