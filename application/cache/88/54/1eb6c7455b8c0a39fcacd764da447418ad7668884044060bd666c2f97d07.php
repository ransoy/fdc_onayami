<?php

/* mypage_c.html */
class __TwigTemplate_88541eb6c7455b8c0a39fcacd764da447418ad7668884044060bd666c2f97d07 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<html lang=\"ja\">
<head>
<meta charset=\"utf-8\">
<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0, user-scalable=no, maximum-scale=1.0\">
<title>お悩み相談</title>
";
        // line 7
        echo twig_include($this->env, $context, "./inc/link.html");
        echo "
<link rel=\"stylesheet\" href=\"";
        // line 8
        echo twig_escape_filter($this->env, (isset($context["base_url"]) ? $context["base_url"] : null), "html", null, true);
        echo "sp/css/my_page.css\">
</head>
<body>
<div class=\"page_wrap page_mypage\">
\t";
        // line 12
        echo twig_include($this->env, $context, "./inc/header.html");
        echo "
\t<section class=\"sec sec_mypage\">
\t\t<h2 class=\"sec_ttl\" id=\"consultation_list\">マイページ</h2>
\t\t<ul class=\"tab_menu\" id=\"tab_menu\">
\t\t\t<li class=\"tab_unresolved\" id=\"tab_unresolved\">未回答の相談</li>
\t\t\t<li class=\"tab_history\" id=\"tab_history\">相談履歴</li>
\t\t\t<li class=\"tab_bookmark current\" id=\"tab_bookmark\">ブックマーク</li>
\t\t</ul>
\t\t<div class=\"sec_inner\">

\t\t\t<div class=\"tab_content content_unresolved\">
\t\t\t</div>

\t\t\t<div class=\"tab_content content_history\">
\t\t\t</div>

\t\t\t<div class=\"tab_content content_bookmark\">

\t\t\t";
        // line 30
        if (((isset($context["list_all_num"]) ? $context["list_all_num"] : null) > 0)) {
            // line 31
            echo "\t\t\t\t<ul class=\"content_list\">
\t\t\t\t\t";
            // line 32
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["bookmark_ar"]) ? $context["bookmark_ar"] : null));
            foreach ($context['_seq'] as $context["key"] => $context["entry"]) {
                // line 33
                echo "\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t<div class=\"box_wrap\">
\t\t\t\t\t\t\t\t<div class=\"left_box\">
\t\t\t\t\t\t\t\t\t<input type=\"checkbox\" name=\"check[]\" value=\"";
                // line 36
                echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "id", array()), "html", null, true);
                echo "\">
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<div class=\"right_box\">
\t\t\t\t\t\t\t\t\t<a href=\"";
                // line 39
                echo twig_escape_filter($this->env, (isset($context["base_url"]) ? $context["base_url"] : null), "html", null, true);
                echo "cate";
                echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "big_cate_id", array()), "html", null, true);
                echo "/";
                echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "cate_id", array()), "html", null, true);
                echo "/";
                echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "id", array()), "html", null, true);
                echo "\" class=\"ic ic_angle-right\">
\t\t\t\t\t\t\t\t\t\t<p class=\"content_category ic ";
                // line 40
                echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "class", array()), "html", null, true);
                echo "\"><strong>";
                echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "big_category_name", array()), "html", null, true);
                echo "</strong>";
                echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "category_name", array()), "html", null, true);
                echo "</p>
\t\t\t\t\t\t\t\t\t\t<h3 class=\"content_ttl\">";
                // line 41
                echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "title", array()), "html", null, true);
                echo "</h3>
\t\t\t\t\t\t\t\t\t\t<p>";
                // line 42
                echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "message", array()), "html", null, true);
                echo "</p>
\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>

\t\t\t\t\t\t\t<div class=\"content_date cf\">
\t\t\t\t\t\t\t\t<p class=\"post_date\">";
                // line 48
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["entry"], "create_date", array()), "Y.m.d H:i"), "html", null, true);
                echo "</p>
\t\t\t\t\t\t\t\t<ul class=\"posted_ability\">
\t\t\t\t\t\t\t\t\t";
                // line 50
                if (($this->getAttribute($context["entry"], "like_count", array()) > 0)) {
                    // line 51
                    echo "\t\t\t\t\t\t\t\t\t<li><p class=\"ic ic_heart\">";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "like_count", array()), "html", null, true);
                    echo "</p></li>
\t\t\t\t\t\t\t\t\t";
                } else {
                    // line 53
                    echo "\t\t\t\t\t\t\t\t\t<li><p class=\"ic ic_heart-o\">";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "like_count", array()), "html", null, true);
                    echo "</p></li>
\t\t\t\t\t\t\t\t\t";
                }
                // line 55
                echo "\t\t\t\t\t\t\t\t\t";
                if (($this->getAttribute($context["entry"], "comment_count", array()) > 0)) {
                    // line 56
                    echo "\t\t\t\t\t\t\t\t\t<li><p class=\"ic ic_comment\">";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "comment_count", array()), "html", null, true);
                    echo "</p></li>
\t\t\t\t\t\t\t\t\t";
                } else {
                    // line 58
                    echo "\t\t\t\t\t\t\t\t\t<li><p class=\"ic ic_comment-o\">";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "comment_count", array()), "html", null, true);
                    echo "</p></li>
\t\t\t\t\t\t\t\t\t";
                }
                // line 60
                echo "\t\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</li>
\t\t\t\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['key'], $context['entry'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 64
            echo "\t\t\t\t</ul>
\t\t\t\t<div class=\"content_bottom cf\">

\t\t\t\t\t<ul class=\"pager_wrap bookmark_pager_wrap\">
\t\t\t\t\t";
            // line 68
            echo (isset($context["pagination"]) ? $context["pagination"] : null);
            echo "
\t\t\t\t\t</ul>

\t\t\t\t\t<ul class=\"btn_wrap\">
\t\t\t\t\t\t<li><a id=\"checked_delete\" href=\"#\" class=\"btn_style btn_gray-o\">チェックした項目を削除</a></li>
\t\t\t\t\t</ul>

\t\t\t\t</div>
\t\t\t";
        } else {
            // line 77
            echo "\t\t\t\t<div class=\"search_found keyword_search cf\">
\t\t\t\t\t<p>ブックマークした相談はありません。<br>キーワード入力で相談を検索することもできます。</p>
\t\t\t\t\t<input type=\"hidden\" name=\"page\" id=\"search_page\">
\t\t\t\t\t<table class=\"sec_search_table\">
\t\t\t\t\t\t<tbody>
\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t<td><label for=\"\"><input type=\"text\" id=\"input_keyword\"  name=\"input_keyword\" placeholder=\"生理中のオナニー 潮ふき方法\" value=\"";
            // line 83
            echo twig_escape_filter($this->env, (isset($context["input_keyword"]) ? $context["input_keyword"] : null), "html", null, true);
            echo "\"></label></td>
\t\t\t\t\t\t\t\t<td class=\"search_btn\">
\t\t\t\t\t\t\t\t\t<ul class=\"btn_wrap\">
\t\t\t\t\t\t\t\t\t\t<li><button type=\"submit\" name=\"keyword\" class=\"btn_style btn_blue\">探す</button></li>
\t\t\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t</tbody>
\t\t\t\t\t</table>
\t\t\t\t</div>
\t\t\t\t<ul class=\"content_list\">
\t\t\t\t\t";
            // line 94
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["search_keyword_consultation_ar"]) ? $context["search_keyword_consultation_ar"] : null));
            foreach ($context['_seq'] as $context["key"] => $context["entry"]) {
                // line 95
                echo "\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t<div class=\"box_wrap\" style=\"padding-left:20px;\">
\t\t\t\t\t\t\t\t<div class=\"right_box\">
\t\t\t\t\t\t\t\t\t<a href=\"";
                // line 98
                echo twig_escape_filter($this->env, (isset($context["base_url"]) ? $context["base_url"] : null), "html", null, true);
                echo "cate";
                echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "big_cate_id", array()), "html", null, true);
                echo "/";
                echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "cate_id", array()), "html", null, true);
                echo "/";
                echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "id", array()), "html", null, true);
                echo "\" class=\"ic ic_angle-right\">
\t\t\t\t\t\t\t\t\t\t<p class=\"content_category ic ";
                // line 99
                echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "class", array()), "html", null, true);
                echo "\"><strong>";
                echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "big_category_name", array()), "html", null, true);
                echo "</strong>";
                echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "category_name", array()), "html", null, true);
                echo "</p>
\t\t\t\t\t\t\t\t\t\t<h3 class=\"content_ttl\">";
                // line 100
                echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "title", array()), "html", null, true);
                echo "</h3>
\t\t\t\t\t\t\t\t\t\t<p>";
                // line 101
                echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "message", array()), "html", null, true);
                echo "</p>
\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t
\t\t\t\t\t\t\t<div class=\"content_date cf\">
\t\t\t\t\t\t\t\t<p class=\"post_date\">";
                // line 107
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["entry"], "create_date", array()), "Y.m.d H:i"), "html", null, true);
                echo "</p>
\t\t\t\t\t\t\t\t<ul class=\"posted_ability\">
\t\t\t\t\t\t\t\t\t";
                // line 109
                if (($this->getAttribute($context["entry"], "like_count", array()) > 0)) {
                    // line 110
                    echo "\t\t\t\t\t\t\t\t\t<li><p class=\"ic ic_heart\">";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "like_count", array()), "html", null, true);
                    echo "</p></li>
\t\t\t\t\t\t\t\t\t";
                } else {
                    // line 112
                    echo "\t\t\t\t\t\t\t\t\t<li><p class=\"ic ic_heart-o\">";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "like_count", array()), "html", null, true);
                    echo "</p></li>
\t\t\t\t\t\t\t\t\t";
                }
                // line 114
                echo "\t\t\t\t\t\t\t\t\t";
                if (($this->getAttribute($context["entry"], "comment_count", array()) > 0)) {
                    // line 115
                    echo "\t\t\t\t\t\t\t\t\t<li><p class=\"ic ic_comment\">";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "comment_count", array()), "html", null, true);
                    echo "</p></li>
\t\t\t\t\t\t\t\t\t";
                } else {
                    // line 117
                    echo "\t\t\t\t\t\t\t\t\t<li><p class=\"ic ic_comment-o\">";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "comment_count", array()), "html", null, true);
                    echo "</p></li>
\t\t\t\t\t\t\t\t\t";
                }
                // line 119
                echo "\t\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</li>
\t\t\t\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['key'], $context['entry'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 123
            echo "\t\t\t\t</ul>\t\t\t\t
\t\t\t\t<div class=\"content_bottom cf\">
\t\t\t\t\t<ul class=\"pager_wrap search_pager_wrap\">
\t\t\t\t\t\t";
            // line 126
            echo (isset($context["pagination"]) ? $context["pagination"] : null);
            echo "
\t\t\t\t\t</ul>
\t\t\t\t</div>
\t\t\t";
        }
        // line 130
        echo "\t\t\t</div>
\t\t</div>
\t</section>
\t";
        // line 133
        if ((isset($context["bonusData"]) ? $context["bonusData"] : null)) {
            // line 134
            echo "\t<section class=\"sec sec_pt_rate\">
\t\t<h2 class=\"sec_ttl\">ポイントレート</h2>
\t\t<div class=\"sec_inner\">
\t\t\t<table>
\t\t\t\t<tr>
\t\t\t\t\t<th style=\"width:53%\"></th>
\t\t\t\t\t<td>レート</td>
\t\t\t\t\t<td>獲得</td>
\t\t\t\t</tr>
\t\t\t\t<tr>
\t\t\t\t\t<th>投稿ボーナス</th>
\t\t\t\t\t<td>";
            // line 145
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["pointSettings"]) ? $context["pointSettings"] : null), "question_bonus", array()), "html", null, true);
            echo "</td>
\t\t\t\t\t<td>";
            // line 146
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["bonusData"]) ? $context["bonusData"] : null), "question_bonus", array()), "html", null, true);
            echo "</td>
\t\t\t\t</tr>
\t\t\t\t<tr>
\t\t\t\t\t<th>回答ボーナス</th>
\t\t\t\t\t<td>";
            // line 150
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["pointSettings"]) ? $context["pointSettings"] : null), "answer_bonus", array()), "html", null, true);
            echo "</td>
\t\t\t\t\t<td>";
            // line 151
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["bonusData"]) ? $context["bonusData"] : null), "answer_bonus", array()), "html", null, true);
            echo "</td>
\t\t\t\t</tr>
\t\t\t\t<tr>
\t\t\t\t\t<th>私もそう思う（回答者）</th>
\t\t\t\t\t<td>";
            // line 155
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["pointSettings"]) ? $context["pointSettings"] : null), "evaluate_bonus", array()), "html", null, true);
            echo "</td>
\t\t\t\t\t<td>";
            // line 156
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["bonusData"]) ? $context["bonusData"] : null), "evaluate_bonus", array()), "html", null, true);
            echo "</td>
\t\t\t\t</tr>
\t\t\t\t<tr>
\t\t\t\t\t<th>私もそう思う（質問者）</th>
\t\t\t\t\t<td>";
            // line 160
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["pointSettings"]) ? $context["pointSettings"] : null), "max_answer_has_bonus", array()), "html", null, true);
            echo "</td>
\t\t\t\t\t<td>";
            // line 161
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["bonusData"]) ? $context["bonusData"] : null), "max_answer_has_bonus", array()), "html", null, true);
            echo "</td>
\t\t\t\t</tr>
\t\t\t</table>
\t\t</div>
\t</section>
\t";
        }
        // line 167
        echo "\t";
        echo twig_include($this->env, $context, "./inc/footer.html");
        echo "
</div>
";
        // line 169
        echo twig_include($this->env, $context, "./inc/script.html");
        echo "
<script>
\t\$(\"#tab_unresolved\").on('click', function(){
\t\twindow.location.href = '";
        // line 172
        echo twig_escape_filter($this->env, (isset($context["base_url"]) ? $context["base_url"] : null), "html", null, true);
        echo "user/my_page/1/';
\t});
\t\$(\"#tab_history\").on('click', function(){
\t\twindow.location.href = '";
        // line 175
        echo twig_escape_filter($this->env, (isset($context["base_url"]) ? $context["base_url"] : null), "html", null, true);
        echo "user/my_page/2/';
\t});

\t\$('#checked_delete').on('click',function() {
\t\tvar cont = 0;
 \t\tvar checks=[];
        \$(\".content_list input[name='check[]']:checked\").each(function(){
\t\t\tcont++;
            checks.push(this.value);
        });

\t\tif (cont == 0) {
\t\t\treturn false;
\t\t}

\t\tif (!confirm('チェックした項目を削除します\\nよろしいですか？')) {
\t\t\treturn false;
\t\t}

\t\t\$.ajax({
\t\t\ttype: \"POST\",
\t\t\turl: \"";
        // line 196
        echo twig_escape_filter($this->env, (isset($context["base_url"]) ? $context["base_url"] : null), "html", null, true);
        echo "user/delete_bookmark_ajax\",
\t\t\tdata: {
\t\t\t    \"checks\":checks,
\t\t\t}
\t\t}).done(function(data){
//\t\t    alert('OK!!');
\t\t\twindow.location = \"";
        // line 202
        echo twig_escape_filter($this->env, (isset($context["base_url"]) ? $context["base_url"] : null), "html", null, true);
        echo "user/my_page/3\";
\t\t}).fail(function(data){
\t\t    alert('error!!!');
\t\t});
\t});

\t\$('.search_pager_wrap a').on('click',function(){

\t\tvar page = \$(this).attr('data-ci-pagination-page');
\t\tvar href = \$(this).attr('href');
\t\tvar keyword = \$('#input_keyword').val();
\t\t\$(this).attr('data-ci-pagination-page', '');
\t\tif(page == 1) {
\t\t\t\$(this).attr('href', href+'?page='+page+'&input_keyword='+keyword+'#consultation_list');
\t\t} else {
\t\t\t\$(this).attr('href', href+'&input_keyword='+keyword+'#consultation_list');
\t\t}
\t});

\t\$('.bookmark_pager_wrap a').on('click',function(){

\t\tvar page = \$(this).attr('data-ci-pagination-page');
\t\tvar href = \$(this).attr('href');
\t\t\$(this).attr('data-ci-pagination-page', '');
\t\tif(page == 1) {
\t\t\t\$(this).attr('href', href+'?page='+page+'#consultation_list');
\t\t} else {
\t\t\t\$(this).attr('href', href+'#consultation_list');
\t\t}
\t});
\t
\t\$(\"button[name='keyword']\").on('click', function(){
\t\tvar val = \$(\"input[name='input_keyword']\").val();
\t\tif (val.trim() != '') {
 \t\t\twindow.location.href = '";
        // line 236
        echo twig_escape_filter($this->env, (isset($context["base_url"]) ? $context["base_url"] : null), "html", null, true);
        echo "user/my_page/3/?input_keyword='+val;
\t\t} else {
\t\t\t\$(\"input[name='input_keyword']\").val('');
\t\t}
\t});

\t\$(\"input[name='input_keyword']\").keypress(function(e) {
\t    if(e.which == 13) {
\t        var val = \$(\"input[name='input_keyword']\").val();
\t\t\tif (val.trim() != '') {
\t \t\t\twindow.location.href = '";
        // line 246
        echo twig_escape_filter($this->env, (isset($context["base_url"]) ? $context["base_url"] : null), "html", null, true);
        echo "user/my_page/3/?input_keyword='+val;
\t\t\t} else {
\t\t\t\t\$(\"input[name='input_keyword']\").val('');
\t\t\t}
\t    }
\t});
</script> 
</body>
</html>
";
    }

    public function getTemplateName()
    {
        return "mypage_c.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  455 => 246,  442 => 236,  405 => 202,  396 => 196,  372 => 175,  366 => 172,  360 => 169,  354 => 167,  345 => 161,  341 => 160,  334 => 156,  330 => 155,  323 => 151,  319 => 150,  312 => 146,  308 => 145,  295 => 134,  293 => 133,  288 => 130,  281 => 126,  276 => 123,  267 => 119,  261 => 117,  255 => 115,  252 => 114,  246 => 112,  240 => 110,  238 => 109,  233 => 107,  224 => 101,  220 => 100,  212 => 99,  202 => 98,  197 => 95,  193 => 94,  179 => 83,  171 => 77,  159 => 68,  153 => 64,  144 => 60,  138 => 58,  132 => 56,  129 => 55,  123 => 53,  117 => 51,  115 => 50,  110 => 48,  101 => 42,  97 => 41,  89 => 40,  79 => 39,  73 => 36,  68 => 33,  64 => 32,  61 => 31,  59 => 30,  38 => 12,  31 => 8,  27 => 7,  19 => 1,);
    }
}
