<?php

/* bb/question_conf.html */
class __TwigTemplate_0861b42f823649aa009145f1f41dad62245d22cd5edbec72434c706573fa6ed9 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!doctype html>
<html>
<head>
<meta charset=\"utf-8\">
<title>相談内容の確認</title>
";
        // line 6
        echo twig_include($this->env, $context, "./inc/link.html");
        echo "
</head>

<body>
<div class=\"page_wrap page_question_conf\">
\t";
        // line 11
        echo twig_include($this->env, $context, "./inc/header.html");
        echo "
<div class=\"content_wrap cf\">
\t<div class=\"content_inner\">
\t<section class=\"sec sec_question_conf\">
<!--\t\t<form action=\"./question_comp.html\" method=\"post\" class=\"question_conf_form validate\">  -->
\t\t\t<h2 class=\"sec_ttl\">相談内容の確認</h2>
\t\t\t<div class=\"input_form\">
\t\t\t\t<dl>
\t\t\t\t\t<dt>ニックネーム</dt>
\t\t\t\t\t<dd><p>";
        // line 20
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["data"]) ? $context["data"] : null), "nickname", array()), "html", null, true);
        echo "さん</p></dd>
\t\t\t\t</dl>
\t\t\t\t<dl>
\t\t\t\t\t<dt>相談するカテゴリ</dt>
\t\t\t\t\t<dd><p>";
        // line 24
        echo twig_escape_filter($this->env, set_value("big_category_name"), "html", null, true);
        echo "</p></dd>
\t\t\t\t</dl>
\t\t\t\t<dl>
\t\t\t\t\t<dt>相談するカテゴリの詳細</dt>
\t\t\t\t\t<dd><p>";
        // line 28
        echo twig_escape_filter($this->env, set_value("category_name"), "html", null, true);
        echo "</p></dd>
\t\t\t\t</dl>
\t\t\t\t<dl>
\t\t\t\t\t<dt>タイトル</dt>
\t\t\t\t\t<dd><p>";
        // line 32
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["data"]) ? $context["data"] : null), "question_subject", array()), "html", null, true);
        echo "</p></dd>
\t\t\t\t</dl>
\t\t\t\t<form id=\"consultation\" name=\"consultation\" method=\"post\" action=\"/bb/post_completion\">
\t\t\t\t<script>
\t\t\t\t\tvar img_select_array = [];
\t\t\t\t</script>\t
\t\t\t\t";
        // line 38
        if (($this->getAttribute((isset($context["data"]) ? $context["data"] : null), "userfile", array()) != "")) {
            // line 39
            echo "\t\t\t\t<dl>
\t\t\t\t\t<dt>画像</dt>
\t\t\t\t\t<dd>
\t\t\t\t\t\t<p><img src=\"/uploads/temp/";
            // line 42
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["data"]) ? $context["data"] : null), "userfile", array()), "html", null, true);
            echo "\"></p>
\t\t\t\t\t</dd>
\t\t\t\t</dl>\t
\t\t\t\t";
        }
        // line 46
        echo "\t\t\t\t<dl>
\t\t\t\t\t<dt>相談内容</dt>
\t\t\t\t\t<dd><p>";
        // line 48
        echo $this->getAttribute((isset($context["data"]) ? $context["data"] : null), "message_next", array());
        echo "</p></dd>
\t\t\t\t</dl>
\t\t\t\t";
        // line 50
        echo (isset($context["form_hidden"]) ? $context["form_hidden"] : null);
        echo "
\t\t\t\t</form>\t\t\t\t
\t\t\t\t<ul class=\"btn_wrap\">

\t\t\t\t\t<li>
\t\t\t\t\t<!--\t<a href=\"#\" onclick=\"javascript:window.history.back(-1);return false;\" class=\"btn_style btn_gray\">戻る</a>  -->
\t\t\t\t\t\t<a href=\"#\" id=\"back\" class=\"btn_style btn_gray-o\">戻る</a>
\t\t\t\t\t</li>
\t\t\t\t\t<li>
\t\t\t\t\t\t<button type=\"submit\" id=\"goto\" class=\"btn_style btn_green\">相談する</button>
\t\t\t\t\t</li>
\t\t\t\t</ul>
\t\t\t</div>
<!--\t\t</form>  -->
\t</section>
\t</div>
</div>

";
        // line 68
        echo twig_include($this->env, $context, "./inc/footer.html");
        echo "
</div>
<script>
\$(function(){
\tvar click = true;
    \$('#back').click(function() {
        \$('#consultation').attr('action', '";
        // line 74
        echo twig_escape_filter($this->env, (isset($context["base_url"]) ? $context["base_url"] : null), "html", null, true);
        echo "bb/posting');
        \$('#consultation').submit();
    });
    \$('#goto').click(function(e) {
        \$('#consultation').attr('action', '";
        // line 78
        echo twig_escape_filter($this->env, (isset($context["base_url"]) ? $context["base_url"] : null), "html", null, true);
        echo "bb/post_completion');
        if (click) {
        \t\$('#consultation').submit();
        \tclick = false;
        }
        e.prevenDefault();
    });
});
</script>
</body>
</html>";
    }

    public function getTemplateName()
    {
        return "bb/question_conf.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  136 => 78,  129 => 74,  120 => 68,  99 => 50,  94 => 48,  90 => 46,  83 => 42,  78 => 39,  76 => 38,  67 => 32,  60 => 28,  53 => 24,  46 => 20,  34 => 11,  26 => 6,  19 => 1,);
    }
}
