<?php

/* mypage_a.html */
class __TwigTemplate_60c58a15292152c4b215e397b2c7e54199d8898cd89b0b227e5960d4c8d28a71 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!doctype html>
<html>
<head>
<meta charset=\"utf-8\">
<title>マイページ</title>
";
        // line 6
        echo twig_include($this->env, $context, "./inc/link.html");
        echo "
</head>

<body>
<div class=\"page_wrap page_mypage\">
\t";
        // line 11
        echo twig_include($this->env, $context, "./inc/header.html");
        echo "
\t<div class=\"content_wrap cf\">
\t\t<div class=\"content\">
\t\t\t<div class=\"content_inner\">
\t\t\t\t<section class=\"sec sec_mypage\">
\t\t\t\t\t<h2 class=\"sec_ttl\" id=\"consultation_list\">マイページ</h2>
\t\t\t\t\t<ul class=\"tab_menu\" id=\"tab_menu\">
\t\t\t\t\t\t<li class=\"tab_unresolved current\" id=\"tab_unresolved\">未回答の相談</li>
\t\t\t\t\t\t<li class=\"tab_history\" id=\"tab_history\">相談履歴</li>
\t\t\t\t\t\t<li class=\"tab_bookmark\" id=\"tab_bookmark\">ブックマーク</li>
\t\t\t\t\t</ul>
\t\t\t\t\t<div class=\"tab_content content_unresolved content_1\">
\t\t\t\t\t";
        // line 23
        if (((isset($context["list_all_num"]) ? $context["list_all_num"] : null) > 0)) {
            // line 24
            echo "\t\t\t\t\t\t<ul class=\"consult_list m_b_20\">
\t\t\t\t\t\t\t";
            // line 25
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["unanswered_ar"]) ? $context["unanswered_ar"] : null));
            foreach ($context['_seq'] as $context["key"] => $context["entry"]) {
                // line 26
                echo "\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t<div class=\"box_wrap\" id=\"unanswered_id";
                // line 27
                echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "id", array()), "html", null, true);
                echo "\">
\t\t\t\t\t\t\t\t\t<div class=\"left_box\">
\t\t\t\t\t\t\t\t\t\t<input type=\"checkbox\" name=\"check[]\" value=\"";
                // line 29
                echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "id", array()), "html", null, true);
                echo "\">
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t<div class=\"right_box\">
\t\t\t\t\t\t\t\t\t\t<a class=\"ic ic_angle-right\" href=\"";
                // line 32
                echo twig_escape_filter($this->env, (isset($context["base_url"]) ? $context["base_url"] : null), "html", null, true);
                echo "cate";
                echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "big_cate_id", array()), "html", null, true);
                echo "/";
                echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "cate_id", array()), "html", null, true);
                echo "/";
                echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "id", array()), "html", null, true);
                echo "\">
\t\t\t\t\t\t\t\t\t\t\t<p class=\"content_category icon16 ";
                // line 33
                echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "class", array()), "html", null, true);
                echo "\"><strong>";
                echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "big_category_name", array()), "html", null, true);
                echo "</strong>";
                echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "category_name", array()), "html", null, true);
                echo "</p>
\t\t\t\t\t\t\t\t\t\t\t<h3 class=\"content_ttl\">";
                // line 34
                echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "title", array()), "html", null, true);
                echo "</h3>
\t\t\t\t\t\t\t\t\t\t\t<p>";
                // line 35
                echo twig_escape_filter($this->env, trim(strip_tags($this->getAttribute($context["entry"], "message", array()))), "html", null, true);
                echo "</p>
\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<div class=\"consult_list_date cf\">
\t\t\t\t\t\t\t\t\t<p class=\"post_date\">";
                // line 40
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["entry"], "create_date", array()), "Y.m.d H:i"), "html", null, true);
                echo "</p>
\t\t\t\t\t\t\t\t\t<ul class=\"posted_ability\">
\t\t\t\t\t\t\t\t\t\t";
                // line 42
                if (($this->getAttribute($context["entry"], "like_count", array()) > 0)) {
                    // line 43
                    echo "\t\t\t\t\t\t\t\t\t\t<li><p class=\"ic ic_heart\">";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "like_count", array()), "html", null, true);
                    echo "</p></li>
\t\t\t\t\t\t\t\t\t\t";
                } else {
                    // line 45
                    echo "\t\t\t\t\t\t\t\t\t\t<li><p class=\"ic ic_heart-o\">";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "like_count", array()), "html", null, true);
                    echo "</p></li>
\t\t\t\t\t\t\t\t\t\t";
                }
                // line 47
                echo "\t\t\t\t\t\t\t\t\t\t";
                if (($this->getAttribute($context["entry"], "comment_count", array()) > 0)) {
                    // line 48
                    echo "\t\t\t\t\t\t\t\t\t\t<li><p class=\"ic ic_comment\">";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "comment_count", array()), "html", null, true);
                    echo "</p></li>
\t\t\t\t\t\t\t\t\t\t";
                } else {
                    // line 50
                    echo "\t\t\t\t\t\t\t\t\t\t<li><p class=\"ic ic_comment-o\">";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "comment_count", array()), "html", null, true);
                    echo "</p></li>
\t\t\t\t\t\t\t\t\t\t";
                }
                // line 52
                echo "\t\t\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['key'], $context['entry'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 56
            echo "\t\t\t\t\t\t</ul>
\t\t\t\t\t\t<div class=\"content_bottom cf\">
\t\t\t\t\t\t\t<ul class=\"btn_wrap\">
\t\t\t\t\t\t\t\t<li><a id=\"checked_delete\" href=\"#\" class=\"btn_style btn_gray-o\">チェックした項目を削除</a></li>
\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t<ul class=\"pager_wrap\">
\t\t\t\t\t\t\t\t";
            // line 63
            echo (isset($context["pagination"]) ? $context["pagination"] : null);
            echo "
\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t</div>
\t\t\t\t\t";
        } else {
            // line 67
            echo "\t\t\t\t\t\t<div class=\"search_found not_found cf\">
\t\t\t\t\t\t\t<p class=\"left_box\">未回答の相談はありません。</p>
\t\t\t\t\t\t\t<ul class=\"btn_wrap right_box\">
\t\t\t\t\t\t\t\t<li><a href=\"";
            // line 70
            echo twig_escape_filter($this->env, (isset($context["base_url"]) ? $context["base_url"] : null), "html", null, true);
            echo "bb/posting\" class=\"btn_style btn_green\">相談する</a></li>
\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t</div>
\t\t\t\t\t";
        }
        // line 74
        echo "\t\t\t\t\t</div>

\t\t\t\t</section>
\t\t\t</div>
\t\t</div>
\t\t";
        // line 79
        echo twig_include($this->env, $context, "./inc/mypage_sidebar.html");
        echo "
\t</div>
\t";
        // line 81
        echo twig_include($this->env, $context, "./inc/footer.html");
        echo "
</div>
";
        // line 83
        echo twig_include($this->env, $context, "./inc/script.html");
        echo "
<script>
\t\$(\"#tab_history\").on('click', function(){
\t\twindow.location.href = '";
        // line 86
        echo twig_escape_filter($this->env, (isset($context["base_url"]) ? $context["base_url"] : null), "html", null, true);
        echo "user/my_page/2/';
\t});
\t\$(\"#tab_bookmark\").on('click', function(){
\t\twindow.location.href = '";
        // line 89
        echo twig_escape_filter($this->env, (isset($context["base_url"]) ? $context["base_url"] : null), "html", null, true);
        echo "user/my_page/3/';
\t});

\t\$('#checked_delete').on('click',function() {

\t\tvar cont = 0;
 \t\tvar checks=[];
        \$(\".consult_list input[name='check[]']:checked\").each(function(){
\t\t\tcont++;
            checks.push(this.value);
        });

        console.log(cont);

\t\tif (cont == 0) {
\t\t\treturn false;
\t\t}

\t\tif (!confirm('チェックした項目を削除します\\nよろしいですか？')) {
\t\t\treturn false;
\t\t}
        console.log(checks);

\t\t\$.ajax({
\t\t\ttype: \"POST\",
\t\t\turl: \"";
        // line 114
        echo twig_escape_filter($this->env, (isset($context["base_url"]) ? $context["base_url"] : null), "html", null, true);
        echo "user/delete_unanswer_ajax\",
\t\t\tdata: {
\t\t\t    \"checks\":checks,
\t\t\t}
\t\t}).done(function(data){
\t\t\twindow.location = \"";
        // line 119
        echo twig_escape_filter($this->env, (isset($context["base_url"]) ? $context["base_url"] : null), "html", null, true);
        echo "user/my_page/1\";
\t\t}).fail(function(data){
\t\t    alert('error!!!');
\t\t});

\t});

</script> 
</body>
</html>
";
    }

    public function getTemplateName()
    {
        return "mypage_a.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  238 => 119,  230 => 114,  202 => 89,  196 => 86,  190 => 83,  185 => 81,  180 => 79,  173 => 74,  166 => 70,  161 => 67,  154 => 63,  145 => 56,  136 => 52,  130 => 50,  124 => 48,  121 => 47,  115 => 45,  109 => 43,  107 => 42,  102 => 40,  94 => 35,  90 => 34,  82 => 33,  72 => 32,  66 => 29,  61 => 27,  58 => 26,  54 => 25,  51 => 24,  49 => 23,  34 => 11,  26 => 6,  19 => 1,);
    }
}
