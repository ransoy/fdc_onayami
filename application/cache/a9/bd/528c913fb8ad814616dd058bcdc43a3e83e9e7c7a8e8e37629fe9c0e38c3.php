<?php

/* ./inc/sidebar.html */
class __TwigTemplate_a9bd528c913fb8ad814616dd058bcdc43a3e83e9e7c7a8e8e37629fe9c0e38c3 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<aside class=\"sidebar\">



\t<section class=\"sec sec_new_consultation_list m_b_30\">
\t\t<h3 class=\"sec_ttl\">人気の相談一覧</h3>
\t\t<ul>
            ";
        // line 8
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["consultation_list_all_ar"]) ? $context["consultation_list_all_ar"] : null));
        foreach ($context['_seq'] as $context["key"] => $context["entry"]) {
            // line 9
            echo "\t\t\t<li>
\t\t\t\t<a href=\"";
            // line 10
            echo twig_escape_filter($this->env, (isset($context["base_url"]) ? $context["base_url"] : null), "html", null, true);
            echo "cate";
            echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "big_cate_id", array()), "html", null, true);
            echo "/";
            echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "cate_id", array()), "html", null, true);
            echo "/";
            echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "id", array()), "html", null, true);
            echo "\" class=\"ic ic_angle-right\">
\t\t\t\t\t<span class=\"sec_list_inner\">
\t\t\t\t\t\t<p class=\"posted_data\"><span>";
            // line 12
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["entry"], "create_date", array()), "Y/m/d H:i"), "html", null, true);
            echo "</span></p>
\t\t\t\t\t\t<p class=\"posted_ttl\">";
            // line 13
            echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "title", array()), "html", null, true);
            echo "</p>
\t\t\t\t\t</span>
\t\t\t\t</a>
\t\t\t</li>
\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['key'], $context['entry'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 18
        echo "\t\t</ul>
\t</section>

   \t";
        // line 21
        if ((isset($context["category_info_ar"]) ? $context["category_info_ar"] : null)) {
            // line 22
            echo "\t<section class=\"sec sec_category_link m_b_30\">
\t\t<ul>
            ";
            // line 24
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["category_info_ar"]) ? $context["category_info_ar"] : null));
            foreach ($context['_seq'] as $context["key"] => $context["entry"]) {
                // line 25
                echo "\t\t\t<li> <a class=\"category_detail_list ";
                echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "current", array()), "html", null, true);
                echo "\" href=\"";
                echo twig_escape_filter($this->env, (isset($context["base_url"]) ? $context["base_url"] : null), "html", null, true);
                echo "cate";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["big_category_info"]) ? $context["big_category_info"] : null), "id", array()), "html", null, true);
                echo "/";
                echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "id", array()), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "name", array()), "html", null, true);
                echo "</a> </li>
\t\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['key'], $context['entry'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 27
            echo "<!--
\t\t\t<li> <a class=\"category_detail_list \" href=\"./\">脅迫･強要</a> </li>
\t\t\t<li> <a class=\"category_detail_list \" href=\"./\">本番強要</a> </li>
\t\t\t<li> <a class=\"category_detail_list \" href=\"./\">売春行為</a> </li>
\t\t\t<li> <a class=\"category_detail_list \" href=\"./\">盗聴･盗撮</a> </li>
\t\t\t<li> <a class=\"category_detail_list \" href=\"./\">お客さんから借金</a> </li>
\t\t\t<li> <a class=\"category_detail_list \" href=\"./\">嫌がらせ</a> </li>
\t\t\t<li> <a class=\"category_detail_list \" href=\"./\">雇用問題</a> </li>
\t\t\t<li> <a class=\"category_detail_list \" href=\"./\">給料未払い</a> </li>
\t\t\t<li> <a class=\"category_detail_list \" href=\"./\">恋愛</a> </li>
\t\t\t<li> <a class=\"category_detail_list \" href=\"./\">いじめ</a> </li>
\t\t\t<li> <a class=\"category_detail_list \" href=\"./\">確定申告</a> </li>
\t\t\t<li> <a class=\"category_detail_list \" href=\"./\">マイナンバー制度</a> </li>
\t\t\t<li> <a class=\"category_detail_list \" href=\"./\">その他</a> </li>
-->
\t\t</ul>
\t</section>
   \t";
        }
        // line 45
        echo "
\t<section class=\"sec sec_side_banner\">
\t\t<ul>
\t\t\t<li>
\t\t\t\t<a href=\"";
        // line 49
        echo twig_escape_filter($this->env, (isset($context["main_url"]) ? $context["main_url"] : null), "html", null, true);
        echo "\" alt=\"風俗求人高収入アルバイト\">
\t\t\t\t  <img src=\"";
        // line 50
        echo twig_escape_filter($this->env, (isset($context["base_url"]) ? $context["base_url"] : null), "html", null, true);
        echo "pc/image/banner/sample_2.gif\" alt=\"\">
\t\t\t\t</a>
\t\t\t</li>
\t\t</ul>
\t</section>

</aside>
";
    }

    public function getTemplateName()
    {
        return "./inc/sidebar.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  123 => 50,  119 => 49,  113 => 45,  93 => 27,  76 => 25,  72 => 24,  68 => 22,  66 => 21,  61 => 18,  50 => 13,  46 => 12,  35 => 10,  32 => 9,  28 => 8,  19 => 1,);
    }
}
