<?php

/* ./inc/mypage_sidebar.html */
class __TwigTemplate_1d577f47f7ac82d9003727fe7ce8fa628a55126e0814b9482676fbf9211f9f29 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<aside class=\"sidebar\">
\t<section class=\"sec sec_mypage_setting m_b_30\">
\t\t<h3 class=\"sec_ttl\">各種設定変更</h3>
\t\t<ul>
\t\t\t<li> <a href=\"";
        // line 5
        echo twig_escape_filter($this->env, (isset($context["main_url"]) ? $context["main_url"] : null), "html", null, true);
        echo "user/settings/basic\" class=\"ic ic_angle-right\"><span class=\"sec_list_inner\"><i class=\"fa fa-key fa-fw\"></i>パスワードの変更</span></a> </li>
\t\t\t<li> <a href=\"";
        // line 6
        echo twig_escape_filter($this->env, (isset($context["main_url"]) ? $context["main_url"] : null), "html", null, true);
        echo "user/settings/profile\" class=\"ic ic_angle-right\"><span class=\"sec_list_inner\"><i class=\"fa fa-smile-o fa-fw\"></i>登録情報の変更</span></a> </li>
\t\t</ul>
\t</section>
</aside>
";
    }

    public function getTemplateName()
    {
        return "./inc/mypage_sidebar.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  29 => 6,  25 => 5,  19 => 1,);
    }
}
