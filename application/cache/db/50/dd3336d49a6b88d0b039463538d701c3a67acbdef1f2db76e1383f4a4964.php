<?php

/* index.html */
class __TwigTemplate_db50dd3336d49a6b88d0b039463538d701c3a67acbdef1f2db76e1383f4a4964 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<html lang=\"ja\">
<head>
<meta charset=\"utf-8\">
<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0, user-scalable=no, maximum-scale=1.0\">
<title>お悩み相談 | ジョイスペ</title>
<meta name=\"description\" content=\"\">
<link rel=\"stylesheet\" href=\"";
        // line 8
        echo twig_escape_filter($this->env, (isset($context["base_url"]) ? $context["base_url"] : null), "html", null, true);
        echo "sp/css/reset.css\">
<link rel=\"stylesheet\" href=\"";
        // line 9
        echo twig_escape_filter($this->env, (isset($context["base_url"]) ? $context["base_url"] : null), "html", null, true);
        echo "sp/css/html5reset.css\">
<link rel=\"stylesheet\" href=\"";
        // line 10
        echo twig_escape_filter($this->env, (isset($context["base_url"]) ? $context["base_url"] : null), "html", null, true);
        echo "sp/css/common.css\">
<link rel=\"stylesheet\" href=\"";
        // line 11
        echo twig_escape_filter($this->env, (isset($context["base_url"]) ? $context["base_url"] : null), "html", null, true);
        echo "sp/css/font-awesome.min.css\">
<link rel=\"stylesheet\" href=\"";
        // line 12
        echo twig_escape_filter($this->env, (isset($context["base_url"]) ? $context["base_url"] : null), "html", null, true);
        echo "sp/css/category_icons.css\">
<link rel=\"stylesheet\" href=\"";
        // line 13
        echo twig_escape_filter($this->env, (isset($context["base_url"]) ? $context["base_url"] : null), "html", null, true);
        echo "sp/css/style.css\">
<script src=\"";
        // line 14
        echo twig_escape_filter($this->env, (isset($context["base_url"]) ? $context["base_url"] : null), "html", null, true);
        echo "sp/js/jquery-1.11.0.js\"></script>
</head>
<body>
<div class=\"page_wrap page_index\">
\t";
        // line 18
        echo twig_include($this->env, $context, "./inc/header.html");
        echo "
\t<section class=\"sec sec_search\">
\t\t<div class=\"sec_inner\">
\t\t\t\t<h2 class=\"sec_ttl fa fa-search\">キーワードから探す</h2>
\t\t\t\t<p id=\"search_error\"></p>
\t\t\t\t<table class=\"sec_search_table\">
\t\t\t\t\t<tr>
\t\t\t\t\t\t<td><label for=\"\">
\t\t\t\t\t\t\t\t<input type=\"text\" name=\"input_keyword\" placeholder=\"生理中のオナニー 潮ふき方法\">
\t\t\t\t\t\t\t</label></td>
\t\t\t\t\t\t<td><ul class=\"btn_wrap\">
\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t<button class=\"btn_style btn_blue\" id=\"btn_keyword\">検索</button>
\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t\t</td>
\t\t\t\t\t</tr>
\t\t\t\t</table>
\t\t\t\t";
        // line 36
        if ((twig_length_filter($this->env, (isset($context["topViewed_ar"]) ? $context["topViewed_ar"] : null)) > 0)) {
            // line 37
            echo "\t\t\t\t<ul class=\"search_tag\">
\t\t\t\t\t<li style=\"display: inline-block;\"><p>注目の相談：</p></li>
\t\t\t\t\t";
            // line 39
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["topViewed_ar"]) ? $context["topViewed_ar"] : null));
            foreach ($context['_seq'] as $context["key"] => $context["entry"]) {
                // line 40
                echo "\t\t\t\t\t<li><a href=\"";
                echo twig_escape_filter($this->env, (isset($context["base_url"]) ? $context["base_url"] : null), "html", null, true);
                echo "cate";
                echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "big_cate_id", array()), "html", null, true);
                echo "/";
                echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "cate_id", array()), "html", null, true);
                echo "\" class=\"t_link\">";
                echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "name", array()), "html", null, true);
                echo "</a></li>
\t\t\t\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['key'], $context['entry'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 42
            echo "\t\t\t\t</ul>
\t\t\t\t";
        }
        // line 44
        echo "\t\t</div>
\t</section>
\t<section class=\"sec sec_category\">
\t\t<h2 class=\"sec_ttl\">カテゴリーから探す</h2>
\t\t";
        // line 48
        echo twig_include($this->env, $context, "./index_category.html");
        echo "
\t\t";
        // line 49
        if ((isset($context["user_login_flag"]) ? $context["user_login_flag"] : null)) {
            // line 50
            echo "\t\t<ul class=\"btn_wrap\">
\t\t\t<li><a href=\"";
            // line 51
            echo twig_escape_filter($this->env, (isset($context["base_url"]) ? $context["base_url"] : null), "html", null, true);
            echo "bb/posting/\" class=\"btn_style btn_green\">新しく相談する</a></li>
\t\t</ul>
\t\t";
        }
        // line 54
        echo "\t</section>
\t<section class=\"sec sec_banner\">
\t\t<div class=\"sec_inner\">
\t\t\t<ul>
\t\t\t\t<li>
\t\t\t\t\t<a href=\"";
        // line 59
        echo twig_escape_filter($this->env, (isset($context["main_url"]) ? $context["main_url"] : null), "html", null, true);
        echo "\" alt=\"風俗求人高収入アルバイト\">
\t\t\t\t\t  <img src=\"";
        // line 60
        echo twig_escape_filter($this->env, (isset($context["base_url"]) ? $context["base_url"] : null), "html", null, true);
        echo "pc/image/banner/sample_2.gif\" alt=\"\">
\t\t\t\t\t</a>
\t\t\t\t</li>
\t\t\t</ul>
\t\t</div>
\t</section>
\t<section class=\"sec sec_consultation_list\">
\t\t<h2 class=\"sec_ttl ic ic_comments\">お悩み相談一覧</h2>
\t\t<ul class=\"sec_consultation_tabs tab_menu\" id=\"tab_menu\">
\t\t\t<li class=\"tab_new_arrivals current\">新着順</li>
\t\t\t<li class=\"tab_popularity\">人気順</li>
\t\t</ul>
\t\t<div class=\"sec_inner\">
\t\t\t<ul class=\"consultation_list_wrap tab_content content_new_arrivals content_1\">
\t            ";
        // line 74
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["consultation_list_ar"]) ? $context["consultation_list_ar"] : null));
        foreach ($context['_seq'] as $context["key"] => $context["entry"]) {
            // line 75
            echo "\t\t\t\t\t<li class=\"consultation_list\"> <a href=\"";
            echo twig_escape_filter($this->env, (isset($context["base_url"]) ? $context["base_url"] : null), "html", null, true);
            echo "cate";
            echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "big_cate_id", array()), "html", null, true);
            echo "/";
            echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "cate_id", array()), "html", null, true);
            echo "/";
            echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "id", array()), "html", null, true);
            echo "\">
\t\t\t\t\t\t<div class=\"sec_box_top\">
\t\t\t\t\t\t\t<p class=\"sec_box_category icon ";
            // line 77
            echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "class", array()), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "big_category_name", array()), "html", null, true);
            echo "</p>
\t\t\t\t\t\t\t<p class=\"sec_box_category_detail\"><span>";
            // line 78
            echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "category_name", array()), "html", null, true);
            echo "</span></p>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<h3 class=\"sec_box_ttl\">";
            // line 80
            echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "title", array()), "html", null, true);
            echo "</h3>
\t\t\t\t\t\t";
            // line 81
            if ((twig_length_filter($this->env, trim(strip_tags($this->getAttribute($context["entry"], "message", array())))) > 41)) {
                // line 82
                echo "\t\t\t\t\t\t\t<p class=\"sec_box_content\">";
                echo twig_escape_filter($this->env, twig_slice($this->env, trim(strip_tags($this->getAttribute($context["entry"], "message", array()))), 0, 41), "html", null, true);
                echo "...</p>
\t\t\t\t\t\t";
            } else {
                // line 84
                echo "\t\t\t\t\t\t\t<p class=\"sec_box_content\">";
                echo twig_escape_filter($this->env, trim(strip_tags($this->getAttribute($context["entry"], "message", array()))), "html", null, true);
                echo "</p>
\t\t\t\t\t\t";
            }
            // line 86
            echo "\t\t\t\t\t\t<div class=\"sec_box_bottom\">
\t\t\t\t\t\t\t<p class=\"posted_data\">";
            // line 87
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["entry"], "create_date", array()), "Y.m.d"), "html", null, true);
            echo "</p>
\t\t\t\t\t\t\t<ul class=\"posted_ability\">
\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t";
            // line 90
            if (($this->getAttribute($context["entry"], "like_count", array()) > 0)) {
                // line 91
                echo "\t\t\t\t\t\t\t\t\t<p class=\"fa fa-heart\">";
                echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "like_count", array()), "html", null, true);
                echo "</p>
\t\t\t\t\t\t\t\t\t";
            } else {
                // line 93
                echo "\t\t\t\t\t\t\t\t\t<p class=\"fa fa-heart-o\">";
                echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "like_count", array()), "html", null, true);
                echo "</p>
\t\t\t\t\t\t\t\t\t";
            }
            // line 95
            echo "\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t";
            // line 97
            if (($this->getAttribute($context["entry"], "comment_count", array()) > 0)) {
                // line 98
                echo "\t\t\t\t\t\t\t\t\t<p class=\"fa fa-comment\">";
                echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "comment_count", array()), "html", null, true);
                echo "</p>
\t\t\t\t\t\t\t\t\t";
            } else {
                // line 100
                echo "\t\t\t\t\t\t\t\t\t<p class=\"fa fa-comment-o\">";
                echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "comment_count", array()), "html", null, true);
                echo "</p>
\t\t\t\t\t\t\t\t\t";
            }
            // line 102
            echo "\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t</a>
\t\t\t\t\t</li>
\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['key'], $context['entry'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 108
        echo "\t\t\t</ul>

\t\t\t<ul class=\"consultation_list_wrap tab_content content_popularity content_2\">
\t            ";
        // line 111
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["consultation_list_popularity_ar"]) ? $context["consultation_list_popularity_ar"] : null));
        foreach ($context['_seq'] as $context["key"] => $context["entry"]) {
            // line 112
            echo "\t\t\t\t\t<li class=\"consultation_list\">
\t\t\t\t\t\t<a href=\"";
            // line 113
            echo twig_escape_filter($this->env, (isset($context["base_url"]) ? $context["base_url"] : null), "html", null, true);
            echo "cate";
            echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "big_cate_id", array()), "html", null, true);
            echo "/";
            echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "cate_id", array()), "html", null, true);
            echo "/";
            echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "id", array()), "html", null, true);
            echo "\">
\t\t\t\t\t\t<div class=\"sec_box_top\">
\t\t\t\t\t\t\t<p class=\"sec_box_category icon ";
            // line 115
            echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "class", array()), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "big_category_name", array()), "html", null, true);
            echo "</p>
\t\t\t\t\t\t\t<p class=\"sec_box_category_detail\"><span>";
            // line 116
            echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "category_name", array()), "html", null, true);
            echo "</span></p>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<h3 class=\"sec_box_ttl\">";
            // line 118
            echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "title", array()), "html", null, true);
            echo "</h3>
\t\t\t\t\t\t";
            // line 119
            if ((twig_length_filter($this->env, trim(strip_tags($this->getAttribute($context["entry"], "message", array())))) > 41)) {
                // line 120
                echo "\t\t\t\t\t\t\t<p class=\"sec_box_content\">";
                echo twig_escape_filter($this->env, twig_slice($this->env, trim(strip_tags($this->getAttribute($context["entry"], "message", array()))), 0, 41), "html", null, true);
                echo "...</p>
\t\t\t\t\t\t";
            } else {
                // line 122
                echo "\t\t\t\t\t\t\t<p class=\"sec_box_content\">";
                echo twig_escape_filter($this->env, trim(strip_tags($this->getAttribute($context["entry"], "message", array()))), "html", null, true);
                echo "</p>
\t\t\t\t\t\t";
            }
            // line 124
            echo "\t\t\t\t\t\t<div class=\"sec_box_bottom\">
\t\t\t\t\t\t\t<p class=\"posted_data\">";
            // line 125
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["entry"], "create_date", array()), "Y.m.d"), "html", null, true);
            echo "</p>
\t\t\t\t\t\t\t<ul class=\"posted_ability\">
\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t";
            // line 128
            if (($this->getAttribute($context["entry"], "like_count", array()) > 0)) {
                // line 129
                echo "\t\t\t\t\t\t\t\t\t<p class=\"fa fa-heart\">";
                echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "like_count", array()), "html", null, true);
                echo "</p>
\t\t\t\t\t\t\t\t\t";
            } else {
                // line 131
                echo "\t\t\t\t\t\t\t\t\t<p class=\"fa fa-heart-o\">";
                echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "like_count", array()), "html", null, true);
                echo "</p>
\t\t\t\t\t\t\t\t\t";
            }
            // line 133
            echo "\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t";
            // line 135
            if (($this->getAttribute($context["entry"], "comment_count", array()) > 0)) {
                // line 136
                echo "\t\t\t\t\t\t\t\t\t<p class=\"fa fa-comment\">";
                echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "comment_count", array()), "html", null, true);
                echo "</p>
\t\t\t\t\t\t\t\t\t";
            } else {
                // line 138
                echo "\t\t\t\t\t\t\t\t\t<p class=\"fa fa-comment-o\">";
                echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "comment_count", array()), "html", null, true);
                echo "</p>
\t\t\t\t\t\t\t\t\t";
            }
            // line 140
            echo "\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t</a>
\t\t\t\t\t</li>
\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['key'], $context['entry'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 146
        echo "\t\t\t</ul>
 
\t\t</div>
\t</section>

\t";
        // line 151
        echo twig_include($this->env, $context, "./inc/footer.html");
        echo "
</div>
<script src=\"";
        // line 153
        echo twig_escape_filter($this->env, (isset($context["base_url"]) ? $context["base_url"] : null), "html", null, true);
        echo "sp/js/jquery.sidr.min.js\"></script>
<script src=\"";
        // line 154
        echo twig_escape_filter($this->env, (isset($context["base_url"]) ? $context["base_url"] : null), "html", null, true);
        echo "sp/js/home.js\" type=\"text/javascript\"></script>

<script>


\t\$(\"input[name='input_keyword']\").keypress(function(e) {
\t    if(e.which == 13) {
\t        searchKeyword()
\t    }
\t});
\t
\t\$(\"#btn_keyword\").on('click', function(){
\t\tsearchKeyword()
\t});

\tfunction searchKeyword() {
\t\tvar val = \$(\"input[name='input_keyword']\").val();
\t\t\$(\"input[name='input_keyword']\").val(val.trim());
\t\tif (val.trim() != '') {
\t\t\tval = val.trim();
\t\t\tpost_cont = \$(\".search_tag\");
\t\t\tcurrent_count = post_cont.children().length;
\t\t\turl = \"";
        // line 176
        echo twig_escape_filter($this->env, (isset($context["base_url"]) ? $context["base_url"] : null), "html", null, true);
        echo "search/ajaxSearchKeywordConsultation\";
\t\t\tparams = { keyword: val.trim() };
\t\t\t\$.ajax({
\t\t\t\ttype: \"POST\",
\t\t\t\turl: url,
\t\t\t\tdata: params,
\t\t\t\tdataType: 'TEXT',
\t\t\t\tcache: false,
\t\t\t\tsuccess: function(data){
\t\t\t\t\tif(data != \"\") {
\t\t\t\t\t\t\$('#search_error').text('').hide();
\t\t\t\t\t\twindow.location.href = \"";
        // line 187
        echo twig_escape_filter($this->env, (isset($context["base_url"]) ? $context["base_url"] : null), "html", null, true);
        echo "search/?input_keyword=\"+val;
\t\t\t\t\t} else {
\t\t\t\t\t\tvar search_error = \$('#search_error');
\t\t\t\t\t\tsearch_error.css('color', '#F00');
\t\t\t\t\t\tsearch_error.css('font-size', '4vw');
\t\t\t\t\t\tsearch_error.text('キーワードが見つかりません。').show();
\t\t\t\t\t}
\t\t\t\t}
\t\t\t});
\t\t} else {
\t\t\t\$('#search_error').text('').hide();
\t\t}
\t}
</script> 
</body>
</html>
";
    }

    public function getTemplateName()
    {
        return "index.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  414 => 187,  400 => 176,  375 => 154,  371 => 153,  366 => 151,  359 => 146,  348 => 140,  342 => 138,  336 => 136,  334 => 135,  330 => 133,  324 => 131,  318 => 129,  316 => 128,  310 => 125,  307 => 124,  301 => 122,  295 => 120,  293 => 119,  289 => 118,  284 => 116,  278 => 115,  267 => 113,  264 => 112,  260 => 111,  255 => 108,  244 => 102,  238 => 100,  232 => 98,  230 => 97,  226 => 95,  220 => 93,  214 => 91,  212 => 90,  206 => 87,  203 => 86,  197 => 84,  191 => 82,  189 => 81,  185 => 80,  180 => 78,  174 => 77,  162 => 75,  158 => 74,  141 => 60,  137 => 59,  130 => 54,  124 => 51,  121 => 50,  119 => 49,  115 => 48,  109 => 44,  105 => 42,  90 => 40,  86 => 39,  82 => 37,  80 => 36,  59 => 18,  52 => 14,  48 => 13,  44 => 12,  40 => 11,  36 => 10,  32 => 9,  28 => 8,  19 => 1,);
    }
}
