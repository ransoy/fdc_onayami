<?php

/* bb/reply_consult_ajax.html */
class __TwigTemplate_db6da1505c427f416693ef9bae0aaae2517bab50271a466ce15a02254b785121 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"comment_reply\">
\t<div class=\"consult_user_date\">
\t\t<p class=\"user_name\">投稿者：";
        // line 3
        echo twig_escape_filter($this->env, (isset($context["replay_nick_name"]) ? $context["replay_nick_name"] : null), "html", null, true);
        echo "さん</p>
\t\t<p class=\"post_date\">";
        // line 4
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute((isset($context["entry"]) ? $context["entry"] : null), "replay_cre_date", array()), "Y年m月d日H時i分"), "html", null, true);
        echo "</p>
\t</div>
\t<div class=\"sec_inner\">
\t\t<div class=\"comment\">
\t\t\t<p>";
        // line 8
        echo twig_escape_filter($this->env, (isset($context["consult_message"]) ? $context["consult_message"] : null), "html", null, true);
        echo "</p>
\t\t</div>
\t\t<div class=\"content_bottom cf\">
\t\t\t<ul class=\"btn_wrap\">
\t\t\t\t<li><a href=\"#\" class=\"btn_style btn_orange-o btn_update_eval\" data-mess-id=\"";
        // line 12
        echo twig_escape_filter($this->env, (isset($context["message_id"]) ? $context["message_id"] : null), "html", null, true);
        echo "\" data-user-id=\"";
        echo twig_escape_filter($this->env, (isset($context["user_id"]) ? $context["user_id"] : null), "html", null, true);
        echo "\">私もそう思う<span class=\"ic ic_heart\">";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entry"]) ? $context["entry"] : null), "evaluate", array()), "html", null, true);
        echo "</span></a></li>
\t\t\t</ul>
\t\t</div>
\t</div>
</div>";
    }

    public function getTemplateName()
    {
        return "bb/reply_consult_ajax.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  41 => 12,  34 => 8,  27 => 4,  23 => 3,  19 => 1,);
    }
}
