<?php

/* search/category_detail.html */
class __TwigTemplate_2d7ff745666b74fbca2b932d5fbefd3cc9e60cbf6c6cadc390ed5a2235b27c46 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<html lang=\"ja\">
<head>
<meta charset=\"utf-8\">
<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0, user-scalable=no, maximum-scale=1.0\">
<title>お悩み相談</title>
";
        // line 7
        echo twig_include($this->env, $context, "./inc/link.html");
        echo "
</head>
<body>
<div class=\"page_wrap page_category_detail\">
\t";
        // line 11
        echo twig_include($this->env, $context, "./inc/header.html");
        echo "
\t<section class=\"sec sec_category\">
\t\t<h2 class=\"sec_ttl\">";
        // line 13
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["big_category_info"]) ? $context["big_category_info"] : null), "name", array()), "html", null, true);
        echo "（";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["category_info"]) ? $context["category_info"] : null), "name", array()), "html", null, true);
        echo "）</h2>
\t\t<div class=\"sec_inner\">
\t\t\t<p>";
        // line 15
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["category_info"]) ? $context["category_info"] : null), "contents", array()), "html", null, true);
        echo "</p>
\t\t</div>
\t</section>
\t<section class=\"sec sec_consultation_list\">
\t\t<h2 class=\"sec_ttl\">";
        // line 19
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["category_info"]) ? $context["category_info"] : null), "name", array()), "html", null, true);
        echo "の相談一覧</h2>
\t\t<div class=\"sec_inner\">
\t\t\t<ul class=\"consultation_list_wrap\">
\t\t\t\t";
        // line 22
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["consultation_list_ar"]) ? $context["consultation_list_ar"] : null));
        foreach ($context['_seq'] as $context["key"] => $context["entry"]) {
            // line 23
            echo "\t\t\t\t<li class=\"consultation_list\">
\t\t\t\t\t<a href=\"";
            // line 24
            echo twig_escape_filter($this->env, (isset($context["base_url"]) ? $context["base_url"] : null), "html", null, true);
            echo "cate";
            echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "big_cate_id", array()), "html", null, true);
            echo "/";
            echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "cate_id", array()), "html", null, true);
            echo "/";
            echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "id", array()), "html", null, true);
            echo "\">
\t\t\t\t\t<div class=\"sec_box_top\">
\t\t\t\t\t\t<p class=\"sec_box_category fa fa-child\">";
            // line 26
            echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "big_category_name", array()), "html", null, true);
            echo "</p>
\t\t\t\t\t\t<p class=\"sec_box_category_detail\"><span>";
            // line 27
            echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "category_name", array()), "html", null, true);
            echo "</span></p>
\t\t\t\t\t</div>
\t\t\t\t\t<h3 class=\"sec_box_ttl\">";
            // line 29
            echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "title", array()), "html", null, true);
            echo "</h3>
\t\t\t\t\t<p class=\"sec_box_content\">";
            // line 30
            echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "message", array()), "html", null, true);
            echo "</p>
\t\t\t\t\t<div class=\"sec_box_bottom\">
\t\t\t\t\t\t<p class=\"posted_data\">";
            // line 32
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["entry"], "create_date", array()), "Y.m.d"), "html", null, true);
            echo "</p>
\t\t\t\t\t\t<ul class=\"posted_ability\">
\t\t\t\t\t\t\t";
            // line 34
            if (($this->getAttribute($context["entry"], "like_count", array()) > 0)) {
                // line 35
                echo "\t\t\t\t\t\t\t<li><p class=\"fa fa-heart\">";
                echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "like_count", array()), "html", null, true);
                echo "</p></li>
\t\t\t\t\t\t\t";
            } else {
                // line 37
                echo "\t\t\t\t\t\t\t<li><p class=\"fa fa-heart-o\">";
                echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "like_count", array()), "html", null, true);
                echo "</p></li>
\t\t\t\t\t\t\t";
            }
            // line 39
            echo "\t\t\t\t\t\t\t";
            if (($this->getAttribute($context["entry"], "comment_count", array()) > 0)) {
                // line 40
                echo "\t\t\t\t\t\t\t<li><p class=\"fa fa-comment\">";
                echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "comment_count", array()), "html", null, true);
                echo "</p></li>
\t\t\t\t\t\t\t";
            } else {
                // line 42
                echo "\t\t\t\t\t\t\t<li><p class=\"fa fa-comment-o\">";
                echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "comment_count", array()), "html", null, true);
                echo "</p></li>
\t\t\t\t\t\t\t";
            }
            // line 44
            echo "\t\t\t\t\t\t</ul>
\t\t\t\t\t</div>
\t\t\t\t\t</a>
\t\t\t\t</li>
\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['key'], $context['entry'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 49
        echo "\t\t\t</ul>
\t\t\t<ul class=\"pager_wrap\">
\t\t\t";
        // line 51
        echo (isset($context["pagination"]) ? $context["pagination"] : null);
        echo "
\t\t\t</ul>
\t\t</div>
\t</section>
\t<section class=\"sec sec_category_link\">
\t\t<h2 class=\"sec_ttl\">カテゴリのタグ一覧</h2>
\t\t<div class=\"sec_inner\">
\t\t\t<ul>
\t            ";
        // line 59
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["category_info_ar"]) ? $context["category_info_ar"] : null));
        foreach ($context['_seq'] as $context["key"] => $context["entry"]) {
            // line 60
            echo "\t\t\t\t<li> <a class=\"category_detail_list ";
            echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "current", array()), "html", null, true);
            echo "\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["base_url"]) ? $context["base_url"] : null), "html", null, true);
            echo "cate";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["big_category_info"]) ? $context["big_category_info"] : null), "id", array()), "html", null, true);
            echo "/";
            echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "id", array()), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "name", array()), "html", null, true);
            echo "</a> </li>
\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['key'], $context['entry'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 62
        echo "\t\t\t</ul>
\t\t</div>
\t</section>
\t";
        // line 65
        echo twig_include($this->env, $context, "./inc/footer.html");
        echo "
</div>
";
        // line 67
        echo twig_include($this->env, $context, "./inc/script.html");
        echo "
</body>
</html>
";
    }

    public function getTemplateName()
    {
        return "search/category_detail.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  185 => 67,  180 => 65,  175 => 62,  158 => 60,  154 => 59,  143 => 51,  139 => 49,  129 => 44,  123 => 42,  117 => 40,  114 => 39,  108 => 37,  102 => 35,  100 => 34,  95 => 32,  90 => 30,  86 => 29,  81 => 27,  77 => 26,  66 => 24,  63 => 23,  59 => 22,  53 => 19,  46 => 15,  39 => 13,  34 => 11,  27 => 7,  19 => 1,);
    }
}
