<?php

/* bb/consult.html */
class __TwigTemplate_ec2223da143cc8c9bd6f958f083b4d332e9c558ef53b1250fb4ad81dd48f1f1d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<html lang=\"ja\">
<head>
<meta charset=\"utf-8\">
<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0, user-scalable=no, maximum-scale=1.0\">
<title>";
        // line 6
        echo twig_escape_filter($this->env, (isset($context["title"]) ? $context["title"] : null), "html", null, true);
        echo "</title>
<meta name=\"description\" content=\"";
        // line 7
        echo twig_escape_filter($this->env, (isset($context["meta_desc"]) ? $context["meta_desc"] : null), "html", null, true);
        echo "\">
";
        // line 8
        echo twig_include($this->env, $context, "./inc/link.html");
        echo "
<link rel=\"stylesheet\" href=\"/sp/css/consult.css?v=20160707\">
</head>
<body>
<div class=\"page_wrap page_consult\">
";
        // line 13
        echo twig_include($this->env, $context, "./inc/header.html");
        echo "
\t<section class=\"sec sec_consult_question\">
\t\t<h2 class=\"sec_ttl icon ";
        // line 15
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["consultation_info"]) ? $context["consultation_info"] : null), "class", array()), "html", null, true);
        echo "\"><strong>";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["consultation_info"]) ? $context["consultation_info"] : null), "big_category_name", array()), "html", null, true);
        echo "</strong></h2>
\t\t<div class=\"sec_inner\">
\t\t\t<h3 class=\"content_ttl\">";
        // line 17
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["consultation_info"]) ? $context["consultation_info"] : null), "title", array()), "html", null, true);
        echo "</h3>
\t\t\t<div class=\"sec_desc\">
\t\t\t\t<p class=\"user_name\">";
        // line 19
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["consultation_info"]) ? $context["consultation_info"] : null), "nick_name", array()), "html", null, true);
        echo "</p>
\t\t\t\t<p class=\"post_date\">";
        // line 20
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["consultation_info"]) ? $context["consultation_info"] : null), "time_ago", array()), "html", null, true);
        echo "</p>
\t\t\t</div>
\t\t\t<div class=\"consult_question_content\">
\t\t\t\t";
        // line 23
        if (($this->getAttribute((isset($context["consultation_info"]) ? $context["consultation_info"] : null), "up_image", array()) != null)) {
            // line 24
            echo "\t\t\t\t<p><img src=\"";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["consultation_info"]) ? $context["consultation_info"] : null), "up_image", array()), "html", null, true);
            echo "\"></p>
\t\t\t\t";
        }
        // line 26
        echo "\t\t\t\t<p>";
        echo nl2br($this->getAttribute((isset($context["consultation_info"]) ? $context["consultation_info"] : null), "message", array()));
        echo "</p>
\t\t\t</div>
\t\t</div>
\t\t<ul class=\"btn_wrap\">
\t\t\t<form id=\"answer\" name=\"answer\" method=\"post\" action=\"/bb/answer\">
\t\t\t\t<input type=\"hidden\" name=\"answer_id\" id=\"answer_id\" value=\"";
        // line 31
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["consultation_info"]) ? $context["consultation_info"] : null), "id", array()), "html", null, true);
        echo "\"></input>
\t\t\t\t<input type=\"hidden\" name=\"bid\" id=\"bid\" value=\"";
        // line 32
        echo twig_escape_filter($this->env, (isset($context["bid"]) ? $context["bid"] : null), "html", null, true);
        echo "\"></input>
\t\t\t\t<input type=\"hidden\" name=\"subid\" id=\"subid\" value=\"";
        // line 33
        echo twig_escape_filter($this->env, (isset($context["subid"]) ? $context["subid"] : null), "html", null, true);
        echo "\"></input>
\t\t\t</form>\t
\t\t\t";
        // line 35
        if ((isset($context["user_login_flag"]) ? $context["user_login_flag"] : null)) {
            // line 36
            echo "\t\t\t<li> 
\t\t\t\t\t<!-- <button type=\"submit\" class=\"btn_style btn_green\" name=\"answer_id\" value=\"";
            // line 37
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["consultation_info"]) ? $context["consultation_info"] : null), "id", array()), "html", null, true);
            echo "\">この相談に回答する</button> -->
\t\t\t\t\t<a href=\"javascript:void(0);\" onclick=\"document.answer.submit();return false;\" class=\"btn_style btn_green\">この相談に回答する</a>
\t\t\t</li>
\t\t\t";
        }
        // line 41
        echo "\t\t\t";
        if ((isset($context["user_login_flag"]) ? $context["user_login_flag"] : null)) {
            // line 42
            echo "\t\t\t<li> 
\t\t\t\t<a href=\"javascript:void(0);\" class=\"btn_style ";
            // line 43
            echo twig_escape_filter($this->env, (isset($context["btn_bookmark_color"]) ? $context["btn_bookmark_color"] : null), "html", null, true);
            echo " ic ic_bookmark btn_bookmark\" data-thread-id=\"";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["consultation_info"]) ? $context["consultation_info"] : null), "id", array()), "html", null, true);
            echo "\" data-user-id=\"";
            echo twig_escape_filter($this->env, (isset($context["is_own_replay"]) ? $context["is_own_replay"] : null), "html", null, true);
            echo "\" data-bookmark-mode=\"";
            echo twig_escape_filter($this->env, (isset($context["flag"]) ? $context["flag"] : null), "html", null, true);
            echo "\">ブックマーク</a>
\t\t\t</li>
\t\t\t";
        }
        // line 46
        echo "\t\t\t<li> 
\t\t\t\t<a href=\"javascript:void(0);\" class=\"btn_style ";
        // line 47
        echo twig_escape_filter($this->env, (isset($context["btn_orange_color"]) ? $context["btn_orange_color"] : null), "html", null, true);
        echo " btn_update_eval\" data-mess-id=\"";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["consultation_info"]) ? $context["consultation_info"] : null), "id", array()), "html", null, true);
        echo "\" data-user-id=\"";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["consultation_info"]) ? $context["consultation_info"] : null), "user_id", array()), "html", null, true);
        echo "\" data-thread-mode=\"0\">私もそう思う<span class=\"ic ic_heart\">";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["consultation_info"]) ? $context["consultation_info"] : null), "like_count", array()), "html", null, true);
        echo "</span></a> 
\t\t\t</li>
\t\t</ul>
\t</section>
\t<section class=\"sec sec_consult_answer\">
\t\t<div class=\"sp_consult_answer\">
\t\t";
        // line 53
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["response_ar"]) ? $context["response_ar"] : null));
        foreach ($context['_seq'] as $context["key"] => $context["entry"]) {
            // line 54
            echo "\t\t<div class=\"consult_answer_list\">
\t\t\t<div class=\"sec_inner\">
\t\t\t\t<div class=\"consult_user_date\">
\t\t\t\t\t<p class=\"user_name\">投稿者：";
            // line 57
            echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "nick_name", array()), "html", null, true);
            echo "さん</p>
\t\t\t\t\t<p class=\"post_date\">";
            // line 58
            echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "time_ago", array()), "html", null, true);
            echo "</p>
\t\t\t\t</div>
\t\t\t\t<div class=\"consult_answer_content\">
\t\t\t\t\t";
            // line 61
            if (($this->getAttribute($context["entry"], "up_image", array()) != null)) {
                // line 62
                echo "\t\t\t\t\t<p><img src=\"";
                echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "up_image", array()), "html", null, true);
                echo "\"></p>
\t\t\t\t\t";
            }
            // line 64
            echo "\t\t\t\t\t";
            if (((isset($context["colorSwitch"]) ? $context["colorSwitch"] : null) == 1)) {
                // line 65
                echo "\t\t\t\t\t\t";
                if (($this->getAttribute($context["entry"], "rank", array()) > 80)) {
                    // line 66
                    echo "\t\t\t\t\t\t<p class=\"onayami_tally_count_1\">";
                    echo nl2br($this->getAttribute($context["entry"], "message", array()));
                    echo "</p>
\t\t\t\t\t\t";
                } elseif (($this->getAttribute(                // line 67
$context["entry"], "rank", array()) > 50)) {
                    // line 68
                    echo "\t\t\t\t\t\t<p class=\"onayami_tally_count_2\">";
                    echo nl2br($this->getAttribute($context["entry"], "message", array()));
                    echo "</p>
\t\t\t\t\t\t";
                } elseif (($this->getAttribute(                // line 69
$context["entry"], "rank", array()) > 5)) {
                    // line 70
                    echo "\t\t\t\t\t\t<p class=\"onayami_tally_count_3\">";
                    echo nl2br($this->getAttribute($context["entry"], "message", array()));
                    echo "</p>
\t\t\t\t\t\t";
                } else {
                    // line 72
                    echo "\t\t\t\t\t\t<p class=\"onayami_tally_count_4\">";
                    echo nl2br($this->getAttribute($context["entry"], "message", array()));
                    echo "</p>
\t\t\t\t\t\t";
                }
                // line 74
                echo "\t\t\t\t\t";
            } else {
                // line 75
                echo "\t\t\t\t\t<p>";
                echo nl2br($this->getAttribute($context["entry"], "message", array()));
                echo "</p>
\t\t\t\t\t";
            }
            // line 77
            echo "\t\t\t\t</div>
\t\t\t\t<div class=\"consult_answer_evaluate\">
\t\t\t\t\t<ul class=\"posted_ability btn_wrap\">
\t\t\t\t\t\t<li><a href=\"javascript:void(0);\" class=\"btn_style ";
            // line 80
            echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "btn_orange_color", array()), "html", null, true);
            echo " btn_update_eval\" data-mess-id=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "id", array()), "html", null, true);
            echo "\" data-user-id=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "user_id", array()), "html", null, true);
            echo "\">私もそう思う<span class=\"ic ic_heart\">";
            echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "evaluate", array()), "html", null, true);
            echo "</span></a></li>
\t\t\t\t\t</ul>
\t\t\t\t</div>
\t\t\t\t";
            // line 83
            if ($this->getAttribute($this->getAttribute($this->getAttribute($context["entry"], "reply_ar", array()), 0, array(), "array"), "flag", array())) {
                // line 84
                echo "\t\t\t\t\t<div class=\"comment_reply\">
\t\t\t\t\t\t<div class=\"consult_user_date\">
\t\t\t\t\t\t\t<p class=\"user_name\">投稿者：";
                // line 86
                echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "replay_nick_name", array()), "html", null, true);
                echo "さん</p>
\t\t\t\t\t\t\t<p class=\"post_date\">";
                // line 87
                echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "replay_cre_date", array()), "html", null, true);
                echo "</p>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"sec_inner\">
\t\t\t\t\t\t\t<div class=\"comment\">
\t\t\t\t\t\t\t\t<p>";
                // line 91
                echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "reply_message", array()), "html", null, true);
                echo "</p>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"content_bottom cf\">
\t\t\t\t\t\t\t\t<ul class=\"btn_wrap\">
\t\t\t\t\t\t\t\t\t<li><a href=\"javascript:void(0);\" class=\"btn_style ";
                // line 95
                echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "replay_orange_color", array()), "html", null, true);
                echo " btn_update_eval\" data-mess-id=\"";
                echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "reply_message_id", array()), "html", null, true);
                echo "\" data-user-id=\"";
                echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "replay_owner_id", array()), "html", null, true);
                echo "\">私もそう思う<span class=\"ic ic_heart\">";
                echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "replay_evaluate", array()), "html", null, true);
                echo "</span></a></li>
\t\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t";
            } else {
                // line 101
                echo "\t\t\t\t\t";
                if ((((isset($context["is_own_replay"]) ? $context["is_own_replay"] : null) != $this->getAttribute($context["entry"], "response_id", array())) && ($this->getAttribute((isset($context["consultation_info"]) ? $context["consultation_info"] : null), "user_id", array()) == (isset($context["is_own_replay"]) ? $context["is_own_replay"] : null)))) {
                    // line 102
                    echo "\t\t\t\t\t<form method=\"post\" action=\"\" class=\"validate_reply";
                    echo twig_escape_filter($this->env, $context["key"], "html", null, true);
                    echo " validate\">
\t\t\t\t\t\t<input type=\"hidden\" name=\"message_id\" value=\"";
                    // line 103
                    echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "id", array()), "html", null, true);
                    echo "\">
\t\t\t\t\t\t<input type=\"hidden\" name=\"thread_id\" value=\"";
                    // line 104
                    echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "thread_id", array()), "html", null, true);
                    echo "\">
\t\t\t\t\t\t<input type=\"hidden\" name=\"replay_nick_name\" value=\"";
                    // line 105
                    echo twig_escape_filter($this->env, $this->getAttribute((isset($context["consultation_info"]) ? $context["consultation_info"] : null), "nick_name", array()), "html", null, true);
                    echo "\">
\t\t\t\t\t\t<input type=\"hidden\" name=\"nick_name\" value=\"";
                    // line 106
                    echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "nick_name", array()), "html", null, true);
                    echo "\">
\t\t\t\t\t\t<div class=\"comment_reply_input\">
\t\t\t\t\t\t\t<h3 class=\"content_ttl\">返信コメント</h3>
\t\t\t\t\t\t\t<p class=\"error\" style=\"display: none;\">・本文を入力してください。</p>
\t\t\t\t\t\t\t<textarea class=\"consult_message\" name=\"consult_message\" placeholder=\"返信コメントを入力してください\"></textarea>
\t\t\t\t\t\t\t<ul class=\"btn_wrap\">
\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t<button type=\"submit\" class=\"btn_style btn_blue\">送信する</button>
\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t</div>
\t\t\t\t\t</form>
\t\t\t\t\t<script>
\t\t\t\t\t\$(function(){
\t\t\t\t\t\t\$(\".validate_reply";
                    // line 120
                    echo twig_escape_filter($this->env, $context["key"], "html", null, true);
                    echo "\").validate({
\t\t\t\t\t\t\t\t\t\t\t\trules: {
\t\t\t\t\t\t\t\t\t\t\t\t\tconsult_message:{
\t\t\t\t\t\t\t\t\t\t\t\t\t\trequired: true
\t\t\t\t\t\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t\t\t\t\t\t},
\t\t\t\t\t\t\t\t\t\t\t\tmessages: {
\t\t\t\t\t\t\t\t\t\t\t\t\tconsult_message:{
\t\t\t\t\t\t\t\t\t\t\t\t\t\trequired: \"返信コメントを入力してください。\"
\t\t\t\t\t\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t\t\t\t\t\t},
\t\t\t\t\t\t\t\t\t\t\t\terrorElement: \"p\",
\t\t\t\t\t\t\t\t\t\t\t\terrorPlacement:function(error,element){
\t\t\t\t\t\t\t\t\t\t\t\t\terror.insertBefore(\$(element));
\t\t\t\t\t\t\t\t\t\t\t\t},
\t\t\t\t\t\t\t\t\t\t\t\tsubmitHandler: function(form) {
\t\t\t\t\t\t\t\t\t\t\t\t\tvar \$form = \$('.validate_reply";
                    // line 136
                    echo twig_escape_filter($this->env, $context["key"], "html", null, true);
                    echo "');
\t\t\t\t\t\t\t\t\t\t\t\t\t\$.ajax({
\t\t\t\t\t\t\t\t\t\t\t\t\t    url:\"";
                    // line 138
                    echo twig_escape_filter($this->env, (isset($context["base_url"]) ? $context["base_url"] : null), "html", null, true);
                    echo "bb/reply_answer\",
\t\t\t\t\t\t\t\t\t\t\t\t        type:\"POST\",
\t\t\t\t\t\t\t\t\t\t\t\t        data:\$form.serialize(),
\t\t\t\t\t\t\t\t\t\t\t\t\t}).done(function(data){
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\$('.validate_reply";
                    // line 142
                    echo twig_escape_filter($this->env, $context["key"], "html", null, true);
                    echo "').replaceWith(data.html);
\t\t\t\t\t\t\t\t\t\t\t\t\t}).fail(function(data){
\t\t\t\t\t\t\t\t\t\t\t\t\t    alert('error!!!');
\t\t\t\t\t\t\t\t\t\t\t\t\t});
\t\t\t\t\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t\t\t\t\t});
\t\t\t\t\t});
\t\t\t\t\t</script>
\t\t\t\t\t";
                }
                // line 151
                echo "\t\t\t\t";
            }
            echo "\t
\t\t\t</div>
\t\t</div>
\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['key'], $context['entry'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 155
        echo "\t\t</div>
\t\t";
        // line 156
        if ((twig_length_filter($this->env, (isset($context["response_ar"]) ? $context["response_ar"] : null)) == 10)) {
            // line 157
            echo "\t\t<div class=\"more_comment\" data-consult-id=\"";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["consultation_info"]) ? $context["consultation_info"] : null), "id", array()), "html", null, true);
            echo "\">
\t\t\t<ul class=\"btn_wrap_more\">
\t\t\t\t<li><a id=\"show_more_data\" href=\"javascript:void(0)\" class=\"btn_other\">もっと見る</a></li>
\t\t\t</ul>
\t\t</div>
\t\t";
        }
        // line 163
        echo "\t</section>
";
        // line 164
        echo twig_include($this->env, $context, "./inc/footer.html");
        echo "
</div>
";
        // line 166
        echo twig_include($this->env, $context, "./inc/script.html");
        echo "
<script>
var base_url = '";
        // line 168
        echo twig_escape_filter($this->env, (isset($context["base_url"]) ? $context["base_url"] : null), "html", null, true);
        echo "';
\$(function(){
\t\$('textarea').blur(function(){
\t\t\$(this).val(\$(this).val().trim());
\t});
});
</script>
</body>
</html>";
    }

    public function getTemplateName()
    {
        return "bb/consult.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  390 => 168,  385 => 166,  380 => 164,  377 => 163,  367 => 157,  365 => 156,  362 => 155,  351 => 151,  339 => 142,  332 => 138,  327 => 136,  308 => 120,  291 => 106,  287 => 105,  283 => 104,  279 => 103,  274 => 102,  271 => 101,  256 => 95,  249 => 91,  242 => 87,  238 => 86,  234 => 84,  232 => 83,  220 => 80,  215 => 77,  209 => 75,  206 => 74,  200 => 72,  194 => 70,  192 => 69,  187 => 68,  185 => 67,  180 => 66,  177 => 65,  174 => 64,  168 => 62,  166 => 61,  160 => 58,  156 => 57,  151 => 54,  147 => 53,  132 => 47,  129 => 46,  117 => 43,  114 => 42,  111 => 41,  104 => 37,  101 => 36,  99 => 35,  94 => 33,  90 => 32,  86 => 31,  77 => 26,  71 => 24,  69 => 23,  63 => 20,  59 => 19,  54 => 17,  47 => 15,  42 => 13,  34 => 8,  30 => 7,  26 => 6,  19 => 1,);
    }
}
