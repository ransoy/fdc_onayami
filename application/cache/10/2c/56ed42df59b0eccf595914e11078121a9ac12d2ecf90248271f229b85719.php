<?php

/* 404.html */
class __TwigTemplate_102c56ed42df59b0eccf595914e11078121a9ac12d2ecf90248271f229b85719 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!doctype html>
<html>
<head>
<meta charset=\"utf-8\">
<title>お問い合わせ</title>
";
        // line 6
        $this->loadTemplate("inc/link.html", "404.html", 6)->display($context);
        // line 7
        echo "</head>
<body>
<div class=\"page_wrap page_404\">
\t";
        // line 10
        $this->loadTemplate("inc/header.html", "404.html", 10)->display($context);
        // line 11
        echo "\t<div class=\"content_wrap cf\">
\t\t<div class=\"content_inner basic_bg\">
\t\t\t<section class=\"sec sec_404\">
\t\t\t\t<div class=\"area_404\">
\t\t\t\t\t<h3 class=\"ttl_404\">お探しのページは見つかりません。</h3>
\t\t\t\t\t<p class=\"sub_ttl_404\">404 Not Found</p>
\t\t\t\t\t<p class=\"desc_404\">お探しのページは一時的にアクセスができない状況にあるか、移動もしくは削除された可能性があります。<br>
\t\t\t\t\tまた、URL、ファイル名にタイプミスがないか再度ご確認ください。</p>
\t\t\t\t\t<ul class=\"btn_wrap m_t_30\">
\t\t\t\t\t\t<li><a href=\"/\" class=\"btn_style btn_green\">トップページへ</a></li>
\t\t\t\t\t</ul>
\t\t\t\t</div>\t\t\t
\t\t\t</section>
\t\t</div>
\t</div>
\t";
        // line 26
        $this->loadTemplate("inc/footer.html", "404.html", 26)->display($context);
        // line 27
        echo "</div>
";
        // line 28
        $this->loadTemplate("inc/script.html", "404.html", 28)->display($context);
        // line 29
        echo "</body>
</html>
";
    }

    public function getTemplateName()
    {
        return "404.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  59 => 29,  57 => 28,  54 => 27,  52 => 26,  35 => 11,  33 => 10,  28 => 7,  26 => 6,  19 => 1,);
    }
}
