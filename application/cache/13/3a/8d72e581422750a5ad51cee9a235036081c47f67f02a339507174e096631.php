<?php

/* theme_url_put_template.html */
class __TwigTemplate_133a8d72e581422750a5ad51cee9a235036081c47f67f02a339507174e096631 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<blockquote><div class=\"insert_source\" ><p class=\"reaction_title\" >";
        echo (isset($context["info_title"]) ? $context["info_title"] : null);
        echo "</p><p class=\"reaction_description\"  >";
        echo (isset($context["info_description"]) ? $context["info_description"] : null);
        echo "</p>";
        if (((isset($context["info_image"]) ? $context["info_image"] : null) != "")) {
            echo "<p  class=\"c_img\"><a href=\"";
            echo twig_escape_filter($this->env, (isset($context["info_image"]) ? $context["info_image"] : null), "html", null, true);
            echo "\" ><img src=\"";
            echo twig_escape_filter($this->env, (isset($context["info_image"]) ? $context["info_image"] : null), "html", null, true);
            echo "\" alt=\"\"></a>";
        }
        echo "</p>
<cite><a href=\"";
        // line 2
        echo (isset($context["info_source_url"]) ? $context["info_source_url"] : null);
        echo "\" rel=\"nofollow\">出典：";
        echo (isset($context["info_source_url"]) ? $context["info_source_url"] : null);
        echo "</a></cite></div></blockquote>";
    }

    public function getTemplateName()
    {
        return "theme_url_put_template.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  34 => 2,  19 => 1,);
    }
}
