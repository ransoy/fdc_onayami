<?php

/* bb/question.html */
class __TwigTemplate_f69baa400eab7c97a46534ca26506a8eb7cff098e7e67a41f987cc5d1a7dd6ce extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<html lang=\"ja\">
<head>
<meta charset=\"utf-8\">
<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0, user-scalable=no, maximum-scale=1.0\">
<title>お悩み相談</title>
";
        // line 7
        echo twig_include($this->env, $context, "./inc/link.html");
        echo "
</head>
<body>
<div class=\"page_wrap page_question\">
\t";
        // line 11
        echo twig_include($this->env, $context, "./inc/header.html");
        echo "
\t<section class=\"sec sec_question\">
\t\t<form action=\"";
        // line 13
        echo twig_escape_filter($this->env, (isset($context["base_url"]) ? $context["base_url"] : null), "html", null, true);
        echo "bb/confirm\" method=\"post\" class=\"question_form validate\">
\t\t\t<input type=\"hidden\" name=\"nickname\" value=\"";
        // line 14
        echo twig_escape_filter($this->env, (isset($context["nickname"]) ? $context["nickname"] : null), "html", null, true);
        echo "\">
\t\t\t<h2 class=\"sec_ttl\">相談をする</h2>
\t\t\t<div class=\"sec_inner\">
\t\t\t\t<dl class=\"list_nickname\">
\t\t\t\t\t<dt>ニックネーム</dt>
\t\t\t\t\t<dd>
\t\t\t\t\t\t<p>";
        // line 20
        echo twig_escape_filter($this->env, (isset($context["nickname"]) ? $context["nickname"] : null), "html", null, true);
        echo "さん</p>
\t\t\t\t\t\t<p class=\"notice\">※ニックネームは公開されるので、個人が特定されるようなお名前はお控えください。</p>
\t\t\t\t\t\t<p class=\"t_right\"><a href=\"";
        // line 22
        echo twig_escape_filter($this->env, (isset($context["main_url"]) ? $context["main_url"] : null), "html", null, true);
        echo "user/settings\" class=\"t_link\">ニックネームの変更</a></p>
\t\t\t\t\t</dd>
\t\t\t\t</dl>
\t\t\t\t<dl class=\"input_category_list\">
\t\t\t\t\t<dt class=\"ic_required\">相談するカテゴリを選択</dt>
\t\t\t\t\t<dd>
\t\t\t\t\t\t<select name=\"question_category_list\" required=\"\" aria-required=\"true\" id=\"changeselect\" >
\t\t\t\t\t\t\t<option value=\"\">選択してください</option>
\t\t\t\t\t\t\t";
        // line 30
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["form_cate_opt"]) ? $context["form_cate_opt"] : null));
        foreach ($context['_seq'] as $context["key"] => $context["bcat"]) {
            // line 31
            echo "\t\t\t\t\t\t\t\t";
            if (((isset($context["select_big"]) ? $context["select_big"] : null) == $this->getAttribute($context["bcat"], "id", array()))) {
                // line 32
                echo "\t\t\t\t\t\t\t\t\t<option value=\"";
                echo twig_escape_filter($this->env, $this->getAttribute($context["bcat"], "id", array()), "html", null, true);
                echo "\" selected=\"selected\">";
                echo twig_escape_filter($this->env, $this->getAttribute($context["bcat"], "name", array()), "html", null, true);
                echo "</option>
\t\t\t\t\t\t\t\t";
            } else {
                // line 34
                echo "\t\t\t\t\t\t\t\t<option value=\"";
                echo twig_escape_filter($this->env, $this->getAttribute($context["bcat"], "id", array()), "html", null, true);
                echo "\" ";
                if (($this->getAttribute($context["bcat"], "id", array()) == $this->getAttribute((isset($context["values"]) ? $context["values"] : null), "big_cate_id", array()))) {
                    echo " selected=\"selected\" ";
                }
                echo " >";
                echo twig_escape_filter($this->env, $this->getAttribute($context["bcat"], "name", array()), "html", null, true);
                echo "</option>
\t\t\t\t\t\t\t\t";
            }
            // line 36
            echo "\t\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['key'], $context['bcat'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 37
        echo "\t\t\t\t\t\t</select>
\t\t\t\t\t</dd>
\t\t\t\t</dl>
\t\t\t\t<dl class=\"input_category\">
\t\t\t\t\t<dt class=\"ic_required\">カテゴリー詳細を選択</dt>
\t\t\t\t\t<dd>
\t\t\t\t\t\t<select name=\"question_category\" id=\"sub_question_cat\" required=\"\" aria-required=\"true\">
\t\t\t\t\t\t\t<option value=\"\">選択してください。</option>
\t\t\t\t\t\t\t";
        // line 45
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["get_sub_cat"]) ? $context["get_sub_cat"] : null));
        foreach ($context['_seq'] as $context["key"] => $context["scat"]) {
            // line 46
            echo "\t\t\t\t\t\t\t\t";
            if (((isset($context["sm_id"]) ? $context["sm_id"] : null) == $this->getAttribute($context["scat"], "id", array()))) {
                // line 47
                echo "\t\t\t\t\t\t\t\t\t<option value=\"";
                echo twig_escape_filter($this->env, $this->getAttribute($context["scat"], "id", array()), "html", null, true);
                echo "\" selected=\"selected\">";
                echo twig_escape_filter($this->env, $this->getAttribute($context["scat"], "name", array()), "html", null, true);
                echo "</option>
\t\t\t\t\t\t\t\t";
            } else {
                // line 49
                echo "\t\t\t\t\t\t\t\t\t<option value=\"";
                echo twig_escape_filter($this->env, $this->getAttribute($context["scat"], "id", array()), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, $this->getAttribute($context["scat"], "name", array()), "html", null, true);
                echo "</option>
\t\t\t\t\t\t\t\t";
            }
            // line 51
            echo "\t\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['key'], $context['scat'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 52
        echo "\t\t\t\t\t\t</select>
\t\t\t\t\t</dd>
\t\t\t\t</dl>
\t\t\t\t<dl class=\"list_ttl\">
\t\t\t\t\t<dt class=\"ic_required\">タイトル</dt>
\t\t\t\t\t<dd><input type=\"text\" name=\"question_subject\" maxlength=\"50\" value=\"";
        // line 57
        echo twig_escape_filter($this->env, set_value("question_subject"), "html", null, true);
        echo "\"></dd>
\t\t\t\t</dl>
\t\t\t\t<dl class=\"list_content\">
\t\t\t\t\t<dt class=\"ic_required\">内容</dt>
\t\t\t\t\t<dd><textarea name=\"question_message\" maxlength=\"1000\" id=\"\" value=\"\">";
        // line 61
        echo twig_escape_filter($this->env, set_value("question_message"), "html", null, true);
        echo "</textarea></dd>
\t\t\t\t</dl>
\t\t\t\t<dl class=\"input_image\">
\t\t\t\t\t<dt class=\"\">画像</dt>
\t\t\t\t\t<dd class=\"cf ui-count_form\">
\t\t\t\t\t\t<input id=\"upload_img\" type=\"file\" value=\"\" name=\"userfile\">
\t\t\t\t\t\t<input id=\"image_file_name\" type=\"hidden\" value=\"\" name=\"image_file_name\">
\t\t\t\t\t\t<p class=\"input_desc\">※3Mbyte以内</p>
\t\t\t\t\t</dd>
\t\t\t\t</dl>
\t\t\t\t<ul class=\"btn_wrap\">
\t\t\t\t\t<li><button type=\"submit\" class=\"btn_style btn_green\">相談内容の確認</button></li>
\t\t\t\t</ul>
\t\t\t</div>
\t\t</form>
\t</section>
\t";
        // line 77
        echo twig_include($this->env, $context, "./inc/footer.html");
        echo "
</div>
";
        // line 79
        echo twig_include($this->env, $context, "./inc/script.html");
        echo "
<script>
\$(function(){
\tvar sm_id = '";
        // line 82
        echo twig_escape_filter($this->env, (isset($context["sm_id"]) ? $context["sm_id"] : null), "html", null, true);
        echo "';
\tif (!sm_id){
\t\tif (\$('#changeselect').val()) {
\t\t\tajaxCat(\$('#changeselect').val());
\t\t}
\t}
\t\$('#changeselect').change(function(){
\t\tajaxCat(\$(this).val());
\t});                                       
\t\$('#sub_question_cat').change(function(){
\t\t\$.cookie(\"sub_cat\", \$(this).val());
\t});
\t\$('input[type=text], textarea').blur(function(){
\t\t\$(this).val(\$(this).val().trim());
\t});
});
function ajaxCat(id) {
\t\$.ajax({
\t\ttype: \"POST\",
\t\turl: '/bb/sub_category_ajax',
\t\tdata: {bid:id},
\t\tdataType: 'JSON',
\t\tcache: false,
\t\tsuccess:
\t\t\tfunction(data){
\t\t\t\t\tvar opt = '<option value>選択してください。</option>';
\t\t\t\t\t\$.each(data, function(i, category){
\t\t\t\t\t\t\tselctd = (typeof \$.cookie(\"sub_cat\") !== \"undefined\" && \$.cookie(\"sub_cat\") == category['id'])? 'selected=selected':'';
\t\t\t\t\t\t\topt += '<option value=\"'+category['id']+'\"'+selctd+'>'+category['name']+'</option>';
\t\t\t\t\t});
\t\t\t\t\t\$(\"#sub_question_cat\").html(opt);
\t\t\t\t}
\t});
}
</script>
</body>
</html>
";
    }

    public function getTemplateName()
    {
        return "bb/question.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  184 => 82,  178 => 79,  173 => 77,  154 => 61,  147 => 57,  140 => 52,  134 => 51,  126 => 49,  118 => 47,  115 => 46,  111 => 45,  101 => 37,  95 => 36,  83 => 34,  75 => 32,  72 => 31,  68 => 30,  57 => 22,  52 => 20,  43 => 14,  39 => 13,  34 => 11,  27 => 7,  19 => 1,);
    }
}
