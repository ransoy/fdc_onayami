<?php

/* bb/consult_ajax.html */
class __TwigTemplate_994a0c48f649777f64a2025fdbf34eba1f6f886faae4962a28fc72f51dba9461 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["response_ar"]) ? $context["response_ar"] : null));
        foreach ($context['_seq'] as $context["key"] => $context["entry"]) {
            // line 2
            echo "<div class=\"consult_answer_list\">
\t<div class=\"consult_user_date\">
\t\t<p class=\"user_name\">投稿者：";
            // line 4
            echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "nick_name", array()), "html", null, true);
            echo "さん</p>
\t\t<p class=\"post_date\">";
            // line 5
            echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "time_ago", array()), "html", null, true);
            echo "</p>
\t</div>
\t<div class=\"sec_inner\">
\t\t<div class=\"consult_answer_content\">
\t\t\t";
            // line 9
            if (($this->getAttribute($context["entry"], "up_image", array()) != null)) {
                // line 10
                echo "\t\t\t<p><img src=\"";
                echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "up_image", array()), "html", null, true);
                echo "\"></p>
\t\t\t";
            }
            // line 12
            echo "\t\t\t";
            if (((isset($context["colorSwitch"]) ? $context["colorSwitch"] : null) == 1)) {
                // line 13
                echo "\t\t\t\t";
                if (($this->getAttribute($context["entry"], "rank", array()) > 80)) {
                    // line 14
                    echo "\t\t\t\t<p class=\"onayami_tally_count_1\">";
                    echo nl2br($this->getAttribute($context["entry"], "message", array()));
                    echo "</p>
\t\t\t\t";
                } elseif (($this->getAttribute(                // line 15
$context["entry"], "rank", array()) > 50)) {
                    // line 16
                    echo "\t\t\t\t<p class=\"onayami_tally_count_2\">";
                    echo nl2br($this->getAttribute($context["entry"], "message", array()));
                    echo "</p>
\t\t\t\t";
                } elseif (($this->getAttribute(                // line 17
$context["entry"], "rank", array()) > 5)) {
                    // line 18
                    echo "\t\t\t\t<p class=\"onayami_tally_count_3\">";
                    echo nl2br($this->getAttribute($context["entry"], "message", array()));
                    echo "</p>
\t\t\t\t";
                } else {
                    // line 20
                    echo "\t\t\t\t<p class=\"onayami_tally_count_4\">";
                    echo nl2br($this->getAttribute($context["entry"], "message", array()));
                    echo "</p>
\t\t\t\t";
                }
                // line 22
                echo "\t\t\t";
            } else {
                // line 23
                echo "\t\t\t<p>";
                echo nl2br($this->getAttribute($context["entry"], "message", array()));
                echo "</p>
\t\t\t";
            }
            // line 25
            echo "\t\t</div>
\t\t<div class=\"consult_answer_evaluate\">
\t\t\t<ul class=\"posted_ability btn_wrap\">
\t\t\t\t<li><a href=\"javascript:void(0);\" class=\"btn_style ";
            // line 28
            echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "btn_orange_color", array()), "html", null, true);
            echo " btn_update_eval\" data-mess-id=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "id", array()), "html", null, true);
            echo "\" data-user-id=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "user_id", array()), "html", null, true);
            echo "\">私もそう思う<span class=\"ic ic_heart\">";
            echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "evaluate", array()), "html", null, true);
            echo "</span></a></li>
\t\t\t</ul>
\t\t</div>
\t\t";
            // line 31
            if ($this->getAttribute($this->getAttribute($this->getAttribute($context["entry"], "reply_ar", array()), 0, array(), "array"), "flag", array())) {
                // line 32
                echo "\t\t\t<div class=\"comment_reply\">
\t\t\t\t<div class=\"consult_user_date\">
\t\t\t\t\t<p class=\"user_name\">投稿者：";
                // line 34
                echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "replay_nick_name", array()), "html", null, true);
                echo "さん</p>
\t\t\t\t\t<p class=\"post_date\">";
                // line 35
                echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "replay_cre_date", array()), "html", null, true);
                echo "</p>
\t\t\t\t</div>
\t\t\t\t<div class=\"sec_inner\">
\t\t\t\t\t<div class=\"comment\">
\t\t\t\t\t\t<p>";
                // line 39
                echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "reply_message", array()), "html", null, true);
                echo "</p>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"content_bottom cf\">
\t\t\t\t\t\t<ul class=\"btn_wrap\">
\t\t\t\t\t\t\t<li><a href=\"javascript:void(0);\" class=\"btn_style ";
                // line 43
                echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "replay_orange_color", array()), "html", null, true);
                echo " btn_update_eval\" data-mess-id=\"";
                echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "reply_message_id", array()), "html", null, true);
                echo "\" data-user-id=\"";
                echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "replay_owner_id", array()), "html", null, true);
                echo "\">私もそう思う<span class=\"ic ic_heart\">";
                echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "replay_evaluate", array()), "html", null, true);
                echo "</span></a></li>
\t\t\t\t\t\t</ul>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</div>
\t\t";
            } else {
                // line 49
                echo "\t\t\t";
                if ((((isset($context["is_own_replay"]) ? $context["is_own_replay"] : null) != $this->getAttribute($context["entry"], "response_id", array())) && ($this->getAttribute((isset($context["consultation_info"]) ? $context["consultation_info"] : null), "user_id", array()) == (isset($context["is_own_replay"]) ? $context["is_own_replay"] : null)))) {
                    // line 50
                    echo "\t\t\t<form method=\"post\" action=\"\" class=\"validate_reply";
                    echo twig_escape_filter($this->env, $context["key"], "html", null, true);
                    echo "\">
\t\t\t\t<input type=\"hidden\" name=\"message_id\" value=\"";
                    // line 51
                    echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "id", array()), "html", null, true);
                    echo "\">
\t\t\t\t<input type=\"hidden\" name=\"thread_id\" value=\"";
                    // line 52
                    echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "thread_id", array()), "html", null, true);
                    echo "\">
\t\t\t\t<input type=\"hidden\" name=\"replay_nick_name\" value=\"";
                    // line 53
                    echo twig_escape_filter($this->env, $this->getAttribute((isset($context["consultation_info"]) ? $context["consultation_info"] : null), "nick_name", array()), "html", null, true);
                    echo "\">
\t\t\t\t<input type=\"hidden\" name=\"nick_name\" value=\"";
                    // line 54
                    echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "nick_name", array()), "html", null, true);
                    echo "\">
\t\t\t\t<div class=\"comment_reply_input\">
\t\t\t\t\t<h3 class=\"content_ttl\">返信コメント</h3>
\t\t\t\t\t<p class=\"error\" style=\"display: none;\">・本文を入力してください。</p>
\t\t\t\t\t<textarea class=\"consult_message\" name=\"consult_message\" placeholder=\"返信コメントを入力してください\"></textarea>
\t\t\t\t\t<ul class=\"btn_wrap\">
\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t<button type=\"submit\" class=\"btn_style btn_blue\">送信する</button>
\t\t\t\t\t\t</li>
\t\t\t\t\t</ul>
\t\t\t\t</div>
\t\t\t</form>
\t\t\t<script>
\t\t\t\$(function(){
\t\t\t\t\$(\".validate_reply";
                    // line 68
                    echo twig_escape_filter($this->env, $context["key"], "html", null, true);
                    echo "\").validate({
\t\t\t\t\t\t\t\t\t\trules: {
\t\t\t\t\t\t\t\t\t\t\tconsult_message:{
\t\t\t\t\t\t\t\t\t\t\t\trequired: true
\t\t\t\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t\t\t\t},
\t\t\t\t\t\t\t\t\t\tmessages: {
\t\t\t\t\t\t\t\t\t\t\tconsult_message:{
\t\t\t\t\t\t\t\t\t\t\t\trequired: \"返信コメントを入力してください。\"
\t\t\t\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t\t\t\t},
\t\t\t\t\t\t\t\t\t\terrorElement: \"p\",
\t\t\t\t\t\t\t\t\t\terrorPlacement:function(error,element){
\t\t\t\t\t\t\t\t\t\t\terror.insertBefore(\$(element));
\t\t\t\t\t\t\t\t\t\t},
\t\t\t\t\t\t\t\t\t\tsubmitHandler: function(form) {
\t\t\t\t\t\t\t\t\t\t\tvar \$form = \$('.validate_reply";
                    // line 84
                    echo twig_escape_filter($this->env, $context["key"], "html", null, true);
                    echo "');
\t\t\t\t\t\t\t\t\t\t\t\$.ajax({
\t\t\t\t\t\t\t\t\t\t\t    url:\"";
                    // line 86
                    echo twig_escape_filter($this->env, (isset($context["base_url"]) ? $context["base_url"] : null), "html", null, true);
                    echo "bb/reply_answer\",
\t\t\t\t\t\t\t\t\t\t        type:\"POST\",
\t\t\t\t\t\t\t\t\t\t        data:\$form.serialize(),
\t\t\t\t\t\t\t\t\t\t\t}).done(function(data){
\t\t\t\t\t\t\t\t\t\t\t\t\$('.validate_reply";
                    // line 90
                    echo twig_escape_filter($this->env, $context["key"], "html", null, true);
                    echo "').replaceWith(data.html);
\t\t\t\t\t\t\t\t\t\t\t}).fail(function(data){
\t\t\t\t\t\t\t\t\t\t\t    alert('error!!!');
\t\t\t\t\t\t\t\t\t\t\t});
\t\t\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t\t\t});
\t\t\t});
\t\t\t</script>
\t\t\t";
                }
                // line 99
                echo "\t\t";
            }
            echo "\t
\t</div>
</div>
";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['key'], $context['entry'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
    }

    public function getTemplateName()
    {
        return "bb/consult_ajax.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  223 => 99,  211 => 90,  204 => 86,  199 => 84,  180 => 68,  163 => 54,  159 => 53,  155 => 52,  151 => 51,  146 => 50,  143 => 49,  128 => 43,  121 => 39,  114 => 35,  110 => 34,  106 => 32,  104 => 31,  92 => 28,  87 => 25,  81 => 23,  78 => 22,  72 => 20,  66 => 18,  64 => 17,  59 => 16,  57 => 15,  52 => 14,  49 => 13,  46 => 12,  40 => 10,  38 => 9,  31 => 5,  27 => 4,  23 => 2,  19 => 1,);
    }
}
