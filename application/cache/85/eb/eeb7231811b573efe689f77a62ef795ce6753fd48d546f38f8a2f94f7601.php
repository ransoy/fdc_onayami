<?php

/* bb/consult.html */
class __TwigTemplate_85ebeeb7231811b573efe689f77a62ef795ce6753fd48d546f38f8a2f94f7601 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!doctype html>
<html>
<head>
<meta charset=\"utf-8\">
<title>";
        // line 5
        echo twig_escape_filter($this->env, (isset($context["title"]) ? $context["title"] : null), "html", null, true);
        echo "</title>
<meta name=\"description\" content=\"";
        // line 6
        echo twig_escape_filter($this->env, (isset($context["meta_desc"]) ? $context["meta_desc"] : null), "html", null, true);
        echo "\">
";
        // line 7
        echo twig_include($this->env, $context, "./inc/link.html");
        echo "
<link rel=\"stylesheet\" href=\"/pc/css/consult.css?v=20160707\">
<script>
\$(function() {
\thighslide_set();
});
</script>
</head>
<body>
<div id=\"highslide-container\"></div>
<div id=\"img_zoom_wrap\">
\t<div id=\"img_zoom\" style=\"width:150%;\"></div>
</div>
<div class=\"page_wrap page_consult\">
\t";
        // line 21
        echo twig_include($this->env, $context, "./inc/header.html");
        echo "
\t<div class=\"content_wrap cf\">
\t\t<div class=\"content\">
\t\t\t<section class=\"sec sec_consult_question m_b_30\">
\t\t\t\t<div class=\"content_inner\">
\t\t\t\t\t<h2 class=\"sec_ttl icon ";
        // line 26
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["consultation_info"]) ? $context["consultation_info"] : null), "class", array()), "html", null, true);
        echo "\"><strong>";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["consultation_info"]) ? $context["consultation_info"] : null), "big_category_name", array()), "html", null, true);
        echo "</strong></h2>
\t\t\t\t\t<h3 class=\"content_ttl\">";
        // line 27
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["consultation_info"]) ? $context["consultation_info"] : null), "title", array()), "html", null, true);
        echo "</h3>
\t\t\t\t\t<p class=\"sec_desc\"> <span class=\"user_name\">相談者：";
        // line 28
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["consultation_info"]) ? $context["consultation_info"] : null), "nick_name", array()), "html", null, true);
        echo "さん</span> <span class=\"post_date\">";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["consultation_info"]) ? $context["consultation_info"] : null), "time_ago", array()), "html", null, true);
        echo "</span> </p>
\t\t\t\t\t<input type=\"hidden\" id=\"comment_count\" value=\"";
        // line 29
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["consultation_info"]) ? $context["consultation_info"] : null), "comment_count", array()), "html", null, true);
        echo "\">
\t\t\t\t\t<div class=\"consult_question_content\">
\t\t\t\t\t\t";
        // line 31
        if (($this->getAttribute((isset($context["consultation_info"]) ? $context["consultation_info"] : null), "up_image", array()) != null)) {
            // line 32
            echo "\t\t\t\t\t\t<p><img src=\"";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["consultation_info"]) ? $context["consultation_info"] : null), "up_image", array()), "html", null, true);
            echo "\"></p>
\t\t\t\t\t\t";
        }
        // line 34
        echo "\t\t\t\t\t\t<p>";
        echo nl2br($this->getAttribute((isset($context["consultation_info"]) ? $context["consultation_info"] : null), "message", array()));
        echo "</p>
\t\t\t\t\t</div>

\t\t\t\t\t<ul class=\"btn_wrap\">
\t\t\t\t\t\t<form id=\"answer\" name=\"answer\" method=\"post\" action=\"";
        // line 38
        echo twig_escape_filter($this->env, (isset($context["base_url"]) ? $context["base_url"] : null), "html", null, true);
        echo "bb/answer\">
\t\t\t\t\t\t\t<input type=\"hidden\" name=\"answer_id\" id=\"answer_id\" value=\"";
        // line 39
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["consultation_info"]) ? $context["consultation_info"] : null), "id", array()), "html", null, true);
        echo "\"></input>
\t\t\t\t\t\t\t<input type=\"hidden\" name=\"bid\" id=\"bid\" value=\"";
        // line 40
        echo twig_escape_filter($this->env, (isset($context["bid"]) ? $context["bid"] : null), "html", null, true);
        echo "\"></input>
\t\t\t\t\t\t\t<input type=\"hidden\" name=\"subid\" id=\"subid\" value=\"";
        // line 41
        echo twig_escape_filter($this->env, (isset($context["subid"]) ? $context["subid"] : null), "html", null, true);
        echo "\"></input>
\t\t\t\t\t\t</form>\t
\t\t\t\t\t\t";
        // line 43
        if ((isset($context["user_login_flag"]) ? $context["user_login_flag"] : null)) {
            // line 44
            echo "\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t<a href=\"javascript:void(0);\" onclick=\"document.answer.submit();return false;\" class=\"btn_style btn_green\">回答する</a>
\t\t\t\t\t\t</li>
\t\t\t\t\t\t";
        }
        // line 48
        echo "
\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t<a href=\"javascript:void(0);\" class=\"btn_style ";
        // line 50
        echo twig_escape_filter($this->env, (isset($context["btn_orange_color"]) ? $context["btn_orange_color"] : null), "html", null, true);
        echo " btn_update_eval\" data-mess-id=\"";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["consultation_info"]) ? $context["consultation_info"] : null), "id", array()), "html", null, true);
        echo "\" data-user-id=\"";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["consultation_info"]) ? $context["consultation_info"] : null), "user_id", array()), "html", null, true);
        echo "\" data-thread-mode=\"0\">私もそう思う<span class=\"ic ic_heart\">";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["consultation_info"]) ? $context["consultation_info"] : null), "like_count", array()), "html", null, true);
        echo "</span></a>
\t\t\t\t\t\t</li>
\t\t\t\t\t\t";
        // line 52
        if ((isset($context["user_login_flag"]) ? $context["user_login_flag"] : null)) {
            // line 53
            echo "\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t<a href=\"javascript:void(0);\" class=\"btn_style ";
            // line 54
            echo twig_escape_filter($this->env, (isset($context["btn_bookmark_color"]) ? $context["btn_bookmark_color"] : null), "html", null, true);
            echo " ic ic_bookmark btn_bookmark\" data-thread-id=\"";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["consultation_info"]) ? $context["consultation_info"] : null), "id", array()), "html", null, true);
            echo "\" data-user-id=\"";
            echo twig_escape_filter($this->env, (isset($context["is_own_replay"]) ? $context["is_own_replay"] : null), "html", null, true);
            echo "\" data-bookmark-mode=\"";
            echo twig_escape_filter($this->env, (isset($context["flag"]) ? $context["flag"] : null), "html", null, true);
            echo "\">ブックマークする</a>
\t\t\t\t\t\t</li>
\t\t\t\t\t\t";
        }
        // line 57
        echo "\t\t\t\t\t</ul>
\t\t\t\t</div>
\t\t\t</section>
\t\t\t<section class=\"sec sec_consult_answer\">

\t\t\t\t";
        // line 62
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["response_ar"]) ? $context["response_ar"] : null));
        foreach ($context['_seq'] as $context["key"] => $context["entry"]) {
            // line 63
            echo "\t\t\t\t<div class=\"content_inner\">
\t\t\t\t\t<p class=\"user_name\">";
            // line 64
            echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "nick_name", array()), "html", null, true);
            echo "さん</p>
\t\t\t\t\t<div class=\"consult_answer_list\">
\t\t\t\t\t\t<div class=\"comment\">
\t\t\t\t\t\t\t";
            // line 67
            if (($this->getAttribute($context["entry"], "up_image", array()) != null)) {
                // line 68
                echo "\t\t\t\t\t\t\t<p><img src=\"";
                echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "up_image", array()), "html", null, true);
                echo "\"></p>
\t\t\t\t\t\t\t";
            }
            // line 70
            echo "\t\t\t\t\t\t\t";
            if (((isset($context["colorSwitch"]) ? $context["colorSwitch"] : null) == 1)) {
                // line 71
                echo "\t\t\t\t\t\t\t\t";
                if (($this->getAttribute($context["entry"], "rank", array()) > 80)) {
                    // line 72
                    echo "\t\t\t\t\t\t\t\t<p class=\"onayami_tally_count_1\">";
                    echo nl2br($this->getAttribute($context["entry"], "message", array()));
                    echo "</p>
\t\t\t\t\t\t\t\t";
                } elseif (($this->getAttribute(                // line 73
$context["entry"], "rank", array()) > 50)) {
                    // line 74
                    echo "\t\t\t\t\t\t\t\t<p class=\"onayami_tally_count_2\">";
                    echo nl2br($this->getAttribute($context["entry"], "message", array()));
                    echo "</p>
\t\t\t\t\t\t\t\t";
                } elseif (($this->getAttribute(                // line 75
$context["entry"], "rank", array()) > 5)) {
                    // line 76
                    echo "\t\t\t\t\t\t\t\t<p class=\"onayami_tally_count_3\">";
                    echo nl2br($this->getAttribute($context["entry"], "message", array()));
                    echo "</p>
\t\t\t\t\t\t\t\t";
                } else {
                    // line 78
                    echo "\t\t\t\t\t\t\t\t<p class=\"onayami_tally_count_4\">";
                    echo nl2br($this->getAttribute($context["entry"], "message", array()));
                    echo "</p>
\t\t\t\t\t\t\t\t";
                }
                // line 80
                echo "\t\t\t\t\t\t\t";
            } else {
                // line 81
                echo "\t\t\t\t\t\t\t<p>";
                echo nl2br($this->getAttribute($context["entry"], "message", array()));
                echo "</p>
\t\t\t\t\t\t\t";
            }
            // line 83
            echo "\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"content_bottom cf\">
\t\t\t\t\t\t\t<p class=\"post_date\">";
            // line 85
            echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "time_ago", array()), "html", null, true);
            echo "</p>

\t\t\t\t\t\t\t<ul class=\"btn_wrap\">
\t\t\t\t\t\t\t\t<li><a href=\"javascript:void(0);\" class=\"btn_style ";
            // line 88
            echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "btn_orange_color", array()), "html", null, true);
            echo " btn_update_eval\" data-mess-id=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "id", array()), "html", null, true);
            echo "\" data-user-id=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "user_id", array()), "html", null, true);
            echo "\">私もそう思う<span class=\"ic ic_heart\">";
            echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "evaluate", array()), "html", null, true);
            echo "</span></a></li>
\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>

<!--\t\t\t\t\t";
            // line 93
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["entry"], "reply_ar", array()));
            foreach ($context['_seq'] as $context["key2"] => $context["entry2"]) {
                echo " -->
\t\t\t\t\t";
                // line 94
                if ($this->getAttribute($this->getAttribute($this->getAttribute($context["entry"], "reply_ar", array()), 0, array(), "array"), "flag", array())) {
                    // line 95
                    echo "\t\t\t\t\t<ul class=\"destination\">
\t\t\t\t\t\t<li>";
                    // line 96
                    echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "replay_nick_name", array()), "html", null, true);
                    echo "</li>
\t\t\t\t\t\t<li>";
                    // line 97
                    echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "nick_name", array()), "html", null, true);
                    echo "</li>
\t\t\t\t\t</ul>

 \t\t\t\t\t<div class=\"comment_reply\">
\t\t\t\t\t\t<div class=\"comment\">
\t\t\t\t\t\t\t<p>
\t\t\t\t\t\t\t";
                    // line 103
                    echo nl2br($this->getAttribute($context["entry"], "reply_message", array()));
                    echo "
\t\t\t\t\t\t\t</p>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"content_bottom cf\">
\t\t\t\t\t\t\t<p class=\"post_date\">";
                    // line 107
                    echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "replay_cre_date", array()), "html", null, true);
                    echo "</p>

\t\t\t\t\t\t\t<ul class=\"btn_wrap\">
\t\t\t\t\t\t\t\t<li><a href=\"javascript:void(0);\" class=\"btn_style ";
                    // line 110
                    echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "replay_orange_color", array()), "html", null, true);
                    echo " btn_update_eval\" data-mess-id=\"";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "reply_message_id", array()), "html", null, true);
                    echo "\" data-user-id=\"";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "replay_owner_id", array()), "html", null, true);
                    echo "\">私もそう思う<span class=\"ic ic_heart\">";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "replay_evaluate", array()), "html", null, true);
                    echo "</span></a></li>
\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t\t";
                } else {
                    // line 115
                    echo "\t\t\t\t\t\t";
                    if ((((isset($context["is_own_replay"]) ? $context["is_own_replay"] : null) != $this->getAttribute($context["entry"], "response_id", array())) && ($this->getAttribute((isset($context["consultation_info"]) ? $context["consultation_info"] : null), "user_id", array()) == (isset($context["is_own_replay"]) ? $context["is_own_replay"] : null)))) {
                        // line 116
                        echo "\t\t\t\t\t\t<div class=\"comment_reply_input comment_reply";
                        echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "id", array()), "html", null, true);
                        echo "\">
\t\t\t\t\t\t\t<form method=\"post\" action=\"\" class=\"validate_reply";
                        // line 117
                        echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "id", array()), "html", null, true);
                        echo " validate\">
\t\t\t\t\t\t\t<input type=\"hidden\" name=\"message_id\" value=\"";
                        // line 118
                        echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "id", array()), "html", null, true);
                        echo "\">
\t\t\t\t\t\t\t<input type=\"hidden\" name=\"thread_id\" value=\"";
                        // line 119
                        echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "thread_id", array()), "html", null, true);
                        echo "\">
\t\t\t\t\t\t\t<input type=\"hidden\" name=\"replay_nick_name\" value=\"";
                        // line 120
                        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["consultation_info"]) ? $context["consultation_info"] : null), "nick_name", array()), "html", null, true);
                        echo "\">
\t\t\t\t\t\t\t<input type=\"hidden\" name=\"nick_name\" value=\"";
                        // line 121
                        echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "nick_name", array()), "html", null, true);
                        echo "\">
\t\t\t\t\t\t\t<h3 class=\"content_ttl\">返信コメント</h3>
\t\t\t\t\t\t\t<p class=\"error\" style=\"display:none;\">・本文を入力してください。</p>
\t\t\t\t\t\t\t<textarea class=\"consult_message\" name=\"consult_message\" placeholder=\"返信コメントを入力してください\" maxlength=\"300\"></textarea>
\t\t\t\t\t\t\t<p class=\"input_desc\">※全角300文字以内</p>
\t\t\t\t\t\t\t<ul class=\"btn_wrap\">
\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t<button type=\"submit\" class=\"btn_style btn_blue\">送信する</button>
\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t\t</form>
\t\t\t\t\t\t</div>\t\t\t\t\t\t
\t\t\t\t\t\t";
                    }
                    // line 134
                    echo "\t\t\t\t\t<script>
\t\t\t\t\t\$(function() {
\t\t\t\t\t\t\$(\".validate_reply";
                    // line 136
                    echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "id", array()), "html", null, true);
                    echo "\").validate({
\t\t\t\t\t\t\trules: {
\t\t\t\t\t\t\t\tconsult_message:{
\t\t\t\t\t\t\t\t\trequired: true
\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t},
\t\t\t\t\t\t\tmessages: {
\t\t\t\t\t\t\t\tconsult_message:{
\t\t\t\t\t\t\t\t\trequired: \"返信コメントを入力してください。\"
\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t},
\t\t\t\t\t\t\terrorElement: \"p\",
\t\t\t\t\t\t\terrorPlacement:function(error,element){
\t\t\t\t\t\t\t\terror.insertBefore(\$(element));
\t\t\t\t\t\t\t},
\t\t\t\t\t\t\tsubmitHandler: function(form) {
\t\t\t\t\t\t\t\tvar \$form = \$('.validate_reply";
                    // line 152
                    echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "id", array()), "html", null, true);
                    echo "');
\t\t\t\t\t\t\t\t\$.ajax({
\t\t\t\t\t\t\t\t    url:\"";
                    // line 154
                    echo twig_escape_filter($this->env, (isset($context["base_url"]) ? $context["base_url"] : null), "html", null, true);
                    echo "bb/reply_answer\",
\t\t\t\t\t\t\t        type:\"POST\",
\t\t\t\t\t\t\t        data:\$form.serialize(),
\t\t\t\t\t\t\t\t}).done(function(data){
\t\t\t\t\t\t\t\t\t\$('.comment_reply";
                    // line 158
                    echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "id", array()), "html", null, true);
                    echo "').replaceWith(data.html);
\t\t\t\t\t\t\t\t}).fail(function(data){
\t\t\t\t\t\t\t\t    alert('error!!!');
\t\t\t\t\t\t\t\t});
\t\t\t\t\t\t\t}
\t\t\t\t\t\t});
\t\t\t\t\t});
\t\t\t\t\t</script>
\t\t\t\t\t";
                }
                // line 167
                echo "
<!--\t\t\t\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['key2'], $context['entry2'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 168
            echo "  -->

\t\t\t\t</div>
\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['key'], $context['entry'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 172
        echo "\t\t\t</section>
\t\t\t";
        // line 173
        if (((twig_length_filter($this->env, (isset($context["response_ar"]) ? $context["response_ar"] : null)) == 10) && ($this->getAttribute((isset($context["consultation_info"]) ? $context["consultation_info"] : null), "comment_count", array()) > 10))) {
            // line 174
            echo "\t\t\t\t\t<div class=\"more_comment\" data-consult-id=\"";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["consultation_info"]) ? $context["consultation_info"] : null), "id", array()), "html", null, true);
            echo "\">
\t\t\t\t\t\t<ul class=\"btn_wrap_more\">
\t\t\t\t\t\t\t<li><a id=\"show_more_data\" href=\"javascript:void(0)\" class=\"btn_other\">もっと見る</a></li>
\t\t\t\t\t\t</ul>
\t\t\t\t\t</div>
\t\t\t";
        }
        // line 180
        echo "\t\t</div>
\t\t";
        // line 181
        echo twig_include($this->env, $context, "./inc/sidebar.html");
        echo "
\t</div>
\t";
        // line 183
        echo twig_include($this->env, $context, "./inc/footer.html");
        echo "
</div>
";
        // line 185
        echo twig_include($this->env, $context, "./inc/script.html");
        echo "
<script>
\$(function() {
\t//hide validate element
\t\$('.validate2 .error').hide();
\t\$('.error').hide();
\t
\t\$('input[type=text], textarea').blur(function(){
\t\t\$(this).val(\$(this).val().trim());
\t});
});
</script>
</body>
</html>
";
    }

    public function getTemplateName()
    {
        return "bb/consult.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  427 => 185,  422 => 183,  417 => 181,  414 => 180,  404 => 174,  402 => 173,  399 => 172,  390 => 168,  383 => 167,  371 => 158,  364 => 154,  359 => 152,  340 => 136,  336 => 134,  320 => 121,  316 => 120,  312 => 119,  308 => 118,  304 => 117,  299 => 116,  296 => 115,  282 => 110,  276 => 107,  269 => 103,  260 => 97,  256 => 96,  253 => 95,  251 => 94,  245 => 93,  231 => 88,  225 => 85,  221 => 83,  215 => 81,  212 => 80,  206 => 78,  200 => 76,  198 => 75,  193 => 74,  191 => 73,  186 => 72,  183 => 71,  180 => 70,  174 => 68,  172 => 67,  166 => 64,  163 => 63,  159 => 62,  152 => 57,  140 => 54,  137 => 53,  135 => 52,  124 => 50,  120 => 48,  114 => 44,  112 => 43,  107 => 41,  103 => 40,  99 => 39,  95 => 38,  87 => 34,  81 => 32,  79 => 31,  74 => 29,  68 => 28,  64 => 27,  58 => 26,  50 => 21,  33 => 7,  29 => 6,  25 => 5,  19 => 1,);
    }
}
