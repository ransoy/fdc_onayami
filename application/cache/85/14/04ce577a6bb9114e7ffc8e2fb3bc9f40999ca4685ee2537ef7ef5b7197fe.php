<?php

/* 404.html */
class __TwigTemplate_851404ce577a6bb9114e7ffc8e2fb3bc9f40999ca4685ee2537ef7ef5b7197fe extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<html lang=\"ja\">
<head>
<meta charset=\"utf-8\">
<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0, user-scalable=no, maximum-scale=1.0\">
<title>お問い合わせ</title>
";
        // line 7
        $this->loadTemplate("inc/link.html", "404.html", 7)->display($context);
        // line 8
        echo "</head>
<body>
<div class=\"page_wrap page_404\">
\t";
        // line 11
        $this->loadTemplate("inc/header.html", "404.html", 11)->display(array_merge($context, array("is_visitor" => (isset($context["is_visitor"]) ? $context["is_visitor"] : null))));
        // line 12
        echo "\t<section class=\"sec sec_404\">
\t\t<h2 class=\"sec_ttl\">ページが見つかません。</h2>
\t\t<div class=\"sec_inner\">
\t\t\t<p>ページは削除されたか、移動されたか、名前が変更されたか、または一時的に利用できない可能性があります。</p>
\t\t</div>
\t</section>
\t";
        // line 18
        $this->loadTemplate("inc/footer.html", "404.html", 18)->display($context);
        // line 19
        echo "</div>
";
        // line 20
        $this->loadTemplate("inc/script.html", "404.html", 20)->display($context);
        // line 21
        echo "</body>
</html>
";
    }

    public function getTemplateName()
    {
        return "404.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  51 => 21,  49 => 20,  46 => 19,  44 => 18,  36 => 12,  34 => 11,  29 => 8,  27 => 7,  19 => 1,);
    }
}
