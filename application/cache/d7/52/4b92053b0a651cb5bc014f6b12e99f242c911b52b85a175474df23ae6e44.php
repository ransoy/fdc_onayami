<?php

/* index.html */
class __TwigTemplate_d7524b92053b0a651cb5bc014f6b12e99f242c911b52b85a175474df23ae6e44 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!doctype html>
<html>
<head>
<meta charset=\"utf-8\">
<title>女子の性のお悩み相談室|ジョイスペ</title>
<meta name=\"description\" content=\"セックス、恋愛、からだ、ひとりエッチなど「女子だけ」でお悩みを相談することができます。\">
<link rel=\"stylesheet\" href=\"";
        // line 7
        echo twig_escape_filter($this->env, (isset($context["base_url"]) ? $context["base_url"] : null), "html", null, true);
        echo "pc/css/reset.css\">
<link rel=\"stylesheet\" href=\"";
        // line 8
        echo twig_escape_filter($this->env, (isset($context["base_url"]) ? $context["base_url"] : null), "html", null, true);
        echo "pc/css/html5reset.css\">
<link rel=\"stylesheet\" href=\"";
        // line 9
        echo twig_escape_filter($this->env, (isset($context["base_url"]) ? $context["base_url"] : null), "html", null, true);
        echo "pc/css/adjust.css\">
<link rel=\"stylesheet\" href=\"";
        // line 10
        echo twig_escape_filter($this->env, (isset($context["base_url"]) ? $context["base_url"] : null), "html", null, true);
        echo "pc/css/common.css\">
<link rel=\"stylesheet\" href=\"";
        // line 11
        echo twig_escape_filter($this->env, (isset($context["base_url"]) ? $context["base_url"] : null), "html", null, true);
        echo "pc/css/component.css\">
<link rel=\"stylesheet\" href=\"";
        // line 12
        echo twig_escape_filter($this->env, (isset($context["base_url"]) ? $context["base_url"] : null), "html", null, true);
        echo "pc/css/font-awesome.min.css\">
<link rel=\"stylesheet\" href=\"";
        // line 13
        echo twig_escape_filter($this->env, (isset($context["base_url"]) ? $context["base_url"] : null), "html", null, true);
        echo "pc/css/style.css\">
<link rel=\"stylesheet\" href=\"";
        // line 14
        echo twig_escape_filter($this->env, (isset($context["base_url"]) ? $context["base_url"] : null), "html", null, true);
        echo "pc/css/category_icons.css\">
<script src=\"";
        // line 15
        echo twig_escape_filter($this->env, (isset($context["base_url"]) ? $context["base_url"] : null), "html", null, true);
        echo "pc/js/jquery-1.11.0.js\"></script>
</head>

<body>
<div class=\"page_wrap page_index\">
\t";
        // line 20
        echo twig_include($this->env, $context, "./inc/header.html");
        echo "
\t<div class=\"block content_block_1\">
\t\t<div class=\"content_wrap cf\">
\t\t\t<div class=\"content\">
\t\t\t\t<section class=\"sec sec_search m_b_30\">
\t\t\t\t\t<div class=\"sec_inner\">
\t\t\t\t\t\t<div class=\"sec_block m_b_30\">
\t\t\t\t\t\t\t<h2 class=\"sec_ttl\">キーワードから探す</h2>
\t\t\t\t\t\t\t<p class=\"sec_desc\">同じ悩みを持った人の相談をここで確認！</p>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t
\t\t\t\t\t\t<div class=\"sec_block\">
\t\t\t\t\t\t\t<p id=\"search_error\"></p>
\t\t\t\t\t\t\t<table class=\"sec_search_table m_b_20 validate\">
\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t<td class=\"ic ic_search\"></td>
\t\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t\t<label for=\"\"><input type=\"text\" name=\"input_keyword\" placeholder=\"生理中のオナニー 潮ふき方法 イケない\"></label></td>
\t\t\t\t\t\t\t\t\t<td class=\"search_btn\">
\t\t\t\t\t\t\t\t\t\t<ul class=\"btn_wrap\">
\t\t\t\t\t\t\t\t\t\t\t<li><button id=\"btn_keyword\" type=\"submit\" class=\"btn_style btn_blue\">探す</button></li>
\t\t\t\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t</table>
\t\t\t\t\t\t\t";
        // line 45
        if ((twig_length_filter($this->env, (isset($context["topViewed_ar"]) ? $context["topViewed_ar"] : null)) > 0)) {
            // line 46
            echo "\t\t\t\t\t\t\t<ul class=\"search_tag\">
\t\t\t\t\t\t\t\t<li><p>注目の相談：</p></li>
\t\t\t\t\t\t\t\t";
            // line 48
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["topViewed_ar"]) ? $context["topViewed_ar"] : null));
            foreach ($context['_seq'] as $context["key"] => $context["entry"]) {
                // line 49
                echo "\t\t\t\t\t\t\t\t<li><a href=\"";
                echo twig_escape_filter($this->env, (isset($context["base_url"]) ? $context["base_url"] : null), "html", null, true);
                echo "cate";
                echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "big_cate_id", array()), "html", null, true);
                echo "/";
                echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "cate_id", array()), "html", null, true);
                echo "\" class=\"t_link\">";
                echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "name", array()), "html", null, true);
                echo "</a></li>
\t\t\t\t\t\t\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['key'], $context['entry'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 51
            echo "\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t\t";
        }
        // line 53
        echo "\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</section>
\t\t\t\t<section class=\"sec sec_category m_b_30\">
\t\t\t\t\t<div class=\"sec_block\">
\t\t\t\t\t\t<h2 class=\"sec_ttl\">カテゴリーから探す</h2>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"sec_inner\">

\t\t                ";
        // line 72
        echo "\t\t                
\t\t                ";
        // line 73
        echo twig_include($this->env, $context, "./index_category.html");
        echo "

\t\t\t\t\t\t<div class=\"create_consultation\">
\t\t\t\t\t\t\t<ul class=\"btn_wrap\">
\t\t\t\t\t\t\t\t<li><a href=\"";
        // line 77
        echo twig_escape_filter($this->env, (isset($context["base_url"]) ? $context["base_url"] : null), "html", null, true);
        echo "bb/posting/\" class=\"btn_style btn_green\">新しく相談する</a></li>
\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t</div>

\t\t\t\t\t</div>
\t\t\t\t</section>
\t\t\t</div>
\t\t\t";
        // line 84
        echo twig_include($this->env, $context, "./inc/sidebar.html");
        echo "
\t\t</div>
\t</div>

\t<div class=\"content_block_2\">
\t\t\t<section class=\"sec sec_consultation_list p_b_30\">
\t\t\t\t<h2 class=\"sec_ttl ic ic_comments m_b_30\" id=\"consultation_list\">お悩み相談一覧</h2>\t\t\t\t
\t\t\t\t<form id=\"answer\" name=\"answer\" method=\"post\" action=\"";
        // line 91
        echo twig_escape_filter($this->env, (isset($context["base_url"]) ? $context["base_url"] : null), "html", null, true);
        echo "bb/answer\">
\t\t\t\t\t<input type=\"hidden\" name=\"answer_id\" id=\"answer_id\" ></input>
\t\t\t\t\t<input type=\"hidden\" name=\"bid\" id=\"bid\" ></input>
\t\t\t\t\t<input type=\"hidden\" name=\"subid\" id=\"subid\" ></input>
\t\t\t\t</form>\t
\t\t\t\t<ul class=\"consultation_list_wrap\">
\t\t\t\t";
        // line 97
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["consultation_list_ar"]) ? $context["consultation_list_ar"] : null));
        foreach ($context['_seq'] as $context["key"] => $context["entry"]) {
            // line 98
            echo "\t\t\t\t\t<li class=\"consultation_list\">
\t\t\t\t\t\t<a href=\"";
            // line 99
            echo twig_escape_filter($this->env, (isset($context["base_url"]) ? $context["base_url"] : null), "html", null, true);
            echo "cate";
            echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "big_cate_id", array()), "html", null, true);
            echo "/";
            echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "cate_id", array()), "html", null, true);
            echo "/";
            echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "id", array()), "html", null, true);
            echo "\">
\t\t\t\t\t\t\t<div class=\"sec_box_top\">
\t\t\t\t\t\t\t\t<p class=\"sec_box_category icon16 ";
            // line 101
            echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "class", array()), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "big_category_name", array()), "html", null, true);
            echo "</p>
\t\t\t\t\t\t\t\t<p class=\"sec_box_category_detail\"><span>";
            // line 102
            echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "category_name", array()), "html", null, true);
            echo "</span></p>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<h3 class=\"sec_box_ttl\">";
            // line 104
            echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "title", array()), "html", null, true);
            echo "</h3>
\t\t\t\t\t\t\t";
            // line 105
            if ((twig_length_filter($this->env, trim(strip_tags($this->getAttribute($context["entry"], "message", array())))) > 41)) {
                // line 106
                echo "\t\t\t\t\t\t\t\t<p class=\"sec_box_content m_b_10\">";
                echo twig_escape_filter($this->env, twig_slice($this->env, trim(strip_tags($this->getAttribute($context["entry"], "message", array()))), 0, 41), "html", null, true);
                echo "...</p>
\t\t\t\t\t\t\t";
            } else {
                // line 108
                echo "\t\t\t\t\t\t\t\t<p class=\"sec_box_content m_b_10\">";
                echo twig_escape_filter($this->env, trim(strip_tags($this->getAttribute($context["entry"], "message", array()))), "html", null, true);
                echo "</p>
\t\t\t\t\t\t\t";
            }
            // line 110
            echo "\t\t\t\t\t\t\t<ul class=\"btn_wrap t_center m_b_20\">
\t\t\t\t\t\t\t\t<li><p class=\"btn_style btn_blue\">続きを読む</p></li>
\t\t\t\t\t\t\t\t<li><a onclick=\"set_answer(";
            // line 112
            echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "big_cate_id", array()), "html", null, true);
            echo ", ";
            echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "cate_id", array()), "html", null, true);
            echo ", ";
            echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "id", array()), "html", null, true);
            echo ");\" class=\"btn_style btn_green\">回答する</a></li>
\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t\t<div class=\"sec_box_bottom\">
\t\t\t\t\t\t\t\t<p class=\"posted_data\">";
            // line 115
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["entry"], "create_date", array()), "Y.m.d"), "html", null, true);
            echo "</p>
\t\t\t\t\t\t\t\t<ul class=\"posted_ability\">
\t\t\t\t\t\t\t\t\t";
            // line 117
            if (($this->getAttribute($context["entry"], "like_count", array()) > 0)) {
                // line 118
                echo "\t\t\t\t\t\t\t\t\t<li><p class=\"ic ic_heart\">";
                echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "like_count", array()), "html", null, true);
                echo "</p></li>
\t\t\t\t\t\t\t\t\t";
            } else {
                // line 120
                echo "\t\t\t\t\t\t\t\t\t<li><p class=\"ic ic_heart-o\">";
                echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "like_count", array()), "html", null, true);
                echo "</p></li>
\t\t\t\t\t\t\t\t\t";
            }
            // line 122
            echo "\t\t\t\t\t\t\t\t\t";
            if (($this->getAttribute($context["entry"], "comment_count", array()) > 0)) {
                // line 123
                echo "\t\t\t\t\t\t\t\t\t<li><p class=\"ic ic_comment\">";
                echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "comment_count", array()), "html", null, true);
                echo "</p></li>
\t\t\t\t\t\t\t\t\t";
            } else {
                // line 125
                echo "\t\t\t\t\t\t\t\t\t<li><p class=\"ic ic_comment-o\">";
                echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "comment_count", array()), "html", null, true);
                echo "</p></li>
\t\t\t\t\t\t\t\t\t";
            }
            // line 127
            echo "\t\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</a>
\t\t\t\t\t</li>
\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['key'], $context['entry'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 132
        echo "\t\t\t\t</ul>
\t\t\t\t<ul class=\"pager_wrap\">
\t\t\t\t";
        // line 134
        echo (isset($context["pagination"]) ? $context["pagination"] : null);
        echo "
\t\t\t\t</ul>
\t\t\t</section>
\t</div>

\t<div class=\"content_block_3\">
\t\t<div class=\"content_wrap\">
\t\t\t<section class=\"sec sec_site_notice\">
\t\t\t\t<p>お悩み相談室では女性の性に関する悩みを自由に相談できる掲示板です。匿名で相談または回答することができるので恥ずかしいあの話も気軽に話すことができます。一人で悩まずみんなで女性の性を語り合って解決しましょう！</p>
\t\t\t</section>
\t\t</div>
\t</div>
\t";
        // line 146
        echo twig_include($this->env, $context, "./inc/footer.html");
        echo "
</div>
<script src=\"";
        // line 148
        echo twig_escape_filter($this->env, (isset($context["base_url"]) ? $context["base_url"] : null), "html", null, true);
        echo "pc/js/home.js\"></script>
<script src=\"";
        // line 149
        echo twig_escape_filter($this->env, (isset($context["base_url"]) ? $context["base_url"] : null), "html", null, true);
        echo "pc/js/pagination.js\"></script>
<script>
\t\$(function(){
\t\tif(\$('.search_tag li').length == 0) {
\t\t\t\$('.search_tag').css('border-top','none');
\t\t}

\t\t\$(\"input[name='input_keyword']\").keypress(function(e) {
\t\t    if(e.which == 13) {
\t\t        searchKeyword()
\t\t    }
\t\t});
\t\t
\t\t\$(\"#btn_keyword\").on('click', function(){
\t\t\tsearchKeyword()
\t\t});
\t});

\tfunction set_answer(big_cate_id, cate_id, thread_id){
\t\tdocument.answer.bid.value = big_cate_id;
\t\tdocument.answer.subid.value = cate_id;
\t\tdocument.answer.answer_id.value = thread_id;
\t\tdocument.answer.submit();
\t\treturn false;
\t}

\tfunction searchKeyword() {
\t\tvar val = \$(\"input[name='input_keyword']\").val();
\t\t\$(\"input[name='input_keyword']\").val(val.trim());
\t\tif (val.trim() != '') {
\t\t\tval = val.trim();
\t\t\tpost_cont = \$(\".search_tag\");
\t\t\tcurrent_count = post_cont.children().length;
\t\t\turl = \"";
        // line 182
        echo twig_escape_filter($this->env, (isset($context["base_url"]) ? $context["base_url"] : null), "html", null, true);
        echo "search/ajaxSearchKeywordConsultation\";
\t\t\tparams = { keyword: val.trim() };
\t\t\t\$.ajax({
\t\t\t\ttype: \"POST\",
\t\t\t\turl: url,
\t\t\t\tdata: params,
\t\t\t\tdataType: 'TEXT',
\t\t\t\tcache: false,
\t\t\t\tsuccess: function(data){
\t\t\t\t\tif(data != \"\") {
\t\t\t\t\t\t\$('#search_error').text('').hide();
\t\t\t\t\t\twindow.location.href = \"";
        // line 193
        echo twig_escape_filter($this->env, (isset($context["base_url"]) ? $context["base_url"] : null), "html", null, true);
        echo "search/?input_keyword=\"+val;
\t\t\t\t\t} else {
\t\t\t\t\t\tvar search_error = \$('#search_error');
\t\t\t\t\t\tsearch_error.css('background-color', '#ef4f5a');
\t\t\t\t\t\tsearch_error.css('color', '#fff');
\t\t\t\t\t\tsearch_error.css('border-radius', '4px');
\t\t\t\t\t\tsearch_error.css('padding', '10px');
\t\t\t\t\t\tsearch_error.css('margin', '0 0 10px');
\t\t\t\t\t\tsearch_error.text('キーワードが見つかりません。').show();
\t\t\t\t\t}
\t\t\t\t}
\t\t\t});
\t\t} else {
\t\t\t\$('#search_error').text('').hide();
\t\t}
\t}
</script> 
</body>
</html>
";
    }

    public function getTemplateName()
    {
        return "index.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  357 => 193,  343 => 182,  307 => 149,  303 => 148,  298 => 146,  283 => 134,  279 => 132,  269 => 127,  263 => 125,  257 => 123,  254 => 122,  248 => 120,  242 => 118,  240 => 117,  235 => 115,  225 => 112,  221 => 110,  215 => 108,  209 => 106,  207 => 105,  203 => 104,  198 => 102,  192 => 101,  181 => 99,  178 => 98,  174 => 97,  165 => 91,  155 => 84,  145 => 77,  138 => 73,  135 => 72,  124 => 53,  120 => 51,  105 => 49,  101 => 48,  97 => 46,  95 => 45,  67 => 20,  59 => 15,  55 => 14,  51 => 13,  47 => 12,  43 => 11,  39 => 10,  35 => 9,  31 => 8,  27 => 7,  19 => 1,);
    }
}
