<?php

/* ./inc/header.html */
class __TwigTemplate_f24489854647e1097bc7a4ee7a0de585688cb05a90ecab96d7ecca50373cb9c1 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<header class=\"header_wrap\">
\t<div class=\"header\">
\t\t<div class=\"header_inner cf\">
\t\t\t<div class=\"left_box\">
\t\t\t\t<h1 class=\"site_logo\"><a href=\"";
        // line 5
        echo twig_escape_filter($this->env, (isset($context["base_url"]) ? $context["base_url"] : null), "html", null, true);
        echo "\"><img src=\"";
        echo twig_escape_filter($this->env, (isset($context["base_url"]) ? $context["base_url"] : null), "html", null, true);
        echo "pc/image/common/logo.png\" alt=\"ロゴ\"></a></h1>
\t\t\t</div>
\t\t\t<div class=\"right_box\">
\t\t\t\t<div class=\"header_block m_b_10\">
\t\t\t\t\t<ul class=\"header_banner_area\">
\t\t\t\t\t\t<li><a href=\"";
        // line 10
        echo twig_escape_filter($this->env, (isset($context["main_url"]) ? $context["main_url"] : null), "html", null, true);
        echo "\" alt=\"風俗求人高収入アルバイト\"><img src=\"";
        echo twig_escape_filter($this->env, (isset($context["base_url"]) ? $context["base_url"] : null), "html", null, true);
        echo "pc/image/banner/sample_2.gif\" alt=\"バナー\"></a></li>
\t\t\t\t\t</ul>
\t\t\t\t\t<ul class=\"header_site_ability\">
\t\t\t\t\t\t<li>会員数：<span>";
        // line 13
        echo twig_escape_filter($this->env, (isset($context["total_users"]) ? $context["total_users"] : null), "html", null, true);
        echo "</span>人</li>
\t\t\t\t\t</ul>
\t\t\t\t</div>
\t\t\t\t<div class=\"header_block\">
\t\t\t\t\t<ul class=\"header_user_menu\">
\t\t\t\t\t\t<li><a href=\"";
        // line 18
        echo twig_escape_filter($this->env, (isset($context["main_url"]) ? $context["main_url"] : null), "html", null, true);
        echo "user/settings/\" class=\"ic ic_cog\">登録情報確認・変更</a></li>
\t\t\t\t\t\t<li><a href=\"";
        // line 19
        echo twig_escape_filter($this->env, (isset($context["base_url"]) ? $context["base_url"] : null), "html", null, true);
        echo "faq\">FAQ</a></li>
\t\t\t\t\t</ul>
\t\t\t\t\t<ul class=\"header_account_menu btn_wrap\">
\t\t\t\t\t\t<form id=\"form_signup\" method=\"post\" action=\"";
        // line 22
        echo twig_escape_filter($this->env, (isset($context["main_url"]) ? $context["main_url"] : null), "html", null, true);
        echo "user/signup\" name=\"form_signup\">
\t\t\t\t\t\t<input type=\"hidden\" name=\"onayamisignup\" value=\"onayami\">
\t\t\t\t\t\t</form>
\t\t\t\t\t\t<form id=\"form_login\" method=\"post\" action=\"";
        // line 25
        echo twig_escape_filter($this->env, (isset($context["main_url"]) ? $context["main_url"] : null), "html", null, true);
        echo "user/login\" name=\"form_login\">
\t\t\t\t\t\t<input type=\"hidden\" name=\"onayamilogin\" value=\"onayami\">
\t\t\t\t\t\t</form>
\t\t\t\t\t\t";
        // line 28
        if ((isset($context["user_login_flag"]) ? $context["user_login_flag"] : null)) {
            // line 29
            echo "\t\t\t\t\t\t<li><a href=\"";
            echo twig_escape_filter($this->env, (isset($context["base_url"]) ? $context["base_url"] : null), "html", null, true);
            echo "user/logout\" class=\"logout btn_style btn_gray\">ログアウト</a></li>
\t\t\t\t\t\t";
        } else {
            // line 31
            echo "\t\t\t\t\t\t<li><a href=\"javascript:void(0);\" class=\"onayami_login btn_style btn_green\">ログイン</a></li>
\t\t\t\t\t\t<li><a href=\"javascript:void(0);\" class=\"onayami_signup btn_style btn_magenta\">新規会員登録</a></li>
\t\t\t\t\t\t";
        }
        // line 34
        echo "\t\t\t\t\t</ul>
\t\t\t\t</div>
\t\t\t</div>
\t\t</div>
\t</div>
\t<nav class=\"header_nav\">
\t\t<div class=\"header_inner\">
\t\t\t<ul>
\t\t\t\t<li><a href=\"";
        // line 42
        echo twig_escape_filter($this->env, (isset($context["base_url"]) ? $context["base_url"] : null), "html", null, true);
        echo "\" class=\"ic ic_home\">ホーム</a></li>
\t\t\t\t";
        // line 43
        if ((isset($context["user_login_flag"]) ? $context["user_login_flag"] : null)) {
            // line 44
            echo "\t\t\t\t<li><a href=\"";
            echo twig_escape_filter($this->env, (isset($context["base_url"]) ? $context["base_url"] : null), "html", null, true);
            echo "user/my_page\" class=\"ic ic_female\">マイページ</a></li>
\t\t\t\t<li><a href=\"";
            // line 45
            echo twig_escape_filter($this->env, (isset($context["base_url"]) ? $context["base_url"] : null), "html", null, true);
            echo "bb/posting/\" class=\"ic ic_comments\">無料で相談</a></li>
\t\t\t\t";
        }
        // line 47
        echo "\t\t\t</ul>
\t\t</div>
\t</nav>
\t<nav class=\"breadcrumb\">
\t\t<div class=\"header_inner\">
\t\t\t<ul>
\t\t\t";
        // line 53
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["breadcrumb_list"]) ? $context["breadcrumb_list"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["entry"]) {
            // line 54
            echo "\t\t\t\t";
            if (($this->getAttribute($context["entry"], "url", array()) == "")) {
                // line 55
                echo "\t\t\t\t<li><a href=\"\">";
                echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "text", array()), "html", null, true);
                echo "</a></li>
\t\t\t\t";
            } else {
                // line 57
                echo "\t\t\t\t<li><a href=\"";
                echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "url", array()), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "text", array()), "html", null, true);
                echo "</a></li>
\t\t\t\t";
            }
            // line 59
            echo "<!--\t\t\t\t<li><a href=\"";
            echo twig_escape_filter($this->env, (isset($context["base_url"]) ? $context["base_url"] : null), "html", null, true);
            echo "\">トップページ</a></li> -->
<!--\t\t\t\t<li><a href=\"";
            // line 60
            echo twig_escape_filter($this->env, (isset($context["base_url"]) ? $context["base_url"] : null), "html", null, true);
            echo "\">次のぱんくず</a></li> -->
\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['entry'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 62
        echo "\t\t\t</ul>
\t\t</div>
\t</nav>
</header>
<script type=\"text/javascript\">
\$(function() {
\t\$('.onayami_signup').on('click', function(){
\t\t\$('#form_signup').submit();
\t});
\t\$('.onayami_login').on('click', function(){
\t\t\$('#form_login').submit();
\t});
});
</script> 

";
    }

    public function getTemplateName()
    {
        return "./inc/header.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  154 => 62,  146 => 60,  141 => 59,  133 => 57,  127 => 55,  124 => 54,  120 => 53,  112 => 47,  107 => 45,  102 => 44,  100 => 43,  96 => 42,  86 => 34,  81 => 31,  75 => 29,  73 => 28,  67 => 25,  61 => 22,  55 => 19,  51 => 18,  43 => 13,  35 => 10,  25 => 5,  19 => 1,);
    }
}
