<?php

/* theme_url_template.html */
class __TwigTemplate_0f766aa529a44ce49556d2c748ee71ee2ab9b8027e80983652b809b749c0e2fc extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div style=\"margin-top: 20px;\">
<p id=\"bl";
        // line 2
        echo twig_escape_filter($this->env, (isset($context["info_num"]) ? $context["info_num"] : null), "html", null, true);
        echo "_title\" class=\"bl_title_all\" >";
        echo $this->getAttribute((isset($context["info"]) ? $context["info"] : null), "title", array());
        echo "</p>
<input type=\"hidden\" name=\"bl_title[";
        // line 3
        echo twig_escape_filter($this->env, (isset($context["info_num"]) ? $context["info_num"] : null), "html", null, true);
        echo "]\" value=\"";
        echo $this->getAttribute((isset($context["info"]) ? $context["info"] : null), "title", array());
        echo "\">
<p id=\"bl";
        // line 4
        echo twig_escape_filter($this->env, (isset($context["info_num"]) ? $context["info_num"] : null), "html", null, true);
        echo "_description\" class=\"bl_description_all\" >";
        echo $this->getAttribute((isset($context["info"]) ? $context["info"] : null), "description", array());
        echo "</p>
<input type=\"hidden\" name=\"bl_description[";
        // line 5
        echo twig_escape_filter($this->env, (isset($context["info_num"]) ? $context["info_num"] : null), "html", null, true);
        echo "]\" value=\"";
        echo $this->getAttribute((isset($context["info"]) ? $context["info"] : null), "description", array());
        echo "\">
<p><img id=\"bl";
        // line 6
        echo twig_escape_filter($this->env, (isset($context["info_num"]) ? $context["info_num"] : null), "html", null, true);
        echo "\" src=\"\" alt=\"\"></p>
<input type=\"hidden\" name=\"bl[";
        // line 7
        echo twig_escape_filter($this->env, (isset($context["info_num"]) ? $context["info_num"] : null), "html", null, true);
        echo "]\" value=\"\">
<input type=\"hidden\" name=\"bl_source_url[";
        // line 8
        echo twig_escape_filter($this->env, (isset($context["info_num"]) ? $context["info_num"] : null), "html", null, true);
        echo "]\" value=\"";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["info"]) ? $context["info"] : null), "source_url", array()), "html", null, true);
        echo "\">
<p class = \"image_change_btn\" style=\"border-bottom:1px dashed #aaa;\">
\t<a href=\"javascript:void(0)\" id=\"bla";
        // line 10
        echo twig_escape_filter($this->env, (isset($context["info_num"]) ? $context["info_num"] : null), "html", null, true);
        echo "_pre\" class=\"void\">前の画像</a>｜
\t<a href=\"javascript:void(0)\" id=\"bla";
        // line 11
        echo twig_escape_filter($this->env, (isset($context["info_num"]) ? $context["info_num"] : null), "html", null, true);
        echo "_next\" class=\"void\">次の画像</a>｜
\t<a href=\"javascript:void(0)\" id=\"bla";
        // line 12
        echo twig_escape_filter($this->env, (isset($context["info_num"]) ? $context["info_num"] : null), "html", null, true);
        echo "_none\" class=\"void\">画像なし</a>｜
\t<a href=\"javascript:void(0)\" id=\"bla";
        // line 13
        echo twig_escape_filter($this->env, (isset($context["info_num"]) ? $context["info_num"] : null), "html", null, true);
        echo "_img_only\" class=\"void\">画像のみ</a>
</p>
</div>
<script class = \"image_change_btn\">
\$(function(){
\timg_select_array[";
        // line 18
        echo twig_escape_filter($this->env, (isset($context["info_num"]) ? $context["info_num"] : null), "html", null, true);
        echo "] = 0;

\t\$bl";
        // line 20
        echo twig_escape_filter($this->env, (isset($context["info_num"]) ? $context["info_num"] : null), "html", null, true);
        echo " = \$('#bl";
        echo twig_escape_filter($this->env, (isset($context["info_num"]) ? $context["info_num"] : null), "html", null, true);
        echo "');

\tvar img_url";
        // line 22
        echo twig_escape_filter($this->env, (isset($context["info_num"]) ? $context["info_num"] : null), "html", null, true);
        echo " = [];

//\timg_url";
        // line 24
        echo twig_escape_filter($this->env, (isset($context["info_num"]) ? $context["info_num"] : null), "html", null, true);
        echo "[0] = \"/pc/image/common/no_image.jpg\";
\timg_url";
        // line 25
        echo twig_escape_filter($this->env, (isset($context["info_num"]) ? $context["info_num"] : null), "html", null, true);
        echo "[0] = \"\";

\t";
        // line 27
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["info"]) ? $context["info"] : null), "image_ar", array()));
        foreach ($context['_seq'] as $context["key2"] => $context["ar2"]) {
            // line 28
            echo "\t\timg_url";
            echo twig_escape_filter($this->env, (isset($context["info_num"]) ? $context["info_num"] : null), "html", null, true);
            echo "[";
            echo twig_escape_filter($this->env, ($context["key2"] + 1), "html", null, true);
            echo "] = \"";
            echo $context["ar2"];
            echo "\";
\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['key2'], $context['ar2'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 30
        echo "
\tif(img_url";
        // line 31
        echo twig_escape_filter($this->env, (isset($context["info_num"]) ? $context["info_num"] : null), "html", null, true);
        echo "[1]){
\t\t\$bl";
        // line 32
        echo twig_escape_filter($this->env, (isset($context["info_num"]) ? $context["info_num"] : null), "html", null, true);
        echo ".attr('src',img_url";
        echo twig_escape_filter($this->env, (isset($context["info_num"]) ? $context["info_num"] : null), "html", null, true);
        echo "[1]);
\t\t\$('input[name=\"bl[";
        // line 33
        echo twig_escape_filter($this->env, (isset($context["info_num"]) ? $context["info_num"] : null), "html", null, true);
        echo "]\"]').val(img_url";
        echo twig_escape_filter($this->env, (isset($context["info_num"]) ? $context["info_num"] : null), "html", null, true);
        echo "[1]);
\t}

\tvar num = 0;
\tvar num = ";
        // line 37
        echo twig_escape_filter($this->env, twig_length_filter($this->env, $this->getAttribute((isset($context["info"]) ? $context["info"] : null), "image_ar", array())), "html", null, true);
        echo ";

    var select_num";
        // line 39
        echo twig_escape_filter($this->env, (isset($context["info_num"]) ? $context["info_num"] : null), "html", null, true);
        echo "=1;
    var max";
        // line 40
        echo twig_escape_filter($this->env, (isset($context["info_num"]) ? $context["info_num"] : null), "html", null, true);
        echo "=num;

\t\$('#bla";
        // line 42
        echo twig_escape_filter($this->env, (isset($context["info_num"]) ? $context["info_num"] : null), "html", null, true);
        echo "_next').on('click', function(){
\t\tif(select_num";
        // line 43
        echo twig_escape_filter($this->env, (isset($context["info_num"]) ? $context["info_num"] : null), "html", null, true);
        echo ">=max";
        echo twig_escape_filter($this->env, (isset($context["info_num"]) ? $context["info_num"] : null), "html", null, true);
        echo ")return false;
\t\tselect_num";
        // line 44
        echo twig_escape_filter($this->env, (isset($context["info_num"]) ? $context["info_num"] : null), "html", null, true);
        echo " = select_num";
        echo twig_escape_filter($this->env, (isset($context["info_num"]) ? $context["info_num"] : null), "html", null, true);
        echo "+1;
\t\t\$bl";
        // line 45
        echo twig_escape_filter($this->env, (isset($context["info_num"]) ? $context["info_num"] : null), "html", null, true);
        echo ".attr('src',img_url";
        echo twig_escape_filter($this->env, (isset($context["info_num"]) ? $context["info_num"] : null), "html", null, true);
        echo "[select_num";
        echo twig_escape_filter($this->env, (isset($context["info_num"]) ? $context["info_num"] : null), "html", null, true);
        echo "]);
\t\t\$('input[name=\"bl[";
        // line 46
        echo twig_escape_filter($this->env, (isset($context["info_num"]) ? $context["info_num"] : null), "html", null, true);
        echo "]\"]').val(img_url";
        echo twig_escape_filter($this->env, (isset($context["info_num"]) ? $context["info_num"] : null), "html", null, true);
        echo "[select_num";
        echo twig_escape_filter($this->env, (isset($context["info_num"]) ? $context["info_num"] : null), "html", null, true);
        echo "]);
\t\t//\$('#st2').html(select_num";
        // line 47
        echo twig_escape_filter($this->env, (isset($context["key"]) ? $context["key"] : null), "html", null, true);
        echo ");
\t\timg_select_array[";
        // line 48
        echo twig_escape_filter($this->env, (isset($context["info_num"]) ? $context["info_num"] : null), "html", null, true);
        echo "]=select_num";
        echo twig_escape_filter($this->env, (isset($context["info_num"]) ? $context["info_num"] : null), "html", null, true);
        echo ";
\t});

\t\$('#bla";
        // line 51
        echo twig_escape_filter($this->env, (isset($context["info_num"]) ? $context["info_num"] : null), "html", null, true);
        echo "_pre').on('click', function(){
\t\tif(select_num";
        // line 52
        echo twig_escape_filter($this->env, (isset($context["info_num"]) ? $context["info_num"] : null), "html", null, true);
        echo "==1)return false;
\t\tselect_num";
        // line 53
        echo twig_escape_filter($this->env, (isset($context["info_num"]) ? $context["info_num"] : null), "html", null, true);
        echo "=select_num";
        echo twig_escape_filter($this->env, (isset($context["info_num"]) ? $context["info_num"] : null), "html", null, true);
        echo "-1;
\t\t\$bl";
        // line 54
        echo twig_escape_filter($this->env, (isset($context["info_num"]) ? $context["info_num"] : null), "html", null, true);
        echo ".attr('src',img_url";
        echo twig_escape_filter($this->env, (isset($context["info_num"]) ? $context["info_num"] : null), "html", null, true);
        echo "[select_num";
        echo twig_escape_filter($this->env, (isset($context["info_num"]) ? $context["info_num"] : null), "html", null, true);
        echo "]);
\t\t\$('input[name=\"bl[";
        // line 55
        echo twig_escape_filter($this->env, (isset($context["info_num"]) ? $context["info_num"] : null), "html", null, true);
        echo "]\"]').val(img_url";
        echo twig_escape_filter($this->env, (isset($context["info_num"]) ? $context["info_num"] : null), "html", null, true);
        echo "[select_num";
        echo twig_escape_filter($this->env, (isset($context["info_num"]) ? $context["info_num"] : null), "html", null, true);
        echo "]);
\t\t//\$('#st2').html(select_num2);
\t\timg_select_array[";
        // line 57
        echo twig_escape_filter($this->env, (isset($context["info_num"]) ? $context["info_num"] : null), "html", null, true);
        echo "]=select_num";
        echo twig_escape_filter($this->env, (isset($context["info_num"]) ? $context["info_num"] : null), "html", null, true);
        echo ";
\t});

\t\$('#bla";
        // line 60
        echo twig_escape_filter($this->env, (isset($context["info_num"]) ? $context["info_num"] : null), "html", null, true);
        echo "_none').on('click', function(){
\t\t\$img_sw = \$('#bl";
        // line 61
        echo twig_escape_filter($this->env, (isset($context["info_num"]) ? $context["info_num"] : null), "html", null, true);
        echo "').css(\"display\");
\t\tif(\$img_sw == 'none') {
\t\t\t\$('#bl";
        // line 63
        echo twig_escape_filter($this->env, (isset($context["info_num"]) ? $context["info_num"] : null), "html", null, true);
        echo "').show();
\t\t\t\$('#bla";
        // line 64
        echo twig_escape_filter($this->env, (isset($context["info_num"]) ? $context["info_num"] : null), "html", null, true);
        echo "_none').text('画像なし');
\t\t\t\$bl";
        // line 65
        echo twig_escape_filter($this->env, (isset($context["info_num"]) ? $context["info_num"] : null), "html", null, true);
        echo ".attr('src',img_url";
        echo twig_escape_filter($this->env, (isset($context["info_num"]) ? $context["info_num"] : null), "html", null, true);
        echo "[1]);
\t\t\t\$('input[name=\"bl[";
        // line 66
        echo twig_escape_filter($this->env, (isset($context["info_num"]) ? $context["info_num"] : null), "html", null, true);
        echo "]\"]').val(img_url";
        echo twig_escape_filter($this->env, (isset($context["info_num"]) ? $context["info_num"] : null), "html", null, true);
        echo "[1]);
\t\t\timg_select_array[";
        // line 67
        echo twig_escape_filter($this->env, (isset($context["info_num"]) ? $context["info_num"] : null), "html", null, true);
        echo "]=1;
\t\t} else {
\t\t\t\$('#bl";
        // line 69
        echo twig_escape_filter($this->env, (isset($context["info_num"]) ? $context["info_num"] : null), "html", null, true);
        echo "').hide();
\t\t\t\$('#bla";
        // line 70
        echo twig_escape_filter($this->env, (isset($context["info_num"]) ? $context["info_num"] : null), "html", null, true);
        echo "_none').text('画像あり');
\t\t\t\$bl";
        // line 71
        echo twig_escape_filter($this->env, (isset($context["info_num"]) ? $context["info_num"] : null), "html", null, true);
        echo ".attr('src',img_url";
        echo twig_escape_filter($this->env, (isset($context["info_num"]) ? $context["info_num"] : null), "html", null, true);
        echo "[0]);
\t\t\t\$('input[name=\"bl[";
        // line 72
        echo twig_escape_filter($this->env, (isset($context["info_num"]) ? $context["info_num"] : null), "html", null, true);
        echo "]\"]').val(img_url";
        echo twig_escape_filter($this->env, (isset($context["info_num"]) ? $context["info_num"] : null), "html", null, true);
        echo "[0]);
\t\t\timg_select_array[";
        // line 73
        echo twig_escape_filter($this->env, (isset($context["info_num"]) ? $context["info_num"] : null), "html", null, true);
        echo "]=0;
\t\t}

\t});

\t\$('#bla";
        // line 78
        echo twig_escape_filter($this->env, (isset($context["info_num"]) ? $context["info_num"] : null), "html", null, true);
        echo "_img_only').on('click', function(){
\t\t\$title_sw = \$('#bl";
        // line 79
        echo twig_escape_filter($this->env, (isset($context["info_num"]) ? $context["info_num"] : null), "html", null, true);
        echo "_title').css(\"display\");
\t\tif(\$title_sw == 'none') {
\t\t\tvar title = \$('#bl";
        // line 81
        echo twig_escape_filter($this->env, (isset($context["info_num"]) ? $context["info_num"] : null), "html", null, true);
        echo "_title').text();
\t\t\tvar description = \$('#bl";
        // line 82
        echo twig_escape_filter($this->env, (isset($context["info_num"]) ? $context["info_num"] : null), "html", null, true);
        echo "_description').text();
\t\t\t\$('#bl";
        // line 83
        echo twig_escape_filter($this->env, (isset($context["info_num"]) ? $context["info_num"] : null), "html", null, true);
        echo "_title').show();
\t\t\t\$('#bl";
        // line 84
        echo twig_escape_filter($this->env, (isset($context["info_num"]) ? $context["info_num"] : null), "html", null, true);
        echo "_description').show();
\t\t\t\$('input[name=\"bl_title[";
        // line 85
        echo twig_escape_filter($this->env, (isset($context["info_num"]) ? $context["info_num"] : null), "html", null, true);
        echo "]\"]').val(title);
\t\t\t\$('input[name=\"bl_description[";
        // line 86
        echo twig_escape_filter($this->env, (isset($context["info_num"]) ? $context["info_num"] : null), "html", null, true);
        echo "]\"]').val(description);
\t\t\t\$('#bla";
        // line 87
        echo twig_escape_filter($this->env, (isset($context["info_num"]) ? $context["info_num"] : null), "html", null, true);
        echo "_img_only').text('画像のみ');
\t\t} else {
\t\t\t\$('#bl";
        // line 89
        echo twig_escape_filter($this->env, (isset($context["info_num"]) ? $context["info_num"] : null), "html", null, true);
        echo "_title').hide();
\t\t\t\$('#bl";
        // line 90
        echo twig_escape_filter($this->env, (isset($context["info_num"]) ? $context["info_num"] : null), "html", null, true);
        echo "_description').hide();

\t\t\t\$('input[name=\"bl_title[";
        // line 92
        echo twig_escape_filter($this->env, (isset($context["info_num"]) ? $context["info_num"] : null), "html", null, true);
        echo "]\"]').val('');
\t\t\t\$('input[name=\"bl_description[";
        // line 93
        echo twig_escape_filter($this->env, (isset($context["info_num"]) ? $context["info_num"] : null), "html", null, true);
        echo "]\"]').val('');

\t\t\t\$('#bla";
        // line 95
        echo twig_escape_filter($this->env, (isset($context["info_num"]) ? $context["info_num"] : null), "html", null, true);
        echo "_img_only').text('テキストあり');
\t\t}
\t});

});
</script>";
    }

    public function getTemplateName()
    {
        return "theme_url_template.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  361 => 95,  356 => 93,  352 => 92,  347 => 90,  343 => 89,  338 => 87,  334 => 86,  330 => 85,  326 => 84,  322 => 83,  318 => 82,  314 => 81,  309 => 79,  305 => 78,  297 => 73,  291 => 72,  285 => 71,  281 => 70,  277 => 69,  272 => 67,  266 => 66,  260 => 65,  256 => 64,  252 => 63,  247 => 61,  243 => 60,  235 => 57,  226 => 55,  218 => 54,  212 => 53,  208 => 52,  204 => 51,  196 => 48,  192 => 47,  184 => 46,  176 => 45,  170 => 44,  164 => 43,  160 => 42,  155 => 40,  151 => 39,  146 => 37,  137 => 33,  131 => 32,  127 => 31,  124 => 30,  111 => 28,  107 => 27,  102 => 25,  98 => 24,  93 => 22,  86 => 20,  81 => 18,  73 => 13,  69 => 12,  65 => 11,  61 => 10,  54 => 8,  50 => 7,  46 => 6,  40 => 5,  34 => 4,  28 => 3,  22 => 2,  19 => 1,);
    }
}
