<?php

/* bb/answer_conf.html */
class __TwigTemplate_0fae28e22ef1abbcd364b1aa0d714c57861d67a9a785499045c14ace811bcddc extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<html lang=\"ja\">
<head>
<meta charset=\"utf-8\">
<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0, user-scalable=no, maximum-scale=1.0\">
<title>お悩み相談</title>
";
        // line 7
        echo twig_include($this->env, $context, "./inc/link.html");
        echo "
</head>
<body>
<div class=\"page_wrap page_answer_conf\">
\t";
        // line 11
        echo twig_include($this->env, $context, "./inc/header.html");
        echo "
\t<section class=\"sec sec_answer_conf\">\t
\t\t\t<h2 class=\"sec_ttl\">入力内容の確認</h2>
\t\t\t<div class=\"sec_inner\">
\t\t\t\t<div class=\"input_form\">
\t\t\t\t\t<dl class=\"input_nickname\">
\t\t\t\t\t\t<dt>ニックネーム</dt>
\t\t\t\t\t\t<dd>
\t\t\t\t\t\t\t<p>";
        // line 19
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["data"]) ? $context["data"] : null), "nickname", array()), "html", null, true);
        echo "さん</p>
\t\t\t\t\t\t</dd>
\t\t\t\t\t</dl>
\t\t\t\t\t<script>
\t\t\t\t\t\tvar img_select_array = [];
\t\t\t\t\t</script>\t
\t\t\t\t\t";
        // line 25
        if (($this->getAttribute((isset($context["data"]) ? $context["data"] : null), "userfile", array()) != "")) {
            // line 26
            echo "\t\t\t\t\t<dl>
\t\t\t\t\t\t<dt>画像</dt>
\t\t\t\t\t\t<dd>
\t\t\t\t\t\t\t<p><img src=\"/uploads/temp/";
            // line 29
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["data"]) ? $context["data"] : null), "userfile", array()), "html", null, true);
            echo "\"></p>
\t\t\t\t\t\t</dd>
\t\t\t\t\t</dl>
\t\t\t\t\t";
        }
        // line 33
        echo "\t\t\t\t\t<form method=\"post\" action=\"\" id=\"answer\" class=\"answer_conf_form validate\">
\t\t\t\t\t";
        // line 34
        echo (isset($context["form_hidden"]) ? $context["form_hidden"] : null);
        echo "
\t\t\t\t\t<dl class=\"input_content\">
\t\t\t\t\t\t<dt>回答内容</dt>
\t\t\t\t\t\t<dd>
\t\t\t\t\t\t\t<p>";
        // line 38
        echo $this->getAttribute((isset($context["data"]) ? $context["data"] : null), "message_next", array());
        echo "</p>
\t\t\t\t\t\t</dd>
\t\t\t\t\t</dl>
\t\t\t\t\t</form>
\t\t\t\t\t<ul class=\"btn_wrap\">
\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t<a href=\"#\" id=\"back\" class=\"btn_style btn_gray-o\">戻る</a>
\t\t\t\t\t\t</li>
\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t<button type=\"submit\" id=\"goto\" class=\"btn_style btn_green\">回答する</button>
\t\t\t\t\t\t</li>
\t\t\t\t\t</ul>
\t\t\t\t</div>
\t\t\t</div>
\t</section>
\t";
        // line 53
        echo twig_include($this->env, $context, "./inc/footer.html");
        echo "
</div>
<script>
\$(function() {
\tvar click = true;
\t\$('#back').click(function() {
        \$('#answer').attr('action', '";
        // line 59
        echo twig_escape_filter($this->env, (isset($context["base_url"]) ? $context["base_url"] : null), "html", null, true);
        echo "bb/answer');
        \$('#answer').submit();
    });
    \$('#goto').click(function(e) {
        \$('#answer').attr('action', '";
        // line 63
        echo twig_escape_filter($this->env, (isset($context["base_url"]) ? $context["base_url"] : null), "html", null, true);
        echo "bb/answer_completion');
        if (click) {
        \t\$('#answer').submit();
        \tclick = false;
        };
      \te.preventDefault();
    });
});
</script>
";
        // line 72
        echo twig_include($this->env, $context, "./inc/script.html");
        echo "
</body>
</html>
";
    }

    public function getTemplateName()
    {
        return "bb/answer_conf.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  124 => 72,  112 => 63,  105 => 59,  96 => 53,  78 => 38,  71 => 34,  68 => 33,  61 => 29,  56 => 26,  54 => 25,  45 => 19,  34 => 11,  27 => 7,  19 => 1,);
    }
}
