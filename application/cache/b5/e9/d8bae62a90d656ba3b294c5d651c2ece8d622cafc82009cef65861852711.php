<?php

/* mypage_c.html */
class __TwigTemplate_b5e9d8bae62a90d656ba3b294c5d651c2ece8d622cafc82009cef65861852711 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!doctype html>
<html>
<head>
<meta charset=\"utf-8\">
<title>マイページ</title>
";
        // line 6
        echo twig_include($this->env, $context, "./inc/link.html");
        echo "
</head>

<body>
<div class=\"page_wrap page_mypage\">
\t";
        // line 11
        echo twig_include($this->env, $context, "./inc/header.html");
        echo "
\t<div class=\"content_wrap cf\">
\t\t<div class=\"content\">
\t\t\t<div class=\"content_inner\">
\t\t\t\t<section class=\"sec sec_mypage\">
\t\t\t\t\t<h2 class=\"sec_ttl\" id=\"consultation_list\">マイページ</h2>
\t\t\t\t\t<ul class=\"tab_menu\" id=\"tab_menu\">
\t\t\t\t\t\t<li class=\"tab_unresolved\" id=\"tab_unresolved\">未回答の相談</li>
\t\t\t\t\t\t<li class=\"tab_history\" id=\"tab_history\">相談履歴</li>
\t\t\t\t\t\t<li class=\"tab_bookmark current\" id=\"tab_bookmark\">ブックマーク</li>
\t\t\t\t\t</ul>

\t\t\t\t\t<div class=\"tab_content content_bookmark content_3\">
\t\t\t\t\t";
        // line 24
        if (((isset($context["list_all_num"]) ? $context["list_all_num"] : null) > 0)) {
            // line 25
            echo "\t\t\t\t\t\t<ul class=\"consult_list m_b_20\">
\t\t\t\t            ";
            // line 26
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["bookmark_ar"]) ? $context["bookmark_ar"] : null));
            foreach ($context['_seq'] as $context["key"] => $context["entry"]) {
                // line 27
                echo "\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t<div class=\"box_wrap\">
\t\t\t\t\t\t\t\t\t<div class=\"left_box\">
\t\t\t\t\t\t\t\t\t\t<input type=\"checkbox\" name=\"check[]\" value=\"";
                // line 30
                echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "id", array()), "html", null, true);
                echo "\">
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t<div class=\"right_box\">
\t\t\t\t\t\t\t\t\t\t<a href=\"";
                // line 33
                echo twig_escape_filter($this->env, (isset($context["base_url"]) ? $context["base_url"] : null), "html", null, true);
                echo "cate";
                echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "big_cate_id", array()), "html", null, true);
                echo "/";
                echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "cate_id", array()), "html", null, true);
                echo "/";
                echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "id", array()), "html", null, true);
                echo "\" class=\"ic ic_angle-right\">
\t\t\t\t\t\t\t\t\t\t\t<p class=\"content_category ic ";
                // line 34
                echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "class", array()), "html", null, true);
                echo "\"><strong>";
                echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "big_category_name", array()), "html", null, true);
                echo "</strong>";
                echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "category_name", array()), "html", null, true);
                echo "</p>
\t\t\t\t\t\t\t\t\t\t\t<h3 class=\"content_ttl\">";
                // line 35
                echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "title", array()), "html", null, true);
                echo "</h3>
\t\t\t\t\t\t\t\t\t\t\t<p>";
                // line 36
                echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "message", array()), "html", null, true);
                echo "</p>
\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</div>

\t\t\t\t\t\t\t\t<div class=\"consult_list_date cf\">
\t\t\t\t\t\t\t\t\t<p class=\"post_date\">";
                // line 42
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["entry"], "create_date", array()), "Y.m.d H:i"), "html", null, true);
                echo "</p>
\t\t\t\t\t\t\t\t\t<ul class=\"posted_ability\">
\t\t\t\t\t\t\t\t\t\t";
                // line 44
                if (($this->getAttribute($context["entry"], "like_count", array()) > 0)) {
                    // line 45
                    echo "\t\t\t\t\t\t\t\t\t\t<li><p class=\"ic ic_heart\">";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "like_count", array()), "html", null, true);
                    echo "</p></li>
\t\t\t\t\t\t\t\t\t\t";
                } else {
                    // line 47
                    echo "\t\t\t\t\t\t\t\t\t\t<li><p class=\"ic ic_heart-o\">";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "like_count", array()), "html", null, true);
                    echo "</p></li>
\t\t\t\t\t\t\t\t\t\t";
                }
                // line 49
                echo "\t\t\t\t\t\t\t\t\t\t";
                if (($this->getAttribute($context["entry"], "comment_count", array()) > 0)) {
                    // line 50
                    echo "\t\t\t\t\t\t\t\t\t\t<li><p class=\"ic ic_comment\">";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "comment_count", array()), "html", null, true);
                    echo "</p></li>
\t\t\t\t\t\t\t\t\t\t";
                } else {
                    // line 52
                    echo "\t\t\t\t\t\t\t\t\t\t<li><p class=\"ic ic_comment-o\">";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "comment_count", array()), "html", null, true);
                    echo "</p></li>
\t\t\t\t\t\t\t\t\t\t";
                }
                // line 54
                echo "\t\t\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t\t\t</div>

\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['key'], $context['entry'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 59
            echo "\t\t\t\t\t\t</ul>
\t\t\t\t\t\t<div class=\"content_bottom cf\">
\t\t\t\t\t\t\t<ul class=\"btn_wrap\">
\t\t\t\t\t\t\t\t<li><a id=\"checked_delete\" href=\"#\" class=\"btn_style btn_gray-o\">チェックした項目を削除</a></li>
\t\t\t\t\t\t\t</ul>

\t\t\t\t\t\t\t<ul class=\"pager_wrap bookmark_pager_wrap\">
\t\t\t\t\t\t\t\t";
            // line 66
            echo (isset($context["pagination"]) ? $context["pagination"] : null);
            echo "
\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t</div>
\t\t\t\t\t";
        } else {
            // line 70
            echo "\t\t\t\t\t\t<div class=\"search_found keyword_search\">
\t\t\t\t\t\t\t<p class=\"m_b_20\">ブックマークした相談はありません。<br>キーワード入力で相談を検索することもできます。</p>
\t\t\t\t\t\t\t<input type=\"hidden\" name=\"page\" id=\"search_page\">
\t\t\t\t\t\t\t<table class=\"sec_search_table\">
\t\t\t\t\t\t\t\t<tbody>
\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t<td><label for=\"\"><input type=\"text\" id=\"input_keyword\" name=\"input_keyword\" placeholder=\"生理中のオナニー 潮ふき方法 イケない\" value=\"";
            // line 76
            echo twig_escape_filter($this->env, (isset($context["input_keyword"]) ? $context["input_keyword"] : null), "html", null, true);
            echo "\"></label></td>
\t\t\t\t\t\t\t\t\t\t<td class=\"search_btn\">
\t\t\t\t\t\t\t\t\t\t\t<ul class=\"btn_wrap\">
\t\t\t\t\t\t\t\t\t\t\t\t<li><button name=\"keyword\" type=\"submit\" class=\"btn_style btn_blue\">探す</button></li>
\t\t\t\t\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t</tbody>
\t\t\t\t\t\t\t</table>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<ul class=\"consult_list m_b_20\">
\t\t\t\t\t\t\t";
            // line 87
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["search_keyword_consultation_ar"]) ? $context["search_keyword_consultation_ar"] : null));
            foreach ($context['_seq'] as $context["key"] => $context["entry"]) {
                // line 88
                echo "\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t<div class=\"box_wrap\" style=\"padding-left:8px;\">
\t\t\t\t\t\t\t\t\t<div class=\"right_box\">
\t\t\t\t\t\t\t\t\t\t<a href=\"";
                // line 91
                echo twig_escape_filter($this->env, (isset($context["base_url"]) ? $context["base_url"] : null), "html", null, true);
                echo "cate";
                echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "big_cate_id", array()), "html", null, true);
                echo "/";
                echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "cate_id", array()), "html", null, true);
                echo "/";
                echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "id", array()), "html", null, true);
                echo "\" class=\"ic ic_angle-right\">
\t\t\t\t\t\t\t\t\t\t\t<p class=\"content_category ic ";
                // line 92
                echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "class", array()), "html", null, true);
                echo "\"><strong>";
                echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "big_category_name", array()), "html", null, true);
                echo "</strong>";
                echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "category_name", array()), "html", null, true);
                echo "</p>
\t\t\t\t\t\t\t\t\t\t\t<h3 class=\"content_ttl\">";
                // line 93
                echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "title", array()), "html", null, true);
                echo "</h3>
\t\t\t\t\t\t\t\t\t\t\t<p>";
                // line 94
                echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "message", array()), "html", null, true);
                echo "</p>
\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</div>

\t\t\t\t\t\t\t\t<div class=\"consult_list_date cf\">
\t\t\t\t\t\t\t\t\t<p class=\"post_date\">";
                // line 100
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["entry"], "create_date", array()), "Y.m.d H:i"), "html", null, true);
                echo "</p>
\t\t\t\t\t\t\t\t\t<ul class=\"posted_ability\">
\t\t\t\t\t\t\t\t\t\t";
                // line 102
                if (($this->getAttribute($context["entry"], "like_count", array()) > 0)) {
                    // line 103
                    echo "\t\t\t\t\t\t\t\t\t\t<li><p class=\"ic ic_heart\">";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "like_count", array()), "html", null, true);
                    echo "</p></li>
\t\t\t\t\t\t\t\t\t\t";
                } else {
                    // line 105
                    echo "\t\t\t\t\t\t\t\t\t\t<li><p class=\"ic ic_heart-o\">";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "like_count", array()), "html", null, true);
                    echo "</p></li>
\t\t\t\t\t\t\t\t\t\t";
                }
                // line 107
                echo "\t\t\t\t\t\t\t\t\t\t";
                if (($this->getAttribute($context["entry"], "comment_count", array()) > 0)) {
                    // line 108
                    echo "\t\t\t\t\t\t\t\t\t\t<li><p class=\"ic ic_comment\">";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "comment_count", array()), "html", null, true);
                    echo "</p></li>
\t\t\t\t\t\t\t\t\t\t";
                } else {
                    // line 110
                    echo "\t\t\t\t\t\t\t\t\t\t<li><p class=\"ic ic_comment-o\">";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "comment_count", array()), "html", null, true);
                    echo "</p></li>
\t\t\t\t\t\t\t\t\t\t";
                }
                // line 112
                echo "\t\t\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t\t\t</div>

\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['key'], $context['entry'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 117
            echo "\t\t\t\t\t\t</ul>
\t\t\t\t\t\t<div class=\"content_bottom cf\">
\t\t\t\t\t\t\t<ul class=\"pager_wrap search_pager_wrap\">
\t\t\t\t\t\t\t\t";
            // line 120
            echo (isset($context["pagination"]) ? $context["pagination"] : null);
            echo "
\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t</div>
\t\t\t\t\t";
        }
        // line 124
        echo "\t\t\t\t\t</div>

\t\t\t\t</section>
\t\t\t</div>
\t\t</div>
\t\t";
        // line 129
        echo twig_include($this->env, $context, "./inc/mypage_sidebar.html");
        echo "
\t</div>
\t";
        // line 131
        echo twig_include($this->env, $context, "./inc/footer.html");
        echo "
</div>
";
        // line 133
        echo twig_include($this->env, $context, "./inc/script.html");
        echo "
<script>
\t\$(\"#tab_unresolved\").on('click', function(){
\t\twindow.location.href = '";
        // line 136
        echo twig_escape_filter($this->env, (isset($context["base_url"]) ? $context["base_url"] : null), "html", null, true);
        echo "user/my_page/1/';
\t});
\t\$(\"#tab_history\").on('click', function(){
\t\twindow.location.href = '";
        // line 139
        echo twig_escape_filter($this->env, (isset($context["base_url"]) ? $context["base_url"] : null), "html", null, true);
        echo "user/my_page/2/';
\t});

\t\$('#checked_delete').on('click',function() {
\t\tvar cont = 0;
 \t\tvar checks=[];
        \$(\".consult_list input[name='check[]']:checked\").each(function(){
\t\t\tcont++;
            checks.push(this.value);
        });

\t\tif (cont == 0) {
\t\t\treturn false;
\t\t}

\t\tif (!confirm('チェックした項目を削除します\\nよろしいですか？')) {
\t\t\treturn false;
\t\t}

\t\t\$.ajax({
\t\t\ttype: \"POST\",
\t\t\turl: \"";
        // line 160
        echo twig_escape_filter($this->env, (isset($context["base_url"]) ? $context["base_url"] : null), "html", null, true);
        echo "user/delete_bookmark_ajax\",
\t\t\tdata: {
\t\t\t    \"checks\":checks,
\t\t\t}
\t\t}).done(function(data){
//\t\t    alert('OK!!');
\t\t\twindow.location = \"";
        // line 166
        echo twig_escape_filter($this->env, (isset($context["base_url"]) ? $context["base_url"] : null), "html", null, true);
        echo "user/my_page/3\";
\t\t}).fail(function(data){
\t\t    alert('error!!!');
\t\t});
\t});\t
\t
\t\$('.search_pager_wrap a').on('click',function(){

\t\tvar page = \$(this).attr('data-ci-pagination-page');
\t\tvar href = \$(this).attr('href');
\t\tvar keyword = \$('#input_keyword').val();
\t\t\$(this).attr('data-ci-pagination-page', '');
\t\tif(page == 1) {
\t\t\t\$(this).attr('href', href+'?page='+page+'&input_keyword='+keyword+'#consultation_list');
\t\t} else {
\t\t\t\$(this).attr('href', href+'&input_keyword='+keyword+'#consultation_list');
\t\t}
\t});
\t
\t\$('.bookmark_pager_wrap a').on('click',function(){

\t\tvar page = \$(this).attr('data-ci-pagination-page');
\t\tvar href = \$(this).attr('href');
\t\t\$(this).attr('data-ci-pagination-page', '');
\t\tif(page == 1) {
\t\t\t\$(this).attr('href', href+'?page='+page+'#consultation_list');
\t\t} else {
\t\t\t\$(this).attr('href', href+'#consultation_list');
\t\t}
\t});

\t\$(\"button[name='keyword']\").on('click', function(){
\t\tvar val = \$(\"input[name='input_keyword']\").val();
\t\tif (val.trim() != '') {
 \t\t\twindow.location.href = '";
        // line 200
        echo twig_escape_filter($this->env, (isset($context["base_url"]) ? $context["base_url"] : null), "html", null, true);
        echo "user/my_page/3/?input_keyword='+val;
\t\t} else {
\t\t\t\$(\"input[name='input_keyword']\").val('');
\t\t}
\t});

\t\$(\"input[name='input_keyword']\").keypress(function(e) {
\t    if(e.which == 13) {
\t        var val = \$(\"input[name='input_keyword']\").val();
\t\t\tif (val.trim() != '') {
\t \t\t\twindow.location.href = '";
        // line 210
        echo twig_escape_filter($this->env, (isset($context["base_url"]) ? $context["base_url"] : null), "html", null, true);
        echo "user/my_page/3/?input_keyword='+val;
\t\t\t} else {
\t\t\t\t\$(\"input[name='input_keyword']\").val('');
\t\t\t}
\t    }
\t});
</script> 
</body>
</html>
";
    }

    public function getTemplateName()
    {
        return "mypage_c.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  391 => 210,  378 => 200,  341 => 166,  332 => 160,  308 => 139,  302 => 136,  296 => 133,  291 => 131,  286 => 129,  279 => 124,  272 => 120,  267 => 117,  257 => 112,  251 => 110,  245 => 108,  242 => 107,  236 => 105,  230 => 103,  228 => 102,  223 => 100,  214 => 94,  210 => 93,  202 => 92,  192 => 91,  187 => 88,  183 => 87,  169 => 76,  161 => 70,  154 => 66,  145 => 59,  135 => 54,  129 => 52,  123 => 50,  120 => 49,  114 => 47,  108 => 45,  106 => 44,  101 => 42,  92 => 36,  88 => 35,  80 => 34,  70 => 33,  64 => 30,  59 => 27,  55 => 26,  52 => 25,  50 => 24,  34 => 11,  26 => 6,  19 => 1,);
    }
}
