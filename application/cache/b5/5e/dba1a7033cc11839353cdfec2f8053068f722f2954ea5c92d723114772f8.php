<?php

/* bb/question_conf.html */
class __TwigTemplate_b55edba1a7033cc11839353cdfec2f8053068f722f2954ea5c92d723114772f8 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<html lang=\"ja\">
<head>
<meta charset=\"utf-8\">
<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0, user-scalable=no, maximum-scale=1.0\">
<title>お悩み相談</title>
";
        // line 7
        echo twig_include($this->env, $context, "./inc/link.html");
        echo "
</head>
<body>
<div class=\"page_wrap page_question_conf\">
\t";
        // line 11
        echo twig_include($this->env, $context, "./inc/header.html");
        echo "
\t<section class=\"sec sec_question_conf\">
\t\t";
        // line 14
        echo "\t\t\t<h2 class=\"sec_ttl\">入力内容の確認</h2>
\t\t\t<div class=\"sec_inner\">
\t\t\t\t<div class=\"input_form\">
\t\t\t\t\t<dl>
\t\t\t\t\t\t<dt>ニックネーム</dt>
\t\t\t\t\t\t<dd>
\t\t\t\t\t\t\t<p>";
        // line 20
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["data"]) ? $context["data"] : null), "nickname", array()), "html", null, true);
        echo "さん</p>
\t\t\t\t\t\t</dd>
\t\t\t\t\t</dl>
\t\t\t\t\t<dl>
\t\t\t\t\t\t<dt>相談するカテゴリ</dt>
\t\t\t\t\t\t<dd>
\t\t\t\t\t\t\t<p>";
        // line 26
        echo twig_escape_filter($this->env, set_value("big_category_name"), "html", null, true);
        echo "</p>
\t\t\t\t\t\t</dd>
\t\t\t\t\t</dl>
\t\t\t\t\t<dl>
\t\t\t\t\t\t<dt>相談するカテゴリの詳細</dt>
\t\t\t\t\t\t<dd>
\t\t\t\t\t\t\t<p>";
        // line 32
        echo twig_escape_filter($this->env, set_value("category_name"), "html", null, true);
        echo "</p>
\t\t\t\t\t\t</dd>
\t\t\t\t\t</dl>
\t\t\t\t\t<dl>
\t\t\t\t\t\t<dt>タイトル</dt>
\t\t\t\t\t\t<dd>
\t\t\t\t\t\t\t<p>";
        // line 38
        echo twig_escape_filter($this->env, set_value("question_subject"), "html", null, true);
        echo "</p>
\t\t\t\t\t\t</dd>
\t\t\t\t\t</dl>\t\t\t\t\t
\t\t\t\t\t<form id=\"consultation\" name=\"consultation\" method=\"post\" action=\"/bb/post_completion\">
\t\t\t\t\t<script>
\t\t\t\t\t\tvar img_select_array = [];
\t\t\t\t\t</script>\t
\t\t\t\t\t";
        // line 45
        if (($this->getAttribute((isset($context["data"]) ? $context["data"] : null), "userfile", array()) != "")) {
            // line 46
            echo "\t\t\t\t\t<dl>
\t\t\t\t\t\t<dt>画像</dt>
\t\t\t\t\t\t<dd>
\t\t\t\t\t\t\t<p><img src=\"/uploads/temp/";
            // line 49
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["data"]) ? $context["data"] : null), "userfile", array()), "html", null, true);
            echo "\"></p>
\t\t\t\t\t\t</dd>
\t\t\t\t\t</dl>\t
\t\t\t\t\t";
        }
        // line 53
        echo "\t\t\t\t\t<dl>
\t\t\t\t\t\t<dt>相談内容</dt>
\t\t\t\t\t\t<dd><p>";
        // line 55
        echo $this->getAttribute((isset($context["data"]) ? $context["data"] : null), "message_next", array());
        echo "</p></dd>
\t\t\t\t\t</dl>\t
\t\t\t\t\t";
        // line 57
        echo (isset($context["form_hidden"]) ? $context["form_hidden"] : null);
        echo "
\t\t\t\t\t</form>
\t\t\t\t\t<ul class=\"btn_wrap\">
\t\t\t\t\t\t<li>
\t\t\t\t\t\t<!--\t<a href=\"#\" onclick=\"javascript:window.history.back(-1);return false;\" class=\"btn_style btn_gray\">戻る</a>  -->
\t\t\t\t\t\t\t<a id=\"back\" class=\"btn_style btn_gray-o\">戻る</a>
\t\t\t\t\t\t</li>
\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t<button type=\"submit\" id=\"goto\" class=\"btn_style btn_green\">相談する</button>
\t\t\t\t\t\t</li>
\t\t\t\t\t</ul>
\t\t\t\t</div>
\t\t\t</div>
\t\t";
        // line 71
        echo "\t</section>
\t";
        // line 72
        echo twig_include($this->env, $context, "./inc/footer.html");
        echo "
</div>
";
        // line 74
        echo twig_include($this->env, $context, "./inc/script.html");
        echo "
<script>
\$(function(){
\tvar click = true;
    \$('#back').click(function() {
        \$('#consultation').attr('action', '";
        // line 79
        echo twig_escape_filter($this->env, (isset($context["base_url"]) ? $context["base_url"] : null), "html", null, true);
        echo "bb/posting');
        \$('#consultation').submit();
    });
    \$('#goto').click(function(e) {
        \$('#consultation').attr('action', '";
        // line 83
        echo twig_escape_filter($this->env, (isset($context["base_url"]) ? $context["base_url"] : null), "html", null, true);
        echo "bb/post_completion');
        if (click) {
        \t\$('#consultation').submit();
        \tclick = false;
        }
        e.preventDefault();
    });
});
</script>
</body>
</html>
";
    }

    public function getTemplateName()
    {
        return "bb/question_conf.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  146 => 83,  139 => 79,  131 => 74,  126 => 72,  123 => 71,  107 => 57,  102 => 55,  98 => 53,  91 => 49,  86 => 46,  84 => 45,  74 => 38,  65 => 32,  56 => 26,  47 => 20,  39 => 14,  34 => 11,  27 => 7,  19 => 1,);
    }
}
