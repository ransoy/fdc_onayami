<?php

/* ./inc/header.html */
class __TwigTemplate_c9e0e8d8d4f32566998429a0790acc9a26b08fa1b9ed691fa7c1edd62fe50a52 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<header class=\"header\">
\t<div class=\"left_box\">
\t\t<h1><a href=\"";
        // line 3
        echo twig_escape_filter($this->env, (isset($context["base_url"]) ? $context["base_url"] : null), "html", null, true);
        echo "\"><img src=\"";
        echo twig_escape_filter($this->env, (isset($context["base_url"]) ? $context["base_url"] : null), "html", null, true);
        echo "sp/image/common/logo.png\" alt=\"ロゴ\"></a></h1>
\t</div>
\t<div class=\"right_box\">
\t\t<nav class=\"user_menu\">
\t\t\t<form id=\"form_signup\" method=\"post\" action=\"";
        // line 7
        echo twig_escape_filter($this->env, (isset($context["main_url"]) ? $context["main_url"] : null), "html", null, true);
        echo "user/signup\" name=\"form_signup\">
\t\t\t<input type=\"hidden\" name=\"onayamisignup\" value=\"onayami\">
\t\t\t</form>
\t\t\t<form id=\"form_login\" method=\"post\" action=\"";
        // line 10
        echo twig_escape_filter($this->env, (isset($context["main_url"]) ? $context["main_url"] : null), "html", null, true);
        echo "user/login\" name=\"form_login\">
\t\t\t<input type=\"hidden\" name=\"onayamilogin\" value=\"onayami\">
\t\t\t</form>
\t\t\t<ul>
\t\t\t\t";
        // line 14
        if ((isset($context["user_login_flag"]) ? $context["user_login_flag"] : null)) {
            // line 15
            echo "\t\t\t\t<li><a class=\"logout\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["base_url"]) ? $context["base_url"] : null), "html", null, true);
            echo "user/logout\">
\t\t\t\t\t<p class=\"fa fa-sign-out\"></p>
\t\t\t\t\t<p><span>ログアウト</span></p></a></li>
\t\t\t\t";
        } else {
            // line 19
            echo "\t\t\t\t<li><a id=\"onayami_signup\" href=\"javascript:void(0);\">
\t\t\t\t\t<p class=\"fa fa-pencil-square-o\"></p>
\t\t\t\t\t<p><span>新規登録</span></p></a>
\t\t\t\t</li>
\t\t\t\t<li><a id=\"onayami_login\" href=\"javascript:void(0);\">
\t\t\t\t\t<p class=\"fa fa-lock\"></p>
\t\t\t\t\t<p><span>ログイン</span></p></a>
\t\t\t\t</li>
\t\t\t\t";
        }
        // line 28
        echo "\t\t\t\t<li>
\t\t\t\t\t<a href=\"#sidr\" id=\"simple-menu\">
\t\t\t\t\t\t<p class=\"fa fa-bars\"></p>
\t\t\t\t\t\t<p><span>メニュー</span></p>
\t\t\t\t\t</a>
\t\t\t\t</li>
\t\t\t</ul>
\t\t</nav>
\t</div>
</header>
<ul class=\"breadcrumb\">
\t";
        // line 39
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["breadcrumb_list"]) ? $context["breadcrumb_list"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["entry"]) {
            // line 40
            echo "\t\t";
            if (($this->getAttribute($context["entry"], "url", array()) == "")) {
                // line 41
                echo "\t\t<li><a href=\"\">";
                echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "text", array()), "html", null, true);
                echo "</a></li>
\t\t";
            } else {
                // line 43
                echo "\t\t<li><a href=\"";
                echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "url", array()), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "text", array()), "html", null, true);
                echo "</a></li>
\t\t";
            }
            // line 45
            echo "\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['entry'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 46
        echo "</ul>
<div id=\"sidr\" style=\"display:none;\">
\t<ul>
\t\t<li><a href=\"";
        // line 49
        echo twig_escape_filter($this->env, (isset($context["base_url"]) ? $context["base_url"] : null), "html", null, true);
        echo "\">トップ</a></li>

\t\t";
        // line 51
        if ((isset($context["user_login_flag"]) ? $context["user_login_flag"] : null)) {
            // line 52
            echo "\t\t<li><a href=\"";
            echo twig_escape_filter($this->env, (isset($context["base_url"]) ? $context["base_url"] : null), "html", null, true);
            echo "user/my_page\">マイページ</a></li>
\t\t";
        }
        // line 54
        echo "\t\t<li><a href=\"";
        echo twig_escape_filter($this->env, (isset($context["base_url"]) ? $context["base_url"] : null), "html", null, true);
        echo "bb/posting\">悩みを投稿する</a></li>
\t\t";
        // line 55
        if ((isset($context["user_can_point"]) ? $context["user_can_point"] : null)) {
            // line 56
            echo "\t\t<li><a href=\"";
            echo twig_escape_filter($this->env, (isset($context["main_url"]) ? $context["main_url"] : null), "html", null, true);
            echo "user/bonus/bonus_list/\">ボーナス確認</a></li>
\t\t<li><a href=\"";
            // line 57
            echo twig_escape_filter($this->env, (isset($context["main_url"]) ? $context["main_url"] : null), "html", null, true);
            echo "user/misc/more_point1/\">多くのボーナスを獲得するには</a></li>
\t\t";
        }
        // line 59
        echo "\t\t<li><a href=\"";
        echo twig_escape_filter($this->env, (isset($context["main_url"]) ? $context["main_url"] : null), "html", null, true);
        echo "user/settings/user/settings/\">設定</a></li>
\t\t<li><a href=\"";
        // line 60
        echo twig_escape_filter($this->env, (isset($context["base_url"]) ? $context["base_url"] : null), "html", null, true);
        echo "/contact\">FAQ/お問合せ</a></li>
\t\t<li><a href=\"";
        // line 61
        echo twig_escape_filter($this->env, (isset($context["main_url"]) ? $context["main_url"] : null), "html", null, true);
        echo "\">高収入求人トップへ</a></li>
\t</ul>
</div>
<script type=\"text/javascript\">
\$(function() {
\t\$('#onayami_signup').on('click', function(){
\t\t\$('#form_signup').submit();
\t});
\t\$('#onayami_login').on('click', function(){
\t\t\$('#form_login').submit();
\t});
});
</script>";
    }

    public function getTemplateName()
    {
        return "./inc/header.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  150 => 61,  146 => 60,  141 => 59,  136 => 57,  131 => 56,  129 => 55,  124 => 54,  118 => 52,  116 => 51,  111 => 49,  106 => 46,  100 => 45,  92 => 43,  86 => 41,  83 => 40,  79 => 39,  66 => 28,  55 => 19,  47 => 15,  45 => 14,  38 => 10,  32 => 7,  23 => 3,  19 => 1,);
    }
}
