<?php

/* bb/consult_ajax.html */
class __TwigTemplate_452cb8aca5295d59d8ff5fc75d7801ec4febc03babd5641d9893dc6f354c148c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["response_ar"]) ? $context["response_ar"] : null));
        foreach ($context['_seq'] as $context["key"] => $context["entry"]) {
            // line 2
            echo "<p class=\"user_name\">";
            echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "nick_name", array()), "html", null, true);
            echo "さん</p>
<div class=\"content_inner\">
\t<div class=\"consult_answer_list\">
\t\t<div class=\"comment\">
\t\t\t";
            // line 6
            if (($this->getAttribute($context["entry"], "up_image", array()) != null)) {
                // line 7
                echo "\t\t\t<p><img src=\"";
                echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "up_image", array()), "html", null, true);
                echo "\"></p>
\t\t\t";
            }
            // line 9
            echo "\t\t\t";
            if (((isset($context["colorSwitch"]) ? $context["colorSwitch"] : null) == 1)) {
                // line 10
                echo "\t\t\t\t";
                if (($this->getAttribute($context["entry"], "rank", array()) > 80)) {
                    // line 11
                    echo "\t\t\t\t<p class=\"onayami_tally_count_1\">";
                    echo nl2br($this->getAttribute($context["entry"], "message", array()));
                    echo "</p>
\t\t\t\t";
                } elseif (($this->getAttribute(                // line 12
$context["entry"], "rank", array()) > 50)) {
                    // line 13
                    echo "\t\t\t\t<p class=\"onayami_tally_count_2\">";
                    echo nl2br($this->getAttribute($context["entry"], "message", array()));
                    echo "</p>
\t\t\t\t";
                } elseif (($this->getAttribute(                // line 14
$context["entry"], "rank", array()) > 5)) {
                    // line 15
                    echo "\t\t\t\t<p class=\"onayami_tally_count_3\">";
                    echo nl2br($this->getAttribute($context["entry"], "message", array()));
                    echo "</p>
\t\t\t\t";
                } else {
                    // line 17
                    echo "\t\t\t\t<p class=\"onayami_tally_count_4\">";
                    echo nl2br($this->getAttribute($context["entry"], "message", array()));
                    echo "</p>
\t\t\t\t";
                }
                // line 19
                echo "\t\t\t";
            } else {
                // line 20
                echo "\t\t\t<p>";
                echo nl2br($this->getAttribute($context["entry"], "message", array()));
                echo "</p>
\t\t\t";
            }
            // line 22
            echo "\t\t</div>
\t\t<div class=\"content_bottom cf\">
\t\t\t<p class=\"post_date\">";
            // line 24
            echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "time_ago", array()), "html", null, true);
            echo "</p>

\t\t\t<ul class=\"btn_wrap\">
\t\t\t\t<li><a href=\"javascript:void(0);\" class=\"btn_style ";
            // line 27
            echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "btn_orange_color", array()), "html", null, true);
            echo " btn_update_eval\" data-mess-id=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "id", array()), "html", null, true);
            echo "\" data-user-id=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "user_id", array()), "html", null, true);
            echo "\">私もそう思う<span class=\"ic ic_heart\">";
            echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "evaluate", array()), "html", null, true);
            echo "</span></a></li>
\t\t\t</ul>
\t\t</div>
\t</div>

<!--\t\t\t\t\t";
            // line 32
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["entry"], "reply_ar", array()));
            foreach ($context['_seq'] as $context["key2"] => $context["entry2"]) {
                echo " -->
\t";
                // line 33
                if ($this->getAttribute($this->getAttribute($this->getAttribute($context["entry"], "reply_ar", array()), 0, array(), "array"), "flag", array())) {
                    // line 34
                    echo "\t<ul class=\"destination\">
\t\t<li>";
                    // line 35
                    echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "replay_nick_name", array()), "html", null, true);
                    echo "</li>
\t\t<li>";
                    // line 36
                    echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "nick_name", array()), "html", null, true);
                    echo "</li>
\t</ul>

\t\t<div class=\"comment_reply\">
\t\t<div class=\"comment\">
\t\t\t<p>
\t\t\t";
                    // line 42
                    echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "reply_message", array()), "html", null, true);
                    echo "
\t\t\t</p>
\t\t</div>
\t\t<div class=\"content_bottom cf\">
\t\t\t<p class=\"post_date\">";
                    // line 46
                    echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "replay_cre_date", array()), "html", null, true);
                    echo "</p>

\t\t\t<ul class=\"btn_wrap\">
\t\t\t\t<li><a href=\"javascript:void(0);\" class=\"btn_style ";
                    // line 49
                    echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "replay_orange_color", array()), "html", null, true);
                    echo " btn_update_eval\" data-mess-id=\"";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "reply_message_id", array()), "html", null, true);
                    echo "\" data-user-id=\"";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "replay_owner_id", array()), "html", null, true);
                    echo "\">私もそう思う<span class=\"ic ic_heart\">";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "replay_evaluate", array()), "html", null, true);
                    echo "</span></a></li>
\t\t\t</ul>
\t\t</div>
\t</div>
\t";
                } else {
                    // line 54
                    echo "\t\t";
                    if ((((isset($context["is_own_replay"]) ? $context["is_own_replay"] : null) != $this->getAttribute($context["entry"], "response_id", array())) && ($this->getAttribute((isset($context["consultation_info"]) ? $context["consultation_info"] : null), "user_id", array()) == (isset($context["is_own_replay"]) ? $context["is_own_replay"] : null)))) {
                        // line 55
                        echo "\t\t<div class=\"comment_reply_input comment_reply";
                        echo twig_escape_filter($this->env, $context["key"], "html", null, true);
                        echo "\">
\t\t\t<form method=\"post\" action=\"\" class=\"validate_reply";
                        // line 56
                        echo twig_escape_filter($this->env, $context["key"], "html", null, true);
                        echo "\">
\t\t\t<input type=\"hidden\" name=\"message_id\" value=\"";
                        // line 57
                        echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "id", array()), "html", null, true);
                        echo "\">
\t\t\t<input type=\"hidden\" name=\"thread_id\" value=\"";
                        // line 58
                        echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "thread_id", array()), "html", null, true);
                        echo "\">
\t\t\t<input type=\"hidden\" name=\"replay_nick_name\" value=\"";
                        // line 59
                        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["consultation_info"]) ? $context["consultation_info"] : null), "nick_name", array()), "html", null, true);
                        echo "\">
\t\t\t<input type=\"hidden\" name=\"nick_name\" value=\"";
                        // line 60
                        echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "nick_name", array()), "html", null, true);
                        echo "\">
\t\t\t<h3 class=\"content_ttl\">返信コメント</h3>
\t\t\t<p class=\"error\">・本文を入力してください。</p>
\t\t\t<textarea class=\"consult_message\" name=\"consult_message\" placeholder=\"返信コメントを入力してください\"></textarea>
\t\t\t<ul class=\"btn_wrap\">
\t\t\t\t<li>
\t\t\t\t\t<button type=\"submit\" class=\"btn_style btn_blue\">送信する</button>
\t\t\t\t</li>
\t\t\t</ul>
\t\t\t</form>
\t\t</div>\t\t\t\t\t\t
\t\t";
                    }
                    // line 72
                    echo "\t<script>
\t\$(function() {
\t\t\$(\".validate_reply";
                    // line 74
                    echo twig_escape_filter($this->env, $context["key"], "html", null, true);
                    echo "\").validate({
\t\t\trules: {
\t\t\t\tconsult_message:{
\t\t\t\t\trequired: true
\t\t\t\t}
\t\t\t},
\t\t\tmessages: {
\t\t\t\tconsult_message:{
\t\t\t\t\trequired: \"返信コメントを入力してください。\"
\t\t\t\t}
\t\t\t},
\t\t\terrorElement: \"p\",
\t\t\terrorPlacement:function(error,element){
\t\t\t\terror.insertBefore(\$(element));
\t\t\t},
\t\t\tsubmitHandler: function(form) {
\t\t\t\tvar \$form = \$('.validate_reply";
                    // line 90
                    echo twig_escape_filter($this->env, $context["key"], "html", null, true);
                    echo "');
\t\t\t\t\$.ajax({
\t\t\t\t    url:\"";
                    // line 92
                    echo twig_escape_filter($this->env, (isset($context["base_url"]) ? $context["base_url"] : null), "html", null, true);
                    echo "bb/reply_answer\",
\t\t\t        type:\"POST\",
\t\t\t        data:\$form.serialize(),
\t\t\t\t}).done(function(data){
\t\t\t\t\t\$('.comment_reply";
                    // line 96
                    echo twig_escape_filter($this->env, $context["key"], "html", null, true);
                    echo "').replaceWith(data.html);
\t\t\t\t}).fail(function(data){
\t\t\t\t    alert('error!!!');
\t\t\t\t});
\t\t\t}
\t\t});
\t});
\t</script>
\t";
                }
                // line 105
                echo "
<!--\t\t\t\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['key2'], $context['entry2'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 106
            echo "  -->

</div>
";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['key'], $context['entry'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
    }

    public function getTemplateName()
    {
        return "bb/consult_ajax.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  248 => 106,  241 => 105,  229 => 96,  222 => 92,  217 => 90,  198 => 74,  194 => 72,  179 => 60,  175 => 59,  171 => 58,  167 => 57,  163 => 56,  158 => 55,  155 => 54,  141 => 49,  135 => 46,  128 => 42,  119 => 36,  115 => 35,  112 => 34,  110 => 33,  104 => 32,  90 => 27,  84 => 24,  80 => 22,  74 => 20,  71 => 19,  65 => 17,  59 => 15,  57 => 14,  52 => 13,  50 => 12,  45 => 11,  42 => 10,  39 => 9,  33 => 7,  31 => 6,  23 => 2,  19 => 1,);
    }
}
