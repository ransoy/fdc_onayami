<?php

/* bb/answer.html */
class __TwigTemplate_3ba24ce456654b3b654292286075a111fc1979210dd0e5d386292d13ff9524ee extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!doctype html>
<html>
<head>
<meta charset=\"utf-8\">
<title>回答を入力</title>
";
        // line 6
        echo twig_include($this->env, $context, "./inc/link.html");
        echo "
<script>
\$(function() {
\thighslide_set();
});
</script>
</head>
<body>
<div id=\"highslide-container\"></div>
<div id=\"img_zoom_wrap\">
\t<div id=\"img_zoom\" style=\"width:150%;\"></div>
</div>\t
<div class=\"page_wrap page_answer\">
\t";
        // line 19
        echo twig_include($this->env, $context, "./inc/header.html");
        echo "
\t<div class=\"content_wrap cf\">
\t\t<div class=\"content\">
\t\t\t<section class=\"sec sec_consult_question m_b_40\">
\t\t\t\t<div class=\"content_inner\">
\t\t\t\t\t<h2 class=\"sec_ttl ic ";
        // line 24
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["consultation_info"]) ? $context["consultation_info"] : null), "class", array()), "html", null, true);
        echo "\"><strong>";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["consultation_info"]) ? $context["consultation_info"] : null), "big_category_name", array()), "html", null, true);
        echo "</strong></h2>
\t\t\t\t\t<h3 class=\"content_ttl\">";
        // line 25
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["consultation_info"]) ? $context["consultation_info"] : null), "title", array()), "html", null, true);
        echo "</h3>
\t\t\t\t\t<p class=\"sec_desc\">
\t\t\t\t\t\t<span>相談者：";
        // line 27
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["consultation_info"]) ? $context["consultation_info"] : null), "nick_name", array()), "html", null, true);
        echo "さん</span>
\t\t\t\t\t\t<span class=\"post_date\">";
        // line 28
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["consultation_info"]) ? $context["consultation_info"] : null), "time_ago", array()), "html", null, true);
        echo "</span>
\t\t\t\t\t</p>
\t\t\t\t\t<div class=\"consult_question_content\">
\t\t\t\t\t\t";
        // line 31
        if (($this->getAttribute((isset($context["consultation_info"]) ? $context["consultation_info"] : null), "up_image", array()) != null)) {
            // line 32
            echo "\t\t\t\t\t\t<p><img src=\"";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["consultation_info"]) ? $context["consultation_info"] : null), "up_image", array()), "html", null, true);
            echo "\"></p>
\t\t\t\t\t\t";
        }
        // line 34
        echo "\t\t\t\t\t\t<p>";
        echo nl2br($this->getAttribute((isset($context["consultation_info"]) ? $context["consultation_info"] : null), "message", array()));
        echo "</p>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</section>
\t\t\t<section class=\"sec sec_answer m_b_30\">
\t\t\t\t<div class=\"content_inner\">
\t\t\t\t\t<form method=\"post\" action=\"";
        // line 40
        echo twig_escape_filter($this->env, (isset($context["base_url"]) ? $context["base_url"] : null), "html", null, true);
        echo "bb/answer_confirm\" class=\"answer_form validate\">
\t\t\t\t\t\t<input type=\"hidden\" name=\"nickname\" value=\"";
        // line 41
        echo twig_escape_filter($this->env, (isset($context["nickname"]) ? $context["nickname"] : null), "html", null, true);
        echo "\">
\t\t\t\t\t\t<input type=\"hidden\" name=\"answer_id\" value=\"";
        // line 42
        echo twig_escape_filter($this->env, (isset($context["answer_id"]) ? $context["answer_id"] : null), "html", null, true);
        echo "\">
\t\t\t\t\t\t<h2 class=\"sec_ttl\">回答の入力</h2>
\t\t\t\t\t\t<div class=\"input_form\">
\t\t\t\t\t\t\t<dl class=\"input_nickname\">
\t\t\t\t\t\t\t\t<dt class=\"ic_required\">回答者のお名前</dt>
\t\t\t\t\t\t\t\t<dd>
\t\t\t\t\t\t\t\t\t<p>";
        // line 48
        echo twig_escape_filter($this->env, (isset($context["nickname"]) ? $context["nickname"] : null), "html", null, true);
        echo "さん</p>
\t\t\t\t\t\t\t\t\t<p class=\"t_right\"><a href=\"";
        // line 49
        echo twig_escape_filter($this->env, (isset($context["main_url"]) ? $context["main_url"] : null), "html", null, true);
        echo "user/settings\" class=\"t_link\">ニックネームの変更</a></p>
\t\t\t\t\t\t\t\t</dd>
\t\t\t\t\t\t\t</dl>
\t\t\t\t\t\t\t<dl class=\"input_content\">
\t\t\t\t\t\t\t\t<dt class=\"ic_required\">回答内容</dt>
\t\t\t\t\t\t\t\t<dd class=\"cf ui-count_form\">
\t\t\t\t\t\t\t\t\t";
        // line 56
        echo "\t\t\t\t\t\t\t\t\t";
        echo form_textarea("answer_message", set_value("answer_message", "", false), array("placeholder" => "回答内容を入力してください", "maxlength" => 1000, "class" => "ui-count_input"));
        echo "
\t\t\t\t\t\t\t\t\t<p class=\"input_desc\">※全角30文字～1000文字</p>
\t\t\t\t\t\t\t\t\t<p class=\"input_count\">1000</p>
\t\t\t\t\t\t\t\t</dd>
\t\t\t\t\t\t\t</dl>
\t\t\t\t\t\t\t<dl>
\t\t\t\t\t\t\t\t<dt>画像アップロード</dt>
\t\t\t\t\t\t\t\t<dd>
\t\t\t\t\t\t\t\t\t<input id=\"upload_img\" type=\"file\" value=\"\" name=\"userfile\"></p>
\t\t\t\t\t\t\t\t\t<input id=\"image_file_name\" type=\"hidden\" value=\"\" name=\"image_file_name\">
\t\t\t\t\t\t\t\t</dd>
\t\t\t\t\t\t\t</dl>
\t\t\t\t\t\t\t<ul class=\"btn_wrap\">
\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t<button type=\"submit\" class=\"btn_style btn_green\">入力内容の確認へ</button>
\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t</div>
\t\t\t\t\t</form>
\t\t\t\t</div>
\t\t\t</section>
\t\t</div>
\t\t";
        // line 78
        echo twig_include($this->env, $context, "./inc/sidebar.html");
        echo "
\t</div>
\t";
        // line 80
        echo twig_include($this->env, $context, "./inc/footer.html");
        echo "
</div>
";
        // line 82
        echo twig_include($this->env, $context, "./inc/script.html");
        echo "
<script>
\$(function(){\t
\t\$('textarea').blur(function(){
\t\t\$(this).val(\$(this).val().trim());
\t});
});
</script>
</body>
</html>
";
    }

    public function getTemplateName()
    {
        return "bb/answer.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  155 => 82,  150 => 80,  145 => 78,  119 => 56,  110 => 49,  106 => 48,  97 => 42,  93 => 41,  89 => 40,  79 => 34,  73 => 32,  71 => 31,  65 => 28,  61 => 27,  56 => 25,  50 => 24,  42 => 19,  26 => 6,  19 => 1,);
    }
}
