<?php

/* bb/reply_consult_ajax.html */
class __TwigTemplate_3b0ad104b43726dda825a23fdf4e7a0c34daa48f9b304f054b88f1e0b0565752 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "\t<ul class=\"destination\">
\t\t<li>";
        // line 2
        echo twig_escape_filter($this->env, (isset($context["replay_nick_name"]) ? $context["replay_nick_name"] : null), "html", null, true);
        echo "</li>
\t\t<li>";
        // line 3
        echo twig_escape_filter($this->env, (isset($context["nick_name"]) ? $context["nick_name"] : null), "html", null, true);
        echo "</li>
\t</ul>

\t\t<div class=\"comment_reply\">
\t\t<div class=\"comment\">
\t\t\t<p>
\t\t\t";
        // line 9
        echo twig_escape_filter($this->env, (isset($context["consult_message"]) ? $context["consult_message"] : null), "html", null, true);
        echo "
\t\t\t</p>
\t\t</div>
\t\t<div class=\"content_bottom cf\">
\t\t\t<p class=\"post_date\">ちょうど今</p>

\t\t\t<ul class=\"btn_wrap\">
\t\t\t\t<li><a href=\"javascript:void(0);\" class=\"btn_style btn_orange-o btn_update_eval\" data-mess-id=\"";
        // line 16
        echo twig_escape_filter($this->env, (isset($context["message_id"]) ? $context["message_id"] : null), "html", null, true);
        echo "\" data-user-id=\"";
        echo twig_escape_filter($this->env, (isset($context["user_id"]) ? $context["user_id"] : null), "html", null, true);
        echo "\">私もそう思う<span class=\"ic ic_heart\">0</span></a></li>
\t\t\t</ul>
\t\t</div>";
    }

    public function getTemplateName()
    {
        return "bb/reply_consult_ajax.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  45 => 16,  35 => 9,  26 => 3,  22 => 2,  19 => 1,);
    }
}
