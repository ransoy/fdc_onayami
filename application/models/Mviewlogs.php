<?php
    class Mviewlogs extends CI_Model
    {
        function __construct() {
            parent::__construct();
        }

        function insert($data)
        {
            $data['create_date'] = date('Y-m-d H:i:s');
            $data['create_ip'] = $_SERVER['REMOTE_ADDR'];
            if(!empty($this->user['id']))
                $data[' user_id'] = $this->user['id'];
            $this->db->insert('bbs_view_logs', $data);
            return  $this->db->insert_id();
        }

        function AccessToday($id)
        {

            $data = array($id, $_SERVER['REMOTE_ADDR']);
            $user = (!empty($this->user['id'])) ? '= '.$this->user['id']:'IS NULL';
            $sql = "
                SELECT 
                    avbl.thread_id as id
                FROM bbs_view_logs AS avbl 
                WHERE avbl.thread_id = ?
                AND avbl.user_id ".$user."
                AND avbl.create_ip = ?
                AND DATE(avbl.create_date) = CURDATE()
            ";
            $query = $this->db->query($sql, $data);
            $row =  $query->row_array();
            return (empty($row))? false : true;
        }

        function get3daysTopFourViewed()
        {
            $sql = "
                SELECT bbs_view_logs.big_cate_id, bbs_view_logs.cate_id,
                    COUNT(bbs_view_logs.thread_id) AS viewed_count,
                    bbs_categorys.name
                FROM bbs_view_logs
                INNER JOIN bbs_categorys ON bbs_view_logs.cate_id = bbs_categorys.id
                INNER JOIN bbs_threads ON bbs_view_logs.thread_id = bbs_threads.id
                WHERE bbs_threads.display_flag = 1
                AND bbs_view_logs.create_date BETWEEN CURDATE() - INTERVAL 2 DAY AND CURDATE() + INTERVAL 1 DAY
                GROUP BY bbs_view_logs.cate_id
                ORDER BY viewed_count DESC
                LIMIT 4;
            ";
            $query = $this->db->query($sql);
            return $query->result_array();
        }

    }
?>
