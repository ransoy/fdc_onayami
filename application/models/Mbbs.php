<?php
class Mbbs extends CI_Model {
    function __construct() {
        // Call the Model constructor
        parent::__construct();
    }

    /**
     * @author  [VJS] Kiyoshi Suzuki
     * @name    insert_consultation
     * @param
     */
    public function insert_consultation($user_id, $title, $message, $big_cate_id, $cate_id = 0)
    {
        $data = array(
            "board_id"=>1,
            "big_cate_id"=>$big_cate_id,
            "cate_id"=>$cate_id,
            "user_id"=>$user_id,
            "title"=>$title,
            "message"=>$message,
            "create_ip"=> $this->input->ip_address(),
            "update_date" =>date("Y-m-d H:i:s"),
            "create_date" =>date("Y-m-d H:i:s"),
        );
        $query=$this->db->insert("bbs_threads", $data);
        if ($query) {     //データ取得が成功したらTrue、失敗したらFalseを返す
            $id = $this->db->insert_id();
            return $id;
        } else {
            return false;
        }
    }

    public function get_consultation($id)
    {
        $this->db->select("
            bbs_threads.id,
            bbs_threads.user_id,
            bbs_threads.title,
            bbs_threads.big_cate_id,
            bbs_threads.cate_id,
            bbs_threads.like_count,
            bbs_threads.message,
            bbs_threads.up_image,
            bbs_threads.image_up_flag,
            users.nick_name, 
            bbs_big_categorys.name AS big_category_name, 
            bbs_big_categorys.class,bbs_categorys.name AS category_name,
             (CASE 
                        WHEN (TIMESTAMPDIFF(HOUR, bbs_threads.create_date, NOW()) >= 24) THEN DATE_FORMAT(bbs_threads.create_date,'%Y/%m/%d %H:%i')
                        WHEN (TIMESTAMPDIFF(MINUTE, bbs_threads.create_date, NOW()) >= 60 ) THEN CONCAT(TIMESTAMPDIFF(HOUR, bbs_threads.create_date, NOW()),'時間前')
                        WHEN (TIMESTAMPDIFF(SECOND, bbs_threads.create_date, NOW()) >= 60 ) THEN CONCAT(TIMESTAMPDIFF(MINUTE, bbs_threads.create_date, NOW()),'分前')
                        ELSE 'ちょうど今'
                    END
                ) as time_ago,
            ");
        $this->db->select('(IFNULL(count(bbs_messages.id),0) - IFNULL(sum(bbs_messages.msg_from_flag),0)) as comment_count');
        $this->db->from('bbs_threads');
        $this->db->join('users', 'users.id = bbs_threads.user_id', 'INNER');
        $this->db->join('bbs_big_categorys', 'bbs_big_categorys.id = bbs_threads.big_cate_id', 'INNER');
        $this->db->join('bbs_categorys', 'bbs_categorys.id = bbs_threads.cate_id', 'INNER');
        $this->db->join('bbs_messages', 'bbs_messages.thread_id = bbs_threads.id AND bbs_messages.display_flag = 1', 'LEFT');

        $this->db->where('bbs_threads.id', $id);
        $this->db->where('bbs_threads.display_flag', 1);
        $query = $this->db->get();
        if ($query) {     //データ取得が成功したらTrue、失敗したらFalseを返す
            return $query->row_array();
        } else {
            return false;
        }
    }

    /* 返信 */
    public function insert_reply($user_id, $thread_id, $message, $message_id)
    {
        $data = array(
            "board_id"=>1,
            "thread_id"=>$thread_id,
            "user_id"=>$user_id,
            "attr"=>1,
            "message"=>$message,
            "msg_from_flag"=>1,
            "update_date" =>date("Y-m-d H:i:s"),
            "create_date" =>date("Y-m-d H:i:s"),
        );
        $query=$this->db->insert("bbs_messages", $data);
        if ($query) {     //データ取得が成功したらTrue、失敗したらFalseを返す
            $id = $this->db->insert_id();

            $this->db->where('id', $message_id);
            $ar = array('reply_id' => $id);
            $query = $this->db->update('bbs_messages', $ar);

            return $id;
        } else {
            return false;
        }
    }

    /* 回答 */
    public function insert_response($user_id, $thread_id, $message)
    {
        $data = array(
            "board_id"=>1,
            "thread_id"=>$thread_id,
            "user_id"=>$user_id,
            "attr"=>1,
            "create_ip" => $this->input->ip_address(),
            "message"=>$message,
            "update_date" =>date("Y-m-d H:i:s"),
            "create_date" =>date("Y-m-d H:i:s"),
        );
        $query=$this->db->insert("bbs_messages", $data);
        if ($query) {     //データ取得が成功したらTrue、失敗したらFalseを返す
            $id = $this->db->insert_id();
            return $id;
        } else {
            return false;
        }
    }

    /* みんなの回答取得 */
    public function get_response($id, $start = 0)
    {
        $this->db->select("
            bbs_messages.id,
            bbs_messages.message, 
            bbs_messages.evaluate,
            bbs_messages.thread_id,
            users.id AS response_id,
            bbs_messages.up_image,
            bbs_messages.reply_id,
            bbs_messages.user_id, 
            users.nick_name,
             (CASE 
                WHEN (TIMESTAMPDIFF(HOUR, bbs_messages.create_date, NOW()) >= 24) THEN DATE_FORMAT(bbs_messages.create_date,'%Y/%m/%d %H:%i')
                WHEN (TIMESTAMPDIFF(MINUTE, bbs_messages.create_date, NOW()) >= 60 ) THEN CONCAT(TIMESTAMPDIFF(HOUR, bbs_messages.create_date, NOW()),'時間前')
                WHEN (TIMESTAMPDIFF(SECOND, bbs_messages.create_date, NOW()) >= 60 ) THEN CONCAT(TIMESTAMPDIFF(MINUTE, bbs_messages.create_date, NOW()),'分前')
                ELSE 'ちょうど今'
                END
             ) as time_ago,
             IFNULL(bbs_messages_rank.rank,0) as rank
            ");
        $this->db->from('bbs_messages');
        $this->db->join('users', 'users.id = bbs_messages.user_id', 'INNER');
        $this->db->join('bbs_messages_rank', 'bbs_messages.id = bbs_messages_rank.id', 'LEFT');
        $this->db->where('bbs_messages.board_id', 1);
        $this->db->where('bbs_messages.thread_id', $id);
        $this->db->where('bbs_messages.msg_from_flag', 0);
        $this->db->where('bbs_messages.display_flag', 1);
        $this->db->limit(10, $start);
        $this->db->order_by('bbs_messages.id', 'DESC');
        $query = $this->db->get();

        if ($query) {     //データ取得が成功したらTrue、失敗したらFalseを返す
            $array = $query->result_array();
            foreach ($array as $key => $val) {
                if ($val['reply_id'] != NULL) {
                    $reply_id = $val['reply_id'];
                    $this->db->select("
                        bbs_messages.id,
                        bbs_messages.message, 
                        bbs_messages.evaluate,
                        bbs_messages.reply_id,
                        users.id AS response_id,
                        bbs_messages.thread_id,
                        bbs_messages.up_image, 
                        users.nick_name,
                        (CASE 
                            WHEN (TIMESTAMPDIFF(HOUR, bbs_messages.create_date, NOW()) >= 24) THEN DATE_FORMAT(bbs_messages.create_date,'%Y/%m/%d %H:%i')
                            WHEN (TIMESTAMPDIFF(MINUTE, bbs_messages.create_date, NOW()) >= 60 ) THEN CONCAT(TIMESTAMPDIFF(HOUR, bbs_messages.create_date, NOW()),'時間前')
                            WHEN (TIMESTAMPDIFF(SECOND, bbs_messages.create_date, NOW()) >= 60 ) THEN CONCAT(TIMESTAMPDIFF(MINUTE, bbs_messages.create_date, NOW()),'分前')
                            ELSE 'ちょうど今'
                            END
                         ) as time_ago,
                        ");
                    $this->db->from('bbs_messages');
                    $this->db->join('users', 'users.id = bbs_messages.user_id', 'INNER');
                    $this->db->where('bbs_messages.id', $reply_id);
                    $this->db->where('bbs_messages.board_id', 1);
                    $this->db->where('bbs_messages.thread_id', $id);
                    $this->db->where('bbs_messages.msg_from_flag', 1);
                    $this->db->where('bbs_messages.display_flag', 1);
                    $query = $this->db->get();
                    $res =  $query->row_array();
                    $array[$key]['reply_message_id'] = $res['id'];
                    $array[$key]['reply_message'] = $res['message'];
                    $array[$key]['replay_owner_id'] = $res['response_id'];
                    $array[$key]['replay_nick_name'] = $res['nick_name'];
                    $array[$key]['replay_cre_date'] = $res['time_ago'];
                    $array[$key]['replay_evaluate'] = $res['evaluate'];
                    $array[$key]['replay_orange_color'] = ($res['evaluate'] > 0)? 'btn_orange': 'btn_orange-o';
                }
            }
            return $array;

        } else {
            return false;
        }
    }

    public function get_consultation_list_all_num()
    {
        $this->db->select('bbs_threads.id');
        $this->db->from('bbs_threads');
        $this->db->join('users', 'users.id = bbs_threads.user_id', 'INNER');
        $this->db->join('bbs_big_categorys', 'bbs_big_categorys.id = bbs_threads.big_cate_id', 'INNER');
        $this->db->join('bbs_categorys', 'bbs_categorys.id = bbs_threads.cate_id', 'INNER');
        $this->db->where('bbs_threads.display_flag', 1);
        $query = $this->db->get();
        $res = $query->num_rows();
        return $res;
    }

    public function get_consultation_list_all($limit = null, $params = array(), $offset = null, $content = null)
    {
        $sidebar = true;
        if ($content == null || ($content != null && $content != 'sidebar')) {
            $sidebar = false;
        }
        $this->db->select('
            bt.id, 
            bt.big_cate_id, 
            bt.cate_id, 
            bt.id, 
            bt.title, 
            bt.create_date, 
            bt.message,
        ');
        if (!$sidebar) { //select only these fields if needed
            $this->db->select('
                users.nick_name, 
                bbc.icon, 
                bbc.name AS big_category_name, 
                bc.name AS category_name, 
                bbc.class,
                (bt.like_count + IFNULL(sum(bm.evaluate),0)) AS like_count,
                (IFNULL(count(bm.id),0) - IFNULL(sum(bm.msg_from_flag),0)) as comment_count
            ');
        }
        $this->db->from('bbs_threads AS bt');
        if (!$sidebar) { // only join these fields when needed
            $this->db->join('users', 'users.id = bt.user_id', 'INNER');
            $this->db->join('bbs_big_categorys AS bbc', 'bbc.id = bt.big_cate_id', 'INNER');
            $this->db->join('bbs_categorys AS bc', 'bc.id = bt.cate_id', 'INNER');
            $this->db->join('bbs_messages AS bm', 'bm.thread_id = bt.id AND bm.display_flag = 1', 'LEFT');
        }
        $this->db->where('bt.display_flag', 1);
        if($content == 'unanswered') {
            $this->db->where('bm.id', null);
        }
        if (!empty($params)) {
            $this->db->where($params['row']." = ", $params['value']);
        }        
        $this->db->group_by('bt.id');
        if ($content == 'popular') { 
            //join and order by popularity
            $this->db->join("
                (   SELECT bvl.create_date, bvl.thread_id, COUNT(bvl.thread_id) AS viewed_count
                    FROM bbs_view_logs AS bvl
                    WHERE bvl.create_date BETWEEN CURDATE() - INTERVAL 2 DAY AND CURDATE() + INTERVAL 1 DAY
                    GROUP BY bvl.thread_id 
                    ORDER BY viewed_count desc
                ) AS bbs_view_logs
                ", 
                'bt.id = bbs_view_logs.thread_id', 
                'LEFT');
            $this->db->where('bbs_view_logs.create_date BETWEEN CURDATE() - INTERVAL 2 DAY AND CURDATE() + INTERVAL 1 DAY');   
            $this->db->order_by("bbs_view_logs.viewed_count", "desc");
            $this->db->order_by("bbs_view_logs.create_date", "desc");             
        } else {
            $this->db->order_by("bt.create_date", "desc");            
        }
        if ($limit != null) {
            $this->db->limit($limit);
        }
        if ($offset != null) {
            $this->db->offset($offset);
        }
        $query = $this->db->get();
        if ($query) {     //データ取得が成功したらTrue、失敗したらFalseを返す
            return $query->result_array();
        } else {
            return false;
        }
    }

    public function search_consultation_list_num($big_category_id = null, $category_id = null)
    {
        $this->db->select('bbs_threads.id');
        $this->db->from('bbs_threads');
        $this->db->join('bbs_big_categorys', 'bbs_big_categorys.id = bbs_threads.big_cate_id', 'INNER');
        $this->db->join('bbs_categorys', 'bbs_categorys.id = bbs_threads.cate_id', 'INNER');
        $this->db->join('bbs_messages', 'bbs_messages.thread_id = bbs_threads.id AND bbs_messages.display_flag = 1', 'LEFT');
        $this->db->where('bbs_messages.reply_id', null);
        $this->db->where('bbs_threads.board_id', 1);
        $this->db->where('bbs_threads.display_flag', 1);
        $this->db->where('bbs_threads.big_cate_id', $big_category_id);
        if ($category_id != null) {
            $this->db->where('bbs_threads.cate_id', $category_id);
        }
        $this->db->group_by('bbs_threads.id');
        $query = $this->db->get();
        $res = $query->num_rows();
        return $res;
    }
    
    public function search_consultation_list($limit = null, $offset = null, $big_category_id = null, $category_id = null)
    {
        $this->db->select('bbs_threads.id, bbs_threads.big_cate_id, bbs_threads.cate_id, bbs_threads.id');
        $this->db->select('bbs_threads.title, bbs_threads.create_date, bbs_threads.message');
        $this->db->select('bbs_big_categorys.name AS big_category_name, bbs_categorys.name AS category_name, bbs_big_categorys.class');
        $this->db->select('(bbs_threads.like_count + IFNULL(sum(bbs_messages.evaluate),0)) AS like_count');
        $this->db->select('(IFNULL(count(bbs_messages.id),0) - IFNULL(sum(bbs_messages.msg_from_flag),0)) as comment_count');
        $this->db->from('bbs_threads');
        $this->db->join('bbs_big_categorys', 'bbs_big_categorys.id = bbs_threads.big_cate_id', 'INNER');
        $this->db->join('bbs_categorys', 'bbs_categorys.id = bbs_threads.cate_id', 'INNER');
        $this->db->join('bbs_messages', 'bbs_messages.thread_id = bbs_threads.id AND bbs_messages.display_flag = 1', 'LEFT');

        $this->db->where('bbs_threads.board_id', 1);
        $this->db->where('bbs_threads.display_flag', 1);
        $this->db->where('bbs_threads.big_cate_id', $big_category_id);
        if ($category_id != null) {
            $this->db->where('bbs_threads.cate_id', $category_id);
        }
        $this->db->group_by('bbs_threads.id');
        $this->db->order_by('bbs_threads.create_date', 'desc');
        if ($limit != null) {
            $this->db->limit($limit);
        }
        if ($offset != null) {
            $this->db->offset($offset);
        }
        $query = $this->db->get();
        if ($query) {     //データ取得が成功したらTrue、失敗したらFalseを返す
            return $query->result_array();
        } else {
            return false;
        }
    }

    public function search_keyword_consultation_list_num($keyword = null)
    {
        $this->db->select('bbs_threads.id');  
        $this->db->from('bbs_threads'); 
        $this->db->join('bbs_messages', 'bbs_messages.thread_id = bbs_threads.id AND bbs_messages.display_flag = 1', 'LEFT');
        $this->db->join('bbs_big_categorys', 'bbs_big_categorys.id = bbs_threads.big_cate_id', 'INNER');
        $this->db->join('bbs_categorys', 'bbs_categorys.id = bbs_threads.cate_id', 'INNER');

        $this->db->where('bbs_threads.board_id', 1);
        $this->db->where('bbs_threads.display_flag', 1);        
        $keyword = $this->db->escape_like_str($keyword);
        $like = "(bbs_threads.title LIKE '%{$keyword}%' OR bbs_threads.message LIKE '%{$keyword}%' OR bbs_messages.message LIKE '%{$keyword}%')";
        $this->db->where($like, NULL, FALSE);
        $this->db->group_by('bbs_threads.id');

        $query = $this->db->get();
        $res = $query->num_rows();
        return $res;
    }

    public function search_keyword_consultation_list($keyword = null, $limit = null, $offset = null)
    {
        $this->db->select('bbs_threads.id, bbs_threads.big_cate_id, bbs_threads.cate_id, bbs_threads.id');
        $this->db->select('bbs_threads.title, bbs_threads.create_date, bbs_threads.message');
        $this->db->select('bbs_big_categorys.name AS big_category_name, bbs_categorys.name AS category_name, bbs_big_categorys.class');
        $this->db->select('(bbs_threads.like_count + IFNULL(sum(bbs_messages.evaluate),0)) AS like_count');
        $this->db->select('(IFNULL(count(bbs_messages.id),0) - IFNULL(sum(bbs_messages.msg_from_flag),0)) as comment_count');
        $this->db->from('bbs_threads');
        $this->db->join('bbs_messages', 'bbs_messages.thread_id = bbs_threads.id AND bbs_messages.display_flag = 1', 'LEFT');
        $this->db->join('bbs_big_categorys', 'bbs_big_categorys.id = bbs_threads.big_cate_id', 'INNER');
        $this->db->join('bbs_categorys', 'bbs_categorys.id = bbs_threads.cate_id', 'INNER');

        $this->db->where('bbs_threads.board_id', 1);
        $this->db->where('bbs_threads.display_flag', 1);
        $keyword = $this->db->escape_like_str($keyword);
        $like = "(bbs_threads.title LIKE '%{$keyword}%' OR bbs_threads.message LIKE '%{$keyword}%' OR bbs_messages.message LIKE '%{$keyword}%')";
        $this->db->where($like, NULL, FALSE);
        $this->db->group_by('bbs_threads.id');
        $this->db->order_by('bbs_threads.create_date', 'desc');
        if ($limit != null) {
            $this->db->limit($limit);
        }
        if ($offset != null) {
            $this->db->offset($offset);
        }
        $query = $this->db->get();
        if ($query) {     //データ取得が成功したらTrue、失敗したらFalseを返す
            return $query->result_array();
        } else {
            return false;
        }
    }

    public function my_page_search_keyword_consultation_list_num($keyword = null) 
    {
        $this->db->select('bbs_threads.id');
        $this->db->from('bbs_threads');
        $this->db->join('bbs_messages', 'bbs_messages.thread_id = bbs_threads.id AND bbs_messages.display_flag = 1', 'LEFT');
        $this->db->join('bbs_big_categorys', 'bbs_big_categorys.id = bbs_threads.big_cate_id', 'INNER');
        $this->db->join('bbs_categorys', 'bbs_categorys.id = bbs_threads.cate_id', 'INNER');

        $this->db->where('bbs_threads.board_id', 1);
        $this->db->where('bbs_messages.reply_id', null);
        $this->db->where('bbs_threads.display_flag', 1);
        $keyword = $this->db->escape_like_str($keyword);
        $like = "(bbs_threads.title LIKE '%{$keyword}%' OR bbs_threads.message LIKE '%{$keyword}%' OR bbs_messages.message LIKE '%{$keyword}%')";
        $this->db->where($like, NULL, FALSE);
        $this->db->group_by('bbs_threads.id');
        $query = $this->db->get();
        $res = $query->num_rows();
        return $res;
    }

    public function my_page_search_keyword_consultation_list($keyword = null, $limit = null, $offset = null)
    {
        $this->db->select('bbs_threads.id, bbs_threads.big_cate_id, bbs_threads.cate_id, bbs_threads.id');
        $this->db->select('bbs_threads.title, bbs_threads.create_date, bbs_threads.message');
        $this->db->select('bbs_big_categorys.name AS big_category_name, bbs_categorys.name AS category_name, bbs_big_categorys.class');
        $this->db->select('(bbs_threads.like_count + IFNULL(sum(bbs_messages.evaluate),0)) AS like_count');
        $this->db->select('(IFNULL(count(bbs_messages.id),0) - IFNULL(sum(bbs_messages.msg_from_flag),0)) as comment_count');
        $this->db->from('bbs_threads');
        $this->db->join('bbs_messages', 'bbs_messages.thread_id = bbs_threads.id AND bbs_messages.display_flag = 1', 'LEFT');
        $this->db->join('bbs_big_categorys', 'bbs_big_categorys.id = bbs_threads.big_cate_id', 'INNER');
        $this->db->join('bbs_categorys', 'bbs_categorys.id = bbs_threads.cate_id', 'INNER');

        $this->db->where('bbs_threads.board_id', 1);
        $this->db->where('bbs_threads.display_flag', 1);
        $keyword = $this->db->escape_like_str($keyword);
        $like = "(bbs_threads.title LIKE '%{$keyword}%' OR bbs_threads.message LIKE '%{$keyword}%' OR bbs_messages.message LIKE '%{$keyword}%')";
        $this->db->where($like, NULL, FALSE);
        $this->db->group_by('bbs_threads.id');        
        $this->db->order_by('bbs_threads.create_date', 'desc');
        if ($limit != null) {
            $this->db->limit($limit);
        }
        if ($offset != null) {
            $this->db->offset($offset);
        }
        $query = $this->db->get();
        if ($query) {     //データ取得が成功したらTrue、失敗したらFalseを返す
            return $query->result_array();
        } else {
            return false;
        }
    }

    /* 未回答の相談を総数取得 */
    public function get_unanswered_num($user_id)
    {
        $this->db->select('bbs_threads.id');
        $this->db->from('bbs_threads');
        $this->db->join('bbs_messages', 'bbs_messages.thread_id = bbs_threads.id AND bbs_messages.display_flag = 1', 'LEFT');
        $this->db->where('bbs_threads.user_id', $user_id);
        $this->db->where('bbs_threads.display_flag', 1);        
        $this->db->where('bbs_messages.id', null);
        $this->db->group_by("bbs_threads.id");
        $query = $this->db->get();
        $res = $query->num_rows();
        return $res;
    }

    /* 未回答の相談を取得 */
    public function get_unanswered($user_id, $limit = null, $offset = null)
    {
        $this->db->select('bbs_threads.id, bbs_threads.big_cate_id, bbs_threads.cate_id, bbs_threads.id');
        $this->db->select('bbs_threads.title, bbs_threads.create_date, bbs_threads.message, bbs_threads.like_count');
        $this->db->select('bbs_big_categorys.class, bbs_big_categorys.name AS big_category_name, bbs_categorys.name AS category_name');
        $this->db->select('IFNULL(count(bbs_messages.id),0) AS comment_count');
        $this->db->from('bbs_threads');
        $this->db->join('bbs_messages', 'bbs_messages.thread_id = bbs_threads.id AND bbs_messages.display_flag = 1', 'LEFT');
        $this->db->join('bbs_big_categorys', 'bbs_big_categorys.id = bbs_threads.big_cate_id', 'INNER');
        $this->db->join('bbs_categorys', 'bbs_categorys.id = bbs_threads.cate_id', 'INNER');
        $this->db->where('bbs_threads.user_id', $user_id);
        $this->db->where('bbs_threads.display_flag', 1);
        $this->db->where('bbs_messages.id', null);        
        $this->db->group_by("bbs_threads.id");
        $this->db->order_by("bbs_threads.create_date", "DESC");
        if ($limit != null) {
            $this->db->limit($limit);
        }
        if ($offset != null) {
            $this->db->offset($offset);
        }
        $query = $this->db->get();
//echo $this->db->last_query();
        if ($query) {     //データ取得が成功したらTrue、失敗したらFalseを返す
            return $query->result_array();
        } else {
            return false;
        }
    }

    /* マイページ未回答の相談を非表示 */
    public function del_unanswered($user_id, $ar)
    {
        $data = array(
                       'display_flag' => 3
                    );

        $i = 0;
        $sql = '(';
        foreach ($ar as $key => $val) {
            $sql .= ($i > 0)? ' OR id='.$val:'id='.$val;
            $i++;
        }
        $sql .= ')';
        $this->db->where($sql);
        $this->db->where('user_id', $user_id);
        $this->db->update('bbs_threads', $data);
        $res = $this->db->affected_rows();
        if ($this->db->affected_rows() > 0) {     //データ取得が成功したらTrue、失敗したらFalseを返す
            # deduct points to post owner target=1
            #deduct all onayami points for the comments target=4 
            $this->load->model('Mpoints');
            if($this->Mpoints->allowedUser($user_id)) {
                $cond = array(
                    'target' => 1, 
                    'user_id' => $user_id,
                    'thread_id' => $ar,
                    'bonus_requested_flag' => 0
                );
                $rows = $this->Mpoints->getOnayamiPoints($ar, $user_id, array(1));
                $result = $this->Mpoints->deduct($cond);
                if (!empty($rows)) {
                    foreach ($rows as $key => $value) {
                         $this->Mpoints->updateScoutBonus($value['user_id'], $value['point'] , '投稿ボーナスを引く', 1);
                    }
                }                 
                # delete comments and deduct points to all commenters under these posts
                $this->deleteMultipleComments($ar,$user_id);
            }            
            return true;
        } else {
            return false;
        }
    }


    /* 回答がある相談、総数取得 */
    public function get_answered_num($user_id)
    {
        $this->db->select('bbs_threads.id');
        $this->db->from('bbs_threads');
        $this->db->join('bbs_messages', 'bbs_messages.thread_id = bbs_threads.id AND bbs_messages.display_flag = 1', 'INNER');
        $this->db->where('bbs_threads.user_id', $user_id);
        $this->db->where('bbs_threads.display_flag', 1);
        $this->db->where('bbs_messages.reply_id', null);
        $this->db->group_by("bbs_threads.id");
        $query = $this->db->get();
        $res = $query->num_rows();
        return $res;
    }

    /* 回答がある相談を取得 */
    public function get_answered($user_id, $limit = null, $offset = null)
    {
        $this->db->select('bbs_threads.id, bbs_threads.big_cate_id, bbs_threads.cate_id, bbs_threads.id');
        $this->db->select('bbs_threads.title, bbs_threads.create_date, bbs_threads.message');
        $this->db->select('bbs_big_categorys.class, bbs_big_categorys.name AS big_category_name, bbs_categorys.name AS category_name');
        $this->db->select('(bbs_threads.like_count + IFNULL(sum(bbs_messages.evaluate),0)) AS like_count');
        $this->db->select('(IFNULL(count(bbs_messages.id),0) - IFNULL(sum(bbs_messages.msg_from_flag),0)) as comment_count');
        $this->db->from('bbs_threads');
        $this->db->join('bbs_messages', 'bbs_messages.thread_id = bbs_threads.id AND bbs_messages.display_flag = 1', 'INNER');
        $this->db->join('bbs_big_categorys', 'bbs_big_categorys.id = bbs_threads.big_cate_id', 'INNER');
        $this->db->join('bbs_categorys', 'bbs_categorys.id = bbs_threads.cate_id', 'INNER');
        $this->db->where('bbs_threads.user_id', $user_id);
        $this->db->where('bbs_threads.display_flag', 1);
        $this->db->group_by("bbs_threads.id");
        $this->db->order_by("bbs_threads.create_date", "DESC");
        if ($limit != null) {
            $this->db->limit($limit);
        }
        if ($offset != null) {
            $this->db->offset($offset);
        }
        $query = $this->db->get();
        if ($query) {     //データ取得が成功したらTrue、失敗したらFalseを返す
            return $query->result_array();
        } else {
            return false;
        }
    }

    /**
     * [add_evaluate description]
     * @param [type]  $user_id    [description]
     * @param [type]  $num        [description]
     * @param [type]  $message_id [description]
     * @param integer $mode       [description]
     */
    public function add_evaluate($user_id, $num, $message_id, $mode = 1)
    {
        $this->db->from('bbs_evaluate_log');
        $this->db->where('user_id', $user_id);
        $this->db->where('message_id', $message_id);
        $this->db->where("DATE_FORMAT(`create_date`, '%Y/%m/%d') = DATE_FORMAT(NOW(), '%Y/%m/%d')");
        $query = $this->db->get();
        $res = $query->num_rows();
        if ($query->num_rows() > 0) {
            return false;
        } else {
            $data = array(
                "board_id"=>1,
                "user_id"=>$user_id,
                "num"=>$num,
                "message_id"=>$message_id,
                "update_date" =>date("Y-m-d H:i:s"),
                "create_date" =>date("Y-m-d H:i:s"),
            );
            $query=$this->db->insert("bbs_evaluate_log", $data);
            if ($query) {     //成功したらTrue、失敗したらFalseを返す
                $id = $this->db->insert_id();
                if ($mode) {
                    $this->db->set('evaluate', 'evaluate+1', FALSE);
                    $this->db->where('id', $message_id);
                    $query = $this->db->update('bbs_messages');
                } else {           
                    $this->db->set('like_count', 'like_count+1', FALSE);       
                    $this->db->where('id', $message_id);
                    $query = $this->db->update('bbs_threads');
                }

                return array(
                        'id' => $id,
                        'flag' => $query
                    );
            } else {
                return false;
            }
        }
    }

    /**
     * [update_bookmark description]
     * @param  [int] $user_id   [id of the user]
     * @param  [int] $thread_id [id of the thread]
     * @param  [int] $mode      [1 for bookmark, 0 for unbookmark]
     * @return [boolean]        [return true if the query is success]
     */
    public function update_bookmark($user_id, $thread_id, $mode) {

        $this->db->from('bbs_bookmark');
        $this->db->where('user_id', $user_id);
        $this->db->where('thread_id', $thread_id);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $this->db->where('user_id', $user_id);
            $this->db->where('thread_id', $thread_id);
            $mode = ($mode)? 0 : 1 ;
            $ar = array('flag' => $mode);
            $query = $this->db->update('bbs_bookmark', $ar);
            if ($query) {
                return $mode;
            } else {
                return false;
            }
        } else {
            $data = array(
                "board_id"  => 1,
                "user_id"   => $user_id,
                "thread_id" => $thread_id,
                "flag"      => 1,
                "update_date" =>date("Y-m-d H:i:s"),
                "create_date" =>date("Y-m-d H:i:s"),
            );
            $query = $this->db->insert("bbs_bookmark", $data);
            return $query; 
        }
    }

    /* ブックマークを数取得 */
    public function get_bookmark_num($user_id)
    {
        $this->db->select('bbs_threads.id');
        $this->db->from('bbs_bookmark');
        $this->db->join('bbs_threads', 'bbs_bookmark.thread_id = bbs_threads.id', 'INNER');
        $this->db->where('bbs_bookmark.user_id', $user_id);
        $this->db->where('bbs_bookmark.flag', 1);
        $this->db->where('bbs_bookmark.board_id', 1);
        $this->db->where('bbs_threads.display_flag', 1);
        $query = $this->db->get();
        $res = $query->num_rows();
        return $res;
    }


    /* ブックマークを取得 */
    public function get_bookmark($user_id, $limit = null, $offset = null)
    {
        $this->db->select('bbs_threads.id, bbs_threads.big_cate_id, bbs_threads.cate_id, bbs_threads.id');
        $this->db->select('bbs_threads.title, bbs_threads.create_date, bbs_threads.message');
        $this->db->select('bbs_bookmark.id AS bookmark_id, bbs_big_categorys.class, bbs_big_categorys.name AS big_category_name, bbs_categorys.name AS category_name');
        $this->db->select('(bbs_threads.like_count + IFNULL(sum(bbs_messages.evaluate),0)) AS like_count');
        $this->db->select('(IFNULL(count(bbs_messages.id),0) - IFNULL(sum(bbs_messages.msg_from_flag),0)) as comment_count');
        $this->db->from('bbs_bookmark');
        $this->db->join('bbs_threads', 'bbs_bookmark.thread_id = bbs_threads.id', 'INNER');
        $this->db->join('bbs_big_categorys', 'bbs_big_categorys.id = bbs_threads.big_cate_id', 'INNER');
        $this->db->join('bbs_categorys', 'bbs_categorys.id = bbs_threads.cate_id', 'INNER');
        $this->db->join('bbs_messages', 'bbs_messages.thread_id = bbs_threads.id AND bbs_messages.display_flag = 1', 'LEFT');
        $this->db->where('bbs_bookmark.user_id', $user_id);
        $this->db->where('bbs_bookmark.flag', 1);
        $this->db->where('bbs_bookmark.board_id', 1);
        $this->db->where('bbs_threads.display_flag', 1);
        $this->db->group_by("bbs_threads.id");
        $this->db->order_by("bbs_threads.create_date", "DESC");
        if ($limit != null) {
            $this->db->limit($limit);
        }
        if ($offset != null) {
            $this->db->offset($offset);
        }
        $query = $this->db->get();
        if ($query) {     //データ取得が成功したらTrue、失敗したらFalseを返す
            return $query->result_array();
        } else {
            return false;
        }
    }

    /* マイページ未回答の相談を非表示 */
    public function del_bookmark($user_id, $ar)
    {
        $i = 0;
        $sql = '(';
        foreach ($ar as $key => $val) {
            $sql .= ($i > 0)? ' OR thread_id='.$val:'thread_id='.$val;
            $i++;
        }
        $sql .= ')';
        $this->db->where($sql);
        $this->db->where('user_id', $user_id);
        $this->db->where('board_id', 1);

        $data = array('flag' => 0);
        $this->db->update('bbs_bookmark', $data);
        $res = $this->db->affected_rows();
        if ($this->db->affected_rows() > 0) {//成功したらTrue、失敗したらFalseを返す
            return true;
        } else {
            return false;
        }
    }
        
    /**
     * Get Category and Sub Category
     * title and descriptions
     */
    public function getCategoryMeta() {
       $this->db->select(
            '
             bbs_big_categorys.id,
             bbs_big_categorys.title as b_title,
             bbs_big_categorys.description as b_desc,
             bbs_categorys.id as sub_id,
             bbs_categorys.title as sub_title,
             bbs_categorys.description as sub_description
            '
        );
       $this->db->from('bbs_big_categorys');
       $this->db->join('bbs_categorys','bbs_categorys.big_category_id = bbs_big_categorys.id','LEFT');
       $query = $this->db->get();
       return $query->result_array();
    }

    public function check_bookmark($user_id, $thread_id) {
        $this->db->select('flag');
        $this->db->from('bbs_bookmark');
        $this->db->where('user_id', $user_id);
        $this->db->where('thread_id', $thread_id);
        $query = $this->db->get();
        return $query->row_array();
    }
        
        /**
         * Delete batch message
         * @param string $post_ids
         * @param number $user_id
         */
        function deleteMultipleComments($ar, $user_id) 
        {
            $cond = array();
            $this->db->set('display_flag', 3);
            $this->db->where_in('thread_id', $ar);
            $this->db->update('bbs_messages');

            # deduct points to commenters target=2
            # deduct all onayami weekly points for the comments target=3
            # deduct all onayami points for the comments target=4
            $this->load->model('Mpoints');
            $cond['target'] = array('2', '3', '4');
            $cond['thread_id'] = $ar;
            $cond['bonus_requested_flag'] = 0;
            $rows = $this->Mpoints->getOnayamiPoints($ar, null, array(2,3,4));
            $result = $this->Mpoints->deduct($cond);
            if ($result) {
                foreach ($rows as $key => $value) {
                    $point_msg[2] = '回答ボーナスを引く';
                    $point_msg[3] = '私もそう思う）を引く（回答者)';
                    $point_msg[4] = '私もそう思うを引く（質問者）)';
                    $this->Mpoints->updateScoutBonus($value['user_id'], $value['point'] , $point_msg[$value['target']], 1);
                }
            }
        }

    function updateData($id, $data) {
        $this->db->where('id', $id);
        return $this->db->update('bbs_threads', $data);
    }

    function updateResponse($id, $data) {
        $this->db->where('id', $id);
        return $this->db->update('bbs_messages', $data);
    }

    /**
     * Get post detail of the thread
     * @param string $id
     */
    function getPostCommentCount($id = null) {
        $sql = "
            SELECT 
                bt.id, 
                bt.title, 
                count(bm.id) as comment_count
             "."
            FROM bbs_threads AS bt
            LEFT JOIN bbs_messages AS bm ON bt.id = bm.thread_id AND bm.reply_id IS NULL AND bm.display_flag = 1
            WHERE bt.id = ?
                AND bt.display_flag = 1
        ";
        $query = $this->db->query($sql, $id);
        return $query->row_array();

    }
}