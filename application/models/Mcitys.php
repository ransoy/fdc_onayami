<?php
class Mcitys extends CI_Model {

    function __construct() {
        // Call the Model constructor
        parent::__construct();
    }

    /**
     * @author  [VJS] Kiyoshi Suzuki
     * @name 	getCitys
     * @param 	
     */
    public function get_citys()
    {
        $query = $this->db->get("mst_cities");
        return $query->result_array();
    }

    public function get_city($id)
    {
        $query = $this->db->get_where('mst_cities', array('id' => $id));
        return $query->row_array();
    }

    public function get_city_by_group($id)
    {
        $this->db->where('city_group_id', $id);
        $query = $this->db->get('mst_cities');
        return $query->result_array();
    }

    public function get_city_groups()
    {
       $ar = array(
            array('name' => '北海道・東北', 'id' => 1),
            array('name' => '北関東', 'id' => 3),
            array('name' => '関東', 'id' => 2),
            array('name' => '北陸・甲信越', 'id' => 4),
            array('name' => '東海', 'id' => 5),
            array('name' => '関西', 'id' => 6),
            array('name' => '中国・四国', 'id' => 7),
            array('name' => '九州・沖縄', 'id' => 8),
        );
        return $ar;
    }

}