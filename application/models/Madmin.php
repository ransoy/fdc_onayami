<?php
class Madmin extends CI_Model {

    function __construct() {
        // Call the Model constructor
        parent::__construct();
    }

    public function can_log_in()
    {
        $this->db->where("login_id", $this->input->post("login_id"));//POSTされたuser_idデータとDB情報を照合する
        $this->db->where("password", base64_encode($this->input->post("password")));//POSTされたパスワードデータとDB情報を照合する
        $query = $this->db->get("admin");
        if ($query->num_rows() == 1) {    //ユーザーが存在した場合の処理
            $row = $query->row();
            return $row;
//            return true;
        } else {//ユーザーが存在しなかった場合の処理
            return false;
        }
    }

    /* 大カテゴリー */
    public function get_big_category($id = null)
    {
        $this->db->select('id, name, contents, class, icon');
        $this->db->from('bbs_big_categorys');
        $this->db->where('board_id', 1);
        if ($id != null) {
            $this->db->where('id', $id);
        }
        $query = $this->db->get();
        return $query->result_array();
    }

    /* 小カテゴリーall */
    public function get_category_all()
    {
        $this->db->select('id, big_category_id, name');
        $this->db->from('bbs_categorys');
        $this->db->where('board_id', 1);
        $query = $this->db->get();
        return $query->result_array();
    }

    /* 小カテゴリー */
    public function get_categorys($id = null,$other = null)
    {
        $this->db->select('*');
        $this->db->from('bbs_categorys');
        $this->db->where('board_id', 1);
        if ($id != null) {
            $this->db->where('big_category_id', $id);
            if ($other != null) {
                $this->db->where('id !=', $other);
            }
        }
        $query = $this->db->get();
        return $query->result_array();
    }

    /* 小カテゴリー 1個*/
    public function get_category($id = null)
    {
        $this->db->select('*');
        $this->db->from('bbs_categorys');
        $this->db->where('board_id', 1);
        if ($id != null) {
            $this->db->where('id', $id);
        }
        $query = $this->db->get();
        return $query->result_array();
    }

    /* 単体取得 */
    public function get_p_cate_by_id($id)
    {
        $this->db->where("id", $id);
        $query = $this->db->get("board_cate_parent");
        $row = $query->row_array();
        return $row;
    }

    /* 単体取得 */
    public function get_cate_by_id($id)
    {
        $this->db->where("id", $id);
        $query = $this->db->get("board_category");
        $row = $query->row_array();
        return $row;
    }

    /* 親カテゴリから子カテゴリ一覧取得
    */
/*    public function get_categorys($cate_pid)
    {
        $this->db->where("cate_parent_id", $cate_pid);
        $this->db->where("board_id", 1);
        $query = $this->db->get("board_category");
//echo $this->db->last_query();
        return $query->result_array();
    }*/

    public function checkLinkCategory($bigID = null, $subID = null, $id = null) {
        $this->db->from('bbs_threads');
        $this->db->where('bbs_threads.big_cate_id', $bigID);
        $this->db->where('bbs_threads.cate_id', $subID);
        $this->db->where('bbs_threads.id', $id);
        $query = $this->db->get();
        return $query->num_rows();
    }

    /* 親カテゴリグループで配列*/
    public function get_cate_group($priority_sw = 0)
    {
        $ar = array();

        $this->db->select('id, name, pc_img, sp_img');
        $this->db->from('board_cate_parent');
        $this->db->order_by("priority", "ASC");
        $this->db->where("priority > 0");
        $query = $this->db->get();
        $ar = $query->result_array();

        foreach ($ar as $key => $val) {
            $temp_ar = array();
            $this->db->select('id, name');
            $this->db->from('board_category');

            if ($priority_sw == 1) {
                $this->db->order_by("priority", "ASC");
                $this->db->where("priority > 0");
            }

            $this->db->where("cate_parent_id", $val['id']);
            $query = $this->db->get();
            $category_ar = $query->result_array();

            $etc_temp = array();
            foreach ($category_ar as $key2 => $val2) {
                if ($val2['name'] == 'その他') {
                    $etc_temp = $val2;
                } else {
                    $temp_ar[] = $val2;
                }
            }
            $temp_ar[] = $etc_temp;
            $ar[$key]['category'] = $temp_ar;
        }
        return $ar;
    }


    /* 受付分野名取得 */
    public function get_p_categorys($ar = '')
    {
        if($ar != '') {
            foreach ($ar as $value) {
                $this->db->or_where('id', $value);
            }
        }
        $query = $this->db->get("board_cate_parent");
        return $query->result_array();
    }

    /* 受付分野名取得１個 */
    public function get_p_category($id)
    {
        $this->db->or_where('id', $id);
        $query = $this->db->get("board_cate_parent");
        return $query->result_array();
    }



    /**
     * @author  [VJS] Kiyoshi Suzuki
     * @name    add_category
     * @param   
     */
    public function add_category()
    {
        $data=array(
            "name"=>$this->input->post("new_category_name"),
        );
        $query=$this->db->insert("board_category", $data);
        if($query){     //データ取得が成功したらTrue、失敗したらFalseを返す
            return true;
        }else{
            return false;
        }
    }


    /**
     * @author  [VJS] Kiyoshi Suzuki
     * @name    add_category
     * @param   
     */
    public function add_new_p_category($arr)
    {
        $data=array(
            "board_id" => 1,
            "name" => $arr['new_cate'],
            "update_date"=>(date("Y-m-d H:i:s")),
            "create_date"=>(date("Y-m-d H:i:s")),
        );
        $query=$this->db->insert("board_cate_parent", $data);
        if($query){     //データ取得が成功したらTrue、失敗したらFalseを返す
            return true;
        }else{
            return false;
        }
    }

    /**
     * @author  [VJS] Kiyoshi Suzuki
     * @name    add_category
     * @param   
     */
    public function add_new_category($arr)
    {
        $data=array(
            "board_id" => 1,
            "cate_parent_id" => $arr['p_cate_id'],
            "name" => $arr['new_cate'],
            "update_date"=>(date("Y-m-d H:i:s")),
            "create_date"=>(date("Y-m-d H:i:s")),
        );
        $query=$this->db->insert("board_category", $data);
        if($query){     //データ取得が成功したらTrue、失敗したらFalseを返す
            return true;
        }else{
            return false;
        }
    }


    public function update_p_cate_contents($arr)
    {
        foreach ($arr as $key => $val) {
            $this->db->where('id', $key);
            $ar = array('contents' => $val);
            $query = $this->db->update('board_cate_parent', $ar);
        }
        if ($query) {
            return true;
        } else {
            return false;
        }
    }

    public function update_cate_contents($arr)
    {
        foreach ($arr as $key => $val) {
            $this->db->where('id', $key);
            $ar = array('contents' => $val);
            $query = $this->db->update('board_category', $ar);
        }
        if ($query) {
            return true;
        } else {
            return false;
        }
    }



    /* 全ユーザーのメールアドレスを取得 */
    public function get_user_email_all()
    {
        $this->db->select('*');
        $this->db->from('users');
        $this->db->where("user_type", 1);
        $query = $this->db->get();
//echo $this->db->last_query();
        $rows = $query->num_rows();
        if ($rows > 0) {//存在した場合の処理
            $res = $query->result_array();
            return $res;
        } else {//存在しなかった場合の処理
            return false;
        }
    }

    /* 全ユーザーのメールアドレスを取得 */
    public function get_bar_email_all()
    {
        $this->db->select('*');
        $this->db->from('users');
        $this->db->where("user_type", 2);
        $query = $this->db->get();
//echo $this->db->last_query();
        $rows = $query->num_rows();
        if ($rows > 0) {//存在した場合の処理
            $res = $query->result_array();
            return $res;
        } else {//存在しなかった場合の処理
            return false;
        }
    }


    /* 相談スレッドの公開、非公開まとめて設定 FIX用*/
    public function fix_consultation_flag_all($flag)
    {
        $this->db->from('thread');
        $this->db->where("display_flag", $flag);
        $query = $this->db->get();
        $res_array = $query->result_array();
//        echo $this->db->last_query();

        $thread_id_ar = array();
        foreach ($res_array as $key => $val) {
            $thread_id_ar[] = $val['id'];
        }


        $ar = array(
            'display_flag' => $flag,
        );
        foreach ($thread_id_ar as $key => $val) {
            $this->db->or_where('consultation_id', $val);
        }
        $query = $this->db->update('consultation_view_log', $ar);
        echo $this->db->last_query();
        if (!$query) {     //成功したらTrue、失敗したらFalseを返す
            return false;
        }
        return true;
    }


    /* 相談スレッドの公開、非公開まとめて設定 FIX用*/
    public function fix_thread_flag_all($flag)
    {
        $ar = array(
            'display_flag' => $flag,
        );
        $query = $this->db->update('thread', $ar);
        echo $this->db->last_query();
        if (!$query) {     //成功したらTrue、失敗したらFalseを返す
            return false;
        }
        return true;
    }


}