<?php
class Musers extends CI_Model {

    function __construct() {
        // Call the Model constructor
        parent::__construct();
    }

    /**
     * @author  [IVS] My Phuong Thi Le
     * @name    get_users
     * @todo    get data users
     * @param   id
     */
    public function get_usersAA($id = null) {
        $sql = 'SELECT * FROM users WHERE display_flag = 1 and id = ?';
        $query = $this->db->query($sql, $id);
        return $query->row_array();
    }


    public function get_users($id = null)
    {
       $this->db->select('*');
        $this->db->from('users');
        if ($id != null) {
            $this->db->where('id', $id);
        }
        $query = $this->db->get();
        if ($query->num_rows() == 1) {    //ユーザーが存在した場合の処理
            if ($id != null) {
                $res = $query->row_array();
            } else {
                $res = $query->result_array();
            }
            return $res;
        } else {                  //ユーザーが存在しなかった場合の処理
            return null;
        }
    }


    public function can_log_in()
    {
        $this->db->select('*');
        $this->db->from('users');
        $array = array(
                'email_address' => $this->input->post('email'),
                'password' => base64_encode($this->input->post('password'))
                );
        $this->db->where($array);
        $query = $this->db->get();
        if ($query->num_rows() == 1) {    //ユーザーが存在した場合の処理
            $row = $query->row_array();
            return $row;
//            return true;
        } else {                  //ユーザーが存在しなかった場合の処理
            return false;
        }
    }

    public function get_total_user_num()
    {
        $ret = 0; //ユーザー総数
        $this->db->select('count(*) AS user_total_no');
        $this->db->from('users');
        $this->db->where('display_flag', 1);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $row = $query->row();
            if ($row) {
                $ret = $row->user_total_no;
            }
        }
        return $ret;
    }

    public function get_user_total_number() {
        $total = $this->get_total_user_num();
        if ($total < 10000) {
             $strtotalUserNo = $total;
        } else {
            $ten_thousands = (int)($total/10000);
            $below_ten_thousands = $total - (10000*$ten_thousands);
            if ( $below_ten_thousands == 0) {
                $strtotalUserNo = $ten_thousands."万";
            } else {
                $strtotalUserNo = $ten_thousands."万".$below_ten_thousands;
            }
        }
        return $strtotalUserNo;
    }

}