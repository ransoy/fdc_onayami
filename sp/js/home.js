$(function(){
	$('.content_2').hide();
	$('.content_3').hide();
	$('.tab_new_arrivals, .tab_popularity, .tab_unanswered').click(function(){
		var tab = $(this).index();
		if (tab == 0) {
			$('.tab_new_arrivals').addClass('current');
			$('.tab_popularity').removeClass('current');
			$('.tab_unanswered').removeClass('current');
			$('.content_1').show();
			$('.content_2').hide();
			$('.content_3').hide();
		} else if (tab == 1) {			
			$('.tab_new_arrivals').removeClass('current');
			$('.tab_popularity').addClass('current');
			$('.tab_unanswered').removeClass('current');
			$('.content_1').hide();
			$('.content_2').show();
			$('.content_3').hide();
		} else if (tab == 2) {			
			$('.tab_new_arrivals').removeClass('current');
			$('.tab_popularity').removeClass('current');
			$('.tab_unanswered').addClass('current');
			$('.content_1').hide();
			$('.content_2').hide();
			$('.content_3').show();
		}
	});
	//サイドメニュー
	$("#simple-menu").sidr({
		name: 'sidr',
		side: 'right'
	});

	$('#sidr').show();
	
	// function get_consultation_list_sp_ajax($option) {
	// 	$.ajax({
	// 		url: ,
	// 		type: "POST",
	// 		dataType: 'json',
	// 		data: ,
	// 		success: function(data){

	// 		}
	// 	});
	// }
});